#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''

#
# plot rocketlogger data (channel I1 only)
#
# usage: ./plot_rld.py [filename] ([offset_s]) ([duration_s])
#

import sys
import os
import numpy as np
import matplotlib.pyplot as plt
from rocketlogger.data import RocketLoggerData


if len(sys.argv) < 2:
    print("no filename provided")
    sys.exit(1)

filename = sys.argv[1]
if not os.path.isfile(filename):
    print("file '%s' not found" % filename)
    sys.exit(1)

offset = 0
if len(sys.argv) > 2:
    offset = int(sys.argv[2])

duration = 0
if len(sys.argv) > 3:
    duration = int(sys.argv[3])

rlddata   = RocketLoggerData(filename).merge_channels()
timeidx   = rlddata.get_time()
currentch = rlddata.get_data('I1') * 1000    # convert to mA

timediff_s = timeidx[-1] - timeidx[0]
samplecnt  = len(currentch)
samplerate = samplecnt / timediff_s

first = int(offset * samplerate)
last  = -1
if duration != 0:
    last = int(duration * samplerate) + offset

avgcurr = np.average(currentch[first:last])

print("sample points:    %d" % (samplecnt))
print("sample frequency: %d Hz" % (samplerate))
print("average current:  %.3f mA" % (avgcurr))

plt.plot(timeidx[first:last], currentch[first:last])
plt.ylabel("current [mA]")
plt.xlabel("time [s]")
plt.show()
