#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''


# Removes invalid blocks (e.g. with an invalid timestamp) from a miniseed file.

import sys
import os
import struct
import datetime

# config
year_min  = 2000    # blocks with an older timestamp will be discarded
time_ofs  = None    # if set, this offset will be added to all timestamps in all headers (UNIX timestamp; remark: 1.1.2000 was 946681200)
max_gap   = 1       # max. gap (missing data) between two blocks, in seconds
rate_min  = 125     # min. sampling rate, in Hz
rate_max  = 250     # max. sampling rate, in Hz
overwrite = False   # if set to true, the original / input file will be overwritten, otherwise the modified file will be saved as '[filename].fixed'
skiplast  = False   # whether to skip the last block
fixfract  = False   # suppress invalid fractional seconds warning by obspy (clears the LSB of fract in first block)
debug     = False   # verbose output

# constants
mseed_header_size  = 48
mseed_dinfo_size   = 8
mseed_sec_ofs      = 26
mseed_fractsec_ofs = 28
mseed_datetime_ofs = 20
mseed_datetime_len = 10
current_year       = datetime.date.today().year


# parameter check
if len(sys.argv) < 2:
    print("no filename provided")
    sys.exit(1)

filename = sys.argv[1]

if not os.path.isfile(filename):
    print("file '%s' not found" % filename)
    sys.exit(1)

if os.path.splitext(filename)[1].upper() not in ['.MSEED']:
    print("file '%s' does not appear to be a miniseed file" % filename)
    sys.exit(1)


# process file
block_cnt    = 0
block_skip   = 0
file_changed = False
out_buffer   = bytearray()
with open(filename, 'rb') as input_file:
    filesize       = os.path.getsize(filename)
    prev_timestamp = None
    while True:
        # read the miniseed header
        header = input_file.read(mseed_header_size)
        if len(header) < mseed_header_size:
            break   # file end reached
        block_cnt += 1
        # parse the header
        (seq_no, dq, station, loc, ch, net, year, doy, h, m, s, fract, samples, rate_f, rate_m, act_flags, io_flags, dq_flags, blockettes, time_corr, data_ofs, bl_ofs) = struct.unpack('<6scx5s2s3s2sHHBBBxHHhhBBBBlHH', header)
        sampling_rate = rate_f * rate_m
        timestamp     = datetime.datetime(year, 1, 1, h, m, s, fract * 100) + datetime.timedelta(doy - 1)
        # fix fractional seconds if required
        if fixfract and block_cnt == 1:   # first block only
            header2 = bytearray(header)
            if header2[mseed_fractsec_ofs] > 99:
                header2[mseed_fractsec_ofs] = header2[mseed_fractsec_ofs + 1]
                header                      = bytes(header2)
                file_changed                = True
        # add offset to timestamp if required
        if time_ofs:
            timestamp = timestamp + datetime.timedelta(seconds=time_ofs)    # unix timestamp
            header2   = bytearray(header)
            header2[mseed_datetime_ofs : mseed_datetime_ofs + mseed_datetime_len] = struct.pack('<HHBBBxH', timestamp.year, timestamp.timetuple().tm_yday, timestamp.hour, timestamp.minute, timestamp.second, int(timestamp.microsecond / 100))
            header       = bytes(header2)
            file_changed = True
        # read the data record information
        data_info     = input_file.read(mseed_dinfo_size)
        (btype, endian, block_len_p2) = struct.unpack('<H3xBBx', data_info)
        block_len     = pow(2, block_len_p2)
        # check the offsets and block length
        if data_ofs != 64 or bl_ofs != 48 or block_len < 256:
            print("invalid data or blockette offset (corrupted file?)")
            break
        if debug:
            print("station: %s, channel: %s, seqno: %s, timestamp: %s, samples: %d, rate: %dHz" % (station.decode('utf-8'), ch.decode('utf-8'), seq_no.decode('utf-8'), timestamp.strftime('%Y-%m-%d %H:%M:%S.%f'), samples, rate_f * rate_m))
        # check number of samples
        if samples < 1:
            print("invalid number of samples detected")
            input_file.seek(block_len - mseed_header_size - mseed_dinfo_size, 1)   # skip to the next header
            block_skip += 1
            continue
        # validate timestamp
        if year < year_min or year > current_year:
            print("invalid timestamp detected (year: %d)" % year)
            input_file.seek(block_len - mseed_header_size - mseed_dinfo_size, 1)   # skip to the next header
            block_skip += 1
            continue
        # look for time jumps
        if prev_timestamp:
            timediff = (timestamp - prev_timestamp).total_seconds()
            if timediff > max_gap:
                print("large timestamp difference detected (%.3fs, @%s)" % (timediff, timestamp.strftime('%Y-%m-%d %H:%M:%S.%f')))
            elif timediff <= 0:
                print("negative timestamp difference detected (%.3fs, @%s)" % (timediff, timestamp.strftime('%Y-%m-%d %H:%M:%S.%f')))
                input_file.seek(block_len - mseed_header_size - mseed_dinfo_size, 1)   # skip to the next header
                block_skip += 1
                continue
        prev_timestamp = timestamp + datetime.timedelta(seconds=((samples - 1) / sampling_rate))
        # check the sampling rate
        if sampling_rate < rate_min or sampling_rate > rate_max:
            print("invalid sampling rate detected (%dHz)" % sampling_rate)
            input_file.seek(block_len - mseed_header_size - mseed_dinfo_size, 1)   # skip to the next header
            block_skip += 1
            continue
        # read the data
        data_len = block_len - mseed_header_size - mseed_dinfo_size
        data     = input_file.read(data_len)    # includes 8 padding bytes
        if len(data) != data_len:
            block_skip += 1
            print("unexpected end of file")
            break
        if skiplast and input_file.tell() == filesize:
            block_skip += 1
            print("end of file reached")
            break;
        # write to output file
        out_buffer.extend(header)
        out_buffer.extend(data_info)
        out_buffer.extend(data)

# if there are any changes, save modified file
if (block_skip or file_changed) and len(out_buffer) > 0:
    out_filename = filename
    if not overwrite:
        out_filename += ".fixed"
    with open(out_filename, 'wb') as output_file:
        output_file.write(out_buffer)
    print("modified file saved as '%s'" % out_filename)

print("%d blocks processed, %d blocks skipped" % (block_cnt, block_skip))
