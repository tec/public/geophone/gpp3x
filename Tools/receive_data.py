#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''

# read data from GPP UART and write it to a file

import serial
import time
import binascii
import os
from enum import Enum
import struct


# packet types
class UARTPktType(Enum):
    ADC1_RAW     = 1
    ADC2_RAW     = 2
    ADC3_RAW     = 3
    ADC_MINISEED = 4
    DEBUG_MSG    = 5
    APP_HEALTH   = 6

# config
serialport  = '/dev/ttyACM0'
baudrate    = 1000000
outputpath  = '/tmp/gpp_stream/'
typestolog  = [ UARTPktType.ADC_MINISEED, UARTPktType.APP_HEALTH, UARTPktType.DEBUG_MSG ]    # packet types to accept
channelmap  = [ 'Z', 'E', 'N' ]
addpadding  = False
samplerate  = 125
samplesize  = 3
headerlen   = 12
crclen      = 4
frame       = 126    # framing byte
escape      = 125    # escape byte

# global variables
last_seq_no = -1


def parse_packet(pkt):
    global last_seq_no

    if len(pkt) < (headerlen + crclen):
        print("invalid packet length")
        return

    pkt_type = int(pkt[0])
    seq_no   = int.from_bytes(pkt[2:4], 'little')
    offset   = int.from_bytes(pkt[4:6], 'little')
    length   = int.from_bytes(pkt[6:8], 'little')
    arg      = int.from_bytes(pkt[8:12], 'little')

    # sanity check
    if len(pkt) < (headerlen + length + crclen):
        print("invalid payload length")
        return

    # check the crc
    crc      = int.from_bytes(pkt[-4:], 'little')
    crc_calc = binascii.crc32(bytearray(pkt[0:-4]))
    if crc != crc_calc:
        print("invalid CRC 0x%08x (crc: 0x%08x, length: %u, payload: %u)" % (crc_calc, crc, len(pkt), length))
        return

    # print infos
    print("pkt_type: %u, seq_no: %u, offset: %u, length: %u, arg: %u, crc: 0x%08x" % (pkt_type, seq_no, offset, length, arg, crc))

    # check for missing packets
    seq_no_delta = 1
    if last_seq_no >= 0:
        if seq_no < last_seq_no:
            seq_no_delta = 65536 - (last_seq_no - seq_no)
        else:
            seq_no_delta = seq_no - last_seq_no
        if seq_no_delta > 1:
            print("%u packet(s) missing" % (seq_no_delta - 1))
            if seq_no_delta > 10:
                seq_no_delta = 10   # limit to 10
    last_seq_no = seq_no

    pkt_type = UARTPktType(pkt_type)
    if pkt_type not in typestolog:
        return

    if pkt_type in [ UARTPktType.ADC1_RAW, UARTPktType.ADC2_RAW, UARTPktType.ADC1_RAW ]:
        # raw ADC waveform
        # write data to a file
        filename = outputpath + ("%07u.%s.DAT" % (arg, channelmap[pkt_type - 1]))
        with open(filename, 'ab') as f:
            padding_bytes = 0
            if seq_no_delta > 1 and addpadding:
                # insert zeros into the file
                padding_bytes = samplerate * samplesize * (seq_no_delta - 1)
                f.write(bytearray(padding_bytes))
            f.write(bytearray(pkt[headerlen:-4]))
            print("  %u bytes written to file %s" % (len(pkt[headerlen:-4]) + padding_bytes, filename))

    elif pkt_type == UARTPktType.ADC_MINISEED:
        # miniseed
        # extract infos
        channel = bytes(pkt[headerlen + 15 : headerlen + 18]).decode('utf-8').strip()
        samples = int.from_bytes(pkt[headerlen + 30 : headerlen + 32], 'little')
        rate    = int.from_bytes(pkt[headerlen + 32 : headerlen + 34], 'little')
        print("  miniseed block (%d samples, %dHz, channel %s)" % (samples, rate, channel))
        filename = outputpath + ("%07u.%s.MSEED" % (arg, channel))
        with open(filename, 'ab') as f:
            f.write(bytearray(pkt[headerlen:-4]))
            print("  %u bytes written to file %s" % (len(pkt[headerlen:-4]), filename))

    elif pkt_type == UARTPktType.DEBUG_MSG:
        # debug output message
        msg = bytes(pkt[headerlen:-4]).decode('utf-8')
        print("  debug message:")
        print(msg)

    elif pkt_type == UARTPktType.APP_HEALTH:
        raw_bytes = bytes(pkt[headerlen:-4])
        (uptime, msg_cnt, core_vcc, core_temp, cpu_dc, stack, nv_mem, supply_vcc, supply_cur, temp, hum) = struct.unpack('<IHHhHBBHHhH', raw_bytes)
        print("  app health (device ID: %d, uptime: %ds, vcc: %dmV, cpu: %.2f%%, stack: %d%%, sdcard: %d%%, temp: %.1f°C, hum: %d%%)" % (arg, uptime, core_vcc, cpu_dc / 100, stack, nv_mem, temp / 100, int(hum / 100)))


def read_from_serial():
    with serial.Serial(serialport, baudrate) as ser:
        packet  = []
        started = False
        escaped = False
        while True:
            if ser.inWaiting() > 0:
                data = ser.read(ser.inWaiting())
                for d in data:
                    if not escaped:
                        # potentially a control byte
                        if d == escape:
                            escaped = True    # the next byte is a regular data byte
                            continue
                        elif d == frame:      # frame start / stop
                            if not started or len(packet) == 0:
                                # start of packet
                                started = True
                            else:
                                # end of packet
                                parse_packet(packet)
                                packet.clear()
                                started = False
                            continue
                    if started:
                        # regular data byte
                        packet.append(d)
                    escaped = False
            else:
                time.sleep(0.001)


# create output folder
if not os.path.isdir(outputpath):
    os.mkdir(outputpath)

try:
    # make sure the serial port is valid
    read_from_serial()
except serial.serialutil.SerialException:
    print("failed to open serial port %s" % (serialport,))
except KeyboardInterrupt:
    print("\b\baborted")
