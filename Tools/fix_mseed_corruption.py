#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''


# This script attempts to fix miniseed file corruption that can occur with GPP3X firmware version 2.5.0 or earlier.
# Note: Corrupted files are updated in-place (overwrite).

import os
import sys
import struct


def decode_steim1(enc_data, data_len, num_samples):
    if data_len / 64 <= 0:
        print("invalid frame")
        return False
    iout = []
    pval = None
    rem  = num_samples
    fidx = 0
    X0   = 0
    Xn   = 0
    while fidx < (data_len / 64) and rem > 0:
        frame         = enc_data[fidx * 64 : fidx * 64 + 64]
        widx          = 1
        (diff_bits, ) = struct.unpack('<I', frame[0:4])
        if fidx == 0:
            (X0, Xn) = struct.unpack('<ii', frame[4:12])
            widx     = 3
        if pval == 0:
            # potentially corrupted
            (X0_tmp, Xn_tmp) = struct.unpack('<ii', frame[4:12])
            iout.append([fidx, X0_tmp])
        while widx < 16 and rem > 0:
            diff      = [ 0 ] * 4
            nibble    = (diff_bits >> (30 - (2 * widx))) & 0x3
            diffcount = 0
            if nibble == 0:
                pass
            elif nibble == 1:
                (diff[0], diff[1], diff[2], diff[3]) = struct.unpack('<bbbb', frame[widx * 4 : widx * 4 + 4])
                diffcount = 4
            elif nibble == 2:
                (diff[0], diff[1]) = struct.unpack('<hh', frame[widx * 4 : widx * 4 + 4])
                diffcount = 2
            elif nibble == 3:
                (diff[0], ) = struct.unpack('<i', frame[widx * 4 : widx * 4 + 4])
                diffcount = 1
            idx = 0
            while idx < diffcount:
                if pval is None:
                    pval = X0
                else:
                    pval = pval + diff[idx]
                idx += 1
                rem -= 1
            widx += 1
        fidx += 1
    # Check data integrity by comparing last sample to Xn (reverse integration constant)
    if pval != Xn:
        for i in iout:
            if (Xn - pval) == i[1]:
                return (False, i[0], i[1])
        return (False, -1, 0)
    return (True, -1, 0)


def fix_mseed_block(block_idx, block_data, block_len, num_samples):
    (res, fi, val) = decode_steim1(block_data, block_len, num_samples)
    if (fi < 0):
        print("  frame could not be identified")
    else:
        print("  frame %d of block %d is corrupted" % (fi, block_idx))
        # fix frame
        i = fi * 64
        block_data_new = bytearray(block_data)
        (diff_bits, ) = struct.unpack('<I', block_data[i : i + 4])
        if (diff_bits >> 24) == 2:
            if (val < 32768) and (val >= -32768):
                block_data_new[i + 12 : i + 14] = struct.pack('<h', val)
            else:
                diff_bits                       = (diff_bits | (3 << 28) | (3 << 24))
                block_data_new[i : i + 4]       = struct.pack('<I', diff_bits)
                block_data_new[i + 4 : i + 8]   = struct.pack('<i', val)
                (val2, )                        = struct.unpack('<h', block_data[i + 14: i + 16])
                block_data_new[i + 12 : i + 16] = struct.pack('<i', val2)
        elif (diff_bits >> 24) == 3:
            diff_bits                     = (diff_bits | (3 << 28))
            block_data_new[i : i + 4]     = struct.pack('<I', diff_bits)
            block_data_new[i + 4 : i + 8] = struct.pack('<i', val)
        elif (diff_bits >> 24) == 1:
            if (val >= -128) and (val < 128):
                block_data_new[i + 12 : i + 13] = struct.pack('<b', val)
            else:
                print("  diff size is 8, but value is %d" % (val))
        # check again
        (res, fi, val) = decode_steim1(block_data_new, block_len, num_samples)
        if res != True:
            print("  failed to fix frame")
            return None
        return block_data_new


def fix_corrupted_mseed_file(filename):
    print("checking file '%s'..." % filename)
    output = bytearray()
    blocks = 0
    fixed  = 0
    with open(filename, 'rb') as input_file:
        while True:
            # read and parse the header
            header = input_file.read(56)
            if len(header) < 56:
                break   # file end reached
            blocks += 1
            (sn, dq, st, l, ch, net, y, d, h, m, s, f, sc, rf, rm, fa, fi, fd, bk, tc, do, bo) = struct.unpack('<6scx5s2s3s2sHHBBBxHHhhBBBBlHH', header[0:48])
            (btype, enc, endian, bl_p2) = struct.unpack('<H2xBBBx', header[48:])
            # basic checks
            bl = pow(2, bl_p2)
            if (do != 64) or (bo != 48) or (bl_p2 < 8):
                print("  invalid header detected, cannot proceed (corrupted file?)")
                break
            # read the data
            data = input_file.read(bl - 56)
            if len(data) != (bl - 56):
                print("  unexpected end of file")
                break
            # check the data
            if not decode_steim1(data[8:], bl - 64, sc)[0]:
                data       = bytearray(data)
                fixed_data = fix_mseed_block(blocks, data[8:], bl - 64, sc)
                if fixed_data is not None:
                    data[8:] = fixed_data
                    fixed += 1
            output.extend(header + data)
    if fixed:
        with open(filename, 'wb') as of:
            of.write(output)
    print("  %d miniseed blocks checked, %u issues fixed" % (blocks, fixed))


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("no path provided")
        sys.exit(1)
    path = sys.argv[1]
    if not os.path.isdir(path):
        print("path '%s' not found" % path)
        sys.exit(1)
    for fname in os.listdir(path):
        if ".MSEED" in fname:
            fix_corrupted_mseed_file("%s/%s" % (path, fname))
