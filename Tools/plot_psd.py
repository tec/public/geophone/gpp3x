#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''

# Power Spectral Density

import sys
import os
import matplotlib.pyplot as plt
import numpy as np
import urllib
import warnings
from pkg_resources import PkgResourcesDeprecationWarning
warnings.simplefilter("ignore", category=PkgResourcesDeprecationWarning)
from obspy import read
from obspy.signal.invsim import cosine_taper
from datetime import datetime


fftLen = 1024
sampleRate = 125
maxRawValue = (1 << 23)
xaxisDatetime = True
deployment = "kangerlussuaq"


def cosineTaperWindow(size):
    window=cosine_taper(size, 0.2)
    for i in range(0, len(window)):
        sys.stdout.write(" %.6ff," % window[i])
        if ((i + 1) % 16) == 0:
            sys.stdout.write("\n")


# data is expected in the form: [timestamp],val1,val2,...,valN
# note: timestamp should be UNIX time in seconds or microseconds
def parseCSV(lines, fillgaps = False, maxentries = 0):
    tolerance = 2     # tolerance for detecting timestamp jumps, in seconds
    data = []
    timestamps = []
    prevts = None
    timedelta = None
    for line in lines:
        cols = line.strip().split(',')
        if len(cols) <= 1:
            continue
        ts = int(cols[0])
        if ts > 1e10:
            ts = int(ts / 1e6)   # timestamp is most likely in microseconds -> divide by 1e6
        if prevts:
            tsdiff = ts - prevts
            if not timedelta:
                timedelta = (ts - prevts)
            elif abs(tsdiff - timedelta) >= tolerance:
                print("warning: timestamp jump detected (delta is %ds, but should be %ds)" % (tsdiff, timedelta))
                if fillgaps and abs(tsdiff) < 86400:   # don't attempt to fill gaps longer than 1 day
                    missing = int((abs(tsdiff - timedelta) + tolerance) / abs(timedelta))
                    for _ in range(0, missing):
                        prevts += timedelta
                        timestamps.append(prevts)
                        data.append([-128] * (len(cols) - 1))
                        print("blank entry with timestamp %d inserted" % prevts)
        prevts = ts
        timestamps.append(ts)
        data.append([-int(x) / 2 for x in cols[1:]])
    if not timedelta or len(timestamps) <= 1:
        return [ None, None, None ]
    if timedelta < 0:  # entries in descending order?
        timestamps.reverse()
        data.reverse()
        timedelta *= -1
    if maxentries:
        # keep only the most recent maxentries entries
        timestamps = timestamps[-maxentries:]
        data = data[-maxentries:]
    totaltime = (timestamps[-1] - timestamps[0] + timedelta) / 60
    if xaxisDatetime:  # if requested, convert unix timestamps to datetime strings
        timestamps = [datetime.fromtimestamp(ts).strftime("%d.%m %H:%M") for ts in timestamps]
    print("%d entries found, %d seconds in-between; total duration is %d hours and %d minutes" % (len(data), timedelta, totaltime / 60, totaltime % 60))
    return [np.array(data).transpose(), timedelta, timestamps]


# put PSD data into frequency bins
def binPSD(data):
    output = []
    prevfreq = None
    cnt = 0
    for i in range(1, len(data)):   # skip DC offset
        freq = i * (sampleRate / fftLen)
        # frequencies below 1Hz: just copy
        if freq < 1.0:
            output.append(data[i])
        else:
            # frequencies above 1Hz: combine into 1Hz bins
            freq = int(freq + 0.5)
            if prevfreq and freq != prevfreq and cnt:
                output.append(sum(data[i - cnt: i]) / cnt)
                cnt = 0
            prevfreq = freq
            cnt += 1
    return output


# apply 8-bit quantization
def quantizePSD(data):
    for i in range(0, len(data)):
        tmp = max(min(int(-data[i] * 2), 255), 0)
        data[i] = -tmp / 2
    return data


# calculate the PSD from raw seismic data
def calcPSD(data, chunkSize=600, freqBinning=True, quantize=True):   # chunk size in seconds
    if len(data) == 0:
        return
    psdarray = []
    numsamples = chunkSize * sampleRate
    numsamples += fftLen - (numsamples % fftLen)    # round up to the next integer multiple of NFFT to avoid zero-padding
    for idx in range(0, len(data) - numsamples, numsamples):
        [psd, freqs] = plt.psd(data[idx:idx + numsamples], Fs=sampleRate, NFFT=fftLen, window=cosine_taper(fftLen, 0.2), noverlap=int(fftLen/2))
        psd = 10 * np.log10(psd)
        if freqBinning:
            psd = binPSD(psd)
        if quantize:
            psd = quantizePSD(psd)
        psdarray.append(psd)
    return np.array(psdarray).transpose()


# load the raw data from a miniseed file
def loadMseed(filename, scale=True):
    st = read(filename)
    st.merge(fill_value='latest')
    if st:
        if scale:
            # convert to float
            return np.divide(st[0].data, maxRawValue)
        else:
            return st[0].data
    return None


def getXAxisTicks(numticks, xlen):
    if numticks < 2:
        return None
    ticks = []
    increment = int(xlen / (numticks - 1))
    x = 0
    while x <= xlen:
        ticks.append(int(x))
        x += increment
    return ticks


def showPlot(xlabels = None, xlen = 0):
    plt.grid(False)
    plt.yticks(None)
    if xaxisDatetime and xlabels and len(xlabels) > 1 and xlen:
        plt.xticks(getXAxisTicks(len(xlabels), xlen), xlabels, rotation=90)
        plt.tight_layout()
    else:
        plt.xlabel('Time [min]')
    plt.show()


# plot a PSD matrix as an image map (spectrogram)
def plotPSD(npdata, timestep=10, freqofs=-int(fftLen / sampleRate), subplot=None):
    if npdata.size == 0:
        return
    [nrows, ncols] = npdata.shape
    print("spectrogram size is %d x %d" % (ncols, nrows))
    if subplot is None:
        plt.clf()
        plt.imshow(npdata, aspect='auto', origin='lower', interpolation='none', extent=[0, ncols * timestep, freqofs, nrows + freqofs])
        plt.colorbar()
        plt.title("PSD (%dmin averages)" % timestep)
    else:
        subplot.set_ylabel('Frequency [Hz]')
        return subplot.imshow(npdata, aspect='auto', origin='lower', interpolation='none', extent=[0, ncols * timestep, freqofs, nrows + freqofs])


def getDataFromURL(url):
    request = urllib.request.urlopen(url)
    if request.getcode() != 200:
        print("Error %d while fetching data from %s" % (request.getcode(), url))
        return ""
    else:
        return request.read()


def getPSDDataFromGSN(position, numvalues=10, deployment="etz"):
    gsnurl = "http://gsn-public.tec.ee.ethz.ch"
    vsensor = deployment + "_dpp_geophone_spectrogram__conv"
    fields = "generation_time_microsec,data_csv"
    query = gsnurl + "/multidata?nb=SPECIFIED&nb_value={}&timeline=generation_time&vs%5B1%5D={}&field%5B1%5D={}&c_join%5B1%5D=and&c_vs%5B1%5D={}&c_field%5B1%5D=position&c_min%5B1%5D={}&c_max%5B1%5D={}&time_format=unix&download_format=csv".format(numvalues, vsensor, fields, vsensor, position - 1, position)
    lines = getDataFromURL(query).decode('utf-8').split('\n')[3:]  # the first 3 lines contain header data
    data = []
    for line in lines:
        data.append(",".join(line.split(' ')[:-1]))  # remove the last column (contains the generation time)
    return data


def determineXAxisStepSize(numentries, maxsteps):
    div = 1
    step = 2
    numentries -= 1
    while step < numentries and numentries / div > maxsteps:
        if numentries % step == 0:
            div = step
        step += 1;
    return numentries if (div == 1) else div


def plotDataFromCSVFile(filename, gsnCSV = False):
    lines = []
    with open(filename, "r") as f:
        lines = f.readlines()
    if gsnCSV:
        # expected format is: [generation_time_microsec],[csvdata]
        parsedLines = []
        for line in lines[3:]:
            parsedLines.append(",".join(line.split(' ')[:-1]))  # remove the last column (contains the generation time)
        lines = parsedLines
    [data, timedelta, timestamps] = parseCSV(lines)
    plotPSD(data, timestep=timedelta / 60)
    showPlot(timestamps[0::min(12, len(timestamps) - 1)], timedelta / 60 * len(timestamps))


def plotDataFromMseedFile(filename):
    rawdata = loadMseed(filename)
    plotPSD(calcPSD(rawdata))
    showPlot()


def plotDataFromMseedFolder(path):
    filecnt = 0
    rawdata = []
    for fname in sorted(os.listdir(path)):
        if fname.lower().endswith(".mseed"):
            filecnt += 1
            rawdata.extend(loadMseed(path + '/' + fname))
    print("%d files found in %s" % (filecnt, path))
    plotPSD(calcPSD(rawdata))
    showPlot()


def plotDataFromGSN(positions, num):
    if not positions or len(positions) > 10:
        print("invalid positions")
        return
    if num <= 0 or num > 1009:  # 1009 entries = 1 week
        print("max. number of elements is 1009")
        return
    fig, axes = plt.subplots(len(positions), sharex=True)
    xlen = 0
    xlabels = []
    tdelta = 0
    for i in range(0, len(positions)):
        pos = positions[i]
        if pos <= 0 or pos >= 1000:
            print("invalid position")
            continue
        csvdata = getPSDDataFromGSN(pos, num, deployment)
        if not csvdata:
            print("no data found for position %d" % pos)
            return
        [data, timedelta, timestamps] = parseCSV(csvdata, True, num)
        if xlen == 0:
            # determine x axis scaling from the first plot
            xlen = len(timestamps)
            xlabels = timestamps[0::determineXAxisStepSize(len(timestamps), 15)]
            tdelta = timedelta
        elif len(timestamps) != xlen or timedelta != tdelta:
            print("invalid length, skipping position %d" % pos)
            continue
        ax = axes if len(positions) == 1 else axes[i]
        ax.set_title("Position %d PSD (%dmin averages)" % (pos, timedelta / 60))
        img = plotPSD(data, timestep=timedelta / 60, subplot = ax)
        fig.colorbar(img, ax = ax)
    showPlot(xlabels, tdelta / 60 * xlen)


def main():
    if len(sys.argv) < 2:
        print("no argument provided")
        sys.exit(1)
    arg = sys.argv[1]
    if arg.lower().endswith(".csv") and os.path.exists(arg):
        plotDataFromCSVFile(arg) #, True)
    elif arg.lower().endswith(".mseed") and os.path.exists(arg):
        plotDataFromMseedFile(arg)
    elif os.path.isdir(arg) and os.path.exists(arg):
        plotDataFromMseedFolder(arg)
    elif arg.split(',')[0].isnumeric():
        hours = 24 if len(sys.argv) <= 2 else min(int(sys.argv[2]), 168)
        plotDataFromGSN([int(val) for val in arg.split(',')], hours * 6 + 1)
    else:
        print("file '%s' not found" % arg)


if __name__ == "__main__":
    main()
