#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''


from obspy import read
import sys
import os


if len(sys.argv) < 2:
    print("usage: %s [filename] ([offset_s]) ([duration_s])" % (sys.argv[0]))
    sys.exit(1)

filename = sys.argv[1]
if not os.path.isfile(filename):
    print("file %s not found" % filename)
    sys.exit(1)
offset = 0
if len(sys.argv) > 2:
    offset = int(sys.argv[2])
    if offset < 0:
        print("invalid offset")
        sys.exit(1)
duration = 0
if len(sys.argv) > 3:
    duration = int(sys.argv[3])
    if duration <= 0:
        print("invalid duration")
        sys.exit(1)

st = read(filename)
st.merge()
if st:
    start_dt = st[0].stats.starttime + offset
    end_dt   = st[0].stats.endtime
    if duration > 0:
        end_dt = start_dt + duration
    print("starttime: " + str(start_dt))
    print("endtime:   " + str(end_dt))
    #print("samples:   %s" % str(st[0].data[0:100]))
    st[0].plot(starttime=start_dt, endtime=end_dt)
