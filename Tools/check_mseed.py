#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''


# this script looks for unexpected timestamp differences in miniseed files
#
# usage: ./check_mseed.py [filename/path]
#
# [filename/path]   either a path to a folder with miniseed files or a single miniseed file

import os
import sys
import subprocess
import struct
import datetime
import binascii
import warnings

#warnings.filterwarnings("ignore", category=DeprecationWarning)


# config
max_gap      = 0.015                    # max. acceptable gap between two consecutive blocks (larger gaps will be reported)
sample_rates = [ 100, 125, 200, 250, 500, 1000 ]  # list of allowed sampling rates
has_checksum = True                     # set to True if there is a 32-bit CRC at the end of the miniseed file
simple_check = False                    # if set to true, only the headers will be examined
debug        = False                    # enable verbose output

# constants
mseed_header_size  = 48
mseed_dinfo_size   = 8
mseed_padding_size = 8
mseed_encodings    = { 0: "unknown", 1: "INT16", 2: "INT24", 3: "INT32", 10: "Steim1", 11: "Steim2" }


# helper functions
def decode_steim1(enc_data, data_len, num_samples):
    if data_len / 64 <= 0:
        print("invalid frame")
        return False
    output  = []
    rem     = num_samples
    nframes = data_len / 64
    fidx    = 0
    X0      = 0
    Xn      = 0
    while fidx < nframes and rem > 0:
        # Load a frame (64 bytes)
        frame         = enc_data[fidx * 64 : fidx * 64 + 64]
        widx          = 1
        (diff_bits, ) = struct.unpack('<I', frame[0:4])
        if fidx == 0:
            (X0, Xn) = struct.unpack('<ii', frame[4:12])
            widx     = 3
        # Decode each 32-bit word
        while widx < 16 and rem > 0:
            diff      = [ 0 ] * 4
            nibble    = (diff_bits >> (30 - (2 * widx))) & 0x3
            diffcount = 0
            if nibble == 0:       # Special flag, no differences
                pass
            elif nibble == 1:     # 4x 1-byte differences
                (diff[0], diff[1], diff[2], diff[3]) = struct.unpack('<bbbb', frame[widx * 4 : widx * 4 + 4])
                diffcount = 4
            elif nibble == 2:     # 2x 2-byte differences
                (diff[0], diff[1]) = struct.unpack('<hh', frame[widx * 4 : widx * 4 + 4])
                diffcount = 2
            elif nibble == 3:     # 1x 4-byte difference
                (diff[0], ) = struct.unpack('<i', frame[widx * 4 : widx * 4 + 4])
                diffcount = 1
            idx = 0
            while idx < diffcount:
                if not output:
                    output = [ X0 ]
                else:
                    output.append(output[-1] + diff[idx])
                idx += 1
                rem -= 1
            widx += 1
        fidx += 1
    # Check data integrity by comparing last sample to Xn (reverse integration constant)
    if len(output) > 0 and output[-1] != Xn:
        #print("  steim1 data integrity check failed (last sample: %d, Xn: %d)" % (output[-1], Xn))
        return False
    if len(output) != num_samples:
        #print("  only %d of %d samples unpacked" % (len(output), num_samples))
        return False
    return True


def check_mseed_file(filename, prev_ts):
    print("checking file '%s'..." % filename)
    total_bytes     = os.path.getsize(filename)
    processed_bytes = 0
    issues          = 0
    block_cnt       = 0
    sample_cnt      = 0
    checksum        = 0
    prev_timestamp  = prev_ts
    with open(filename, 'rb') as input_file:
        while True:
            # read the miniseed header
            header = input_file.read(mseed_header_size)
            if len(header) < mseed_header_size:
                rem = len(header)
                if rem >= 4 and has_checksum:
                    file_crc = int.from_bytes(bytearray(header[0:4]), "little")
                    if file_crc != checksum:
                        print("  Checksum does not match (0x%08x vs 0x%08x)" % (file_crc, checksum))
                    rem -= 4
                if rem:
                    print("  %d excess bytes at EOF skipped" % rem)
                    issues += 1
                break   # file end reached
            block_cnt += 1
            # parse the header
            (seq_no, dq, station, loc, ch, net, year, doy, h, m, s, fract, samples, rate_f, rate_m, act_flags, io_flags, dq_flags, blockettes, time_corr, data_ofs, bl_ofs) = struct.unpack('<6scx5s2s3s2sHHBBBxHHhhBBBBlHH', header)
            #determine endianness
            if year > datetime.date.today().year:
                (year2, ) = struct.unpack('>H', struct.pack('<H', year))    # convert to big endian
                if year2 <= datetime.date.today().year:
                    print("  big endian format detected (not supported)")
                    issues += 1
                    break
                print("  invalid timestamp detected (year: %d)" % year)
                issues += 1
                break
            if rate_m < 0:
                sampling_rate = rate_f / abs(rate_m)
            else:
                sampling_rate = rate_f * rate_m
            sample_cnt   += samples
            timestamp     = datetime.datetime(year, 1, 1, h, m, s, fract * 100) + datetime.timedelta(doy - 1)
            timestamp_str = timestamp.strftime('%Y-%m-%d %H:%M:%S.%f')
            # read the data record information
            data_info     = input_file.read(mseed_dinfo_size)
            (btype, enc, endian, block_len_p2) = struct.unpack('<H2xBBBx', data_info)
            block_len     = pow(2, block_len_p2)
            # check the offsets and block length
            if data_ofs != 64 or bl_ofs != 48 or block_len < 256:
                print("  invalid data or blockette offset (corrupted file?)")
                issues += 1
            # check number of samples
            if samples < 1:
                print("  invalid number of samples detected at offset %d" % processed_bytes)
                issues += 1
            # check encoding
            if enc not in mseed_encodings:
                print("  unknown encoding 0x%x" % enc)
                issues += 1
                enc     = 0
            # look for time jumps
            if prev_timestamp:
                timediff = (timestamp - prev_timestamp).total_seconds()
                if timediff > max_gap:
                    print("  gap detected (%.3fs @%s)" % (timediff, timestamp_str))
                    issues += 1
                elif timediff <= 0:
                    print("  negative timestamp difference detected (%.3fs @%s)" % (timediff, timestamp_str))
                    issues += 1
            prev_timestamp = timestamp + datetime.timedelta(seconds=((samples - 1) / sampling_rate))
            # check the sampling rate
            if sampling_rate not in sample_rates:
                print("  invalid sampling rate detected (%dHz)" % sampling_rate)
                issues += 1
            # print the header info
            if debug:
                print("  station: %s, channel: %s, seqno: %s, timestamp: %s, samples: %d, rate: %dHz, block_len: %d, encoding: %s" % (station.decode('utf-8'), ch.decode('utf-8'), seq_no.decode('utf-8'), timestamp_str, samples, rate_f * rate_m, block_len, mseed_encodings[enc]))
            # read the data
            data_len = block_len - mseed_header_size - mseed_dinfo_size
            data     = input_file.read(data_len)            # includes padding bytes
            if len(data) != data_len:
                print("  unexpected end of file")
                issues += 1
                break
            # check the data
            if not decode_steim1(data[mseed_padding_size:], data_len - mseed_padding_size, samples):
                print("  data integrity issue detected at offset %d (timestamp: %s)" % (processed_bytes, timestamp_str))
                issues += 1
            # update the checksum
            if has_checksum:
                checksum = binascii.crc32(bytearray(header) + bytearray(data_info) + bytearray(data), checksum)
            processed_bytes += block_len
    print("  %d of %d bytes processed (%.2f%%), %d samples found" % (processed_bytes, total_bytes, processed_bytes * 100 / total_bytes, sample_cnt))
    return (block_cnt, issues, prev_timestamp)


def check_mseed_blocks(blocklist):
    issues    = 0
    block_cnt = 0
    prev_ts   = { }
    for line in blocklist:
        line = line.strip()
        elem = line.split(', ')
        if len(elem) != 6:
            print("  skipping line '%s'" % line)
            issues += 1
            continue
        block_cnt += 1
        # sanity check for number of samples and sampling rate
        num_samples = int(elem[3].split(' ')[0])
        sample_rate = int(elem[4].split(' ')[0])
        if num_samples < 1 or sample_rate not in sample_rates:
            print("  invalid number of samples or sample rate for line '%s'" % line)
            issues += 1
            continue
        timestamp   = datetime.datetime.strptime(elem[5], '%Y,%j,%H:%M:%S.%f')
        duration_ms = ((num_samples - 1) / sample_rate) * 1000
        idx         = elem[0][-1]  # last letter of the first entry is the channel index
        if idx in prev_ts:
            diff = (timestamp - prev_ts[idx]).total_seconds()
            if diff > max_gap or diff < 0:
                print("  gap detected (%.3fs @%s)" % (diff, line))
                issues += 1
        prev_ts[idx] = timestamp + datetime.timedelta(milliseconds=duration_ms)
    return (block_cnt, issues)


if __name__ == "__main__":
    # check arguments
    if len(sys.argv) < 2:
        print("no filename provided")
        sys.exit(1)
    filename = sys.argv[1]
    if not os.path.isdir(filename) and not os.path.isfile(filename):
        print("invalid file or path '%s'" % filename)

    blocks = 0
    issues = 0

    # thorough file checking
    if not simple_check:
        if os.path.isfile(filename) and (".MSEED" in filename or "miniseed" in filename):
            (blocks, issues, _) = check_mseed_file(filename, None)
        elif os.path.isdir(filename):
            path    = filename
            prev_ts = { }
            for fname in sorted(os.listdir(path)):
                if fname.endswith(".MSEED"):
                    ch = fname[-7]
                    if ch not in prev_ts:
                        prev_ts[ch] = None
                    (b, i, ts)  = check_mseed_file("%s/%s" % (path, fname), prev_ts[ch])
                    blocks     += b
                    issues     += i
                    prev_ts[ch] = ts

    # simple file checking (header only)
    else:
        mseedview_path = './'             # location of the helper tool 'mseedview'
        if '/Tools' not in os.getcwd():
            mseedview_path += 'Tools/'
        if os.path.isfile(filename):
            print("checking file '%s'..." % filename)
            blocklist = subprocess.check_output('%smseedview %s' % (mseedview_path, filename), shell=True).decode('utf-8').strip().split('\n')
        elif os.path.isdir(filename):
            print("scanning directory %s for miniseed files..." % filename)
            blocklist = subprocess.check_output('for f in $(find ' + filename + ' -type f -name "*.MSEED" | sort); do ' + mseedview_path + 'mseedview $f; done', shell=True).decode('utf-8').strip().split('\n')
        (blocks, issues) = check_mseed_blocks(blocklist)

    # print summary
    print("%d miniseed blocks checked, %u issues found" % (blocks, issues))
