#!/bin/sh

# Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
# STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.


LOGGERSERVICE=jlinkswoviewer.service

# stop SWO logger first
systemctl is-active $LOGGERSERVICE > /dev/null
LOGGERSTATUS=$?
if [ $LOGGERSTATUS -eq 0 ]; then
  systemctl stop $LOGGERSERVICE
  echo "SWO logger stopped"
fi

echo "resetting gpp..."
OUTPUT=$(printf "r\nr\nq\n" | JLinkExe -device "STM32L496VG" -if "SWD" -speed "auto" -autoconnect 1)
if [ $? -ne 0 ]; then
  echo "an error occurred:"
  echo "$OUTPUT"
  exit 2
fi

if [ $LOGGERSTATUS -eq 0 ]; then
  echo "restarting SWO logger"
  systemctl start $LOGGERSERVICE
fi

