#!/usr/bin/env python3
'''
Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.
'''


# check a ubx file for errors

import sys
import os


ubx_sync_bytes = b'\xB5\x62'
ubx_len_ofs    = 4
ubx_hdr_len    = 6
ubx_crc_len    = 2


def calc_ubx_crc(msg):      # msg = ubx message without the CRC
    sum1 = 0
    sum2 = 0
    for b in msg[2:]:       # exclude sync bytes
        sum1 = (sum1 + b) & 0xff
        sum2 = (sum2 + sum1) & 0xff
    return sum1.to_bytes(1, 'little') + sum2.to_bytes(1, 'little')


# returns the message length and whether the message is valid
def parse_ubx_msg(data):
    data_len    = len(data)
    payload_len = int.from_bytes(data[ubx_len_ofs:ubx_len_ofs+2], 'little')
    if data_len < (payload_len + ubx_hdr_len + ubx_crc_len):
        return [0, False]
    crc_ofs  = ubx_hdr_len + payload_len
    crc_calc = calc_ubx_crc(data[0:crc_ofs])
    crc_ok   = True
    if calc_ubx_crc(data[0:crc_ofs]) != data[crc_ofs:crc_ofs+2]:
        print("invalid CRC (%s vs %s, payload_len: %u, data_len: %u)" % (crc_calc, data[crc_ofs:crc_ofs+2], payload_len, data_len))
        print("message: " + str(data))
        crc_ok = False
    return [ubx_hdr_len + payload_len + ubx_crc_len, crc_ok]


def check_ubx_data(data):
    parsed  = 0             # number of parsed bytes (combined size of all valid messages)
    skipped = 0             # number of skipped bytes
    found   = 0             # number of found messages (based on header)
    valid   = 0             # number of valid messages
    start   = -1            # start index of the message
    total   = len(data)     # total number of bytes
    idx     = 0

    for idx in range(0, total - 1):
        if data[idx:idx+2] != ubx_sync_bytes and idx != (total - 2): # force parsing in last iteration
            continue
        if start >= 0:
            if idx == (total - 2):
                idx += 2    # last iteration: add the last two bytes
            # header found: parse the message
            [msg_len, msg_valid] = parse_ubx_msg(data[start:idx])
            if msg_len:
                found += 1;
                if msg_valid:
                    valid += 1
                parsed  += msg_len
                skipped += idx - (start + msg_len)      # excess bytes at the end
            else:
                # most likely sync bytes within the message -> skip (but don't update *start*)
                continue
        start = idx

    skipped += idx - start
    print("%u of %u bytes parsed, %u bytes skipped" % (parsed, len(data), skipped))
    print("%u of %u found UBX messages are valid" % (valid, found))



if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("no filename provided")
        sys.exit(1)
    filename = sys.argv[1]
    if not os.path.isfile(filename):
        print("file %s not found" % filename)
        sys.exit(1)

    data = []
    with open(filename, "rb") as f:
        data = f.read()
    check_ubx_data(data)


