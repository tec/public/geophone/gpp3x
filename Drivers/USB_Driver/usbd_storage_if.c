/*
 * Copyright (c) 2017 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "usbd_storage_if.h"
#include "bsp_driver_sd.h"

#define STORAGE_LUN_NBR         1
#define STORAGE_BLK_NBR         0x10000     /* not used */
#define STORAGE_BLK_SIZ         0x200       /* not used */

/* Variables -----------------------------------------------------------------*/
extern SD_HandleTypeDef   hsd1;
extern USBD_HandleTypeDef hUsbDeviceFS;

/* USB Mass storage Standard Inquiry Data */
const int8_t STORAGE_Inquirydata_FS[] = {
    /* LUN 0 */
    0x00,
    0x80,
    0x02,
    0x02,
    (STANDARD_INQUIRY_DATA_LEN - 5),
    0x00,
    0x00,
    0x00,
    'S', 'T', 'M', ' ', ' ', ' ', ' ', ' ', /* Manufacturer : 8 bytes */
    'P', 'r', 'o', 'd', 'u', 'c', 't', ' ', /* Product      : 16 Bytes */
    ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',
    '0', '.', '0' ,'1',                     /* Version      : 4 Bytes */
};


/* Private functions ---------------------------------------------------------*/
static int8_t STORAGE_Init_FS(uint8_t lun);
static int8_t STORAGE_GetCapacity_FS(uint8_t lun,
                                     uint32_t *block_num,
                                     uint16_t *block_size);
static int8_t STORAGE_IsReady_FS(uint8_t lun);
static int8_t STORAGE_IsWriteProtected_FS(uint8_t lun);
static int8_t STORAGE_Read_FS(uint8_t lun,
                              uint8_t *buf,
                              uint32_t blk_addr,
                              uint16_t blk_len);
static int8_t STORAGE_Write_FS(uint8_t lun,
                               uint8_t *buf,
                               uint32_t blk_addr,
                               uint16_t blk_len);
static int8_t STORAGE_GetMaxLun_FS(void);


USBD_StorageTypeDef USBD_Storage_Interface_fops_FS =
{
    STORAGE_Init_FS,
    STORAGE_GetCapacity_FS,
    STORAGE_IsReady_FS,
    STORAGE_IsWriteProtected_FS,
    STORAGE_Read_FS,
    STORAGE_Write_FS,
    STORAGE_GetMaxLun_FS,
    (int8_t*)STORAGE_Inquirydata_FS,
};


/* Private functions ---------------------------------------------------------*/
static int8_t STORAGE_Init_FS (uint8_t lun)
{
    (void)lun;

    if (BSP_SD_Init() != MSD_OK)
    {
        return -1;
    }
    return 0;
}

static int8_t STORAGE_GetCapacity_FS(uint8_t lun, uint32_t *block_num, uint16_t *block_size)
{
    HAL_SD_CardInfoTypeDef sd_card_info;

    (void)lun;

    if (BSP_SD_IsDetected() != SD_PRESENT)
    {
        return -1;
    }

    BSP_SD_GetCardInfo(&sd_card_info);

    *block_num  = sd_card_info.LogBlockNbr - 1;
    *block_size = sd_card_info.LogBlockSize;

    return 0;
}

static int8_t STORAGE_IsReady_FS(uint8_t lun)
{
    static int8_t prev_status = 0;

    (void)lun;

    if (BSP_SD_IsDetected() == SD_PRESENT)
    {
        if (prev_status < 0)
        {
            BSP_SD_Init();
            prev_status = 0;

        }
        if (BSP_SD_GetCardState() == SD_TRANSFER_OK)
        {
            return 0;
        }
    }
    else if (prev_status == 0)
    {
        prev_status = -1;
    }

    return -1;
}

static int8_t STORAGE_IsWriteProtected_FS(uint8_t lun)
{
    (void)lun;
    return 0;
}

static int8_t STORAGE_Read_FS(uint8_t lun,
                              uint8_t *buf,
                              uint32_t blk_addr,
                              uint16_t blk_len)
{
    uint32_t tickstart = HAL_GetTick();

    (void)lun;

    BSP_SD_ReadBlocks((uint32_t*)buf, blk_addr, blk_len, SD_DATATIMEOUT);
    while (BSP_SD_GetCardState() != SD_TRANSFER_OK)
    {
        if ((HAL_GetTick()-tickstart) >=  SD_DATATIMEOUT)
        {
            return -1;
        }
    }
    return 0;
}

static int8_t STORAGE_Write_FS(uint8_t lun,
                               uint8_t *buf,
                               uint32_t blk_addr,
                               uint16_t blk_len)
{
    uint32_t tickstart = HAL_GetTick();

    (void)lun;

    BSP_SD_WriteBlocks((uint32_t*)buf, blk_addr, blk_len, SD_DATATIMEOUT);
    while (BSP_SD_GetCardState() != SD_TRANSFER_OK)
    {
        if ((HAL_GetTick()-tickstart) >=  SD_DATATIMEOUT)
        {
            return -1;
        }
    }
    return 0;
}

static int8_t STORAGE_GetMaxLun_FS(void)
{
    return (STORAGE_LUN_NBR - 1);
}

