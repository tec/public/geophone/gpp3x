#ifndef __USBD_CDC_IF_H
#define __USBD_CDC_IF_H

/* Includes ------------------------------------------------------------------*/
#include <stdbool.h>
#include "usbd_cdc.h"

/* Defines -------------------------------------------------------------------*/
#define USB_CDC_RX_BUFFER_SIZE    4096
#define USB_CDC_TX_BUFFER_SIZE    4096

#define USB_CDC_BAUDRATE          1000000

#ifndef USB_CDC_RX_TS
#define USB_CDC_RX_TS()           HAL_GetTick()
#endif

/* Variables -----------------------------------------------------------------*/
extern USBD_CDC_ItfTypeDef  USBD_Interface_fops_FS;

/* Functions -----------------------------------------------------------------*/
uint8_t CDC_Transmit_FS(uint8_t* Buf, uint16_t Len);
uint8_t CDC_Transmit_NB(uint8_t* Buf, uint16_t Len);
uint8_t CDC_Transmit(void);
uint8_t CDC_Transmit_Buffered(const uint8_t* buf, uint16_t len);


#endif /* __USBD_CDC_IF_H */
