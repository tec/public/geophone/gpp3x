#ifndef __USBD_CONF_H
#define __USBD_CONF_H

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stm32l4xx.h"
#include "stm32l4xx_hal.h"

/* Defines -------------------------------------------------------------------*/
#define USBD_MAX_NUM_INTERFACES     1
#define USBD_MAX_NUM_CONFIGURATION  1
#define USBD_MAX_STR_DESC_SIZ       512
#define USBD_SUPPORT_USER_STRING    0
#define USBD_DEBUG_LEVEL            0
#define USBD_LPM_ENABLED            1
#define USBD_SELF_POWERED           1
#define USBD_OTG_ISR_PRIO           12
#define MSC_MEDIA_PACKET            512
#define USBD_DEVICE_FS_ID           0

/* Macros --------------------------------------------------------------------*/

#define USBD_malloc         (void*)USBD_static_malloc
#define USBD_free           USBD_static_free
//#define USBD_memset         /* Not used */
//#define USBD_memcpy         /* Not used */
//#define USBD_Delay          /* Not used */

#if (USBD_DEBUG_LEVEL > 0)
#define  USBD_UsrLog(...)   printf(__VA_ARGS__); \
                            printf("\n");
#else
#define USBD_UsrLog(...)
#endif

#if (USBD_DEBUG_LEVEL > 1)
#define  USBD_ErrLog(...)   printf("ERROR: ") ; \
                            printf(__VA_ARGS__); \
                            printf("\n");
#else
#define USBD_ErrLog(...)
#endif

#if (USBD_DEBUG_LEVEL > 2)
#define  USBD_DbgLog(...)   printf("DEBUG : ") ; \
                            printf(__VA_ARGS__); \
                            printf("\n");
#else
#define USBD_DbgLog(...)
#endif

/* Functions -----------------------------------------------------------------*/
void* USBD_static_malloc(uint32_t size);
void  USBD_static_free(void *p);


#endif /* __USBD_CONF_H */

