#ifndef __USB_DEVICE_H
#define __USB_DEVICE_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx.h"
#include "stm32l4xx_hal.h"
#include "usbd_def.h"
#include "usbd_cdc_if.h"

/* Structures ----------------------------------------------------------------*/
typedef enum
{
    NONE = 0,
    CDC,
    MSC,
} USB_Device_t;

typedef struct
{
    USB_Device_t    deviceClass;
    uint16_t        txLen;          /* Length of transmitted message (CDC only) */
    uint16_t        rxLen;          /* Length of received message (CDC only) */
    uint8_t*        txBuf;          /* Pointer to TX buffer (CDC only) */
    uint8_t*        rxBuf;          /* Pointer to RX buffer (CDC only) */
    uint32_t        rxTS;           /* RX timestamp */
    uint8_t         rxFlag;         /* Flag is set if there is message received (CDC only) */
    uint8_t         rcvdData[USB_CDC_RX_BUFFER_SIZE];  /* Buffer where the received data will be copied to */
} USB_t;

/* Functions -----------------------------------------------------------------*/
void USB_DEVICE_Init(USB_Device_t USBD_Class);
void USB_DEVICE_DeInit(void);


#endif /* __USB_DEVICE_H */

