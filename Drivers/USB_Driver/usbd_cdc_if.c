/*
 * Copyright (c) 2017 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "usbd_cdc_if.h"
#include "usb_device.h"


uint8_t UserRxBufferFS[USB_CDC_RX_BUFFER_SIZE];
uint8_t UserTxBufferFS[USB_CDC_TX_BUFFER_SIZE];

USBD_CDC_LineCodingTypeDef LineCoding =
{
    USB_CDC_BAUDRATE, /* baud rate*/
    0x00,             /* stop bits-1*/
    0x00,             /* parity - none*/
    0x08              /* nb. of bits 8*/
};

extern USB_t              USB;
extern USBD_HandleTypeDef hUsbDeviceFS;

static int8_t CDC_Init_FS(void);
static int8_t CDC_DeInit_FS(void);
static int8_t CDC_Control_FS(uint8_t cmd, uint8_t* pbuf, uint16_t length);
static int8_t CDC_Receive_FS(uint8_t* buf, uint32_t* len);
static int8_t CDC_Transmit_Cplt(uint8_t* buf, uint32_t* len, uint8_t epnum);


USBD_CDC_ItfTypeDef USBD_Interface_fops_FS =
{
    CDC_Init_FS,
    CDC_DeInit_FS,
    CDC_Control_FS,
    CDC_Receive_FS,
    CDC_Transmit_Cplt
};

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  CDC_Init_FS
  *         Initializes the CDC media low layer over the FS USB IP
  * @param  None
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Init_FS(void)
{
    USBD_CDC_SetTxBuffer(&hUsbDeviceFS, UserTxBufferFS, 0);
    USBD_CDC_SetRxBuffer(&hUsbDeviceFS, UserRxBufferFS);
    USB.txBuf  = UserTxBufferFS;
    USB.rxBuf  = UserRxBufferFS;
    USB.txLen  = 0;
    USB.rxLen  = 0;
    USB.rxFlag = 0;
    return (USBD_OK);
}

/**
  * @brief  CDC_DeInit_FS
  *         DeInitializes the CDC media low layer
  * @param  None
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_DeInit_FS(void)
{
    USB.txLen  = 0;
    USB.rxLen  = 0;
    USB.txBuf  = NULL;
    USB.rxBuf  = NULL;
    USB.rxFlag = 0;
    return (USBD_OK);
}

/**
  * @brief  CDC_Control_FS
  *         Manage the CDC class requests
  * @param  cmd: Command code
  * @param  buf: Buffer containing command data (request parameters)
  * @param  len: Number of data to be sent (in bytes)
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Control_FS(uint8_t cmd, uint8_t* buf, uint16_t len)
{
    (void)buf;
    (void)len;

    switch (cmd)
    {
        case CDC_SEND_ENCAPSULATED_COMMAND:
        break;

        case CDC_GET_ENCAPSULATED_RESPONSE:
        break;

        case CDC_SET_COMM_FEATURE:
        break;

        case CDC_GET_COMM_FEATURE:
        break;

        case CDC_CLEAR_COMM_FEATURE:
        break;

        /*******************************************************************************/
        /* Line Coding Structure                                                       */
        /*-----------------------------------------------------------------------------*/
        /* Offset | Field       | Size | Value  | Description                          */
        /* 0      | dwDTERate   |   4  | Number |Data terminal rate, in bits per second*/
        /* 4      | bCharFormat |   1  | Number | Stop bits                            */
        /*                                        0 - 1 Stop bit                       */
        /*                                        1 - 1.5 Stop bits                    */
        /*                                        2 - 2 Stop bits                      */
        /* 5      | bParityType |  1   | Number | Parity                               */
        /*                                        0 - None                             */
        /*                                        1 - Odd                              */
        /*                                        2 - Even                             */
        /*                                        3 - Mark                             */
        /*                                        4 - Space                            */
        /* 6      | bDataBits  |   1   | Number Data bits (5, 6, 7, 8 or 16).          */
        /*******************************************************************************/
        case CDC_SET_LINE_CODING:
        break;

        case CDC_GET_LINE_CODING:
        break;

        case CDC_SET_CONTROL_LINE_STATE:
        break;

        case CDC_SEND_BREAK:
        break;

        default:
        break;
    }

    return (USBD_OK);
}

/**
  * @brief  CDC_Receive_FS
  *         Data received over USB OUT endpoint are sent over CDC interface
  *         through this function.
  *
  *         @note
  *         This function will block any OUT packet reception on USB endpoint
  *         until exiting this function. If you exit this function before transfer
  *         is complete on CDC interface (ie. using DMA controller) it will result
  *         in receiving more data while previous ones are still not sent.
  *
  * @param  buf: Buffer of data to be received
  * @param  len: Number of data received (in bytes)
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL
  */
static int8_t CDC_Receive_FS(uint8_t* buf, uint32_t* len)
{
    /* Note: this is a callback function (called whenever data has been received) */

    if (USB.deviceClass != CDC) return USBD_FAIL;

    if (!USB.rxFlag)
    {
        USB.rxLen = 0;
        USB.rxTS  = USB_CDC_RX_TS();    /* Only update the timestamp on the first reception */
    }
    uint32_t i = 0;
    while ( (i < *len) && (USB.rxLen < (USB_CDC_RX_BUFFER_SIZE - 1)) )
    {
        USB.rcvdData[USB.rxLen++] = USB.rxBuf[i++];
    }
    USB.rcvdData[USB.rxLen++] = 0;      /* In case the received data is interpreted as a string */
    USB.rxFlag = 1;

    USBD_CDC_SetRxBuffer(&hUsbDeviceFS, buf);   /* Set the buffer for the next reception */
    USBD_CDC_ReceivePacket(&hUsbDeviceFS);      /* Ready to receive the next packet */

    return (USBD_OK);
}

/**
  * @brief  CDC_Transmit_FS
  *         Data send over USB IN endpoint are sent over CDC interface
  *         through this function.
  *         @note
  *
  *
  * @param  Buf: Buffer of data to be send
  * @param  Len: Number of data to be send (in bytes)
  * @retval Result of the operation: USBD_OK if all operations are OK else USBD_FAIL or USBD_BUSY
  */
uint8_t CDC_Transmit_FS(uint8_t* buf, uint16_t len)
{
    uint8_t result = USBD_OK;

    if (USB.deviceClass != CDC)
    {
        return USBD_FAIL;
    }

    USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;
    if (hcdc->TxState != 0)
    {
        return USBD_BUSY;
    }

    memcpy(UserTxBufferFS, buf, len);

    USBD_CDC_SetTxBuffer(&hUsbDeviceFS, UserTxBufferFS, len);
    result = USBD_CDC_TransmitPacket(&hUsbDeviceFS);
    return result;
}

/* transmit the data in buf */
uint8_t CDC_Transmit_NB(uint8_t* buf, uint16_t len)
{
    uint8_t result = USBD_OK;

    if (USB.deviceClass != CDC)
    {
        return USBD_FAIL;
    }

    USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;
    if(hcdc->TxState != 0)
    {
        return USBD_BUSY;
    }

    USBD_CDC_SetTxBuffer(&hUsbDeviceFS, buf, len);
    result = USBD_CDC_TransmitPacket(&hUsbDeviceFS);
    return result;
}

/* transmit what is in the local buffer */
uint8_t CDC_Transmit(void)
{
    if (USB.deviceClass != CDC ||
        USB.txBuf == NULL ||
        USB.txLen == 0)
    {
        return USBD_FAIL;
    }

    USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;
    if (hcdc->TxState != 0)
    {
        return USBD_BUSY;
    }

    USBD_CDC_SetTxBuffer(&hUsbDeviceFS, USB.txBuf, USB.txLen);
    return USBD_CDC_TransmitPacket(&hUsbDeviceFS);
}

/* data in buf (if != 0) is copied to the local buffer before initiating the transfer */
uint8_t CDC_Transmit_Buffered(const uint8_t* buf, uint16_t len)
{
    if (USB.deviceClass != CDC ||
        USB.txBuf == NULL ||
        (len == 0 && USB.txLen == 0))
    {
        return USBD_FAIL;
    }

    USBD_CDC_HandleTypeDef *hcdc = (USBD_CDC_HandleTypeDef*)hUsbDeviceFS.pClassData;
    if (hcdc->TxState != 0)
    {
        return USBD_BUSY;
    }
    if (len)
    {
        USB.txLen = MIN(len, USB_CDC_TX_BUFFER_SIZE);
    }
    if (buf)
    {
        /* copy data */
        memcpy(USB.txBuf, buf, USB.txLen);
    }

    USBD_CDC_SetTxBuffer(&hUsbDeviceFS, USB.txBuf, USB.txLen);
    return USBD_CDC_TransmitPacket(&hUsbDeviceFS);
}

static int8_t CDC_Transmit_Cplt(uint8_t* buf, uint32_t* len, uint8_t epnum)
{
    (void)buf;
    (void)len;
    (void)epnum;

    return (0);
}
