/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __BSP_DRIVER_SD_H
#define __BSP_DRIVER_SD_H

/* Exported types ------------------------------------------------------------*/
#define BSP_SD_CardInfo             HAL_SD_CardInfoTypeDef

/* Exported constants --------------------------------------------------------*/
#define MSD_OK                      ((uint8_t)0x00)
#define MSD_ERROR                   ((uint8_t)0x01)
#define MSD_ERROR_SD_NOT_PRESENT    ((uint8_t)0x02)
  
#define SD_TRANSFER_OK              ((uint8_t)0x00)
#define SD_TRANSFER_BUSY            ((uint8_t)0x01)
#define SD_TRANSFER_ERROR           ((uint8_t)0x02)

#define SD_PRESENT                  ((uint8_t)0x01)
#define SD_NOT_PRESENT              ((uint8_t)0x00)

#define SD_DATATIMEOUT              (300U)  /* ms (typically <150ms is sufficient, but some slower cards require > 150ms) */
#define SD_USE_SEMAPHORES           0       /* use semaphores to wait for DMA transfer done */

#define SD_INIT_TRIES               5

#define SDMMC_IRQ_PRIO              RTOS_ISR_PRIO_LOG_SDMMC_DMA   /* was 8 */
#define SD_DMA_IRQ_PRIO             RTOS_ISR_PRIO_LOG_SDMMC_DMA   /* was 9 */

/* Exported functions --------------------------------------------------------*/
uint8_t BSP_SD_Init(void);
uint8_t BSP_SD_DeInit(void);
uint8_t BSP_SD_ReadBlocks(uint32_t *pData, uint32_t ReadAddr, uint32_t NumOfBlocks, uint32_t Timeout);
uint8_t BSP_SD_WriteBlocks(uint32_t *pData, uint32_t WriteAddr, uint32_t NumOfBlocks, uint32_t Timeout);
uint8_t BSP_SD_ReadBlocks_DMA(uint32_t *pData, uint32_t ReadAddr, uint32_t NumOfBlocks);
uint8_t BSP_SD_WriteBlocks_DMA(uint32_t *pData, uint32_t WriteAddr, uint32_t NumOfBlocks);
uint8_t BSP_SD_Erase(uint32_t StartAddr, uint32_t EndAddr);
uint8_t BSP_SD_GetCardState(void);
void    BSP_SD_GetCardInfo(BSP_SD_CardInfo *CardInfo);
uint8_t BSP_SD_IsDetected(void);

__weak void BSP_SD_AbortCallback(void);
__weak void BSP_SD_ErrorCallback(void);
__weak void BSP_SD_WriteCpltCallback(void);
__weak void BSP_SD_ReadCpltCallback(void);

#endif /* __BSP_DRIVER_SD_H */
