/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"
#include "fatfs.h"
#include "diskio.h"
#include "ff_gen_drv.h"

/* Variables -----------------------------------------------------------------*/

/* External variables --------------------------------------------------------*/
extern RTC_DateTypeDef         Date;
extern RTC_TimeTypeDef         Time;
extern const Diskio_drvTypeDef SD_Driver;

/* Functions -----------------------------------------------------------------*/
uint8_t FATFS_Init(char* path)
{
    return FATFS_LinkDriver(&SD_Driver, path);
}

uint8_t FATFS_DeInit(const char* path)
{
    return FATFS_UnLinkDriver(path);
}

/**
  * @brief  Gets Time from RTC 
  * @param  None
  * @retval Time in DWORD
  */
DWORD get_fattime(void)
{
    const RTC_DateTypeDef* date;
    const RTC_TimeTypeDef* time;
    DWORD                  retval;

    RTC_getDateTime(&date, &time);

    retval = (uint32_t)((time->Seconds / 2) & 0x1F)       |
             (uint32_t)((time->Minutes)     & 0x3F) << 5  |
             (uint32_t)((time->Hours)       & 0x1F) << 11 |
             (uint32_t)((date->Date)        & 0x1F) << 16 |
             (uint32_t)((date->Month)       & 0x0F) << 21;

    if      (date->Year >= 80) { retval |= (uint32_t)((date->Year - 80) & 0x7F) << 25; }
    else if (date->Year < 70)  { retval |= (uint32_t)((date->Year + 20) & 0x7F) << 25; }
    /* else: values between 1970 and 1980 are not allowed */

    return retval;
}
