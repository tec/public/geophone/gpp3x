/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"
#include "fatfs.h"
#include "diskio.h"
#include "ff_gen_drv.h"


/* Defines -------------------------------------------------------------------*/
#define SD_TIMEOUT      SD_DATATIMEOUT          /* Defined in bsp_driver_sd.h */

/*
 * Depending on the use case, the SD card initialization could be done at the
 * application level: if it is the case, disable the define below to disable
 * the BSP_SD_Init() call in the SD_Initialize() and manually add a call to 
 * BSP_SD_Init() elsewhere in the application.
 */
//#define ENABLE_SD_INIT

/* Enable the define below to use the SD driver with DMA.
 * The define has effect in SD_read and SD_write.
 * BSP_DRIVER_SD should handle the DMA MSP initialization.
 */
#define ENABLE_SD_DMA_DRIVER

/* 
 * When using cachable memory region, it may be needed to maintain the cache
 * validity. Enable the define below to activate a cache maintenance at each
 * read and write operation.
 * Notice: This is applicable only for cortex M7 based platform.
 */
//#define ENABLE_SD_DMA_CACHE_MAINTENANCE 1

/* Private variables ---------------------------------------------------------*/
static volatile DSTATUS  Stat        = STA_NOINIT;             /* Disk status */
static volatile UINT     ReadStatus  = 0;
static volatile UINT     WriteStatus = 0;

/* External variables --------------------------------------------------------*/
extern SemaphoreHandle_t xSemaphore_SDRead;
extern SemaphoreHandle_t xSemaphore_SDWrite;

/* Private function prototypes -----------------------------------------------*/
DSTATUS SD_initialize(BYTE);
DSTATUS SD_status(BYTE);
DRESULT SD_read(BYTE, BYTE*, DWORD, UINT);

#if FF_FS_READONLY == 0
DRESULT SD_write (BYTE, const BYTE*, DWORD, UINT);
#endif

DRESULT SD_ioctl (BYTE, BYTE, void*);


const Diskio_drvTypeDef SD_Driver =
{
    SD_initialize,
    SD_status,
    SD_read,
#if FF_FS_READONLY == 0
    SD_write,
#endif
    SD_ioctl,
};


/* Private functions ---------------------------------------------------------*/
static DSTATUS SD_checkStatus(BYTE lun)
{
    Stat = STA_NOINIT;

    UNUSED(lun);

    if (BSP_SD_GetCardState() == MSD_OK)
    {
        Stat &= ~STA_NOINIT;
    }

    return Stat;
}

/**
  * @brief  Initializes a Drive
  * @param  lun : not used
  * @retval DSTATUS: Operation status
  */
DSTATUS SD_initialize(BYTE lun)
{
    Stat = STA_NOINIT;

#if defined(ENABLE_SD_INIT)
    if (BSP_SD_Init() == MSD_OK)
    {
        Stat = SD_checkStatus(lun);
    }
#else
    Stat = SD_checkStatus(lun);
#endif

    if (SD_USE_SEMAPHORES && IS_RTOS_RUNNING())
    {
        xSemaphoreTake(xSemaphore_SDRead, 0);
        xSemaphoreTake(xSemaphore_SDWrite, 0);
    }
    ReadStatus  = 0;
    WriteStatus = 0;

    return Stat;
}

/**
  * @brief  Gets Disk Status
  * @param  lun : not used
  * @retval DSTATUS: Operation status
  */
DSTATUS SD_status(BYTE lun)
{
    return SD_checkStatus(lun);
}

/**
  * @brief  Reads Sector(s)
  * @param  lun : not used
  * @param  *buff: Data buffer to store read data
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to read (1..128)
  * @retval DRESULT: Operation result
  */
DRESULT SD_read(BYTE lun, BYTE *buff, DWORD sector, UINT count)
{
    DRESULT  res = RES_ERROR;
    uint32_t timeout;

    UNUSED(lun);

#if defined(ENABLE_SD_DMA_DRIVER)
    /* Use SD Driver in DMA mode */

  #if defined(ENABLE_SD_DMA_CACHE_MAINTENANCE)
    uint32_t alignedAddr;
  #endif /* SD_DMA_CACHE_MAINTENANCE */

    ReadStatus = 0;
    if (SD_USE_SEMAPHORES && IS_RTOS_RUNNING())
    {
        /* The purpose of this semaphore is not to prevent simultaneous access, but rather to have a way to
         * put the task in a 'waiting' state to free up the CPU while the DMA is transferring the data. */
        xSemaphoreTake(xSemaphore_SDRead, 0);
    }

    if (BSP_SD_ReadBlocks_DMA((uint32_t*)buff, (uint32_t)(sector), count) == MSD_OK)
    {
        /* RTOS is running */
        if (SD_USE_SEMAPHORES && IS_RTOS_RUNNING())
        {
            /* Wait for DMA Semaphore */
            if (xSemaphoreTake(xSemaphore_SDRead, pdMS_TO_TICKS(SD_TIMEOUT)) == pdTRUE)
            {
                /* Read Complete */
                timeout = HAL_GetTick();
                while ((HAL_GetTick() - timeout) < SD_TIMEOUT)
                {
                    if (BSP_SD_GetCardState() == SD_TRANSFER_OK)
                    {
                        res = RES_OK;
  #if defined(ENABLE_SD_DMA_CACHE_MAINTENANCE)
                        /* The SCB_InvalidateDCache_by_Addr() requires a 32-Byte aligned address,
                         * adjust the address and the D-Cache size to invalidate accordingly.
                        */
                        alignedAddr = (uint32_t)buff & ~0x1F;
                        SCB_InvalidateDCache_by_Addr((uint32_t*)alignedAddr, count*BLOCKSIZE + ((uint32_t)buff - alignedAddr));
  #endif /* SD_DMA_CACHE_MAINTENANCE */
                        break;
                    }
                }
            }
            else
            {
                /* Read Timeout */
                res = RES_ERROR;
            }
        }
        /* RTOS is NOT running */
        else
        {
            /* Wait for DMA Complete */
            timeout = HAL_GetTick();
            while ( (ReadStatus == 0) && ((HAL_GetTick() - timeout) < SD_TIMEOUT) );

            /* In case of a timeout return error */
            if (ReadStatus == 0)
            {
                DEBUG_PRINT(DBG_LVL_WARNING, "SD read timeout");
                res = RES_ERROR;
            }
            else
            {
                ReadStatus = 0;
                timeout    = HAL_GetTick();
                while ((HAL_GetTick() - timeout) < SD_TIMEOUT)
                {
                    if (BSP_SD_GetCardState() == SD_TRANSFER_OK)
                    {
                        res = RES_OK;
  #if defined(ENABLE_SD_DMA_CACHE_MAINTENANCE)
                        /* The SCB_InvalidateDCache_by_Addr() requires a 32-Byte aligned address,
                         * adjust the address and the D-Cache size to invalidate accordingly.
                        */
                        alignedAddr = (uint32_t)buff & ~0x1F;
                        SCB_InvalidateDCache_by_Addr((uint32_t*)alignedAddr, count*BLOCKSIZE + ((uint32_t)buff - alignedAddr));
  #endif /* SD_DMA_CACHE_MAINTENANCE */
                        break;
                    }
                }
                if ((HAL_GetTick() - timeout) >= SD_TIMEOUT)
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "SD read timeout");
                }
            }
        }
    }

#else
    /* Use SD Driver in blocking mode */
    if (BSP_SD_ReadBlocks((uint32_t*)buff,
                       (uint32_t) (sector),
                       count, SDMMC_HAL_TIMEOUT) == MSD_OK)
    {
        /* Wait until the read operation is finished */
        timeout = HAL_GetTick();
        while ((HAL_GetTick() - timeout) < SD_TIMEOUT)
        {
            if (BSP_SD_GetCardState() == MSD_OK)
            {
                return RES_OK;
            }
        }
    }
#endif /* ENABLE_SD_DMA_DRIVER */

    return res;
}


/**
  * @brief  Writes Sector(s)
  * @param  lun : not used
  * @param  *buff: Data to be written
  * @param  sector: Sector address (LBA)
  * @param  count: Number of sectors to write (1..128)
  * @retval DRESULT: Operation result
  */
#if FF_FS_READONLY == 0
DRESULT SD_write(BYTE lun, const BYTE *buff, DWORD sector, UINT count)
{
    DRESULT  res = RES_ERROR;
    uint32_t timeout;

    UNUSED(lun);

  #if defined(ENABLE_SD_DMA_DRIVER)
    /* Use SD Driver in DMA mode */

    #if defined(ENABLE_SD_DMA_CACHE_MAINTENANCE)
    uint32_t alignedAddr;
    alignedAddr = (uint32_t)buff &  ~0x1F;
    SCB_CleanDCache_by_Addr((uint32_t*)alignedAddr, count*BLOCKSIZE + ((uint32_t)buff - alignedAddr));
    #endif

    WriteStatus = 0;
    if (SD_USE_SEMAPHORES && IS_RTOS_RUNNING())
    {
        /* The purpose of this semaphore is not to prevent simultaneous access, but rather to have a way to
         * put the task in a 'waiting' state to free up the CPU while the DMA is transferring the data. */
        xSemaphoreTake(xSemaphore_SDWrite, 0);
    }

    if (BSP_SD_WriteBlocks_DMA((uint32_t*)buff, (uint32_t)(sector), count) == MSD_OK)
    {
        /* RTOS is running */
        if (SD_USE_SEMAPHORES && IS_RTOS_RUNNING())
        {
            /* Wait for DMA Semaphore */
            if (xSemaphoreTake(xSemaphore_SDWrite, pdMS_TO_TICKS(SD_TIMEOUT)) == pdTRUE)
            {
                /* Write Complete */
                timeout = HAL_GetTick();
                while ((HAL_GetTick() - timeout) < SD_TIMEOUT)
                {
                    if (BSP_SD_GetCardState() == SD_TRANSFER_OK)
                    {
                        res = RES_OK;
                        break;
                    }
                }
            }
            else
            {
                /* Write Timeout */
                res = RES_ERROR;
            }
        }
        /* RTOS is NOT running */
        else
        {
            /* Wait for DMA complete */
            timeout = HAL_GetTick();
            while ((WriteStatus == 0) && ((HAL_GetTick() - timeout) < SD_TIMEOUT));

            /* In case of a timeout return error */
            if (WriteStatus == 0)
            {
                DEBUG_PRINT(DBG_LVL_WARNING, "SD write timeout");
                res = RES_ERROR;
            }
            else
            {
                WriteStatus = 0;
                timeout     = HAL_GetTick();
                while ((HAL_GetTick() - timeout) < SD_TIMEOUT)
                {
                    if (BSP_SD_GetCardState() == SD_TRANSFER_OK)
                    {
                        res = RES_OK;
                        break;
                    }
                }
                if ((HAL_GetTick() - timeout) >= SD_TIMEOUT)
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "SD write timeout");
                }
            }
        }
    }

  #else
    /* Use SD Driver in blocking mode */
    if (BSP_SD_WriteBlocks((uint32_t*)buff,
                        (uint32_t)(sector),
                        count, SDMMC_HAL_TIMEOUT) == MSD_OK)
    {
        /* Wait until the Write operation is finished */
        timeout = HAL_GetTick();
        while ((HAL_GetTick() - timeout) < SD_TIMEOUT)
        {
            if (BSP_SD_GetCardState() == MSD_OK)
            {
                return RES_OK;
            }
        }
    }
  #endif /* ENABLE_SD_DMA_DRIVER */

    return res;
}
#endif

/**
  * @brief  I/O control operation
  * @param  lun : not used
  * @param  cmd: Control code
  * @param  *buff: Buffer to send/receive control data
  * @retval DRESULT: Operation result
  */
DRESULT SD_ioctl(BYTE lun, BYTE cmd, void *buff)
{
    DRESULT res = RES_ERROR;
    BSP_SD_CardInfo CardInfo;

    UNUSED(lun);

    if (Stat & STA_NOINIT) 
    {
        return RES_NOTRDY;
    }

    switch(cmd)
    {
        /* Make sure that no pending write process */
        case CTRL_SYNC :
          res = RES_OK;
          break;

        /* Get number of sectors on the disk (DWORD) */
        case GET_SECTOR_COUNT :
          BSP_SD_GetCardInfo(&CardInfo);
          *(DWORD*)buff = CardInfo.LogBlockNbr;
          res = RES_OK;
          break;

        /* Get R/W sector size (WORD) */
        case GET_SECTOR_SIZE :
          BSP_SD_GetCardInfo(&CardInfo);
          *(WORD*)buff = CardInfo.LogBlockSize;
          res = RES_OK;
          break;

        /* Get erase block size in unit of sector (DWORD) */
        case GET_BLOCK_SIZE :
          BSP_SD_GetCardInfo(&CardInfo);
          *(DWORD*)buff = CardInfo.LogBlockSize;
          res = RES_OK;
          break;

        default:
          res = RES_PARERR;
    }

    return res;
}


/**
  * @brief Rx Transfer complete callback
  * @param hsd: SD handle
  * @retval None
  */
void BSP_SD_ReadCpltCallback(void)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /* RTOS is running */
    if (SD_USE_SEMAPHORES && IS_RTOS_RUNNING())
    {
        xSemaphoreGiveFromISR(xSemaphore_SDRead, &xHigherPriorityTaskWoken);
    }
    /* RTOS is NOT running */
    else
    {
        ReadStatus = 1;
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/**
  * @brief Tx Transfer complete callback
  * @param hsd: SD handle
  * @retval None
  */
void BSP_SD_WriteCpltCallback(void)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /* RTOS is running */
    if (SD_USE_SEMAPHORES && IS_RTOS_RUNNING())
    {
        xSemaphoreGiveFromISR(xSemaphore_SDWrite, &xHigherPriorityTaskWoken);
    }
    /* RTOS is NOT running */
    else
    {
        WriteStatus = 1;
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
