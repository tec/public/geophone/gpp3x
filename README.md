# Geophone Platform 3x

This is the software repository for the **Geophone Platform 3x** (GPP3x). It features the following sensors:

- 3-axis Geophone sampling.
- 1-axis Geophone triggering.
- GNSS through a [*Ublox F9P*](https://www.u-blox.com/en/product/zed-f9p-module).
- 1 Inclinometer using the [*Murata SCL3300*](https://www.murata.com/en-eu/products/sensor/inclinometer/scl3300).
- 1 IMU providing an accelerometer and a magnetometer through an [*STMicroelectronics LSM303AGR*](https://www.st.com/en/mems-and-sensors/lsm303agr.html).

The hardware files for this project can be found on [GitLab](https://gitlab.ethz.ch/tec/projects/gpp/hardware/app_geophone).

## Other projects

Next to the full-feature GPP, we also have two reduced versions:

- The original GPP featuring 1 Geophone axis and an IMU.
- The GPP mini featuring an IMU and an inclinometer.

## Respository file structure

```
Folder/File                         Description

Drivers
 |- CMSIS                           MCU support library by ARM
 |- FatFs_Driver                    Low-level driver for the FAT file system
 |- STM32L4xx_HAL_Driver            Driver library by ST
 |- USB_Driver                      Low-level driver for the USB device library
Inc                                 Application include files
 |- dpp                             DPP common submodule (message definitions)
Middlewares
 |- ST
 |   |- STM32_USB_Device_Library    USB device library by ST
 |- Third_Party
 |   |- FatFs                       FAT file system library by ChaN
 |   |- FreeRTOS                    Operating system files
Src                                 Application source files
Startup                             MCU vector table, required by the GCC toolchain
Tools                               Misc tools
Trace                               Files for SEGGER SystemView
geophone3x_debug.launch             Debug configuration file to download the firmware
geophone3x_debug_noreset.launch     Debug configuration file to connect to the target without a reset
geophone3x.ioc                      STM32CubeMX config file (currently unused)
STM32L496VG_FLASH.ld                Linker file
```


### Tools and Scripts

| Name                     | Description                                                                                                                                 |
|--------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
| `adcdata_to_csv.py`      | Converts raw ADC data files to CSV format                                                                                                   |
| `check_mseed.py`         | Performs a quick validity check of a single miniseed file or multiple files within a folder                                                 |
| `fix_mseed_corruption.py`| Fixes miniseed file corruption that can occur with GPP3X firmware version 2.5.0 or earlier                                                  |
| `fix_mseed.py`           | Removes invalid blocks from a miniseed file (e.g. to remove blocks with invalid/old timestamp)                                              |
| `githash.sh`             | Used by the IDE to generate the file `gitrev.h` with a define that contains the hash of the current git commit as well as a UNIX timestamp. |
| `gnss_calc_crc.py`       | Calculate the CRC of a UBX message                                                                                                          |
| `gpp-timesync.service`   | Systemd service configuration to periodically send a timestamp via the STM32 virtual COM port to the GPP                                    |
| `gpp-timesync.timer`     | Systemd timer configuration, required for timesync service                                                                                  |
| `gpp_bootloader.hex`     | Precompiled GPP bootloader, works with GPP1x and GPP3x                                                                                      |
| `gpp_debuglog.sh`        | Prints the SWO debug output, requires JLinkSWOViewer*                                                                                       |
| `gpp_program.sh`         | GPP programming script, requires JLinkExe*                                                                                                  |
| `gpp_reset.sh`           | Resets the GPP, requires JLinkExe*                                                                                                          |
| `jlinkswoviewer.service` | Systemd service to continuously log the SWO output to the journal                                                                           |
| `mseedview`              | Utility of libmseed, can be used to inspect the block headers of miniseed files.                                                            |
| `plot_mseed.py`          | Plot a miniseed file                                                                                                                        |
| `print_mseed_gaps.py`    | Print gaps in a miniseed files (obspy)                                                                                                      |
| `receive_data.py`        | Receive streaming data                                                                                                                      |
| `send_timestamp.py`      | Send the current UNIX time via the STM32 virtual COM port to the GPP for time synchronization (`TSYNC_USB`)                                 |

`*` JLink tools: [JLink Software and Documentation Pack](https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack)


### Bootloader

The GPP uses a bootloader. The software is available on [GitLab](https://gitlab.ethz.ch/tec/projects/gpp/software/application_platforms/stm32l4/stm32_bootloader). A precompiled binary can be flashed with the following command:
```
./Tools/gpp_program.sh Tools/gpp_bootloader.hex
```


## Compile-time configuration options

All compile-time configurations can be set in `config.h`. The most important settings are the following:
```
#define DPP_DEVICE_ID               22003           /* Device ID */
#define GPP3X                                       /* Uncomment to compile for GPP3X (HW rev2.0) */

#define ADC_SPS                     125             /* Default sampling rate (samples per second) */

#define GEO_TRG_POS_TH              1700U           /* Positive trigger threshold in mV, only relevant in SYS_OPMODE_TRG */
#define GEO_TRG_NEG_TH              1300U           /* Negative trigger threshold in mV, only relevant in SYS_OPMODE_TRG */

#define TSYNC_METHOD                TSYNC_BOLT      /* How the time should be synced (TSYNC_GNSS, TSYNC_BOLT or TSYNC_USB) */

#define LOG_ADCDATA_MINISEED        0               /* If set to 1, the ADC data will be stored in miniSEED format (otherwise as raw 24-bit ADC samples) */

#define SYSTEM_OPMODE               (SYS_OPMODE_SCHED)      /* Operating mode (bitwise combination of SYS_OPMODE_* flags) */
#define USE_CONFIG_FROM_SD          1               /* If set to 0, the config on the SD card will be ignored */
#define USE_STOP2                   1               /* Whether the MCU should go into STOP2 low-power mode */
#define USE_TICKLESS_IDLE           1               /* FreeRTOS tickless idle mode (stops the CPU when no task other than the Idle Task can run) */
#define USE_SYSTEMVIEW              0               /* Whether to use SEGGER SystemView (adds an overhead in terms of execution time and memory footprint) */
#define USE_BUZZER                  0               /* If set to 0, the buzzer will not be used */
#define STREAMING_TARGET            STREAMING_NONE  /* Interface for ADC data streaming */

#define DEBUG_OUTPUT_LEVEL          DBG_LVL_VERBOSE /* Debug print verbosity level */
#define DEBUG_PRINT_TARGET          DBG_PRINT_SWO   /* Debug print output device */

```

This code is compatible with the old Geophone platform (GPP1x). To compile for the GPP1x, simply comment-out the `#define GPP3X`.


## SD card

All data is logged to the microSD card.
File structure:
```
Folder / File:                      Description:

[YYYY-MM-DD]/*.DAT                  ADC data files (3 files per acqisition ID, one for each ADC channel)
[YYYY-MM-DD]/_ACQ.CSV               Acquisition meta data (one entry per acquisition ID)
[YYYY-MM-DD]/_EVENTS.CSV            Event log
[YYYY-MM-DD]/_HEALTH.CSV            App health log
[YYYY-MM-DD]/_IMU.CSV               IMU data log
[YYYY-MM-DD]/_INCLINO.CSV           Inclinometer data log
[YYYY-MM-DD]/_GNSS.CSV              GNSS data log
[YYYY-MM-DD]/_TSYNC.CSV             Time synchronization status log
[YYYY-MM-DD]/_COMDATA.CSV           Log of messages received from the ComBoard
_ACQID.TXT                          Contains the most recent acquisition ID (loaded at startup, continuously updated during runtime)
_RESETS.TXT                         Contains the reset counter
SCHEDULE.TXT                        Schedule for periodic tasks
INFO.TXT                            Firmware info
CONFIG.TXT                          Configuration file
```
All the collected data is stored in subfolders of the format `YYYY-MM-DD`, i.e. a new subfolder is created each day.


### Schedule file

Periodic task executions can be scheduled by adding a new line (schedule entry) to the file `SCHEDULE.TXT`:
```
[first] [period] [duration] [task]

```
For example `0 600 0 1` will schedule the health task every 10 minutes. `first` is the UNIX timestamp of the first execution. If set to 0, the current time will be used. `duration` is only of use for the geophone task and determines the sample duration.
Note that it is possible to select multiple tasks (bitwise combination), e.g. `5` would schedule both health task and GNSS task (see the `TASK_*` definitions in `main.h` for the bit mapping).


### Config file

At runtime, the following configuration parameters can be adjusted in the file `CONFIG.TXT`:
```
DEVICE_ID
SYS_OPMODE
TRG_TH_POS
TRG_TH_NEG
TRG_GAIN
POSTTRG
TIMEOUT
ADC_PGA
ADC_FORMAT
ADC_SPS
IMU_FREQ_LP
IMU_FREQ_HP
IMU_FREQ_AA
IMU_OPMODE
IMU_TRG_LVL
IMU_DATA_DEC
GNSS_LEAP_S
```

### Convert ADC data files

The ADC data is stored in binary format as raw 24-bit values (big endian) without any header. Accordingly, the file size is exactly `24 * [sampling rate] * [duration] bits` (e.g. 375 bytes per second for 125 samples per second).
There are 3 files per acquisition ID, one for each geophone axis / ADC channel. The filename format is as follows: `[acqid].[channel].DAT`, where channel is either `Z` (vertical), `N` (north), or `E` (east). The channel mapping can be set in `config.h` and must be observed when connecting the geophones to the respective ADCs on the PCB.
To convert the raw data to a CSV file, the following script can be used:
```
./Tools/adcdata_to_csv.py [filename]
```


## Debugging

### How to compile and flash the code

1. Download and install the [necessary external compiler](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads) `gcc-arm-none-eabi`.
2. Download and install [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) (v1.3.0 is recommended, newer versions should also work).
3. Clone the project including its submodules:
    ```
    git clone --recursive git@gitlab.ethz.ch:tec/projects/gpp/software/application_platforms/stm32l4/geophone3x.git
   ```
4. Open the project inside the IDE and click the "hammer" symbol to build the project.
5. Connect a JLink Debugger using the custom cable to the debug connector (pay attention that the marked pin with the red wire corresponds to the position marked by an arrow on the PCB).
6. Use the "bug" symbol to deploy and debug the project.

### How to configure the node

The key configuration variables can be found in [`config.h`](./Inc/config.h):

| Define               | Usage                                                                                                              |
|----------------------|--------------------------------------------------------------------------------------------------------------------|
| `DEVICE_ID`          | Sets the node's ID; must correspond to the one used on the ComBoard for successful communication                   |
| `GPP3X`              | Must be defined if the HW corresponds to the second generation GPP; _must not be defined_ for the first generation |
| `SYSTEM_OPMODE`      | Used to set the application operation modes defined in [`main.h`](./Inc/main.h), which can be ORed together        |
| `DEBUG_PRINT_TARGET` | Used to choose the debug output method as stated [below](#how-to-view-the-serial-log)                              |
| `STREAMING_TARGET`   | Used to choose the data output method as stated [below](#how-to-stream-data)                                       |

### How to view the serial log

The output method of the `DebugPrint` function can be chosen through `DEBUG_PRINT_TARGET`:

| Variable         | Comment                                         |
|------------------|-------------------------------------------------|
| `DBG_PRINT_SWO`  | Requires JLink debugger                         |
| `DBG_PRINT_USB`  | Requires power through USB connector            |
| `DBG_PRINT_UART` | Requires exclusive access to the two debug pins |

#### SWO

To access the SWO serial output, SEGGERs JLink SWO Viewer can be utilized.
To do so, install [SEGGER JLink tools](https://www.segger.com/downloads/jlink#J-LinkSoftwareAndDocumentationPack). On Windows, open the GUI, select the **STM32L496VG MCU** and set the CPU clock speed to 80MHz. On Linux, a command line version is available:
```
/opt/SEGGER/JLink/JLinkSWOViewer -device STM32L496VG -cpufreq 80000000
```
Alternatively, the script `gpp_debuglog.sh` or the IDE's internal `SWV ITM Data Console` can be used.

#### USB

It is also possible to redirect the debug output to the USB connection (virtual COM port of the STM32).
Make sure to not use STOP2 mode by setting `USE_STOP2` to 0.

#### UART

It is also possible to use the LPUART1 interface on the pins PC0/PC1.
Notice that it is not possible to use this option in combination with `DEBUG_USB_MSC_ON_DBGPIN2`.

### How to stream data

To stream binary data, make sure to set `STREAMING_TARGET`:

| Variable        | Comment                                         |
|-----------------|-------------------------------------------------|
| `STREAMING_SWO` | Requires JLink debugger                         |
| `STREAMING_USB` | Requires power through USB connector            |
| `STREAMING_UART`| Requires exclusive access to the two debug pins |

Notice that it is not possible at the moment to have the same method for `DEBUG_PRINT_TARGET` and `STREAMING_TARGET`.

#### UART

To receive the data, use the corresponding [script](./Tools/receive_data.py), which will save the data in a local file.

### How to use SystemView

**SystemView** can be used to trace the FreeRTOS task and interrupt execution on the running target. Make sure `USE_SYSTEMVIEW` is enabled in `config.h` when you flash the application to the MCU. Download [SystemView from SEGGER](https://www.segger.com/downloads/jlink#SystemView). Open the GUI and select `Target > Start Recording`. Make sure the correct MCU is selected as _Target Device_. Leave all other settings in their default state. If the program fails to connect to the target, reset the MCU and try again. Also, disabling `STOP2` mode can help.

*Note:* The default installation folder on Linux is `/opt/SEGGER/SystemView/SystemView`. If the GUI doesn't run, then you are probably missing the package `libqtgui4`.

### How to mount the SD card

The SD card can be mounted in two ways:

1. If `DEBUG_USB_MSC_ON_DBGPIN2` is used, the pin can be used to mount the SD card (useful in combination with the BaseBoard).
2. A long press (default longer than 1 second) on the `User` button will mount the SD card. It can be unmounted again through a short press.
