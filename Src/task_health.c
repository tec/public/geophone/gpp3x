/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
SysHealth_t       SysHealth = { 0 };

/* External variables --------------------------------------------------------*/
extern rb_t       rb_health;

/*** HEALTH TASK **************************************************************/
void vTaskHealth(void* arg)
{
    static dpp_app_health_t health;    /* static to reduce stack size */
    uint8_t                 max_stack_usage = 0;

    (void)arg;

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Health task started");

    while (true)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
        uint32_t start_ts = TIM5_getCounterValue();

        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Node ID: %u", SysConf.DeviceID);

        /* Stack Usage */
        uint32_t u32;
        SysHealth.StackUsage = RTOS_getHighestStackUsage(&u32);
        if ( (SysHealth.StackUsage >= RTOS_MEMUSAGE_WARN_TH) && (SysHealth.StackUsage > max_stack_usage) )
        {
            RTOS_event(EVENT_GEOPHONE3X_STACK_90, u32, EVTLOG_SD | EVTLOG_COM);
            max_stack_usage = SysHealth.StackUsage;
        }

        /* Heap Usage */
        SysHealth.HeapUsedP = RTOS_getHeapUsage();
        if (SysHealth.HeapUsedP >= RTOS_MEMUSAGE_WARN_TH)
        {
            RTOS_event(EVENT_GEOPHONE3X_HEAP_90, SysHealth.HeapUsedP, EVTLOG_SD | EVTLOG_COM);
        }

        /* Timer Daemon Stack Usage */
        u32 = (uxTaskGetStackHighWaterMark(xTimerGetTimerDaemonTaskHandle()) * 100) / configTIMER_TASK_STACK_DEPTH;
        SysHealth.StackTmrSvcWMP = 100 - (uint8_t)u32;
        if (SysHealth.StackTmrSvcWMP >= RTOS_MEMUSAGE_WARN_TH)
        {
            RTOS_event(EVENT_GEOPHONE3X_STACK_TMRSVC, SysHealth.StackTmrSvcWMP, EVTLOG_SD | EVTLOG_COM);
        }

        /* Idle Task Stack Usage */
        u32 = (uxTaskGetStackHighWaterMark(xTaskGetIdleTaskHandle()) * 100) / configMINIMAL_STACK_SIZE;
        SysHealth.StackIdleWMP = 100 - (uint8_t)u32;
        if (SysHealth.StackIdleWMP >= RTOS_MEMUSAGE_WARN_TH)
        {
            RTOS_event(EVENT_GEOPHONE3X_STACK_IDLE, SysHealth.StackIdleWMP, EVTLOG_SD | EVTLOG_COM);
        }

        /* NV Mem */
        RTOS_queueSendLog(LOGTYPE_SD_GETSIZE);

        /* Uptime */
        SysHealth.Uptime = RTC_getUptime();

        /* Battery Voltage */
        if (!USB_IS_CONNECTED())
        {
            SysHealth.Vbat = ADC1_getBatteryVoltage();

            /* Generate warning if voltage is not within expected range */
            if ((SysHealth.Vbat < HEALTH_VBAT_LOW_WARN_TH) || (SysHealth.Vbat > HEALTH_VBAT_HIGH_WARN_TH))
            {
                RTOS_event(EVENT_GEOPHONE3X_VCC_OOR, SysHealth.Vbat, EVTLOG_SD | EVTLOG_COM);
            }
            VSENSE_SWOFF();   /* Turn off battery voltage sensing circuit */
        }
        else
        {
            /* Invalid: Set all bits to 1 */
            SysHealth.Vbat = DPP_MSG_INV_UINT16;
        }

        /* System Voltage */
        SysHealth.Vref = ADC1_getSystemVoltage();    /* should be equal to SYSTEM_VOLTAGE */
        if (SysHealth.Vref < HEALTH_VSYS_LOW_WARN_TH)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "System voltage is out of range (%umV)", SysHealth.Vref);
            SysHealth.Vref = HEALTH_VSYS_LOW_WARN_TH;
        }
        else if (SysHealth.Vref > HEALTH_VSYS_HIGH_WARN_TH)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "System voltage is out of range (%umV)", SysHealth.Vref);
            SysHealth.Vref = HEALTH_VSYS_HIGH_WARN_TH;
        }
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Battery: %umV, System: %umV", ( (SysHealth.Vbat != DPP_MSG_INV_UINT16) ? SysHealth.Vbat : 0), SysHealth.Vref);

        /* SHT31: get temperature and humidity reading */
        SysHealth.Temperature = DPP_MSG_INV_INT16;
        SysHealth.Humidity    = DPP_MSG_INV_UINT16;
        if (SHT31_init() && SHT31_readTempHumidity(&SysHealth.Temperature, &SysHealth.Humidity))
        {
            SHT31_printTempHumidity(SysHealth.Temperature, SysHealth.Humidity);
            if ( (SysHealth.Temperature < (HEALTH_TEMP_LOW_WARN_TH * 100)) || (SysHealth.Temperature > (HEALTH_TEMP_HIGH_WARN_TH * 100)) )
            {
                RTOS_event(EVENT_GEOPHONE3X_TEMP_OOR, 0, EVTLOG_SD | EVTLOG_COM);
            }
            if (SysHealth.Humidity > (HEALTH_HUMIDITY_WARN_TH * 100))
            {
                RTOS_event(EVENT_GEOPHONE3X_HUMID_OOR, 0, EVTLOG_SD | EVTLOG_COM);
            }
        }
        SHT_SWOFF();

        /* CPU duty cycle */
        SysHealth.DutyCycle = RTOS_dutyCycle(DC_CNTR_GET);
        RTOS_dutyCycle(DC_CNTR_RESET);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "CPU duty cycle: %u.%02u%%", SysHealth.DutyCycle / 100, SysHealth.DutyCycle % 100);

        /* DPP App Health Payload */
        health.uptime         = SysHealth.Uptime;
        health.msg_cnt        = SysHealth.CmdCnt;
        health.core_vcc       = SysHealth.Vref;
        health.core_temp      = DPP_MSG_INV_INT16;
        health.cpu_dc         = SysHealth.DutyCycle;
        health.stack          = SysHealth.StackUsage;
        health.nv_mem         = SD_getFillLevel();
        health.supply_vcc     = SysHealth.Vbat;
        health.supply_current = DPP_MSG_INV_UINT16;
        health.temperature    = SysHealth.Temperature;
        health.humidity       = SysHealth.Humidity;

        Bolt_enqueueMessage(DPP_MSG_TYPE_APP_HEALTH, (uint8_t*)&health);

        /* Log Health */
        RB_insert(&rb_health, (uint8_t*)&health);
        RTOS_queueSendLog(LOGTYPE_HEALTH);

        /* Check task runtime */
        uint32_t time_delta_ms = TIM5_TICKS_TO_MS(TIM5_getCounterValue() - start_ts);
        if (time_delta_ms > RTOS_TASK_RUNTIME_TH_MS)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "Task runtime longer than expected (%4lums)", time_delta_ms);
            RTOS_event(EVENT_GEOPHONE3X_TASK_RUNTIME, (time_delta_ms << 16) | RTOS_TASK_ID_HEALTH, EVTLOG_SD | EVTLOG_COM);
        }

    } /* Health task loop */
}
