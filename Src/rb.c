/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Functions -----------------------------------------------------------------*/

void RB_init(rb_t* rb, uint32_t max, uint32_t itemsize, uint8_t* data)
{
    rb->count    = 0;
    rb->max      = max;
    rb->itemsize = itemsize;
    rb->head     = data;
    rb->tail     = data;
    rb->data     = data;

    memset(rb->data, 0, rb->max * rb->itemsize);
}

/* If item is NULL:
 * - Head is shifted accordingly if there is still space left in the ring buffer. If the buffer is full, the last (newest) element will be overwritten.
 * - After calling RB_insert(), custom data can be added manually to head.
 * Note: it is the user's responsibility to carefully check and write not more than `itemsize` bytes to head.
 */
uint8_t* RB_insert(rb_t* rb, const uint8_t* item)
{
    UBaseType_t uxSavedInterruptStatus = 0;

    /* Critical Section */
    if (IS_RTOS_RUNNING())
    {
        if (IS_ISR()) { uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR(); }
        else          { taskENTER_CRITICAL(); }
    }

    /* When RB is empty, head points to an empty block. */
    /* Increment head only if RB is neither empty nor full. */
    if ( (rb->count > 0) && (rb->count < rb->max) )
    {
        /* Check if head points to last block */
        if (rb->head == (rb->data + ((rb->max - 1) * rb->itemsize)))
        {
            rb->head = rb->data;
        }
        else
        {
            rb->head = rb->head + rb->itemsize;
        }
    }
    else
    {
        /* For debugging only */
        if (rb->count == rb->max)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "Ring buffer overflow (items: %lu, size: %lu)", rb->count, rb->itemsize);
        }
    }

    /* Copy data */
    if (item != NULL)
    {
        memcpy(rb->head, item, rb->itemsize);
    }

    if (rb->count < rb->max)     /* Increment counter */
    {
        rb->count++;
    }

    /* Make sure that we return the address to the NEW head as the application might write to that space */
    uint8_t* head = rb->head;

    /* Critical Section */
    if (IS_RTOS_RUNNING())
    {
        if (IS_ISR()) { taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus); }
        else          { taskEXIT_CRITICAL(); }
    }

    return head;
}

uint8_t* RB_getHead(rb_t* rb)
{
    if (rb->count)
    {
        return rb->head;
    }
    else
    {
        return NULL;
    }
}

uint8_t* RB_getTail(rb_t* rb)
{
    if (rb->count)
    {
        return rb->tail;
    }
    else
    {
        return NULL;
    }
}

bool RB_remove(rb_t* rb)
{
    UBaseType_t uxSavedInterruptStatus = 0;

    /* Empty? */
    if (!rb->count) { return false; }

    /* Critical Section */
    if (IS_RTOS_RUNNING())
    {
        if (IS_ISR()) { uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR(); }
        else          { taskENTER_CRITICAL(); }
    }

    /* Increment tail only if count > 1 */
    if (rb->count > 1)
    {
        /* Check if tail points to last block */
        if (rb->tail == (rb->data + ((rb->max - 1) * rb->itemsize)))
        {
            rb->tail = rb->data;
        }
        else
        {
            rb->tail = rb->tail + rb->itemsize;
        }
    }

    rb->count--;

    /* Critical Section */
    if (IS_RTOS_RUNNING())
    {
        if (IS_ISR()) { taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus); }
        else          { taskEXIT_CRITICAL(); }
    }

    return true;
}

bool RB_isEmpty(rb_t* rb)
{
    return (!rb->count);
}

bool RB_isFull(rb_t* rb)
{
    return (rb->max == rb->count);
}

uint32_t RB_spaceLeft(rb_t* rb)
{
    return rb->max - rb->count;
}

void RB_flush(rb_t* rb)
{
    rb->count = 0;
    rb->head  = rb->tail = rb->data;
    memset(rb->data, 0, rb->max * rb->itemsize);
}

void RB_print(rb_t* rb)
{
    uint8_t* current = rb->tail;
    uint8_t* last    = rb->data + ((rb->max - 1) * rb->itemsize);
    uint8_t i = 0;

    DEBUG_PRINTF(DBG_LVL_VERBOSE, "Printing ring buffer %p:", rb);

    if (rb->tail > rb->head) /* Ring buffer wrap around */
    {
        while (current <= last)
        {
            i = (i + 1) % rb->itemsize;

            DEBUG_PRINTF(DBG_LVL_VERBOSE, "  %p: %#04x", current, *current);
            current++;
        }

        current = rb->data;
    }

    while (current <= rb->head)
    {
        i = (i + 1) % rb->itemsize;

        DEBUG_PRINTF(DBG_LVL_VERBOSE, "  %p: %#04x", current, *current);
        current++;
    }
}
