/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
InclinoStat_t           Inclino = { 0 };

#if USE_INCLINO

/* Variables -----------------------------------------------------------------*/
static dpp_inclino_t    dpp_inclino;

/* External variables --------------------------------------------------------*/
extern InclinoStat_t    Inclino;
extern rb_t             rb_inclinodata;

/*** INCLINOMETER TASK ********************************************************/
void vTaskInclino(void* arg)
{
    (void)arg;

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Inclino task started");

    while (true)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
        Inclino.Status = 1;

        uint32_t start_ts = TIM5_getCounterValue();

        /* Turn Inclino on */
        SCL_SWON();
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Inclinometer is running");

        /* Wake Inclino up */
        SCL_up();

        /* Start-up sequence */
        SCL_init();

        /* Verify correct status of device: only measure and log data, when RS bits are valid */
        if (!SCL_verifyResponse())
        {
            /* Inclinometer Sampling */
            /* Acceleration & Angle */
            SCL_sendCommand(SCL3300_READ_ACC_X);
            /* No valid data yet */

            SCL_sendCommand(SCL3300_READ_ACC_Y);
            dpp_inclino.acc_x = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ACC_Z);
            dpp_inclino.acc_y = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ANG_X);
            dpp_inclino.acc_z = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ANG_Y);
            dpp_inclino.ang_x = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ANG_Z);
            dpp_inclino.ang_y = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_TEMP);
            dpp_inclino.ang_z = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_STATUS);
            dpp_inclino.temperature = SCL_getResponse();

            DEBUG_PRINTF(DBG_LVL_VERBOSE, "Incl data - ACC X: %04d, Y: %04d, Z: %04d; ANG X: %06d, Y: %06d, Z: %06d; Temp: %06d", dpp_inclino.acc_x, dpp_inclino.acc_y, dpp_inclino.acc_z, dpp_inclino.ang_x, dpp_inclino.ang_y, dpp_inclino.ang_z, dpp_inclino.temperature);

            Bolt_enqueueMessage(DPP_MSG_TYPE_INCLINO, (uint8_t*)&dpp_inclino);

            /* Log Inclino data */
            RB_insert(&rb_inclinodata, (uint8_t*)&dpp_inclino);
            RTOS_queueSendLog(LOGTYPE_INCLINO);
        }

        Inclino.Status = 0; /* Inclino Task is finished */

        /* Set Inclino to power down mode */
        SCL_down();

        /* Turn Inclino off again */
        SCL_SWOFF();

        /* Check task runtime */
        uint32_t time_delta_ms = TIM5_TICKS_TO_MS(TIM5_getCounterValue() - start_ts);
        if (time_delta_ms > RTOS_TASK_RUNTIME_TH_MS)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "Task runtime longer than expected (%4lums)", time_delta_ms);
            RTOS_event(EVENT_GEOPHONE3X_TASK_RUNTIME, (time_delta_ms << 16) | RTOS_TASK_ID_INCLINO, EVTLOG_SD | EVTLOG_COM);
        }
    } /* Inclinometer task loop */
}

#endif /* USE_INCLINO */
