/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
SysConf_t   SysConf = { 0 };

/* Ring Buffers --------------------------------------------------------------*/
rb_t    rb_adc1;
uint8_t rb_adc1_data[RB_ADC_MAX_ITEMS * sizeof(ADCData_t)];
#if GEO_NUM_AXES > 1
rb_t    rb_adc2;
uint8_t rb_adc2_data[RB_ADC_MAX_ITEMS * sizeof(ADCData_t)];
#endif
#if GEO_NUM_AXES > 2
rb_t    rb_adc3;
uint8_t rb_adc3_data[RB_ADC_MAX_ITEMS * sizeof(ADCData_t)];
#endif
rb_t*   adc_rb[] = { 0,         /* start at index ADC_A1 */
                     &rb_adc1,
#if GEO_NUM_AXES > 1
                     &rb_adc2,
#endif
#if GEO_NUM_AXES > 2
                     &rb_adc3
#endif
};

rb_t    rb_geo;
uint8_t rb_geo_data[RB_GEO_MAX_ITEMS * sizeof(Geophone_t)];

rb_t    rb_bolt;
uint8_t rb_bolt_data[RB_BOLT_MAX_ITEMS * DPP_MSG_PKT_LEN];

rb_t    rb_cmd;
uint8_t rb_cmd_data[RB_CMD_MAX_ITEMS * RB_CMD_MAX_ITEM_LEN];

rb_t    rb_event;
uint8_t rb_event_data[RB_EVENT_MAX_ITEMS * sizeof(dpp_event_t)];

rb_t    rb_health;
uint8_t rb_health_data[RB_HEALTH_MAX_ITEMS * sizeof(dpp_app_health_t)];

rb_t    rb_imustat;
uint8_t rb_imustat_data[RB_IMUSTAT_MAX_ITEMS * sizeof(ImuStat_t)];

rb_t    rb_imudata;
uint8_t rb_imudata_data[RB_IMUDATA_MAX_ITEMS * sizeof(ImuData_t)];

rb_t    rb_schedule;
uint8_t rb_schedule_data[RB_SCHEDULE_MAX_ITEMS * sizeof(ScheduleEntry_t)];

rb_t    rb_inclinodata;
uint8_t rb_inclinodata_data[RB_INCLINODATA_MAX_ITEMS * sizeof(dpp_inclino_t)];

rb_t    rb_gnssdata;
uint8_t rb_gnssdata_data[RB_GNSSDATA_MAX_ITEMS * sizeof(gnss_ubx_msg_t)];

rb_t    rb_comdata;
uint8_t rb_comdata_data[RB_COMDATA_MAX_ITEMS * sizeof(dpp_message_t)];

rb_t    rb_tsync;
uint8_t rb_tsync_data[RB_TSYNC_MAX_ITEMS * sizeof(TSyncInfo_t)];

#if !STREAMING_TARGET_NONE && !STREAMING_BACKLOG_MODE
rb_t    rb_stream;
uint8_t rb_stream_data[RB_STREAM_MAX_ITEMS * sizeof(StreamingData_t)];
#endif

/* External variables --------------------------------------------------------*/
extern Geophone_t     Geo;
extern GNSSStat_t     GNSS;
extern InclinoStat_t  Inclino;
extern SysHealth_t    SysHealth;
extern ImuStat_t      Imu;
extern USB_t          USB;


/* MAIN ----------------------------------------------------------------------*/
int main(void)
{
    /* Basic HW and clock config */
    HAL_Init();
    SystemClock_config();

    /* Init peripherals */
    GPIO_init();
    RTC_init();
    SPI1_init();
    SPI2_init();
    TIM3_init();
    TIM4_init();
    TIM5_init();
    ADC1_init();
#if USE_LPUART1
    LPUART1_init();
#endif
#if USE_USB_CDC
    USB_DEVICE_Init(CDC);
    HAL_Delay(1000);
#endif
#if WATCHDOG_TIMEOUT
    WDT_init();
#endif

    /* Load / init application parameters */
    Application_init();
    Deployment_init();

    /* Self-Test*/
    if (!SelfTest_quick())
    {
        ERROR_INDICATION();
    }
    /* If button is pressed for more than 2 seconds: Enter HW test mode */
    if (Button_pressedFor(2))
    {
        SelfTest_full();
    }
    LED_ALL_OFF();
    LED_ALL_BLINK();

    /* SEGGER SystemView */
#if USE_SYSTEMVIEW
    SEGGER_SYSVIEW_Conf();
#endif

    /* FreeRTOS tasks, queues, etc. */
    RTOS_init();

    /* Enable interrupts */
    __set_BASEPRI(0);

    /* RTOS Kernel Start (blocking call) */
    vTaskStartScheduler();

    Error_Handler();
}

/* Deployment Init -----------------------------------------------------------*/
void Deployment_init(void)
{
    /* Set and check deployment-specific configuration */

}

/* Application Init ----------------------------------------------------------*/
void Application_init(void)
{
    const char* rst_cause = System_getResetCause(&SysHealth.ResetFlags);

    /* Init RBs */
    RB_init(&rb_adc1,       RB_ADC_MAX_ITEMS,         sizeof(ADCData_t),          rb_adc1_data);        /* RB ID 1 */
#if GEO_NUM_AXES > 1
    RB_init(&rb_adc2,       RB_ADC_MAX_ITEMS,         sizeof(ADCData_t),          rb_adc2_data);        /* RB ID 2 */
#endif
#if GEO_NUM_AXES > 2
    RB_init(&rb_adc3,       RB_ADC_MAX_ITEMS,         sizeof(ADCData_t),          rb_adc3_data);        /* RB ID 3 */
#endif
    RB_init(&rb_geo,        RB_GEO_MAX_ITEMS,         sizeof(Geophone_t),         rb_geo_data);         /* RB ID 4 */
    RB_init(&rb_bolt,       RB_BOLT_MAX_ITEMS,        DPP_MSG_PKT_LEN,            rb_bolt_data);        /* RB ID 5 */
    RB_init(&rb_cmd,        RB_CMD_MAX_ITEMS,         RB_CMD_MAX_ITEM_LEN,        rb_cmd_data);         /* RB ID 6 */
    RB_init(&rb_event,      RB_EVENT_MAX_ITEMS,       sizeof(dpp_event_t),        rb_event_data);       /* RB ID 7 */
    RB_init(&rb_health,     RB_HEALTH_MAX_ITEMS,      sizeof(dpp_app_health_t),   rb_health_data);      /* RB ID 8 */
    RB_init(&rb_imustat,    RB_IMUSTAT_MAX_ITEMS,     sizeof(ImuStat_t),          rb_imustat_data);     /* RB ID 9 */
    RB_init(&rb_imudata,    RB_IMUDATA_MAX_ITEMS,     sizeof(ImuData_t),          rb_imudata_data);     /* RB ID 10 */
    RB_init(&rb_inclinodata,RB_INCLINODATA_MAX_ITEMS, sizeof(dpp_inclino_t),      rb_inclinodata_data); /* RB ID 11 */
    RB_init(&rb_gnssdata,   RB_GNSSDATA_MAX_ITEMS,    sizeof(gnss_ubx_msg_t),     rb_gnssdata_data);    /* RB ID 12 */
    RB_init(&rb_schedule,   RB_SCHEDULE_MAX_ITEMS,    sizeof(ScheduleEntry_t),    rb_schedule_data);    /* RB ID 13 */
    RB_init(&rb_comdata,    RB_COMDATA_MAX_ITEMS,     sizeof(dpp_message_t),      rb_comdata_data);     /* RB ID 14 */
    RB_init(&rb_tsync,      RB_TSYNC_MAX_ITEMS,       sizeof(TSyncInfo_t),        rb_tsync_data);       /* RB ID 15 */
#if !STREAMING_TARGET_NONE && !STREAMING_BACKLOG_MODE
    RB_init(&rb_stream,     RB_STREAM_MAX_ITEMS,      sizeof(StreamingData_t),    rb_stream_data);      /* RB ID 16 */
#endif

    /* Print firmware and device info */
    DEBUG_PRINTF(DBG_LVL_INFO, "Firmware:    %s v%u.%u.%u (git rev %08x)", INFO_FW_NAME, INFO_FW_VER_MAJOR, INFO_FW_VER_MINOR, INFO_FW_VER_PATCH, INFO_FW_REV);
    DEBUG_PRINTF(DBG_LVL_INFO, "Compiled on: %s %s (%s)", __DATE__, __TIME__, __VERSION__);
    DEBUG_PRINTF(DBG_LVL_INFO, "MCU ID:      %08X %08X %08X", *(uint32_t*)MCU_UUID_ADDR, *((uint32_t*)MCU_UUID_ADDR + 1), *((uint32_t*)MCU_UUID_ADDR + 2));
    DEBUG_PRINTF(DBG_LVL_INFO, "RTC Date:    %s", RTC_getFormattedString());
    DEBUG_PRINTF(DBG_LVL_INFO, "Reset Flags: 0x%x (%s)", SysHealth.ResetFlags, rst_cause);

    /* Initialize SD card */
    SD_init();

    /* Set default config before loading the settings from the SD card */
    Application_setConfigDefault();

    /* Load config from the SD card */
#if WRITE_DEFAULT_CONFIG
    SD_writeConfig();
    SD_clearSchedule();
    DEBUG_PRINT(DBG_LVL_INFO, "Config cleared");
#elif USE_CONFIG_FROM_SD
    if (!SD_readConfig())
    {
        /* Set application defaults according to the values in the firmware */
        Application_setConfigDefault();
        SD_writeConfig();
        LED_R_BLINK();
        DEBUG_PRINT(DBG_LVL_ERROR, "Failed to load config from SD card");
    }
    else
    {
        SysHealth.ConfigFromSD = true;
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Config loaded from SD card");
    }
    if (!SD_readSchedule())
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "Failed to read schedule from SD card");
    }
#endif

    /* Check OpMode */
    if ((SysConf.OpMode & SYS_OPMODE_CONT) && (SysConf.OpMode & SYS_OPMODE_TRG))
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "Invalid OpMode, cannot combine triggered and continuous");
        SysConf.OpMode &= ~SYS_OPMODE_CONT;   /* Clear continuous bit, use triggered mode only */
    }

    /* Print config */
    DEBUG_PRINTF(DBG_LVL_INFO, "Device ID: %5u, OpMode: 0b%lu", SysConf.DeviceID, BINARY(SysConf.OpMode));

    /* Read reset counter, increment value and save */
    SysHealth.ResetCnt = SD_readResetCounter() + 1;
    SD_writeResetCounter(SysHealth.ResetCnt);
    DEBUG_PRINTF(DBG_LVL_VERBOSE, "Reset counter: %lu", SysHealth.ResetCnt);

    /* Get the current acquisition ID, increment by one and save back to file (to prevent overwrite of potentially existing file) */
    Geo.ID = SD_readAcqID() + 1;
    SD_writeAcqID(Geo.ID);
    DEBUG_PRINTF(DBG_LVL_VERBOSE, "Geo acq ID set to %u", Geo.ID);
}

/* Application Reset ---------------------------------------------------------*/
void Application_reset(void)
{
    DEBUG_PRINT(DBG_LVL_WARNING, "APPLICATION RESET");
    Delay(100);

    /* Suspend scheduler */
    RTOS_SUSPEND();

    /* Reset USB interface */
    if (SysConf.USB->deviceClass != NONE) { USB_DEVICE_DeInit(); }

    /* Mask interrupts */
    __set_BASEPRI( (TICK_INT_PRIORITY + 1) << (8 - __NVIC_PRIO_BITS) );
    __DSB();
    __ISB();

    /* De-init hardware & System reset */
    HAL_RCC_DeInit();
    HAL_DeInit();
    SysTick_reset();
    NVIC_SystemReset();
}

/* Config: Set to Default ----------------------------------------------------*/
void Application_setConfigDefault(void)
{
    /* Init IMU parameters */
    Imu.Start            = 0;
    Imu.ID               = 0;
    Imu.Samples_PreTrg   = 0;
    Imu.Samples_PostTrg  = 0;
    Imu.Freq_LP          = IMU_FREQ;
    Imu.Freq_HP          = IMU_FREQ;
    Imu.Freq_AA          = IMU_FREQ_AA;
    Imu.OpMode           = IMU_SINGLE_SHOT;
    Imu.Level_Trg        = IMU_TRG_LEVEL;
    Imu.Decimation       = IMU_DATA_DEC;
    Imu.Status           = 0;

    /* Init system config */
    SysConf.DeviceID     = DPP_DEVICE_ID;
    SysConf.ComBoard     = false;
    SysConf.OpMode       = SYSTEM_OPMODE;
    SysConf.SysHealth    = &SysHealth;
    SysConf.USB          = &USB;
    SysConf.Geo          = &Geo;
    SysConf.GNSS         = &GNSS;
    SysConf.Inclino      = &Inclino;
    SysConf.Imu          = &Imu;

    /* Init geophone & ADC parameters */
    memset(&Geo, 0, sizeof(Geo));
    Geo.PeakNegVal       = UINT32_MAX;
    Geo.TrgThPos         = GEO_TRG_POS_TH;
    Geo.TrgThNeg         = GEO_TRG_NEG_TH;
    Geo.TrgGain          = TRG_GAIN_STAGE1;
    Geo.PostTrg          = GEO_POSTTRG_S;
    Geo.Timeout          = GEO_TIMEOUT_S;
    Geo.PGA              = ADC_PGA;
    Geo.SPS              = ADC_SPS;
    Geo.Format           = ADC_FORMAT;

    /* GNSS config */
    GNSS.leap_s          = GNSS_DEFAULT_LEAP_S;
}

/* General error handler for critical (non-recoverable) failures, triggers a system reset */
void Application_errorHandler(char* file, int line)
{
    WDT_init();         /* Make sure the watchdog is enabled (in case we get stuck in the error handler) */
    RTOS_SUSPEND();
    DEBUG_PRINTF(DBG_LVL_ERROR, "Error_Handler called from %s:%d", file, line);
    LED_ALL_OFF();
    LED_R_ON();
    /* Try to write the event to Bolt (value: first two letters of the file name and the line number) */
    Bolt_sendEvent(EVENT_GEOPHONE3X_CRIT_ERROR, ((uint32_t)line) << 16 | ((uint16_t)file[1]) << 8 | file[0], true);
#if !STREAMING_TARGET_SWO
    Application_reset();    /* a software reset seems to disconnect the SWO logger */
#else
    while (true);           /* wait until the watchdog triggers a reset */
#endif
}

/* Print debug messages depending on the DEBUG_OUTPUT_LEVEL which can be changed in the config.h file */
void DebugPrint(const char* filename, uint16_t lineno, DebugLevel_t level, const char* msg)
{
    static char snprintf_buf[DEBUG_PRINTF_BUFFER_SIZE];
#if !DEBUG_PRINT_BASEBOARD
    const char* debug_level_str[NUM_DBG_LVL] = { "ERROR  ", "WARNING", "INFO   ", "DEBUG  " };
#else
    const char* debug_level_id[NUM_DBG_LVL]  = { "<3>", "<4>", "<6>", "<7>" };     /* Debug level identifiers for journalctl on baseboard */
#endif

    if (level >= NUM_DBG_LVL) return;

    uint64_t uptime = RTC_getUptimeMS();

#if !DEBUG_PRINT_BASEBOARD
    uint32_t len = snprintf(snprintf_buf, DEBUG_PRINTF_BUFFER_SIZE, "[%9llu] %s %-16s %4d: %s\r\n", uptime, debug_level_str[level], filename, lineno, msg);
#else
    uint32_t len = snprintf(snprintf_buf, DEBUG_PRINTF_BUFFER_SIZE, "%s[%9llu] %-16s %4d: %s\n", debug_level_id[level], uptime, filename, lineno, msg);
#endif

#if DBG_PRINT_TARGET_UART
    /* Write to LPUART (PC0/PC1) */
    LPUART1_transmit((uint8_t*)snprintf_buf, len);

#elif DBG_PRINT_TARGET_USB
    /* Write to USB virtual serial device */
    uint32_t max_delay = USB_CDC_TIMEOUT_MS;
    while ((CDC_Transmit_Buffered((uint8_t*)snprintf_buf, len) == USBD_BUSY) && max_delay)
    {
        HAL_Delay(1);
        max_delay--;
    }

#elif DBG_PRINT_TARGET_SWO
    /* Write to SWO */
    SWO_SEND(snprintf_buf, len);     /* or use printf() */

#elif DBG_PRINT_TARGET_STREAM
    static uint8_t  debug_output_buffer[STREAMING_DATA_MAX_LEN];
    static uint32_t debug_output_ofs = 0;

    uint32_t space_available = STREAMING_DATA_MAX_LEN - debug_output_ofs;
    if (space_available <= len)
    {
        StreamingData_t* pkt = (StreamingData_t*)RB_insert(&rb_stream, 0);
        pkt->type            = STREAMING_DTYPE_DEBUG;
        pkt->len             = STREAMING_DATA_MAX_LEN;
        pkt->arg             = 0;
        memcpy(pkt->data,                    debug_output_buffer, debug_output_ofs);
        memcpy(pkt->data + debug_output_ofs, snprintf_buf,        space_available);
        NOTIFY_STREAM_TASK();
        debug_output_ofs = len - space_available;
        memcpy(debug_output_buffer, &snprintf_buf[space_available], debug_output_ofs);
    }
    else
    {
        memcpy(debug_output_buffer + debug_output_ofs, snprintf_buf, len);
        debug_output_ofs += len;
    }
#endif
}

void DebugPrintf(const char* filename, uint16_t lineno, DebugLevel_t level, const char* msg, ...)
{
    static char debug_printf_buf[DEBUG_PRINTF_BUFFER_SIZE];

    va_list lst;
    va_start(lst, msg);
    vsnprintf(debug_printf_buf, DEBUG_PRINTF_BUFFER_SIZE, msg, lst);
    va_end(lst);
    DebugPrint(filename, lineno, level, debug_printf_buf);
}

void Delay(volatile uint32_t microseconds)
{
    /* Note: Works only when CPU is at 80MHz and needs to be adjusted if the CPU speed is changed! */
    while (microseconds)
    {
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        asm("NOP");
        microseconds--;
    }
}

#ifdef USE_FULL_ASSERT
/* Assertion function for HAL */
void assert_failed(uint8_t* file, uint32_t line)
{

}
#endif /* USE_FULL_ASSERT */

/* required to redirect stdout (printf) to SWO */
int _write(int32_t file, uint8_t *ptr, int32_t len)
{
    (void)file;

    for(int32_t i = 0; i < len; i++)
    {
        ITM_SendChar(ptr[i]);
    }
    return len;
}
