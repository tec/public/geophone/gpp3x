/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Defines -------------------------------------------------------------------*/
#define SELFTEST_NUM_ADC_SAMPLES    50       /* number of ADC samples to read */
#define SELFTEST_ADC_RANGE_TH       1000     /* if ADC sample range (diff between max and min sample) is smaller, the selftest will fail */

/* External variables --------------------------------------------------------*/
extern GNSSStat_t   GNSS;
extern SysHealth_t  SysHealth;

/* Variables -----------------------------------------------------------------*/
static uint8_t      test_buf[MAX(SELFTEST_NUM_ADC_SAMPLES * 4, GNSS_MSG_LEN_MAX)] = { 0 };

/* Self-Test -----------------------------------------------------------------*/
bool SelfTest_quick(void)
{
    /* NOTE: Self-Test should only be performed when there is no other task running, e.g. when stop2 enter criteria are fulfilled. */

#define SD_ERROR_BIT      (1 << 0)    /* 0x01: SD error */
#define BATT_ERROR_BIT    (1 << 1)    /* 0x02: Battery error */
#define ADC_ERROR_BIT     (1 << 2)    /* 0x04: ADC error */
#define SHT_ERROR_BIT     (1 << 3)    /* 0x08: SHT31 error */
#define GNSS_ERROR_BIT    (1 << 4)    /* 0x10: GNSS error */
#define LSM_ERROR_BIT     (1 << 5)    /* 0x20: LSM303C error */
#define SCL_ERROR_BIT     (1 << 6)    /* 0x40: SCL3300 error */
#define COM_ERROR_BIT     (1 << 7)    /* 0x80: Com Board not connected */

    uint8_t res = 0;

    /* Disable EXTI ITs that might cause interference */
    HAL_NVIC_DisableIRQ(EXTI1_IRQn);
    HAL_NVIC_DisableIRQ(EXTI2_IRQn);
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);

    /* Power On Components */
    ADC_SWON();
    ADC_CLK_SWON();
    AVDD_SWON();
    IMU_SWON();
#if USE_INCLINO
    SCL_SWON();
#endif
    Delay(ADC_POWER_ON_DELAY_US);

    /* Suspend Tasks */
    if (IS_RTOS_STARTED()) { RTOS_SUSPEND(); }

    /* SD test */
    if (!SD_mount())
    {
        res |= SD_ERROR_BIT;
    }
    if (SD_getFillLevel() >= SD_FILL_LEVEL_WARN_TH)
    {
        res |= SD_ERROR_BIT;
    }

    /* Battery test: only if no external supply */
    if (!USB_IS_CONNECTED())
    {
        uint32_t u32 = ADC1_getBatteryVoltage();
        if ((u32 < HEALTH_VBAT_LOW_WARN_TH) || (u32 > HEALTH_VBAT_HIGH_WARN_TH))
        {
            res |= BATT_ERROR_BIT;
        }
    }

    /* ADC test */
#ifdef GPP3X
    if (!ADC_test(ADC_A1) || !ADC_test(ADC_A2) || !ADC_test(ADC_A3))
    {
        res |= ADC_ERROR_BIT;
    }
#else
    if (!ADC_test(ADC_A1))
    {
        res |= ADC_ERROR_BIT;
    }
#endif

    /* read a few samples from each ADC */
    for (ADCID_t adc = ADC_A1; adc < ADC_ALL; adc++)
    {
        memset(test_buf, 0, sizeof(test_buf));
        if (!ADC_readSamples(adc, 125, SELFTEST_NUM_ADC_SAMPLES, (int32_t*)test_buf))
        {
            res |= ADC_ERROR_BIT;
            break;
        }
        else
        {
            /* check the data */
            int32_t  min_val = INT32_MAX;
            int32_t  max_val = INT32_MIN;
            int32_t* samples = (int32_t*)test_buf;
            for (uint32_t i = 0; i < SELFTEST_NUM_ADC_SAMPLES; i++)
            {
                if (samples[i] > max_val) { max_val = samples[i]; }
                if (samples[i] < min_val) { min_val = samples[i]; }
            }
            if ((max_val - min_val) < SELFTEST_ADC_RANGE_TH)
            {
                DEBUG_PRINTF(DBG_LVL_WARNING, "Selftest: sample range for ADC %u is %lu - %lu (%lu)", adc, min_val, max_val, max_val - min_val);
                res |= ADC_ERROR_BIT;
                break;
            }
            //DEBUG_PRINTF(DBG_LVL_VERBOSE, "Selftest: sample range for ADC %u is %lu - %lu (%lu)", adc, min_val, max_val, max_val - min_val);
        }
    }

    /* SHT31 test */
    SHT31_init();
    int16_t  temp;
    uint16_t humi;
    if ( !SHT31_readTempHumidity(&temp, &humi)   ||
         temp < (HEALTH_TEMP_LOW_WARN_TH  * 100) ||
         temp > (HEALTH_TEMP_HIGH_WARN_TH * 100) ||
         humi > (HEALTH_HUMIDITY_WARN_TH  * 100)   )
    {
        res |= SHT_ERROR_BIT;
    }

#if USE_GNSS
    /* GPS Test */
    memset(test_buf, 0, sizeof(test_buf));
    GNSS_powerOn();
    const char* mod_ver = GNSS_requestModVer();
    GNSS_powerOff();
    /* Compare "MOD" (model) information */
    if (!strstr(mod_ver, GNSS_VERSION_STRING))
    {
        res |= GNSS_ERROR_BIT;
    }
    //DEBUG_PRINTF(DBG_LVL_VERBOSE, "GNSS receiver model: %s", mod_ver);
#endif /* USE_GNSS */

    /* IMU test */
    IMU_I2C_INIT();
    memset(test_buf, 0, sizeof(test_buf));
    test_buf[0] = LSM303_ACC_WHO_AM_I;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR,  &test_buf[0], 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR,  &test_buf[1], 1, 10);
    test_buf[0] = LSM303_MAG_WHO_AM_I;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, &test_buf[0], 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_MAG_ADDR, &test_buf[2], 1, 10);
    if ((test_buf[1] != LSM303_ACC_WHO_AM_I_VAL) || (test_buf[2] != LSM303_MAG_WHO_AM_I_VAL))
    {
        res |= LSM_ERROR_BIT;
    }

#if USE_INCLINO
    /* SCL3300 test */
    memset(test_buf, 0, sizeof(test_buf));
    SCL_init();
    SCL_sendCommand(SCL3300_READ_WHOAMI);
    HAL_Delay(1);
    SCL_sendCommand(SCL3300_READ_STATUS);
    /* Response now contains data from previous request (off-frame protocol) */
    if ( SCL_verifyResponse() || (SCL_getResponse() != SCL3300_WHOAMI) )
    {
        res |= SCL_ERROR_BIT;

        /* An error occurred - try to read status from Inclinometer (requested with prev. command) */
        SCL_sendCommand(SCL3300_READ_STATUS);
        SCL_verifyStatus(0);
    }

    /* Set Inclino to power down mode and turn off */
    SCL_down();
    SCL_SWOFF();
#endif

    /* Check COM Board */
    if (!Bolt_checkComBoard())
    {
        res |= COM_ERROR_BIT;
    }

    /* Resume Tasks */
    if (IS_RTOS_STARTED()) { RTOS_RESUME(); }

    /* Enable EXTI ITs */
    HAL_NVIC_EnableIRQ(EXTI1_IRQn);
    HAL_NVIC_EnableIRQ(EXTI2_IRQn);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

    /* Print result */
    bool failed = (res & ~COM_ERROR_BIT);   /* a missing COM Board is not a failure */
    DEBUG_PRINTF(failed ? DBG_LVL_ERROR : DBG_LVL_INFO, "Selftest: %s (0x%x)", failed ? "Failed" : "Passed", res);

    SysHealth.SelfTestResult = res;

    return !failed;
}

uint32_t CheckForButtonPress(uint32_t max_wait_time)
{
    while (max_wait_time >= 10)
    {
        if (BTN_IS_PRESSED())
        {
            break;
        }
        HAL_Delay(10);
        max_wait_time -= 10;
    }
    if (BTN_IS_PRESSED())
    {
        /* Wait until button released */
        while (BTN_IS_PRESSED());
        HAL_Delay(10);
        return 0;
    }
    return 1;
}

/* Perform full hardware test ------------------------------------------------*/
void SelfTest_full(void)
{
    /* Suspend RTOS Systick */
    CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);

    /* Init GPIOs */
    GPIO_init();
    Geo_enableTrigger();

    /* Disable GPIO interrupts */
    HAL_NVIC_DisableIRQ(EXTI0_IRQn);
    HAL_NVIC_DisableIRQ(EXTI1_IRQn);
    HAL_NVIC_DisableIRQ(EXTI2_IRQn);
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);

    /* Enable ITs */
    __set_BASEPRI(0);

    /* Init USB CDC */
    USB_DEVICE_Init(CDC);

    DEBUG_PRINT(DBG_LVL_INFO, "Started full hardware testing");

    /* Wait for user acknowledge */
    do
    {
        LED_ALL_TG();
        HAL_Delay(100);
        WDT_POLL();
    }
    while (BTN_IS_PRESSED());
    do
    {
        LED_ALL_TG();
        WDT_POLL();
    }
    while (CheckForButtonPress(100));
    LED_ALL_OFF();

    USB_printf("Full Hardware Test\r\n******************");

    /* Init Peripherals */
    RTC_init();
#ifdef GPP3X
    I2C1_init();
#endif
    I2C2_init();
    SPI1_init();
    SPI2_init();

    /* Power-On Peripherals */
    ADC_SWON();
    ADC_CLK_SWON();
    AVDD_SWON();
    IMU_SWON();
#if USE_INCLINO
    SCL_SWON();
#endif
    VSENSE_SWON();
    STAGE2_SWOFF();
    HAL_Delay(200);

    /* Battery Test ----------------------------------------------------------*/
    uint32_t u32;

    USB_printf("\r\nADC continuous test started. Press button to break...\r\n");
    do
    {
        u32 = ADC1_getBatteryVoltage();
        USB_printf(" * Battery voltage: %lu mV\r\n", u32);
        WDT_POLL();
    }
    while (CheckForButtonPress(1000));

    /* SHT31 Test ------------------------------------------------------------*/
    int32_t i32;

    USB_printf("\r\nSHT31 continuous test started. Press button to break...\r\n");
    do
    {
        memset(test_buf, 0, sizeof(test_buf));
        SHT31_init();

        repeatability_mode_t sht_mode = HIGH;
        SHT31_measurementSingleShot(sht_mode, 0, test_buf);

        /* Temperature */
        i32 = (int32_t) SHT31_TEMP_C((test_buf[0] << 8) | (test_buf[1] & 0xFF));
        USB_printf(" * Temperature: %ld.%ld degC\r\n", (i32/100), (i32%100));

        /* Humidity */
        i32 = (int32_t) SHT31_HUMIDITY((test_buf[3] << 8) | (test_buf[4] & 0xFF));
        USB_printf(" * Humidity: %ld.%ld %%\r\n", (i32/100), (i32%100));

        WDT_POLL();
    }
    while (CheckForButtonPress(1000));

    /* IMU XL & MAG Test -----------------------------------------------------*/
#if defined(LSM303_C)
    uint16_t imu_x, imu_y, imu_z;

    /* IMU XL configuration */
    test_buf[0] = LSM303_ACC_CTRL1;  /* CTRL1 */
    test_buf[1] = LSM303_ACC_CTRL1_HR | LSM303_ACC_CTRL1_ODR_10Hz | LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN | LSM303_ACC_CTRL1_ZEN;  /* High Res, 10Hz OutRate, Cont.Update, XYZ axes enable */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, test_buf, 2, 10);

    test_buf[0] = LSM303_ACC_CTRL4;  /* CTRL4 */
    test_buf[1] = LSM303_ACC_CTRL4_IF_ADD_INC;  /* Address increment enable */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, test_buf, 2, 10);

    /* IMU MAG configuration */
    test_buf[0] = LSM303_MAG_CTRL1;  /* CTRL1 */
    test_buf[1] = LSM303_MAG_CTRL1_OM_UP | LSM303_MAG_CTRL1_ODR10;  /* XY axes enable, 10Hz OutRate */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 2, 10);

    test_buf[0] = LSM303_MAG_CTRL2;  /* CTRL2 */
    test_buf[1] = LSM303_MAG_CTRL2_FS1_16g;  /* +-16gauss FS */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 2, 10);

    test_buf[0] = LSM303_MAG_CTRL3;  /* CTRL3 */
    test_buf[1] = LSM303_MAG_CTRL3_CC;  /* Cont.Update */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 2, 10);

    test_buf[0] = LSM303_MAG_CTRL4;  /* CTRL4 */
    test_buf[1] = LSM303_MAG_CTRL4_OM_UP;  /* Z axis enable */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 2, 10);

    /* IMU XL ID Test */
    test_buf[0] = LSM303_ACC_WHO_AM_I;
    test_buf[1] = 0x00;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, &test_buf[0], 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, &test_buf[1], 1, 10);
    if (test_buf[1] != LSM303_ACC_WHO_AM_I_VAL)
    {
        USB_printf("\r\nIMU Accelerometer ID test failed: ID mismatch.\r\n");
    }
    else
    {
        USB_printf("\r\nIMU Accelerometer ID test passed.\r\n");

        /* IMU XL XYZ Test */
        USB_printf("Accelerometer continuous test started. Press button to break...\r\n");
        do
        {
            test_buf[0] = LSM303_ACC_X_L;
            HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, test_buf, 1, 10);
            HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, test_buf, 6, 10);

            imu_x  = (test_buf[0] & 0x00FF);            /* XL_X_LO */
            imu_x += ((test_buf[1] << 8) & 0xFF00);     /* XL_X_HI */
            imu_y  = (test_buf[2] & 0x00FF);            /* XL_Y_LO */
            imu_y += ((test_buf[3] << 8) & 0xFF00);     /* XL_Y_HI */
            imu_z  = (test_buf[4] & 0x00FF);            /* XL_Z_LO */
            imu_z += ((test_buf[5] << 8) & 0xFF00);     /* XL_Z_HI */

            USB_printf(" * A_X_Raw: 0x%04X\r\n * A_Y_Raw: 0x%04X\r\n * A_Z_Raw: 0x%04X\r\n", imu_x, imu_y, imu_z);

            WDT_POLL();
        }
        while (CheckForButtonPress(1000));
    }

    /* IMU MAG ID Test */
    test_buf[0] = LSM303_MAG_WHO_AM_I;
    test_buf[1] = 0x00;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, &test_buf[0], 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_MAG_ADDR, &test_buf[1], 1, 10);
    if (test_buf[1] != LSM303_MAG_WHO_AM_I_VAL)
    {
        USB_printf("\r\nIMU Magnetometer ID test failed: ID mismatch.\r\n");
    }
    else
    {
        USB_printf("\r\nIMU Magnetometer ID test passed.\r\n");

        /* IMU MAG XYZ Test */
        USB_printf("Magnetometer continuous test started. Press button to break...\r\n");
        do
        {
            test_buf[0] = LSM303_MAG_X_L | AUTO_INCREMENT_REG_ADDR;   /* Reg. Addr. auto-increment */
            HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 1, 10);
            HAL_I2C_Master_Receive(&IMU_I2C, IMU_MAG_ADDR, test_buf, 6, 10);

            imu_x  = (test_buf[0] & 0x00FF);            /* MAG_X_LO */
            imu_x += ((test_buf[1] << 8) & 0xFF00);     /* MAG_X_HI */
            imu_y  = (test_buf[2] & 0x00FF);            /* MAG_Y_LO */
            imu_y += ((test_buf[3] << 8) & 0xFF00);     /* MAG_Y_HI */
            imu_z  = (test_buf[4] & 0x00FF);            /* MAG_Z_LO */
            imu_z += ((test_buf[5] << 8) & 0xFF00);     /* MAG_Z_HI */

            USB_printf(" * M_X_Raw: 0x%04X\r\n * M_Y_Raw: 0x%04X\r\n * M_Z_Raw: 0x%04X\r\n", imu_x, imu_y, imu_z);

            WDT_POLL();
        }
        while (CheckForButtonPress(1000));
    }
#elif defined(LSM303_AGR)
    uint16_t imu_x, imu_y, imu_z;

    /* IMU XL configuration */
    test_buf[0] = LSM303_ACC_CTRL1;  /* CTRL1 */
    test_buf[1] = LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN | LSM303_ACC_CTRL1_ZEN | LSM303_ACC_CTRL1_ODR_10Hz;  /* 10Hz OutRate, XYZ axes enable */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, test_buf, 2, 10);

    test_buf[0] = LSM303_ACC_CTRL4;  /* CTRL4 */
    test_buf[1] = LSM303_ACC_CTRL4_HR | LSM303_ACC_CTRL4_FS_2g | LSM303_ACC_CTRL4_END_LITTLE | LSM303_ACC_CTRL4_BDU;  /* High-resolution, +-2g, Little endian, Block data update */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, test_buf, 2, 10);

    /* IMU MAG configuration */
    test_buf[0] = LSM303_MAG_CTRL1;  /* CTRL1 */
    test_buf[1] = LSM303_MAG_CTRL1_MD_CONT | LSM303_MAG_CTRL1_ODR10;  /* Continuous, 10Hz OutRate */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 2, 10);

    test_buf[0] = LSM303_MAG_CTRL3;  /* CTRL3 */
    test_buf[1] = LSM303_MAG_CTRL3_END_LITTLE | LSM303_MAG_CTRL3_BDU;  /* Little endian, Block data update */
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 2, 10);

    /* IMU XL ID Test */
    test_buf[0] = LSM303_ACC_WHO_AM_I;
    test_buf[1] = 0x00;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, &test_buf[0], 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, &test_buf[1], 1, 10);
    if (test_buf[1] != LSM303_ACC_WHO_AM_I_VAL)
    {
        USB_printf("\r\nIMU Accelerometer ID test failed: ID mismatch.\r\n");
    }
    else
    {
        USB_printf("\r\nIMU Accelerometer ID test passed.\r\n");

        /* IMU XL XYZ Test */
        USB_printf("Accelerometer continuous test started. Press button to break...\r\n");
        do
        {
            test_buf[0] = LSM303_ACC_X_L | AUTO_INCREMENT_REG_ADDR;   /* Reg. Addr. auto-increment */
            HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, test_buf, 1, 10);
            HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, test_buf, 6, 10);

            imu_x  = (test_buf[0] & 0x00FF);            /* XL_X_LO */
            imu_x += ((test_buf[1] << 8) & 0xFF00);     /* XL_X_HI */
            imu_y  = (test_buf[2] & 0x00FF);            /* XL_Y_LO */
            imu_y += ((test_buf[3] << 8) & 0xFF00);     /* XL_Y_HI */
            imu_z  = (test_buf[4] & 0x00FF);            /* XL_Z_LO */
            imu_z += ((test_buf[5] << 8) & 0xFF00);     /* XL_Z_HI */

            USB_printf(" * A_X_Raw: 0x%04X\r\n * A_Y_Raw: 0x%04X\r\n * A_Z_Raw: 0x%04X\r\n", imu_x, imu_y, imu_z);

            WDT_POLL();
        }
        while (CheckForButtonPress(1000));
    }

    /* IMU MAG ID Test */
    test_buf[0] = LSM303_MAG_WHO_AM_I;
    test_buf[1] = 0x00;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, &test_buf[0], 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_MAG_ADDR, &test_buf[1], 1, 10);
    if (test_buf[1] != LSM303_MAG_WHO_AM_I_VAL)
    {
        USB_printf("\r\nIMU Magnetometer ID test failed: ID mismatch.\r\n");
    }
    else
    {
        USB_printf("\r\nIMU Magnetometer ID test passed.\r\n");

        /* IMU MAG XYZ Test */
        USB_printf("Magnetometer continuous test started. Press button to break...\r\n");
        do
        {
            test_buf[0] = LSM303_MAG_X_L | AUTO_INCREMENT_REG_ADDR;   /* Reg. Addr. auto-increment */
            HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, test_buf, 1, 10);
            HAL_I2C_Master_Receive(&IMU_I2C, IMU_MAG_ADDR, test_buf, 6, 10);

            imu_x  = (test_buf[0] & 0x00FF);            /* MAG_X_LO */
            imu_x += ((test_buf[1] << 8) & 0xFF00);     /* MAG_X_HI */
            imu_y  = (test_buf[2] & 0x00FF);            /* MAG_Y_LO */
            imu_y += ((test_buf[3] << 8) & 0xFF00);     /* MAG_Y_HI */
            imu_z  = (test_buf[4] & 0x00FF);            /* MAG_Z_LO */
            imu_z += ((test_buf[5] << 8) & 0xFF00);     /* MAG_Z_HI */

            USB_printf(" * M_X_Raw: 0x%04X\r\n * M_Y_Raw: 0x%04X\r\n * M_Z_Raw: 0x%04X\r\n", imu_x, imu_y, imu_z);

            WDT_POLL();
        }
        while (CheckForButtonPress(1000));
    }
#endif /* LSM303 */

#if USE_INCLINO
    /* Inclinometer Test -----------------------------------------------------*/
    uint16_t inclino_acc_x, inclino_acc_y, inclino_acc_z, inclino_ang_x, inclino_ang_y, inclino_ang_z, inclino_temp;

    /* Inclinometer configuration */
    SCL_up();
    SCL_init();

    if (SCL_verifyResponse())
    {
        USB_printf("\r\nInclinometer enable failed: Error code received or CRC mismatch.\r\n");
    }

    /* Inclinometer ID test */
    memset(test_buf, 0, sizeof(test_buf));
    SCL_sendCommand(SCL3300_READ_WHOAMI);
    HAL_Delay(1);
    SCL_sendCommand(SCL3300_READ_STATUS);
    /* Response now contains data from previous request (off-frame protocol) */
    if (SCL_verifyResponse() || (SCL_getResponse() != SCL3300_WHOAMI) )
    {
        USB_printf("\r\nInclinometer ID test failed: ID mismatch.\r\n");
    }
    else
    {
        USB_printf("\r\nInclinometer ID test passed.\r\n");

        /* Inclinometer XYZ test */
        USB_printf("Inclinometer continuous test started. Press button to break...\r\n");
        do
        {
            SCL_sendCommand(SCL3300_READ_ACC_X);
            /* No valid data yet */

            SCL_sendCommand(SCL3300_READ_ACC_Y);
            inclino_acc_x = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ACC_Z);
            inclino_acc_y = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ANG_X);
            inclino_acc_z = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ANG_Y);
            inclino_ang_x = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_ANG_Z);
            inclino_ang_y = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_TEMP);
            inclino_ang_z = SCL_getResponse();

            SCL_sendCommand(SCL3300_READ_STATUS);
            inclino_temp = SCL_getResponse();

            USB_printf(" * I_XL_X_Raw: 0x%04X\r\n * I_XL_Y_Raw: 0x%04X\r\n * I_XL_Z_Raw: 0x%04X\r\n * I_AN_X_Raw: 0x%04X\r\n * I_AN_Y_Raw: 0x%04X\r\n * I_AN_Z_Raw: 0x%04X\r\n * I_TMP_Raw: 0x%04X\r\n",
                       inclino_acc_x, inclino_acc_y, inclino_acc_z, inclino_ang_x, inclino_ang_y, inclino_ang_z, inclino_temp);

            HAL_Delay(25);

            WDT_POLL();
        }
        while (CheckForButtonPress(1000));
    }

    /* Set Inclino to power down mode */
    SCL_down();
#endif /* USE_INCLINO */

#if USE_GNSS
    /* GNSS Test -------------------------------------------------------------*/

    /* GNSS ID test */
    memset(test_buf, 0, sizeof(test_buf));
    GNSS_powerOn();
    const char* mod_ver = GNSS_requestModVer();
    GNSS_powerOff();
    if (!strstr(mod_ver, GNSS_VERSION_STRING))
    {
        USB_printf("\r\nGNSS ID test failed: ID mismatch.\r\n");
    }
    else
    {
        USB_printf("\r\nGNSS ID test passed.\r\n");

        /* Start-up GNSS functionality */
        GNSS_enable();

        /* GNSS RAWX testing */
        USB_printf("GNSS continuous test started. Press button to break...\r\n");
        do
        {
            /* Check whether we received a packet over UART */
            HAL_Delay(100);

            /* Verify validity */
            if (GNSS.rx_buf == NULL || GNSS.rx_len < GNSS_MSG_LEN_MIN) { continue; }

            gnss_ubx_msg_t* msg = (gnss_ubx_msg_t*)GNSS.rx_buf;

            /* Jump if we didnt receive a packet */
            if ( (msg->class == 0) || (msg->id == 0) || (msg->len == 0) ) { continue; }

            /* Check for RXM-RAWX */
            if ( (msg->class == GNSS_MSG_CLASS_RXM) && (msg->id == GNSS_MSG_ID_RXM_RAWX) )
            {
                /* Get payload size from msg (in case there is another message in buf right after the RAWX) */

                /* Check CRC */
                if (GNSS_checkCRC((gnss_ubx_msg_t*)GNSS.rx_buf, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + msg->len) &&
                    GNSS.rx_buf[19] == GNSS_VERSION_NUMBER)
                {
                    USB_printf(" * GNSS week: 0x%04X\r\n * GNSS leapS: 0x%02X\r\n * GNSS numMeas: 0x%02X\r\n * GNSS recStat: 0x%02X\r\n",
                               msg->rxm_rawx.week, msg->rxm_rawx.leapS, msg->rxm_rawx.numMeas, msg->rxm_rawx.recStat);
                }
            }
            GNSS.rx_len = 0;

            WDT_POLL();
        }
        while (CheckForButtonPress(1000));
    }

    /* Turn GNSS off again */
    GNSS_disable();
    GNSS_powerOff();
#endif /* USE_GNSS */

    /* Trigger Test ----------------------------------------------------------*/
    bool failed = false;
    USB_printf("\r\nTrigger test in progress...\r\n");

    STAGE2_SWOFF();
    Geo_setTriggerThresholds(3000, 0);
    HAL_Delay(1000);
    if (TRG_IS_P() || TRG_IS_N()) { failed = true; }

    STAGE2_SWON();
    Geo_setTriggerThresholds(1450, 1550);
    HAL_Delay(1000);
    if (!TRG_IS_P() && !TRG_IS_N()) { failed = true; }

    STAGE2_SWOFF();
    if (!failed) { USB_printf("Trigger test passed.\r\n"); }
    else         { USB_printf("Trigger test failed.\r\n"); }
    Geo_setTriggerThresholds(3000, 0);

    /* ADC Test ----------------------------------------------------------*/
    USB_printf("\r\nADC continuous test started. Press button to break...\r\n");

    bool adc_ok = true;
    if (!ADC_test(ADC_A1))
    {
        USB_printf("ADC1 status error.\r\n");
        adc_ok    = false;
    }
#ifdef GPP3X
    if (!ADC_test(ADC_A2))
    {
        USB_printf("ADC2 status error.\r\n");
        adc_ok    = false;
    }
    if (!ADC_test(ADC_A3))
    {
        USB_printf("ADC3 status error.\r\n");
        adc_ok    = false;
    }
#endif
    if (adc_ok)
    {
        /* ADC Config CTRL1 */
        ADC_writeRegister(ADC_ALL, ADC_REG_ADDR(ADC_REG_CTRL1, ADC_REG_WRITE), ADC_CTRL1_PD1 | ADC_CTRL1_FORMAT);

        /* ADC Config CTRL2 */
        ADC_writeRegister(ADC_ALL, ADC_REG_ADDR(ADC_REG_CTRL2, ADC_REG_WRITE), 0x00); /* PGA off */

        /* ADC Start (@fixed 1ksps) */
        ADC_sendCmd(ADC_ALL, ADC_CONV_COMMAND(ADC_CONV_RATE_1000Hz));

        uint32_t tickstart = HAL_GetTick();
        do
        {
            if ((HAL_GetTick() - tickstart) > 1000)
            {
                USB_printf("ADC RDY error.\r\n");
                break;
            }
#ifdef GPP3X
            if (ADC1_IS_RDY() && ADC2_IS_RDY() && ADC3_IS_RDY())
            {
                memset(test_buf, 0, sizeof(test_buf));
                test_buf[0] = ADC_REG_ADDR(ADC_REG_DATA, ADC_REG_READ);
                ADC1_ASSERT();
                HAL_SPI_TransmitReceive(&ADC_SPI, test_buf, test_buf, 4, 100);
                ADC1_DEASSERT();
                uint32_t adc1_u32 = ADC_DATA(test_buf[1], test_buf[2], test_buf[3]);

                memset(test_buf, 0, sizeof(test_buf));
                test_buf[0] = ADC_REG_ADDR(ADC_REG_DATA, ADC_REG_READ);
                ADC2_ASSERT();
                HAL_SPI_TransmitReceive(&ADC_SPI, test_buf, test_buf, 4, 100);
                ADC2_DEASSERT();
                uint32_t adc2_u32 = ADC_DATA(test_buf[1], test_buf[2], test_buf[3]);

                memset(test_buf, 0, sizeof(test_buf));
                test_buf[0] = ADC_REG_ADDR(ADC_REG_DATA, ADC_REG_READ);
                ADC3_ASSERT();
                HAL_SPI_TransmitReceive(&ADC_SPI, test_buf, test_buf, 4, 100);
                ADC3_DEASSERT();
                uint32_t adc3_u32 = ADC_DATA(test_buf[1], test_buf[2], test_buf[3]);

                /* Print results only twice per second */
                if (HAL_GetTick() - tickstart > 500)
                {
                    USB_printf(" * ADC1 raw value: %lu, converted: %lumV\r\n * ADC2 raw value: %lu, converted: %lumV\r\n * ADC3 raw value: %lu, converted: %lumV\r\n",
                               adc1_u32, ADC_CONVERT(adc1_u32), adc2_u32, ADC_CONVERT(adc2_u32), adc3_u32, ADC_CONVERT(adc3_u32));
                    tickstart = HAL_GetTick();
                }
            }
#else
            if (ADC1_IS_RDY())
            {
                memset(test_buf, 0, sizeof(test_buf));
                test_buf[0] = ADC_REG_ADDR(ADC_REG_DATA, ADC_REG_READ);
                ADC1_ASSERT();
                HAL_SPI_TransmitReceive(&hspi1, test_buf, test_buf, 4, 100);
                ADC1_DEASSERT();
                uint32_t adc1_u32 = ADC_DATA(test_buf[1], test_buf[2], test_buf[3]);

                /* Print results only twice per second */
                if (HAL_GetTick() - tickstart > 500)
                {
                    USB_printf(" * ADC1 raw value: %lu, converted: %lumV\r\n", adc1_u32, ADC_CONVERT(adc1_u32));
                    tickstart = HAL_GetTick();
                }
            }
#endif
            WDT_POLL();
        }
        while (CheckForButtonPress(0));
    }

    /* ADC Stop */
    ADC_sendCmd(ADC_ALL, ADC_CONV_COMMAND(ADC_CONV_IMPD | ADC_CONV_RATE_1000Hz));

    /* SD Test ---------------------------------------------------------------*/
    if (SD_isMounted())
    {
        SD_eject();
    }

    if (SD_mount())
    {
        SD_updateSize();
        USB_printf("\r\nSD Card test passed.\r\n"
                   "* Free:  %lu kB\r\n"
                   "* Total: %lu kB\r\n", SD_getSizeFree(), SD_getSizeTotal());
    }
    else
    {
        USB_printf("\r\nSD Card test failed.\r\n");
    }


    /* RTC WKUP Test ---------------------------------------------------------*/
    USB_printf("\r\nRTC WKUP test (10 seconds)... ");

    __HAL_RTC_WAKEUPTIMER_CLEAR_FLAG(&hrtc, RTC_FLAG_WUTF);
    HAL_RTCEx_SetWakeUpTimer(&hrtc, 0x4FFF, RTC_WAKEUPCLOCK_RTCCLK_DIV16);
    HAL_Delay(50);
    if (HAL_RTCEx_PollForWakeUpTimerEvent(&hrtc, 15000) == HAL_OK)
    {
        USB_printf("Passed.\r\n");
    }
    else
    {
        USB_printf("Failed: timeout.\r\n");
    }
    HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);


    /* System Reset ----------------------------------------------------------*/
    USB_printf("\r\nHardware test finished. Press button for system reset.\r\n"
               "* * *\r\n");
    WDT_POLL();
    while (CheckForButtonPress(100));

    HAL_Delay(500);

    NVIC_SystemReset();
}


/* Perform a sensor test -----------------------------------------------------*/
#ifdef GPP3X
void SelfTest_geophone(void)
{
    const uint32_t samples_to_collect = 512;
    int32_t        adc_sample_buffer[samples_to_collect * GEO_NUM_AXES];      /* Note: large stack usage, don't call this function from an RTOS task! */

    if (IS_RTOS_STARTED() || WATCHDOG_TIMEOUT) { Error_Handler(); }     /* Function must be called before the RTOS is started and watchdog must be disabled */

    /* Disable EXTI ITs that might cause interference */
    HAL_NVIC_DisableIRQ(EXTI1_IRQn);
    HAL_NVIC_DisableIRQ(EXTI2_IRQn);
    HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
    HAL_NVIC_DisableIRQ(EXTI15_10_IRQn);

    /* Power on the analog part */
    ADC_SWON();
    ADC_CLK_SWON();
    AVDD_SWON();
#if USE_INCLINO
    SCL_SWON();
#endif
    Delay(ADC_POWER_ON_DELAY_US);

    while (true)
    {
        /* Ready to test a sensor */
        LED_R_OFF();
        LED_G_OFF();
        LED_B_ON();
        DEBUG_PRINT(DBG_LVL_INFO, "Press the user button to start the sensor test");

        /* Wait until the user button is pressed */
        while (!BTN_IS_PRESSED());
        while (BTN_IS_PRESSED());
        DEBUG_PRINT(DBG_LVL_INFO, "Test running, please wait...");
        for (uint32_t i = 0; i < 7; i++)
        {
            LED_B_TG();
            HAL_Delay(100);
        }

        /* Collect traces of two geophones ADC1 and ADC2 */
        memset(adc_sample_buffer, 0, sizeof(adc_sample_buffer));
        if (!ADC_readSamples(ADC_ALL, 125, samples_to_collect, adc_sample_buffer))
        {
            DEBUG_PRINT(DBG_LVL_INFO, "Failed to collect sensor data");
            LED_R_ON();
            HAL_Delay(3000);
        }
        else
        {
            DEBUG_PRINT(DBG_LVL_INFO, "Processing data...");

            /* Process the data, compare the traces */
            int32_t  min_val[GEO_NUM_AXES];
            int32_t  max_val[GEO_NUM_AXES];
            uint32_t min_idx[GEO_NUM_AXES];
            uint32_t max_idx[GEO_NUM_AXES];
            float    energy[GEO_NUM_AXES];
            for (uint32_t i = 0; i < GEO_NUM_AXES; i++)
            {
                min_val[i] = INT32_MAX;
                max_val[i] = INT32_MIN;
                energy[i]  = 0.0f;
            }
            for (uint32_t sample = 0; sample < samples_to_collect * GEO_NUM_AXES; sample++)
            {
                uint32_t adc_idx = sample % GEO_NUM_AXES;
                if (adc_sample_buffer[sample] < min_val[adc_idx]) { min_val[adc_idx] = adc_sample_buffer[sample]; min_idx[adc_idx] = sample / GEO_NUM_AXES; }
                if (adc_sample_buffer[sample] > max_val[adc_idx]) { max_val[adc_idx] = adc_sample_buffer[sample]; max_idx[adc_idx] = sample / GEO_NUM_AXES; }
                float val = (float)adc_sample_buffer[sample] / (float)ADC_MAX_AMPLITUDE;
                energy[adc_idx] += val * val;
            }
            uint32_t diff_min    = ABS((min_val[1] - min_val[0]) * 100 / min_val[0]);   /* difference of min. amplitude, in percent */
            uint32_t diff_max    = ABS((max_val[1] - max_val[0]) * 100 / max_val[0]);   /* difference of max. amplitude, in percent */
            uint32_t diff_energy = ABS((energy[1]  - energy[0])  * 100 / energy[0]);    /* difference in signal energy, in percent */
            /* Note: ideally there would be additional checks in the frequency domain */

            /* Check test result */
            if ( (diff_max <= 10) && (diff_min <= 10) &&                    /* no more than 10% deviation for the recorded min/max values */
                 (min_idx[0] == min_idx[1] || max_idx[0] == max_idx[1]) &&  /* min or max values of each channel should be at the same position */
                 diff_energy <= 5)                                          /* energy in the signal should be comparable (allow 5% deviation) */
            {
                DEBUG_PRINT(DBG_LVL_INFO, "Sensor is OK");
                LED_G_ON();
            }
            else
            {
                DEBUG_PRINTF(DBG_LVL_INFO, "Deviation detected");
                LED_R_ON();
            }
            DEBUG_PRINTF(DBG_LVL_INFO, "  d_min=%ld%%, d_max=%ld%%, d_en=%ld%%", diff_min, diff_max, diff_energy);
            DEBUG_PRINTF(DBG_LVL_INFO, "  ADC1: min=%ld @%lu, max=%ld @%lu, en=%lu", min_val[0], min_idx[0], max_val[0], max_idx[0], (uint32_t)(energy[0] * 1000));
            DEBUG_PRINTF(DBG_LVL_INFO, "  ADC2: min=%ld @%lu, max=%ld @%lu, en=%lu", min_val[1], min_idx[1], max_val[1], max_idx[1], (uint32_t)(energy[1] * 1000));
            HAL_Delay(3000);
        }
    }
}
#endif
