/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
ADC_HandleTypeDef           hadc1;
DMA_HandleTypeDef           hdma_spi1_tx;
DMA_HandleTypeDef           hdma_spi1_rx;
DMA_HandleTypeDef           hdma_spi2_rx;
DMA_HandleTypeDef           hdma_spi2_tx;
DMA_HandleTypeDef           hdma_usart1_rx;
DMA_HandleTypeDef           hdma_lpuart_tx;
I2C_HandleTypeDef           hi2c2;
I2C_HandleTypeDef           hi2c1;
SD_HandleTypeDef            hsd1;
SPI_HandleTypeDef           hspi1;
SPI_HandleTypeDef           hspi2;
UART_HandleTypeDef          huart1;
UART_HandleTypeDef          hlpuart1;
TIM_HandleTypeDef           htim3;
TIM_HandleTypeDef           htim4;
TIM_HandleTypeDef           htim5;
TIM_HandleTypeDef           htim17;
LPTIM_HandleTypeDef         hlptim1;
IWDG_HandleTypeDef          hiwdg;
RTC_HandleTypeDef           hrtc;

/* External variables --------------------------------------------------------*/
extern USB_t                USB;

/* Functions -----------------------------------------------------------------*/

void ADC1_init(void)
{
    ADC_MultiModeTypeDef    multimode;

    if (hadc1.State != HAL_ADC_STATE_RESET)
    {
        return;   /* Already initialized */
    }

    /* ADC Peripheral configuration */
    hadc1.Instance                   = ADC1;
    hadc1.Init.ClockPrescaler        = ADC_CLOCK_ASYNC_DIV1;
    hadc1.Init.Resolution            = ADC_RESOLUTION_12B;
    hadc1.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
    hadc1.Init.ScanConvMode          = ADC_SCAN_DISABLE;
    hadc1.Init.EOCSelection          = ADC_EOC_SINGLE_CONV;
    hadc1.Init.LowPowerAutoWait      = DISABLE;
    hadc1.Init.ContinuousConvMode    = DISABLE;
    hadc1.Init.NbrOfConversion       = 1;
    hadc1.Init.DiscontinuousConvMode = DISABLE;
    hadc1.Init.NbrOfDiscConversion   = 1;
    hadc1.Init.ExternalTrigConv      = ADC_SOFTWARE_START;
    hadc1.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
    hadc1.Init.DMAContinuousRequests = DISABLE;
    hadc1.Init.Overrun               = ADC_OVR_DATA_PRESERVED;
    hadc1.Init.OversamplingMode      = DISABLE;
    if (HAL_ADC_Init(&hadc1) != HAL_OK)
    {
        Error_Handler();
    }

    /* Multimode configuration */
    multimode.Mode = ADC_MODE_INDEPENDENT;
    if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
    {
        Error_Handler();
    }

    /* Calibration */
    if (HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED) != HAL_OK)
    {
        Error_Handler();
    }
}

void ADC1_configChannel(uint32_t channel)
{
    ADC_ChannelConfTypeDef  sConfig;

    /* Channel configuration */
    sConfig.Channel      = channel;
    sConfig.Rank         = ADC_REGULAR_RANK_1;
    sConfig.SamplingTime = ADC_SAMPLETIME_24CYCLES_5;
    sConfig.SingleDiff   = ADC_SINGLE_ENDED;
    sConfig.OffsetNumber = ADC_OFFSET_NONE;
    sConfig.Offset       = 0;
    if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
    {
        Error_Handler();
    }
}

/* Samples the battery voltage, return value is in mV (or -1 in case of an error) */
uint16_t ADC1_getBatteryVoltage(void)
{
    uint16_t v_batt = -1;

    ADC1_configChannel(VSENSE_Channel);
    if (!VSENSE_IS_SWON())
    {
        /* Turn on voltage for sensing and wait some time to charge up the capacitor */
        VSENSE_SWON();
        HAL_Delay(MCU_ADC_POWER_ON_DELAY_MS);
    }
    if (HAL_ADC_Start(&hadc1) == HAL_OK)
    {
        if (HAL_ADC_PollForConversion(&hadc1, 20) == HAL_OK)
        {
            /* Convert ADC value to voltage (VDDA = ~3V) */
            uint32_t val = (uint32_t)(HAL_ADC_GetValue(&hadc1)) * SYSTEM_VOLTAGE / MCU_ADC_MAX_VALUE;
            /* Scale according to external voltage divider (12k and 47k) and add a constant offset */
            v_batt = (uint16_t)VSENSE_CONV(val) + MCU_ADC_OFFSET;
        }
        HAL_ADC_Stop(&hadc1);
    }

    return v_batt;
}

/* Measure the system voltage (VRef+) indirectly by measuring VREFINT (see RM p.582) */
uint16_t ADC1_getSystemVoltage(void)
{
    uint16_t v_ref = -1;

    ADC1_configChannel(ADC_CHANNEL_VREFINT);
    if (HAL_ADC_Start(&hadc1) == HAL_OK)
    {
        if (HAL_ADC_PollForConversion(&hadc1, 20) == HAL_OK)
        {
            /* Convert ADC value to voltage */
            v_ref = MCU_ADC_VREF_CHARAC * MCU_ADC_VREFINT_CAL / HAL_ADC_GetValue(&hadc1);
        }
        HAL_ADC_Stop(&hadc1);
    }

    return v_ref;
}

bool Button_pressedFor(uint32_t duration_s)
{
    const uint32_t steps_per_second = 100;
    uint32_t       step_cnt         = 0;
    duration_s *= steps_per_second;
    while (BTN_IS_PRESSED() && (step_cnt < duration_s))
    {
        step_cnt++;
        HAL_Delay(1000 / steps_per_second);
    }
    return step_cnt == duration_s;
}

void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    if (hadc->Instance == ADC1)
    {
        __HAL_RCC_ADC_CLK_ENABLE();
        GPIO_InitStruct.Pin  = VSENSE_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_ANALOG_ADC_CONTROL;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        HAL_GPIO_Init(VSENSE_Port, &GPIO_InitStruct);
    }
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc)
{
    if (hadc->Instance == ADC1)
    {
        __HAL_RCC_ADC_CLK_DISABLE();
        HAL_GPIO_DeInit(VSENSE_Port, VSENSE_Pin);
    }
}

void I2C1_init(void)
{
    /* I2C peripheral configuration */
    hi2c1.Instance              = I2C1;
    hi2c1.Init.Timing           = 0x10909CEC;
    hi2c1.Init.OwnAddress1      = 0;
    hi2c1.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
    hi2c1.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
    hi2c1.Init.OwnAddress2      = 0;
    hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c1.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
    hi2c1.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c1) != HAL_OK)
    {
        Error_Handler();
    }

    /* Analog filter configuration */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        Error_Handler();
    }

    /* Digital filter configuration */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
    {
        Error_Handler();
    }
}

void I2C2_init(void)
{
    /* I2C peripheral configuration */
    hi2c2.Instance              = I2C2;
    hi2c2.Init.Timing           = 0x10909CEC;
    hi2c2.Init.OwnAddress1      = 0;
    hi2c2.Init.AddressingMode   = I2C_ADDRESSINGMODE_7BIT;
    hi2c2.Init.DualAddressMode  = I2C_DUALADDRESS_DISABLE;
    hi2c2.Init.OwnAddress2      = 0;
    hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
    hi2c2.Init.GeneralCallMode  = I2C_GENERALCALL_DISABLE;
    hi2c2.Init.NoStretchMode    = I2C_NOSTRETCH_DISABLE;
    if (HAL_I2C_Init(&hi2c2) != HAL_OK)
    {
        Error_Handler();
    }

    /* Analog filter configuration */
    if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
    {
        Error_Handler();
    }

    /* Digital filter configuration */
    if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
    {
        Error_Handler();
    }
}

void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    if (hi2c->Instance == I2C1)
    {
        /* I2C1 GPIO Config
            PB8 -> I2C1_SCL
            PB9 -> I2C1_SDA
        */
        GPIO_InitStruct.Pin       = GPIO_PIN_8 | GPIO_PIN_9;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull      = GPIO_NOPULL; /* External pullups are power-gated */
        GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF4_I2C1;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
        __HAL_RCC_I2C1_CLK_ENABLE();
    }
    else if (hi2c->Instance == I2C2)
    {
        /* I2C2 GPIO Config
            PB10 -> I2C2_SCL
            PB11 -> I2C2_SDA
        */
        GPIO_InitStruct.Pin       = GPIO_PIN_10 | GPIO_PIN_11;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_OD;
        GPIO_InitStruct.Pull      = GPIO_NOPULL; /* External pullups are power-gated */
        GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF4_I2C2;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
        __HAL_RCC_I2C2_CLK_ENABLE();
    }
}

void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c)
{
    if (hi2c->Instance == I2C1)
    {
        __HAL_RCC_I2C1_CLK_DISABLE();
        /* I2C1 GPIO Config
            PB8 -> I2C1_SCL
            PB9 -> I2C1_SDA
        */
        HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8 | GPIO_PIN_9);
    }
    else if (hi2c->Instance == I2C2)
    {
        __HAL_RCC_I2C2_CLK_DISABLE();
        /* I2C2 GPIO Config
            PB10 -> I2C2_SCL
            PB11 -> I2C2_SDA
        */
        HAL_GPIO_DeInit(GPIOB, GPIO_PIN_10 | GPIO_PIN_11);
    }
}

void HAL_RTC_MspInit(RTC_HandleTypeDef* handle_rtc)
{
    if (handle_rtc->Instance == RTC)
    {
        __HAL_RCC_RTC_ENABLE();
    }
}

void HAL_RTC_MspDeInit(RTC_HandleTypeDef* handle_rtc)
{
    if (handle_rtc->Instance == RTC)
    {
        __HAL_RCC_RTC_DISABLE();
    }
}

void SPI1_init(void)
{
    /* SPI1 peripheral configuration (used for ADCs) */
    hspi1.Instance            = SPI1;
    hspi1.Init.Mode           = SPI_MODE_MASTER;
    hspi1.Init.Direction      = SPI_DIRECTION_2LINES;
    hspi1.Init.DataSize       = SPI_DATASIZE_8BIT;
    hspi1.Init.CLKPolarity    = SPI_POLARITY_LOW;
    hspi1.Init.CLKPhase       = SPI_PHASE_1EDGE;
    hspi1.Init.NSS            = SPI_NSS_SOFT;
    hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;    /* APB2 peripheral, clocked by PCLK2 @80MHz => 5MHz SCLK */
    hspi1.Init.FirstBit       = SPI_FIRSTBIT_MSB;
    hspi1.Init.TIMode         = SPI_TIMODE_DISABLE;
    hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi1.Init.CRCPolynomial  = 7;
    hspi1.Init.CRCLength      = SPI_CRC_LENGTH_DATASIZE;
    hspi1.Init.NSSPMode       = SPI_NSS_PULSE_DISABLE;
    if (HAL_SPI_Init(&hspi1) != HAL_OK)
    {
        Error_Handler();
    }
}

void SPI2_init(void)
{
    /* SPI2 peripheral configuration (used for Bolt) */
    hspi2.Instance            = SPI2;
    hspi2.Init.Mode           = SPI_MODE_MASTER;
    hspi2.Init.Direction      = SPI_DIRECTION_2LINES;
    hspi2.Init.DataSize       = SPI_DATASIZE_8BIT;
    hspi2.Init.CLKPolarity    = SPI_POLARITY_LOW;
    hspi2.Init.CLKPhase       = SPI_PHASE_1EDGE;
    hspi2.Init.NSS            = SPI_NSS_SOFT;
    hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32;    /* APB1 peripheral, clocked by PCKL1 @80MHz => 2.5MHz SCLK */
    hspi2.Init.FirstBit       = SPI_FIRSTBIT_MSB;
    hspi2.Init.TIMode         = SPI_TIMODE_DISABLE;
    hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    hspi2.Init.CRCPolynomial  = 7;
    hspi2.Init.CRCLength      = SPI_CRC_LENGTH_DATASIZE;
    hspi2.Init.NSSPMode       = SPI_NSS_PULSE_DISABLE;
    if (HAL_SPI_Init(&hspi2) != HAL_OK)
    {
        Error_Handler();
    }
}

void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    if (hspi->Instance == SPI1)              /* SPI1: used for ADC sample transfer */
    {
        __HAL_RCC_SPI1_CLK_ENABLE();
        __HAL_RCC_DMA1_CLK_ENABLE();

        /* SPI1 GPIO Config
            PA5  -> SPI1_SCK
            PA6  -> SPI1_MISO
            PA7  -> SPI1_MOSI
        */
        GPIO_InitStruct.Pin       = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
        GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* SPI1 DMA Tx Init */
        hdma_spi1_tx.Instance       = DMA1_Channel3;
        hdma_spi1_tx.Init.Request   = DMA_REQUEST_1;
        hdma_spi1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
        hdma_spi1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_spi1_tx.Init.MemInc    = DMA_MINC_ENABLE;
        hdma_spi1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_spi1_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_spi1_tx.Init.Mode      = DMA_NORMAL;
        hdma_spi1_tx.Init.Priority  = DMA_PRIORITY_LOW;
        if (HAL_DMA_Init(&hdma_spi1_tx) != HAL_OK)
        {
            Error_Handler();
        }

        /* SPI1 DMA Rx Init */
        hdma_spi1_rx.Instance       = DMA1_Channel2;
        hdma_spi1_rx.Init.Request   = DMA_REQUEST_1;
        hdma_spi1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
        hdma_spi1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_spi1_rx.Init.MemInc    = DMA_MINC_ENABLE;
        hdma_spi1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_spi1_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_spi1_rx.Init.Mode      = DMA_NORMAL;
        hdma_spi1_rx.Init.Priority  = DMA_PRIORITY_LOW;
        if (HAL_DMA_Init(&hdma_spi1_rx) != HAL_OK)
        {
            Error_Handler();
        }

        __HAL_LINKDMA(hspi, hdmatx, hdma_spi1_tx);
        __HAL_LINKDMA(hspi, hdmarx, hdma_spi1_rx);

        /* SPI1 Interrupt Init */
        HAL_NVIC_SetPriority(SPI1_IRQn, RTOS_ISR_PRIO_ADC_SPI_DMA, 0);
        HAL_NVIC_EnableIRQ(SPI1_IRQn);

        /* DMA Tx Interrupt Init */
        HAL_NVIC_SetPriority(DMA1_Channel3_IRQn, RTOS_ISR_PRIO_ADC_SPI_DMA, 0);
        HAL_NVIC_EnableIRQ(DMA1_Channel3_IRQn);

        /* DMA Rx Interrupt Init */
        HAL_NVIC_SetPriority(DMA1_Channel2_IRQn, RTOS_ISR_PRIO_ADC_SPI_DMA, 0);
        HAL_NVIC_EnableIRQ(DMA1_Channel2_IRQn);
    }
    else if (hspi->Instance == SPI2)         /* SPI2: used for BOLT */
    {
        __HAL_RCC_SPI2_CLK_ENABLE();
        __HAL_RCC_DMA1_CLK_ENABLE();

        /* SPI2 GPIO Config
            PB13 -> SPI2_SCK
            PB14 -> SPI2_MISO
            PB15 -> SPI2_MOSI
        */
        GPIO_InitStruct.Pin       = GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_PULLDOWN;
        GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF5_SPI2;
        HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

#if SPI2_USE_DMA
        /* SPI2 DMA Tx Init */
        hdma_spi2_tx.Instance       = DMA1_Channel5;
        hdma_spi2_tx.Init.Request   = DMA_REQUEST_1;
        hdma_spi2_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
        hdma_spi2_tx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_spi2_tx.Init.MemInc    = DMA_MINC_ENABLE;
        hdma_spi2_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_spi2_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_spi2_tx.Init.Mode      = DMA_NORMAL;
        hdma_spi2_tx.Init.Priority  = DMA_PRIORITY_LOW;
        if (HAL_DMA_Init(&hdma_spi2_tx) != HAL_OK)
        {
            Error_Handler();
        }

        /* SPI2 DMA Rx Init */
        hdma_spi2_rx.Instance       = DMA1_Channel4;
        hdma_spi2_rx.Init.Request   = DMA_REQUEST_1;
        hdma_spi2_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
        hdma_spi2_rx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_spi2_rx.Init.MemInc    = DMA_MINC_ENABLE;
        hdma_spi2_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_spi2_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
        hdma_spi2_rx.Init.Mode      = DMA_NORMAL;
        hdma_spi2_rx.Init.Priority  = DMA_PRIORITY_LOW;
        if (HAL_DMA_Init(&hdma_spi2_rx) != HAL_OK)
        {
            Error_Handler();
        }

        __HAL_LINKDMA(hspi, hdmatx, hdma_spi2_tx);
        __HAL_LINKDMA(hspi, hdmarx, hdma_spi2_rx);

        /* SPI2 Interrupt Init */
        HAL_NVIC_SetPriority(SPI2_IRQn, RTOS_ISR_PRIO_BOLT_SPI_DMA, 0);
        HAL_NVIC_EnableIRQ(SPI2_IRQn);

        /* DMA Tx Interrupt Init */
        HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, RTOS_ISR_PRIO_BOLT_SPI_DMA, 0);
        HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

        /* DMA Rx Interrupt Init */
        HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, RTOS_ISR_PRIO_BOLT_SPI_DMA, 0);
        HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
#endif
    }
}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{
    if (hspi->Instance == SPI1)
    {
        __HAL_RCC_SPI1_CLK_DISABLE();

        /* SPI1 GPIO Config
            PA5  -> SPI1_SCK
            PA6  -> SPI1_MISO
            PA7  -> SPI1_MOSI
        */
        HAL_GPIO_DeInit(GPIOA, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

        /* SPI1 DMA DeInit */
        HAL_DMA_DeInit(hspi->hdmatx);
        HAL_DMA_DeInit(hspi->hdmarx);

        /* SPI1 Interrupt DeInit */
        HAL_NVIC_DisableIRQ(SPI1_IRQn);
    }
    else if (hspi->Instance == SPI2)
    {
        __HAL_RCC_SPI2_CLK_DISABLE();

        /* SPI2 GPIO Config
            PB13 -> SPI2_SCK
            PB14 -> SPI2_MISO
            PB15 -> SPI2_MOSI
        */
        HAL_GPIO_DeInit(GPIOB, GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15);

        /* SPI2 DMA DeInit */
        HAL_DMA_DeInit(hspi->hdmarx);
        HAL_DMA_DeInit(hspi->hdmatx);

        /* SPI2 Interrupt DeInit */
        HAL_NVIC_DisableIRQ(SPI2_IRQn);
    }
}

void UART1_init(uint32_t baudrate)
{
    HAL_UART_DeInit(&huart1);

    huart1.Instance            = USART1;
    huart1.Init.BaudRate       = baudrate;
    huart1.Init.WordLength     = UART_WORDLENGTH_8B;
    huart1.Init.StopBits       = UART_STOPBITS_1;
    huart1.Init.Parity         = UART_PARITY_NONE;
    huart1.Init.Mode           = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl      = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling   = UART_OVERSAMPLING_16;
    huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
    huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    if (HAL_UART_Init(&huart1) != HAL_OK)
    {
        Error_Handler();
    }
}

void UART1_initDMA(uint8_t* rx_buffer, uint32_t len)
{
    if (!rx_buffer || !len) { return; }

    __HAL_RCC_DMA2_CLK_ENABLE();

    hdma_usart1_rx.Instance                 = DMA2_Channel7;
    hdma_usart1_rx.Init.Request             = DMA_REQUEST_2;
    hdma_usart1_rx.Init.Direction           = DMA_PERIPH_TO_MEMORY;
    hdma_usart1_rx.Init.PeriphInc           = DMA_PINC_DISABLE;
    hdma_usart1_rx.Init.MemInc              = DMA_MINC_ENABLE;
    hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart1_rx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    hdma_usart1_rx.Init.Mode                = DMA_CIRCULAR;
    hdma_usart1_rx.Init.Priority            = DMA_PRIORITY_VERY_HIGH;
    if (HAL_DMA_Init(&hdma_usart1_rx) != HAL_OK)
    {
        Error_Handler();
    }

    __HAL_LINKDMA(&huart1, hdmarx, hdma_usart1_rx);

    /* Interrupt Configuration */
    HAL_NVIC_SetPriority(DMA2_Channel7_IRQn, RTOS_ISR_PRIO_GNSS_UART_DMA, 0);
    HAL_NVIC_SetPriority(USART1_IRQn, RTOS_ISR_PRIO_GNSS_UART_DMA, 0);

    /* Enable RTOR: set timeout to 5ms */
    CLEAR_BIT(huart1.Instance->CR2, USART_CR2_RTOEN);
    CLEAR_BIT(huart1.Instance->CR1, USART_CR1_RTOIE);
    __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_RTOCF);
    huart1.Instance->RTOR = huart1.Init.BaudRate * GNSS_UART_DMARX_TIMEOUT_MS / 1000;
    SET_BIT(huart1.Instance->CR1, USART_CR1_RTOIE);
    SET_BIT(huart1.Instance->CR2, USART_CR2_RTOEN);

    /* Start DMA */
    if (HAL_UART_Receive_DMA(&huart1, rx_buffer, len) != HAL_OK)
    {
        Error_Handler();
    }

    /* Disable unnecessary interrupts  */
    __HAL_DMA_DISABLE_IT(huart1.hdmarx, DMA_IT_HT);
    __HAL_DMA_DISABLE_IT(huart1.hdmarx, DMA_IT_TC);
    __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_FEF | UART_CLEAR_NEF | UART_CLEAR_OREF);

    /* Enable Interrupts after start to avoid IT lock */
    HAL_NVIC_EnableIRQ(DMA2_Channel7_IRQn);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
}

void UART1_deInitDMA(void)
{
    HAL_UART_DMAStop(&huart1);

    HAL_NVIC_DisableIRQ(DMA2_Channel7_IRQn);
    HAL_NVIC_DisableIRQ(USART1_IRQn);
}

void LPUART1_init(void)
{
    HAL_UART_DeInit(&hlpuart1);

    hlpuart1.Instance                    = LPUART1;
    hlpuart1.Init.BaudRate               = 1000000;
    hlpuart1.Init.WordLength             = UART_WORDLENGTH_8B;
    hlpuart1.Init.StopBits               = UART_STOPBITS_1;
    hlpuart1.Init.Parity                 = UART_PARITY_NONE;
    hlpuart1.Init.Mode                   = UART_MODE_TX;
    hlpuart1.Init.HwFlowCtl              = UART_HWCONTROL_NONE;
    hlpuart1.Init.OneBitSampling         = UART_ONE_BIT_SAMPLE_DISABLE;
    hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
    if (HAL_UART_Init(&hlpuart1) != HAL_OK)
    {
        Error_Handler();
    }

#if LPUART_USE_DMA
    __HAL_RCC_DMA2_CLK_ENABLE();

    /* LPUART1 DMA Init */
    /* LPUART_TX Init */
    hdma_lpuart_tx.Instance                 = DMA2_Channel6;
    hdma_lpuart_tx.Init.Request             = DMA_REQUEST_4;
    hdma_lpuart_tx.Init.Direction           = DMA_MEMORY_TO_PERIPH;
    hdma_lpuart_tx.Init.PeriphInc           = DMA_PINC_DISABLE;
    hdma_lpuart_tx.Init.MemInc              = DMA_MINC_ENABLE;
    hdma_lpuart_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_lpuart_tx.Init.MemDataAlignment    = DMA_MDATAALIGN_BYTE;
    hdma_lpuart_tx.Init.Mode                = DMA_NORMAL;
    hdma_lpuart_tx.Init.Priority            = DMA_PRIORITY_LOW;
    if (HAL_DMA_Init(&hdma_lpuart_tx) != HAL_OK)
    {
        Error_Handler();
    }
    __HAL_LINKDMA(&hlpuart1, hdmatx, hdma_lpuart_tx);

    HAL_NVIC_SetPriority(DMA2_Channel6_IRQn, RTOS_ISR_PRIO_LOG_UART_DMA, 0);
    HAL_NVIC_EnableIRQ(DMA2_Channel6_IRQn);
    HAL_NVIC_SetPriority(USART1_IRQn, RTOS_ISR_PRIO_LOG_UART_DMA, 0);
    HAL_NVIC_EnableIRQ(LPUART1_IRQn);
#endif
}


bool LPUART1_transmit(uint8_t* data, uint16_t len)
{
#if LPUART_USE_DMA

    /* Wait until previous DMA transfer has been completed */
    uint32_t tick_start = HAL_GetTick();
    while ( (hlpuart1.gState != HAL_UART_STATE_READY) )
    {
        if ( (HAL_GetTick() - tick_start) >= LPUART_DMA_TIMEOUT_MS ) { return false; }
    }
    return (HAL_UART_Transmit_DMA(&hlpuart1, data, len) == HAL_OK);

#else

    if (data && len)
    {
        return (HAL_UART_Transmit(&hlpuart1, data, len, LPUART_TIMEOUT_MS) == HAL_OK);
    }
    return false;

#endif
}

bool LPUART1_transmitBinary(const uint8_t* data, uint16_t len)
{
#if LPUART_USE_DMA

    static uint8_t lpuart_tx_buf[SERIAL_PKT_MAX_LEN * 2 + 2];   /* In the worst case, every byte needs to be escaped, plus 2 framing bytes */

    if ( !data || !len || (len > SERIAL_PKT_MAX_LEN) ) { return false; }

    /* Prepare the data (insert escape characters) */
    uint32_t buf_len = 0;
    lpuart_tx_buf[buf_len++] = SERIAL_PKT_FRAME_BYTE;
    while (len)
    {
        if ( (*data == SERIAL_PKT_ESC_BYTE) || (*data == SERIAL_PKT_FRAME_BYTE) )
        {
            lpuart_tx_buf[buf_len++] = SERIAL_PKT_ESC_BYTE;
        }
        lpuart_tx_buf[buf_len++] = *data;
        data++;
        len--;
    }
    lpuart_tx_buf[buf_len++] = SERIAL_PKT_FRAME_BYTE;

    /* Wait until previous DMA transfer has been completed */
    uint32_t tick_start = HAL_GetTick();
    while ( (hlpuart1.gState != HAL_UART_STATE_READY) )
    {
        if ( (HAL_GetTick() - tick_start) >= LPUART_DMA_TIMEOUT_MS ) { return false; }
    }
    return (HAL_UART_Transmit_DMA(&hlpuart1, lpuart_tx_buf, buf_len) == HAL_OK);

#else

    if (!data || !len) { return false; }

    UART_SEND_BYTE(&hlpuart1, SERIAL_PKT_FRAME_BYTE);

    while (len)
    {
        /* Escape byte required? */
        if ( (*data == SERIAL_PKT_ESC_BYTE) || (*data == SERIAL_PKT_FRAME_BYTE) )
        {
            UART_SEND_BYTE(&hlpuart1, SERIAL_PKT_ESC_BYTE);
        }
        UART_SEND_BYTE(&hlpuart1, *data);
        data++;
        len--;
    }

    UART_SEND_BYTE(&hlpuart1, SERIAL_PKT_FRAME_BYTE);
    UART_WAIT_TXE(&hlpuart1);

    return true;

#endif
}

void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    if (huart->Instance == USART1)
    {
        __HAL_RCC_USART1_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();
        /* UART1 GPIO Configuration
        PA9     ---> USART1_TX
        PA10    ---> USART1_RX
        */
        GPIO_InitStruct.Pin       = GPIO_PIN_9 | GPIO_PIN_10;
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_NOPULL;
        GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    }
    else if(huart->Instance == LPUART1)
    {
        /* Peripheral clock enable */
        __HAL_RCC_LPUART1_CLK_ENABLE();

        __HAL_RCC_GPIOC_CLK_ENABLE();
        /**LPUART1 GPIO Configuration
        PC0     ------> LPUART1_RX
        PC1     ------> LPUART1_TX
        */
        GPIO_InitStruct.Pin       = GPIO_PIN_1;   /* TX pin only, use GPIO_PIN_0 for debugging */
        GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull      = GPIO_NOPULL;
        GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF8_LPUART1;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{
    if (huart->Instance == USART1)
    {
        HAL_DMA_DeInit(huart->hdmarx);
        /* Do NOT disable DMA2 clock */
        /*__HAL_RCC_DMA2_CLK_DISABLE();*/

        __HAL_RCC_USART1_CLK_DISABLE();
        HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9 | GPIO_PIN_10);
    }
    else if (huart->Instance == LPUART1)
    {
#if LPUART_USE_DMA
        HAL_DMA_DeInit(huart->hdmatx);
#endif
        /* Peripheral clock disable */
        __HAL_RCC_LPUART1_CLK_DISABLE();

        /**LPUART1 GPIO Configuration
        PC0     ------> LPUART1_RX
        PC1     ------> LPUART1_TX
        */
        HAL_GPIO_DeInit(GPIOC, GPIO_PIN_1);   /* TX pin only */
    }
}

bool USB_transmitBinary(const uint8_t* data, uint16_t len)
{
    if (len > USB_CDC_TX_BUFFER_SIZE) { return false; }

    /* Prepare the data (insert escape characters) */
    uint32_t buf_len = 0;
    uint8_t* buf     = USB.txBuf;
    buf[buf_len++]   = SERIAL_PKT_FRAME_BYTE;
    while (len && (buf_len < (USB_CDC_TX_BUFFER_SIZE - 2)))
    {
        if ( (*data == SERIAL_PKT_ESC_BYTE) || (*data == SERIAL_PKT_FRAME_BYTE) )
        {
            buf[buf_len++] = SERIAL_PKT_ESC_BYTE;
        }
        buf[buf_len++] = *data;
        data++;
        len--;
    }
    if (len)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "USB CDC buffer too small");
        return false;
    }
    buf[buf_len++] = SERIAL_PKT_FRAME_BYTE;
    USB.txLen      = buf_len;

    /* Initiate transmission and wait until completed */
    uint32_t max_delay = USB_CDC_TIMEOUT_MS;
    while ((CDC_Transmit() == USBD_BUSY) && max_delay)
    {
        HAL_Delay(1);
        max_delay--;
    }
    if (max_delay == 0)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "USB CDC timeout");
        return false;
    }
    return true;
}

bool USB_printf(const char* str, ...)
{
    va_list lst;
    va_start(lst, str);
    int res = vsnprintf((char*)USB.txBuf, USB_CDC_TX_BUFFER_SIZE, str, lst);
    va_end(lst);

    if (res <= 0)
    {
        return false;
    }
    USB.txLen = res;

    /* Initiate transmission and wait until completed */
    uint32_t max_delay = USB_CDC_TIMEOUT_MS;
    while ((CDC_Transmit() == USBD_BUSY) && max_delay)
    {
        HAL_Delay(1);
        max_delay--;
    }
    if (max_delay == 0)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "USB CDC timeout");
        return false;
    }
    return true;
}

bool SWO_transmitBinary(const uint8_t* data, uint16_t len)
{
    if (!data || !len) { return false; }

    ITM_SendChar(SERIAL_PKT_FRAME_BYTE);

    while (len)
    {
        /* Escape byte required? */
        if ( (*data == SERIAL_PKT_ESC_BYTE) || (*data == SERIAL_PKT_FRAME_BYTE) )
        {
            ITM_SendChar(SERIAL_PKT_ESC_BYTE);
        }
        ITM_SendChar(*data);
        data++;
        len--;
    }

    ITM_SendChar(SERIAL_PKT_FRAME_BYTE);

    return true;
}


/* Used to debounce the pos. trigger */
void TIM3_init(void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;

    /* 400 usec de-bouncing pos and negative trigger */
    HAL_TIM_Base_DeInit(&htim3);

    htim3.Instance               = TIM3;
    htim3.Init.Prescaler         = 79;
    htim3.Init.CounterMode       = TIM_COUNTERMODE_UP;
    htim3.Init.Period            = 399;
    htim3.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
    htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
    {
        Error_Handler();
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }
}

/* Used to debounce the neg. trigger */
void TIM4_init(void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;

    HAL_TIM_Base_DeInit(&htim4);

    htim4.Instance               = TIM4;
    htim4.Init.Prescaler         = 79;
    htim4.Init.CounterMode       = TIM_COUNTERMODE_UP;
    htim4.Init.Period            = 399;
    htim4.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
    htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
    {
        Error_Handler();
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }
}

/* Counts upwards every 0.5ms, used for measuring duty-cycle */
void TIM5_init(void)
{
    TIM_ClockConfigTypeDef sClockSourceConfig;

    HAL_TIM_Base_DeInit(&htim5);

    htim5.Instance               = TIM5;
    htim5.Init.Prescaler         = TIM5_PRESCALER;
    htim5.Init.CounterMode       = TIM_COUNTERMODE_UP;
    htim5.Init.Period            = 0xFFFFFFFF;
    htim5.Init.ClockDivision     = TIM_CLOCKDIVISION_DIV1;
    htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
    if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
    {
        Error_Handler();
    }

    sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
    if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
    {
        Error_Handler();
    }

    /* Start timer */
    __HAL_TIM_SET_COUNTER(&htim5, 0);
    HAL_TIM_Base_Start(&htim5);
}

uint32_t TIM5_getCounterValue(void)
{
    return __HAL_TIM_GET_COUNTER(&htim5);
}

/* Low-power timer, used for time-sync timeout */
void LPTIM1_init(void)
{
    HAL_LPTIM_DeInit(&hlptim1);

    hlptim1.Instance             = LPTIM1;
    hlptim1.Init.Clock.Source    = LPTIM_CLOCKSOURCE_APBCLOCK_LPOSC;
    hlptim1.Init.Clock.Prescaler = LPTIM_PRESCALER_DIV1;
    hlptim1.Init.Trigger.Source  = LPTIM_TRIGSOURCE_SOFTWARE;
    hlptim1.Init.OutputPolarity  = LPTIM_OUTPUTPOLARITY_HIGH;
    hlptim1.Init.UpdateMode      = LPTIM_UPDATE_IMMEDIATE;
    hlptim1.Init.CounterSource   = LPTIM_COUNTERSOURCE_INTERNAL;
    hlptim1.Init.Input1Source    = LPTIM_INPUT1SOURCE_GPIO;
    hlptim1.Init.Input2Source    = LPTIM_INPUT2SOURCE_GPIO;
    if (HAL_LPTIM_Init(&hlptim1) != HAL_OK)
    {
        Error_Handler();
    }
    /* Do not start the timer here! */
}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base)
{
    if (htim_base->Instance == TIM3)                                                             /* Timer 3 is used to debounce the positive trigger */
    {
        __HAL_RCC_TIM3_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM3_IRQn, RTOS_ISR_PRIO_TRG_TIM, 0);
        HAL_NVIC_EnableIRQ(TIM3_IRQn);
    }

    if (htim_base->Instance == TIM4)                                                             /* Timer 4 is used to debounce the negative trigger */
    {
        __HAL_RCC_TIM4_CLK_ENABLE();
        HAL_NVIC_SetPriority(TIM4_IRQn, RTOS_ISR_PRIO_TRG_TIM, 0);
        HAL_NVIC_EnableIRQ(TIM4_IRQn);
    }

    if (htim_base->Instance == TIM5)                                                             /* Timer 5 is used to measure duty-cycle */
    {
        __HAL_RCC_TIM5_CLK_ENABLE();
    }
}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base)
{
    if (htim_base->Instance == TIM3)
    {
        __HAL_RCC_TIM3_CLK_DISABLE();
        HAL_NVIC_DisableIRQ(TIM3_IRQn);
    }

    if (htim_base->Instance == TIM4)
    {
        __HAL_RCC_TIM4_CLK_DISABLE();
        HAL_NVIC_DisableIRQ(TIM4_IRQn);
    }

    if (htim_base->Instance == TIM5)
    {
        __HAL_RCC_TIM5_CLK_DISABLE();
    }
}

void HAL_LPTIM_MspInit(LPTIM_HandleTypeDef* hlptim)
{
    if (hlptim->Instance == LPTIM1)
    {
        __HAL_RCC_LPTIM1_CLK_ENABLE();
        HAL_NVIC_SetPriority(LPTIM1_IRQn, RTOS_ISR_PRIO_TSYNC_TIM, 0);
        HAL_NVIC_EnableIRQ(LPTIM1_IRQn);
    }
}

void HAL_LPTIM_MspDeInit(LPTIM_HandleTypeDef* hlptim)
{
    if (hlptim->Instance == LPTIM1)
    {
        __HAL_RCC_LPTIM1_CLK_DISABLE();
        HAL_NVIC_DisableIRQ(LPTIM1_IRQn);
    }
}

void WDT_init(void)
{
    /* Start independent watchdog timer, triggers a reset after WATCHDOG_TIMEOUT seconds if not polled
     * ATTENTION: Once started, the watchdog cannot be stopped! Only a system reset will stop the watchdog */
    uint32_t reloadTime = WATCHDOG_TIMEOUT * WATCHDOG_FREQ / WATCHDOG_PRESCALER;
    if (reloadTime > WATCHDOG_MAX_RELOAD_VALUE)
    {
        reloadTime = WATCHDOG_MAX_RELOAD_VALUE;
    }

    uint32_t prescaler;
    switch (WATCHDOG_PRESCALER)
    {
    case 4:   prescaler = IWDG_PRESCALER_4;   break;
    case 8:   prescaler = IWDG_PRESCALER_8;   break;
    case 16:  prescaler = IWDG_PRESCALER_16;  break;
    case 32:  prescaler = IWDG_PRESCALER_32;  break;
    case 64:  prescaler = IWDG_PRESCALER_64;  break;
    case 128: prescaler = IWDG_PRESCALER_128; break;
    case 256: prescaler = IWDG_PRESCALER_256; break;
    default:  prescaler = IWDG_PRESCALER_256; break;
    }

    hiwdg.Instance       = IWDG;
    hiwdg.Init.Prescaler = prescaler;               /* Set to defined prescaler */
    hiwdg.Init.Reload    = reloadTime;              /* Set to reload value which should be bigger as the wakeup time */
    hiwdg.Init.Window    = IWDG_WINDOW_DISABLE;     /* Don't use this feature */
    if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
    {
        Error_Handler();
    }
    HAL_IWDG_Refresh(&hiwdg);
}

/* GPIO Init (should only be executed during boot-up or reset) */
void GPIO_init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    /* GPIO Ports Clock Enable */
    SystemClock_enable();

    /* Configure all unused pins in analog mode */
#ifdef GPIOA_UNUSED_PINS
    GPIO_InitStruct.Pin  = GPIOA_UNUSED_PINS;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
#endif

#ifdef GPIOB_UNUSED_PINS
    GPIO_InitStruct.Pin  = GPIOB_UNUSED_PINS;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
#endif

#ifdef GPIOC_UNUSED_PINS
    GPIO_InitStruct.Pin  = GPIOC_UNUSED_PINS;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
#endif

#ifdef GPIOD_UNUSED_PINS
    GPIO_InitStruct.Pin  = GPIOD_UNUSED_PINS;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
#endif

#ifdef GPIOE_UNUSED_PINS
    GPIO_InitStruct.Pin  = GPIOE_UNUSED_PINS;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);
#endif

    GPIO_InitStruct.Pin  = GPIO_PIN_All;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);

    /* Set default output levels */
    ADC_SWOFF();    /* Also sets ADC CS lines and SYNC low and turns off the ADC clock */
    AVDD_SWOFF();
    BOLT_DEASSERT();
    BOLT_MODE_RD();
    BUZZER_OFF();
    COM_TREQ_LOW();
    DAC_DEASSERT();
    DEBUG_PIN1_LOW();
    DEBUG_PIN2_LOW();
    GNSS_SWOFF();   /* Also sets Reset and EXTINT line low */
    IMU_SWOFF();
    LED_R_OFF();
    LED_Y_OFF();
    LED_G_OFF();
    LED_B_OFF();
    SD_SWOFF();
    SHT_SWOFF();
    EXT_SWOFF();
    SCL_SWOFF();    /* Also sets CS line low */
    VSENSE_SWOFF();
    STAGE2_SWOFF();

    /* Debug GPIOs / extension header */
#ifdef GPP3X
  #if DEBUG_USB_MSC_ON_DBGPIN2
    GPIO_InitStruct.Pin   = DEBUG_PIN1_Pin;   /* Only pin 1 is an output */
  #else
    GPIO_InitStruct.Pin   = DEBUG_PIN1_Pin | DEBUG_PIN2_Pin;
  #endif
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(DEBUG_PIN1_Port, &GPIO_InitStruct);

  #if DEBUG_USB_MSC_ON_DBGPIN2
    GPIO_InitStruct.Pin   = DEBUG_PIN2_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    HAL_GPIO_Init(DEBUG_PIN2_Port, &GPIO_InitStruct);
  #endif

  #ifdef GPP3X_REV_2_1
    DEBUG_PIN3_LOW();
    DEBUG_PIN4_LOW();
    GPIO_InitStruct.Pin   = DEBUG_PIN3_Pin | DEBUG_PIN4_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(DEBUG_PIN3_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = EXT_HDR_P3V_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(EXT_HDR_P3V_Port, &GPIO_InitStruct);
  #endif
#endif

    /* ADC */
    /* ADC1 (trigger) */
    GPIO_InitStruct.Pin   = ADC1_CS_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ADC1_CS_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = ADC1_RDY_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(ADC1_RDY_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = TRGP_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TRGP_Port, &GPIO_InitStruct);
    GPIO_EXTI_IT_DISABLE(TRGP_Pin);    /* Mask interrupt, will be enabled at a later stage */

    GPIO_InitStruct.Pin   = TRGN_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TRGN_Port, &GPIO_InitStruct);
    GPIO_EXTI_IT_DISABLE(TRGN_Pin);    /* Mask interrupt, will be enabled at a later stage */

    /* ADC2 */
    GPIO_InitStruct.Pin   = ADC2_CS_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ADC2_CS_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = ADC2_RDY_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;    /* Pulldown since the pin must be low when the ADC is turned off in stop mode */
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(ADC2_RDY_Port, &GPIO_InitStruct);

    /* ADC3 */
    GPIO_InitStruct.Pin   = ADC3_CS_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ADC3_CS_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = ADC3_RDY_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(ADC3_RDY_Port, &GPIO_InitStruct);

    /* Analog front-end Power */
    GPIO_InitStruct.Pin   = ADC_SYNC_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ADC_SYNC_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = ADC_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ADC_ON_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = ADC_CLK_EN_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(ADC_CLK_EN_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = AVDD_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(AVDD_ON_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = TRIGGER_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(TRIGGER_ON_Port, &GPIO_InitStruct);

    /* BOLT */
    GPIO_InitStruct.Pin   = COM_TREQ_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(COM_TREQ_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = COM_IND_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(COM_IND_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = BOLT_IND_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_RISING;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BOLT_IND_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = BOLT_MODE_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BOLT_MODE_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = BOLT_REQ_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BOLT_REQ_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = BOLT_ACK_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;    /* Use polling mode (could also be configured in interrupt mode) */
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BOLT_ACK_Port, &GPIO_InitStruct);

    /* User Button */
    GPIO_InitStruct.Pin   = BTN_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull  = GPIO_PULLUP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BTN_Port, &GPIO_InitStruct);

    /* Buzzer */
    GPIO_InitStruct.Pin   = BUZZER_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(BUZZER_Port, &GPIO_InitStruct);

    /* DAC */
    GPIO_InitStruct.Pin   = DAC_CS_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(DAC_CS_Port, &GPIO_InitStruct);

    /* IMU power */
    GPIO_InitStruct.Pin   = IMU_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(IMU_ON_Port, &GPIO_InitStruct);

    /* Interrupts are configured in initInterruptHandler() - by default, we configure the pins as inputs */
    GPIO_InitStruct.Pin   = IMU_INT_XL_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;        /* To prevent floating inputs */
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(IMU_INT_XL_Port, &GPIO_InitStruct);

    GPIO_InitStruct.Pin   = IMU_INT_MAG_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;        /* To prevent floating inputs */
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(IMU_INT_MAG_Port, &GPIO_InitStruct);

    /* SCL */
#ifdef SCL_ON_Pin
    GPIO_InitStruct.Pin   = SCL_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SCL_ON_Port, &GPIO_InitStruct);
#endif

#ifdef SCL_CS_Pin
    GPIO_InitStruct.Pin   = SCL_CS_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SCL_CS_Port, &GPIO_InitStruct);
#endif

#ifdef GNSS_TPULSE_Pin
    /* GNSS */
    GPIO_InitStruct.Pin   = GNSS_TPULSE_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_RISING_FALLING;
    GPIO_InitStruct.Pull  = GPIO_PULLDOWN;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GNSS_TPULSE_Port, &GPIO_InitStruct);
    GPIO_EXTI_IT_DISABLE(GNSS_TPULSE_Pin);    /* Mask interrupt, will be enabled at a later stage */
#endif

#ifdef GNSS_ON_Pin
    GPIO_InitStruct.Pin   = GNSS_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GNSS_ON_Port, &GPIO_InitStruct);
#endif

#ifdef GNSS_EXTINT_Pin
    GPIO_InitStruct.Pin   = GNSS_EXTINT_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GNSS_EXTINT_Port, &GPIO_InitStruct);
#endif

#ifdef GNSS_RST_Pin
    GPIO_InitStruct.Pin   = GNSS_RST_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GNSS_RST_Port, &GPIO_InitStruct);
#endif

    /* LEDs */
    GPIO_InitStruct.Pin   = LED_R_Pin | LED_Y_Pin | LED_G_Pin | LED_B_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(LED_R_Port, &GPIO_InitStruct);

    /* SD */
    GPIO_InitStruct.Pin   = SD_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SD_ON_Port, &GPIO_InitStruct);

    /* SHT */
    GPIO_InitStruct.Pin   = SHT_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(SHT_ON_Port, &GPIO_InitStruct);

    /* STAGE2 */
    GPIO_InitStruct.Pin   = STAGE2_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(STAGE2_ON_Port, &GPIO_InitStruct);

    /* USB */
    GPIO_InitStruct.Pin   = USB_DETECT_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;    /* Has an external pulldown */
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(USB_DETECT_Port, &GPIO_InitStruct);

    /* VSENSE */
    GPIO_InitStruct.Pin   = VSENSE_ON_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(VSENSE_ON_Port, &GPIO_InitStruct);

    /* DPP reset */
#ifdef DPP_RST_Pin
    GPIO_InitStruct.Pin   = DPP_RST_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
    GPIO_InitStruct.Pull  = GPIO_PULLUP;        /* Driven externally only when a comboard is connected */
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(DPP_RST_Port, &GPIO_InitStruct);
#endif

    /* Interrupts:
     *  EXTI_0:  ADC1_RDY
     *  EXTI_1:  TRG+
     *  EXTI_2:  TRG-
     *  EXTI_3:  ADC2_RDY
     *  EXTI_4:  ADC3_RDY
     *  EXTI_6:  GNSS_TPULSE
     *  EXTI_8:  BTN
     *  EXTI_12: BOLT_IND
     *  EXTI_13: IMU_INT_XL
     *  EXTI_14: IMU_INT_MAG
     *  EXTI_15: BOLT_ACK
     */
    HAL_NVIC_SetPriority(EXTI0_IRQn,     RTOS_ISR_PRIO_ADC_RDY_EXTI, 0);
    HAL_NVIC_SetPriority(EXTI1_IRQn,     RTOS_ISR_PRIO_TRG_EXTI, 0);
    HAL_NVIC_SetPriority(EXTI2_IRQn,     RTOS_ISR_PRIO_TRG_EXTI, 0);
    HAL_NVIC_SetPriority(EXTI3_IRQn,     RTOS_ISR_PRIO_ADC_RDY_EXTI, 0);
    HAL_NVIC_SetPriority(EXTI4_IRQn,     RTOS_ISR_PRIO_ADC_RDY_EXTI, 0);
    HAL_NVIC_SetPriority(EXTI9_5_IRQn,   RTOS_ISR_PRIO_BTN_PPS_EXTI, 0);
    HAL_NVIC_SetPriority(EXTI15_10_IRQn, RTOS_ISR_PRIO_BOLT_IMU_EXTI, 0);

    /* EXTI0, EXTI3 and EXTI4 are initialized in task_geo after a trigger signal has been received */
    /* EXTI1 and EXTI2 are initialized in Geo_triggerEnable() */
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
    HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
}

void PERIPH_prepareStop2(void)
{
    /* Eject SD Card */
    SD_eject();

    /* Stop Peripherals */
    //HAL_ADC_Stop(&hadc1); -> is always stopped after use
    HAL_SPI_Abort(&hspi1);
#if SPI2_USE_DMA
    HAL_SPI_Abort(&hspi2);
#endif
    HAL_UART_Abort(&huart1);
    HAL_TIM_Base_Stop(&htim3);
    HAL_TIM_Base_Stop(&htim4);
    HAL_TIM_Base_Stop(&htim5);

    /* Deinit Peripherals */
    HAL_ADC_DeInit(&hadc1);
#ifdef GPP3X
    HAL_I2C_DeInit(&hi2c1);
#endif
    HAL_I2C_DeInit(&hi2c2);
    __HAL_SPI_DISABLE(&hspi1);
    __HAL_SPI_DISABLE(&hspi2);    /* No need to de-init, disable is sufficient */
    HAL_TIM_Base_DeInit(&htim3);
    HAL_TIM_Base_DeInit(&htim4);
    HAL_TIM_Base_DeInit(&htim5);
    HAL_UART_DeInit(&huart1);
#if USE_LPUART1
    __HAL_UART_DISABLE(&hlpuart1);
#endif

    /* Switch Off Components */
    ADC_SWOFF();    /* Also set ADC CS lines and SYNC low and turns off the ADC clock */
    AVDD_SWOFF();
    BUZZER_OFF();
    DAC_DEASSERT();
    GNSS_SWOFF();
    IMU_SWOFF();
    SCL_SWOFF();
    LED_ALL_OFF();
    SD_SWOFF();
    SHT_SWOFF();
    VSENSE_SWOFF();

    if (!(SysConf.OpMode & SYS_OPMODE_TRG))
    {
        TRIGGER_SWOFF();
    }
}

void PERIPH_resumeFromStop2(void)
{
    /* Power on */
    SD_SWON();

    /* Resume Peripherals */
    ADC1_init();
    __HAL_SPI_ENABLE(&hspi1);
    __HAL_SPI_ENABLE(&hspi2);
    TIM3_init();
    TIM4_init();
    TIM5_init();
#if USE_LPUART1
    __HAL_UART_ENABLE(&hlpuart1);
#endif
}

const char* System_getResetCause(uint8_t* out_reset_flag)
{
    /* RESET Flags:
     * Bit 7: Low-power reset
     * Bit 6: Window watchdog reset
     * Bit 5: Independent watchdog reset
     * Bit 4: Software reset
     * Bit 3: BOR
     * Bit 2: Pin reset (NRST)
     * Bit 1: OBL reset
     * Bit 0: Firewall reset */
    static uint_fast8_t reset_flag = 0;

    if (!reset_flag)
    {
        reset_flag = RCC->CSR >> 24;
        __HAL_RCC_CLEAR_RESET_FLAGS();
    }
    if (out_reset_flag)
    {
        *out_reset_flag = reset_flag;
    }
    if (reset_flag & 0x80) { return "LPM";  }  // illegal low-power mode entry
    if (reset_flag & 0x40) { return "WWDG"; }  // window watchdog
    if (reset_flag & 0x20) { return "IWDG"; }  // independent watchdog
    if (reset_flag & 0x10) { return "SW";   }  // software triggered
    if (reset_flag & 0x08) { return "BOR";  }  // brownout
    if (reset_flag & 0x04) { return "nRST"; }  // reset pin
    if (reset_flag & 0x02) { return "OBL";  }  // option byte loader
    if (reset_flag & 0x01) { return "FWR";  }  // firewall reset
    return "?";
}

/* SYSTEM CLOCK --------------------------------------------------------------*/
void SystemClock_disable(void)
{
    /* Turn off clocks */
    __HAL_RCC_GPIOA_CLK_DISABLE();
    __HAL_RCC_GPIOB_CLK_DISABLE();
    __HAL_RCC_GPIOC_CLK_DISABLE();
    __HAL_RCC_GPIOD_CLK_DISABLE();
    __HAL_RCC_GPIOE_CLK_DISABLE();
    __HAL_RCC_GPIOH_CLK_DISABLE();

    //__HAL_RCC_DMA1_CLK_DISABLE();
    //__HAL_RCC_DMA2_CLK_DISABLE();

    __HAL_RCC_FLASH_CLK_DISABLE();
    __HAL_RCC_PWR_CLK_DISABLE();
    __HAL_RCC_SYSCFG_CLK_DISABLE();

    /* The following turns off all clocks, but is more of a brute-force method (-> better to turn off unused peripherals separately) */
    //RCC->AHB1SMENR  = 0x0;
    //RCC->AHB2SMENR  = 0x0;
    //RCC->AHB3SMENR  = 0x0;
    //RCC->APB1SMENR1 = RCC_APB1SMENR1_LPTIM1SMEN;    /* do not disable LPTIM1 in stop mode */;
    //RCC->APB1SMENR2 = 0x0;
    //RCC->APB2SMENR  = 0x0;
}

void SystemClock_enable(void)
{
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOE_CLK_ENABLE();
    __HAL_RCC_GPIOH_CLK_ENABLE();

    //__HAL_RCC_DMA1_CLK_ENABLE();
    //__HAL_RCC_DMA2_CLK_ENABLE();

    __HAL_RCC_FLASH_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_RCC_SYSCFG_CLK_ENABLE();
}

void SystemClock_config(void)
{
    /* make static to reduce stack usage (~240 bytes) */
    static RCC_OscInitTypeDef RCC_OscInitStruct;
    static RCC_ClkInitTypeDef RCC_ClkInitStruct;
    static RCC_PeriphCLKInitTypeDef PeriphClkInit;

    __HAL_RCC_LSEDRIVE_CONFIG(RCC_LSEDRIVE_LOW);

    /* Initialize OSC, PLL - HSE = 8 MHz, SysClk = 80 MHz */
    RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_LSE;
    RCC_OscInitStruct.LSEState            = RCC_LSE_ON;
#if (MCU_HSE_CRYSTAL)
    RCC_OscInitStruct.OscillatorType     |= RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState            = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM            = 1;
    RCC_OscInitStruct.PLL.PLLN            = 20;
#else
    RCC_OscInitStruct.OscillatorType     |= RCC_OSCILLATORTYPE_MSI;
    RCC_OscInitStruct.MSIState            = RCC_MSI_ON;
    RCC_OscInitStruct.MSICalibrationValue = 0;
    RCC_OscInitStruct.MSIClockRange       = RCC_MSIRANGE_6;
    RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource       = RCC_PLLSOURCE_MSI;
    RCC_OscInitStruct.PLL.PLLM            = 1;
    RCC_OscInitStruct.PLL.PLLN            = 40;
#endif
    RCC_OscInitStruct.PLL.PLLP            = RCC_PLLP_DIV7;
    RCC_OscInitStruct.PLL.PLLQ            = RCC_PLLQ_DIV2;
    RCC_OscInitStruct.PLL.PLLR            = RCC_PLLR_DIV2;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    /* Initialize CPU, AHB and APB bus clocks */
    RCC_ClkInitStruct.ClockType      = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource   = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider  = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
    {
        Error_Handler();
    }

    /* Initialize Periph bus clocks (Note: HCLK = PCLK1 = PCLK2 = SYSCLK = 80MHz, PLLSAI1 = 48MHz) */
    PeriphClkInit.PeriphClockSelection    = RCC_PERIPHCLK_ADC |
                                            RCC_PERIPHCLK_I2C1 |
                                            RCC_PERIPHCLK_I2C2 |
                                            RCC_PERIPHCLK_RTC |
                                            RCC_PERIPHCLK_SDMMC1 |
                                            RCC_PERIPHCLK_USART1 |
#if USE_LPUART1
                                            RCC_PERIPHCLK_LPUART1 |
#endif
                                            RCC_PERIPHCLK_USB |
                                            RCC_PERIPHCLK_LPTIM1;
    PeriphClkInit.AdcClockSelection       = RCC_ADCCLKSOURCE_PLLSAI1;
    PeriphClkInit.I2c1ClockSelection      = RCC_I2C1CLKSOURCE_PCLK1;
    PeriphClkInit.I2c2ClockSelection      = RCC_I2C2CLKSOURCE_PCLK1;
    PeriphClkInit.RTCClockSelection       = RCC_RTCCLKSOURCE_LSE;
    PeriphClkInit.Sdmmc1ClockSelection    = RCC_SDMMC1CLKSOURCE_PLLSAI1;
    PeriphClkInit.Usart1ClockSelection    = RCC_USART1CLKSOURCE_PCLK2;
    PeriphClkInit.UsbClockSelection       = RCC_USBCLKSOURCE_PLLSAI1;
    PeriphClkInit.Lptim1ClockSelection    = RCC_LPTIM1CLKSOURCE_LSE;
#if USE_LPUART1
    PeriphClkInit.Lpuart1ClockSelection   = RCC_LPUART1CLKSOURCE_PCLK1;
#endif
#if (MCU_HSE_CRYSTAL)
    PeriphClkInit.PLLSAI1.PLLSAI1Source   = RCC_PLLSOURCE_HSE;
    PeriphClkInit.PLLSAI1.PLLSAI1M        = 1;
    PeriphClkInit.PLLSAI1.PLLSAI1N        = 12;
#else
    PeriphClkInit.PLLSAI1.PLLSAI1Source   = RCC_PLLSOURCE_MSI;
    PeriphClkInit.PLLSAI1.PLLSAI1M        = 1;
    PeriphClkInit.PLLSAI1.PLLSAI1N        = 24;
#endif
    PeriphClkInit.PLLSAI1.PLLSAI1P        = RCC_PLLP_DIV2;
    PeriphClkInit.PLLSAI1.PLLSAI1Q        = RCC_PLLQ_DIV2;
    PeriphClkInit.PLLSAI1.PLLSAI1R        = RCC_PLLR_DIV2;
    PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK | RCC_PLLSAI1_48M2CLK;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
        Error_Handler();
    }

    /* Configure main internal regulator output voltage */
    if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
    {
        Error_Handler();
    }

#if !(MCU_HSE_CRYSTAL)
    HAL_RCCEx_EnableMSIPLLMode();
#endif
}

void SystemClock_reset(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
    uint32_t tickstart;

    __HAL_RCC_PWR_CLK_ENABLE();

    /* Enable HSI */
    RCC_OscInitStruct.OscillatorType      = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState            = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState        = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    /* Select HSI as system clock */
    RCC_ClkInitStruct.ClockType    = RCC_CLOCKTYPE_SYSCLK;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
    {
        Error_Handler();
    }

    /* Reset HSEON, PLLON, PLLSAIxON bits */
#if defined(RCC_PLLSAI2_SUPPORT)
    CLEAR_BIT(RCC->CR, RCC_CR_HSEON | RCC_CR_PLLON | RCC_CR_PLLSAI1ON | RCC_CR_PLLSAI2ON);
#else
    CLEAR_BIT(RCC->CR, RCC_CR_HSEON | RCC_CR_PLLON | RCC_CR_PLLSAI1ON);
#endif /* RCC_PLLSAI2_SUPPORT */

    /* Ensure PLLRDY, PLLSAIxRDY are reset */
    tickstart = HAL_GetTick();
#if defined(RCC_PLLSAI2_SUPPORT)
    while (READ_BIT(RCC->CR, RCC_CR_PLLRDY | RCC_CR_PLLSAI1RDY | RCC_CR_PLLSAI2RDY) != 0U)
#else
    while (READ_BIT(RCC->CR, RCC_CR_PLLRDY | RCC_CR_PLLSAI1RDY) != 0U)
#endif
    {
        if ((HAL_GetTick() - tickstart) > 3)
        {
            Error_Handler();
        }
    }

    /* Reset PLLCFGR register */
    CLEAR_REG(RCC->PLLCFGR);
    SET_BIT(RCC->PLLCFGR, RCC_PLLCFGR_PLLN_4 );

    /* Reset PLLSAI1CFGR register */
    CLEAR_REG(RCC->PLLSAI1CFGR);
    SET_BIT(RCC->PLLSAI1CFGR,  RCC_PLLSAI1CFGR_PLLSAI1N_4 );

#if defined(RCC_PLLSAI2_SUPPORT)
    /* Reset PLLSAI2CFGR register */
    CLEAR_REG(RCC->PLLSAI2CFGR);
    SET_BIT(RCC->PLLSAI2CFGR,  RCC_PLLSAI2CFGR_PLLSAI2N_4 );
#endif /* RCC_PLLSAI2_SUPPORT */

    /* Reset HSEBYP bit */
    CLEAR_BIT(RCC->CR, RCC_CR_HSEBYP);

    /* Disable MSI */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
    RCC_OscInitStruct.MSIState       = RCC_MSI_OFF;
    RCC_OscInitStruct.PLL.PLLState   = RCC_PLL_NONE;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }
}

void SysTick_init(void)
{
    /* Configure the Systick interrupt time */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

    /* Configure the Systick */
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /* SysTick_IRQn interrupt configuration: set lowest preemption priority (15) */
    HAL_NVIC_SetPriority(SysTick_IRQn, RTOS_ISR_PRIO_SYSTICK, 0);
}

void SysTick_reset(void)
{
    SysTick->CTRL = 0;
    SysTick->LOAD = 0;
    SysTick->VAL  = 0;
}

/* HAL TIMEBASE --------------------------------------------------------------*/
HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority)
{
    RCC_ClkInitTypeDef    clkconfig;
    uint32_t              uwTimclock;
    uint32_t              uwPrescalerValue;
    uint32_t              pFLatency;

    /* Configure the TIM17 IRQ priority */
    HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM17_IRQn, TickPriority, 0);     /* TickPriority is 0 = highest priority */

    /* Enable the TIM17 global Interrupt */
    HAL_NVIC_EnableIRQ(TIM1_TRG_COM_TIM17_IRQn);

    /* Enable TIM17 clock */
    __HAL_RCC_TIM17_CLK_ENABLE();

    /* Get clock configuration */
    HAL_RCC_GetClockConfig(&clkconfig, &pFLatency);

    /* Compute TIM17 clock */
    uwTimclock = HAL_RCC_GetPCLK2Freq();

    /* Compute the prescaler value to have TIM17 counter clock equal to 1MHz */
    uwPrescalerValue = (uint32_t) ((uwTimclock / 1000000) - 1);

    /* Initialize TIM17 */
    htim17.Instance = TIM17;

    /* Initialize TIMx peripheral as follows:
    + Period = [(TIM17CLK/1000) - 1]. to have a (1/1000) s time base.
    + Prescaler = (uwTimclock/1000000 - 1) to have a 1MHz counter clock.
    + ClockDivision = 0
    + Counter direction = Up
    */
    htim17.Init.Period        = (1000000 / 1000) - 1;
    htim17.Init.Prescaler     = uwPrescalerValue;
    htim17.Init.ClockDivision = 0;
    htim17.Init.CounterMode   = TIM_COUNTERMODE_UP;
    if (HAL_TIM_Base_Init(&htim17) == HAL_OK)
    {
        /* Start the TIM time Base generation in interrupt mode */
        return HAL_TIM_Base_Start_IT(&htim17);
    }

    /* Return function status */
    return HAL_ERROR;
}

void HAL_SuspendTick(void)
{
    __HAL_TIM_DISABLE_IT(&htim17, TIM_IT_UPDATE);
}

void HAL_ResumeTick(void)
{
    __HAL_TIM_ENABLE_IT(&htim17, TIM_IT_UPDATE);
}

void HAL_MspInit(void)
{
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();

    /* Use all 4 bits for preemption priority, 0 bits for subpriority */
    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
    HAL_NVIC_SetPriority(MemoryManagement_IRQn, 0, 0);  /* highest priority (0) */
    HAL_NVIC_SetPriority(BusFault_IRQn,         0, 0);
    HAL_NVIC_SetPriority(UsageFault_IRQn,       0, 0);
    HAL_NVIC_SetPriority(DebugMonitor_IRQn,     0, 0);
    HAL_NVIC_SetPriority(SVCall_IRQn,           0, 0);
    HAL_NVIC_SetPriority(PendSV_IRQn,          15, 0);  /* lowest priority (15) */
    HAL_NVIC_SetPriority(SysTick_IRQn, RTOS_ISR_PRIO_SYSTICK, 0);
}

