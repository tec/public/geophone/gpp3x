/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
Geophone_t    Geo = { 0 };

/* External variables --------------------------------------------------------*/
extern rb_t   rb_geo;
extern rb_t   rb_imustat;

/* Functions -----------------------------------------------------------------*/

void Geo_initAcq(TRGType_t trg, uint32_t duration, BaseType_t* const pxHigherPriorityTaskWoken)
{
    if (SysConf.USB->deviceClass == MSC) { return; }   /* do not allow triggers when in USB mass storage mode */

    /* Already running? */
    if (Geo.Status != GEO_ACQSTATUS_STOPPED)
    {
        if (SysConf.OpMode & SYS_OPMODE_TRG)
        {
            Geo.ChunksRemaining = (Geo.PostTrg + 1) * GEO_NUM_AXES;

            if (trg == TRG_POS)
            {
                Geo.TrgLastPosSampleNo = Geo.Samples_TrgADC;
                Geo.TrgCountPos++;
            }
            else if (trg == TRG_NEG)
            {
                Geo.TrgLastNegSampleNo = Geo.Samples_TrgADC;
                Geo.TrgCountNeg++;
            }
        }
        else
        {
            /* Extend by the requested number of samples */
            Geo.ChunksRemaining += (duration * GEO_NUM_AXES);
        }
    }
    else
    {
        if (duration == 0)    /* Continuous mode */
        {
            SysConf.OpMode |= SYS_OPMODE_CONT;
            duration = 1;     /* Must not be zero */
        }
        /* Geophone struct init */
        Geo.Status             = GEO_ACQSTATUS_ACTIVE;
        Geo.Start              = RTC_TIMESTAMP();
        Geo.TrgSource          = trg;
        Geo.TrgCountPos        = (trg == TRG_POS);
        Geo.TrgCountNeg        = (trg == TRG_NEG);
        Geo.TrgLastPosSampleNo = 0;
        Geo.TrgLastNegSampleNo = 0;
        Geo.ChunksRemaining    = duration * GEO_NUM_AXES;
        Geo.First              = 0;  /* Timestamp will be filled when first ADC_RDY pulse is received */
        Geo.End                = 0;
        Geo.Samples            = 0;
        Geo.Samples_TrgADC     = 0;
        Geo.PeakPosVal         = 0;
        Geo.PeakPosSampleNo    = 0;
        Geo.PeakNegVal         = UINT32_MAX;
        Geo.PeakNegSampleNo    = 0;
        Geo.ChunksSampled      = 0;
        Geo.ID++;

        if (IS_ISR())
        {
            vTaskNotifyGiveFromISR(xTaskHandle_Geo, pxHigherPriorityTaskWoken);
        }
        else
        {
            xTaskNotifyGive(xTaskHandle_Geo);
        }
    }
}

void Geo_abort(bool restart)
{
    if (Geo.Status == GEO_ACQSTATUS_ACTIVE)
    {
        if (restart)
        {
            DEBUG_PRINT(DBG_LVL_VERBOSE, "Geo task restart requested");
        }
        else
        {
            SysConf.OpMode &= ~SYS_OPMODE_CONT;
            DEBUG_PRINT(DBG_LVL_VERBOSE, "Geo task stop requested");
        }
        Geo.Status = GEO_ACQSTATUS_ABORT;   /* Request acquisition abort */
    }
}

void Geo_enableTrigger(void)
{
    if (!TRIGGER_IS_SWON())
    {
        TRIGGER_SWON();
        RTOS_TASK_DELAY(DAC_SETTLE_TIME_MS);
    }

    __HAL_GPIO_EXTI_CLEAR_IT(TRGP_Pin);
    __HAL_GPIO_EXTI_CLEAR_IT(TRGN_Pin);

    GPIO_EXTI_IT_ENABLE(TRGP_Pin);
    GPIO_EXTI_IT_ENABLE(TRGN_Pin);

    HAL_NVIC_EnableIRQ(EXTI1_IRQn);   /* TRG+ */
    HAL_NVIC_EnableIRQ(EXTI2_IRQn);   /* TRG- */
}

void Geo_disableTrigger(void)
{
    HAL_NVIC_DisableIRQ(EXTI1_IRQn);
    HAL_NVIC_DisableIRQ(EXTI2_IRQn);

    GPIO_EXTI_IT_DISABLE(TRGP_Pin);
    GPIO_EXTI_IT_DISABLE(TRGN_Pin);

    __HAL_GPIO_EXTI_CLEAR_IT(TRGP_Pin);
    __HAL_GPIO_EXTI_CLEAR_IT(TRGN_Pin);
}

int16_t Geo_getTriggerBias(GeoTrgBias_t bias)
{
    /* Only defined for certain devices */
    if ( (bias > GEO_TRG_BIAS_DEV_ID) && (bias < NUM_GEO_TRG_BIAS) )
    {
        /* Iterate through table to find the correct entry */
        for (uint32_t i = 0; i < geo_trg_bias_cnt; i++)
        {
            if (SysConf.DeviceID == geo_trg_bias[i][GEO_TRG_BIAS_DEV_ID])
            {
                return geo_trg_bias[i][bias];
            }
        }
    }
    DEBUG_PRINTF(DBG_LVL_WARNING, "Calibration value for trigger bias %u not found", bias);

    return 0;   /* Default value, no bias */
}

bool Geo_setTriggerThresholds(uint16_t th_pos_mv, uint16_t th_neg_mv)
{
    /* Make sure the trigger supply is enabled and DAC initialized */
    TRIGGER_SWON();
    DAC_init();

    /* Mask ITs */
    bool it_enabled = GPIO_EXTI_IT_IS_EN(TRGP_Pin);
    GPIO_EXTI_IT_DISABLE(TRGP_Pin);
    GPIO_EXTI_IT_DISABLE(TRGN_Pin);

    /* Get AmpOut bias (midpoint between thresholds) */
    int16_t ampout_bias = Geo_getTriggerBias((Geo.TrgGain == TRG_GAIN_STAGE2) ? GEO_TRG_BIAS_STAGE2 : GEO_TRG_BIAS_STAGE1);

    /* Set DAC TH+ if argument valid */
    if ( (th_pos_mv != DPP_MSG_INV_UINT16) && (th_pos_mv < DAC_VREF) )
    {
        Geo.TrgThPos     = th_pos_mv;
        uint16_t dac_val = ((uint32_t)th_pos_mv - Geo_getTriggerBias(GEO_TRG_BIAS_POS_TH) + ampout_bias) * DAC_MAX_VALUE / DAC_VREF;
        DAC_sendCmd(DAC_CMD_LOAD_INPUT_DAC_A_DAC_B, dac_val);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Threshold for TRG+ set to %lumV", th_pos_mv);
    }

    /* Set DAC TH- if argument valid */
    if ( (th_neg_mv != DPP_MSG_INV_UINT16) && (th_neg_mv < DAC_VREF) )
    {
        Geo.TrgThNeg     = th_neg_mv;
        uint16_t dac_val = ((uint32_t)th_neg_mv - Geo_getTriggerBias(GEO_TRG_BIAS_NEG_TH) + ampout_bias) * DAC_MAX_VALUE / DAC_VREF;
        DAC_sendCmd(DAC_CMD_LOAD_INPUT_DAC_B_DAC_A, dac_val);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Threshold for TRG- set to %lumV", th_neg_mv);
    }

    /* Wait for the DAC voltage to settle */
    RTOS_TASK_DELAY(DAC_SETTLE_TIME_MS);

    /* Unmask ITs */
    if (it_enabled)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(TRGP_Pin);
        __HAL_GPIO_EXTI_CLEAR_IT(TRGN_Pin);
        GPIO_EXTI_IT_ENABLE(TRGP_Pin);
        GPIO_EXTI_IT_ENABLE(TRGN_Pin);
    }

    return true;
}

static void Geo_generateAcqMsg(Geophone_t* pGeo)
{
    static dpp_geophone_acq_t geo_acq;   /* static to reduce stack size */

    /* DPP ACQ: Payload */
    geo_acq.start_time          = pGeo->Start;
    geo_acq.first_time          = pGeo->First;
    geo_acq.samples             = pGeo->Samples;
    geo_acq.peak_pos_val        = pGeo->PeakPosVal;
    geo_acq.peak_pos_sample     = pGeo->PeakPosSampleNo;
    geo_acq.peak_neg_val        = pGeo->PeakNegVal;
    geo_acq.peak_neg_sample     = pGeo->PeakNegSampleNo;
    geo_acq.trg_count_pos       = pGeo->TrgCountPos;
    geo_acq.trg_count_neg       = pGeo->TrgCountNeg;
    geo_acq.trg_last_pos_sample = pGeo->TrgLastPosSampleNo;
    geo_acq.trg_last_neg_sample = pGeo->TrgLastNegSampleNo;
    geo_acq.trg_gain            = (pGeo->TrgGain == TRG_GAIN_STAGE2) ? TRG_GAIN_VALUE_STAGE2 : TRG_GAIN_VALUE_STAGE1;
    geo_acq.trg_th_pos          = pGeo->TrgThPos;
    geo_acq.trg_th_neg          = pGeo->TrgThNeg;
    geo_acq.trg_source          = pGeo->TrgSource;
    geo_acq.adc_pga             = pGeo->PGA;
    geo_acq.acq_id              = pGeo->ID;
    geo_acq.adc_sps             = ADC_SPS_1000;
    if      (pGeo->SPS == 500)  { geo_acq.adc_sps = ADC_SPS_500; }
    else if (pGeo->SPS == 250)  { geo_acq.adc_sps = ADC_SPS_250; }
    else if (pGeo->SPS == 125)  { geo_acq.adc_sps = ADC_SPS_125; }

    DEBUG_PRINTF(DBG_LVL_INFO, "ACQ ID: %lu, samples: %lu, SPS: %u, peak+: %lu @%lu, peak-: %lu @%lu, trg+: %lu, trg-: %lu", geo_acq.acq_id, geo_acq.samples, geo_acq.adc_sps, geo_acq.peak_pos_val, geo_acq.peak_pos_sample, geo_acq.peak_neg_val, geo_acq.peak_neg_sample, geo_acq.trg_count_pos, geo_acq.trg_count_neg);

    Bolt_enqueueMessage(DPP_MSG_TYPE_GEOPHONE_ACQ, (uint8_t*)&geo_acq);
}


/*** GEOPHONE TASK ************************************************************/
void vTaskGeo(void* arg)
{
    (void)arg;

    /* Configure and enable trigger */
    Geo_setTriggerThresholds(Geo.TrgThPos, Geo.TrgThNeg);

    if (Geo.TrgGain == TRG_GAIN_STAGE2)  { STAGE2_SWON(); vTaskDelay(pdMS_TO_TICKS(GEO_STAGE2_PWRON_DELAY_MS)); }
    else                                 { STAGE2_SWOFF(); }
    if (SysConf.OpMode & SYS_OPMODE_TRG) { Geo_enableTrigger(); }
    else                                 { Geo_disableTrigger(); }

    ADC_disableInterrupts();

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Geo task started");

#if !GEO_WAIT_FOR_TSYNC
    /* Start geophone sampling if continuous mode is enabled */
    if (SysConf.OpMode & SYS_OPMODE_CONT)
    {
        Geo_initAcq(TRG_EXT, 0, 0);
    }
#endif

    while (true)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        if (Geo.Status == GEO_ACQSTATUS_STOPPED) continue;

        /* Acquire the SPI bus that is necessary to talk to the ADCs */
        if (xSemaphoreTake(ADC_SPI_SEMAPHORE, pdMS_TO_TICKS(GEO_TASK_SEM_WAIT_MS)) == pdFALSE)
        {
            DEBUG_PRINT(DBG_LVL_ERROR, "Could not acquire SPI semaphore");
            Geo.Status = GEO_ACQSTATUS_STOPPED;
            continue;
        }
        /* Ensure that the inclinometer is powered on (but deasserted) since it is connected to the same SPI bus */
        //SCL_SWON();   /* TODO determine whether this is really necessary */

        /* Make sure the Geo Stop semaphore is taken */
        xSemaphoreTake(xSemaphore_GeoStop, 0);

        /* Configure ADCs and start sampling */
        ADC_start(false);       /* was (GEO_NUM_AXES > 1), but sync not really necessary since conversion start is only apart by a few microseconds, and sync delays start by 1 sample period */

        /* Enable interrupts to read the data (have to do it after sync, otherwise we first read unsynchronized data) */
        ADC_enableInterrupts();

#if IMU_SAMPLE_WITH_GEO
        /* Make sure IMU stop semaphore is taken */
        xSemaphoreTake(xSemaphore_ImuStop, 0);

        /* IMU Start */
        xTaskNotify(xTaskHandle_Imu, 1, eSetValueWithOverwrite);
        DEBUG_PRINT(DBG_LVL_VERBOSE, "IMU has been started");
#endif /* IMU_SAMPLE_WITH_GEO */

        uint32_t timeout_ticks = pdMS_TO_TICKS(S_TO_MS(Geo.Timeout) + 2000);      /* Add a small margin */
        if (SysConf.OpMode & SYS_OPMODE_CONT)
        {
            timeout_ticks = portMAX_DELAY;   /* In continuous mode, wait indefinitely */
        }
        do
        {
            /* Prevent changing working directory from now on */
            RTOS_queueSendLog(LOGTYPE_SD_PREVENTCHGDIR);

            /* Wait for Geo Stop Semaphore after all chunks have been acquired */
            if (xSemaphoreTake(xSemaphore_GeoStop, timeout_ticks) == pdFALSE)
            {
                /* Timeout */
                DEBUG_PRINT(DBG_LVL_ERROR, "Acquisition timed out while waiting for more chunks");
                RTOS_event(EVENT_GEOPHONE3X_ACQ_ERR_TIMEOUT, 0, EVTLOG_SD | EVTLOG_COM);
            }

            /* Push Geo struct ASAP */
            Geophone_t* pGeo = (Geophone_t*)RB_insert(&rb_geo, (uint8_t*)&Geo);

            /* End current acquisition, enabling next trigger to notify the task again */
            Geo.Status = GEO_ACQSTATUS_STOPPED;

#if IMU_SAMPLE_WITH_GEO
            /* Stop IMU */
            xSemaphoreGive(xSemaphore_ImuStop);
#endif /* IMU_SAMPLE_WITH_GEO */

            /* Log Geo */
            RTOS_queueSendLog(LOGTYPE_ACQ);

            /* Allow changing working directory from now on */
            RTOS_queueSendLog(LOGTYPE_SD_ALLOWCHGDIR);

            /* Continuous mode: trigger next acquisition */
            if (SysConf.OpMode & SYS_OPMODE_CONT)
            {
                Geo_initAcq(TRG_EXT, 0, 0);
            }

            /* Send acquisition message to Bolt */
            Geo_generateAcqMsg(pGeo);
        }
        while (SysConf.OpMode & SYS_OPMODE_CONT);

        /* Stop sampling */
        ADC_disableInterrupts();
        ADC_stop(true);   /* stop and power down ADCs */

        /* Notify if Inclinometer had to wait that it can use the SPI bus again */
        xSemaphoreGive(ADC_SPI_SEMAPHORE);

    } /* Geo task loop */
}
