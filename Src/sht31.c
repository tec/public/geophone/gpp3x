/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"


/* Functions -----------------------------------------------------------------*/

bool SHT31_init(void)
{
    /* Check if power enabled */
    if (!SHT_IS_SWON())
    {
        SHT_SWON();
        DEBUG_PRINT(DBG_LVL_VERBOSE, "SHT31 power on");
    }
    I2C2_init();
    HAL_Delay(SHT31_POWERON_DELAY_MS);  /* Delay anyway regardless of whether it was already enabled just in case SHT_SWON() is called by the application before SHT31_init() */
    return SHT31_reset(true);
}

bool SHT31_reset(bool soft)
{
    bool res;
    if (soft)
    {
        /* Soft reset */
        uint8_t cmd[2] = { SHT31_CMD_RESET_SOFT_MSB, SHT31_CMD_RESET_SOFT_LSB };
        res = (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS) == HAL_OK);
    }
    else
    {
        /* General reset */
        uint8_t cmd = SHT31_CMD_RESET_GENERAL;
        res = (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, &cmd, 1, SHT31_I2C_TIMEOUT_MS) == HAL_OK);
    }
    if (!res)
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "SHT31 reset failed");
    }
    HAL_Delay(SHT31_RESET_DELAY_MS);
    return res;
}

bool SHT31_measurementSingleShot(repeatability_mode_t repeatability, bool clock_stretching, uint8_t* result)
{
    uint8_t cmd[2] = { 0 };

    if (clock_stretching)
    {
        cmd[0] = SHT31_CMD_MEAS_SINGLE_CLK_STR_EN;

        switch (repeatability)
        {
            case HIGH:
            {
                cmd[1] = SHT31_CMD_MEAS_SINGLE_CLK_STR_EN_H;
                break;
            }
            case MEDIUM:
            {
                cmd[1] = SHT31_CMD_MEAS_SINGLE_CLK_STR_EN_M;
                break;
            }
            case LOW:
            {
                cmd[1] = SHT31_CMD_MEAS_SINGLE_CLK_STR_EN_L;
                break;
            }
            default:
                DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
        }
    }
    else
    {
        cmd[0] = SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS;

        switch (repeatability)
        {
            case HIGH:
            {
                cmd[1] = SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS_H;
                break;
            }
            case MEDIUM:
            {
                cmd[1] = SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS_M;
                break;
            }
            case LOW:
            {
                cmd[1] = SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS_L;
                break;
            }
            default:
            {
                DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
                break;
            }
        }
    }

    HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS);
    HAL_Delay(SHT31_MEASUREMENT_DELAY_MS);

    if (HAL_I2C_Master_Receive(&SHT_I2C, SHT31_ADDR, result, 6, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        // Check CRC
        if (crc8(result, 2, 0xff) != result[2] || crc8(&result[3], 2, 0xff) != result[5])
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 invalid data (CRC)");
            return false;
        }
        else
        {
            int32_t t_conv = SHT31_TEMP_C(  (result[0] << 8) | (result[1] & 0xFF));
            int32_t h_conv = SHT31_HUMIDITY((result[3] << 8) | (result[4] & 0xFF));
            SHT31_printTempHumidity(t_conv, h_conv);
            return true;
        }
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C read failed");
        return false;
    }
}

bool SHT31_measurementStart(repeatability_mode_t repeatability, measurements_per_second_t mps)
{
    uint8_t cmd[2] = { 0 };

    switch (mps)
    {
        case MSP_0_5:
        {
            cmd[0] = SHT31_CMD_MEAS_PERIODIC_MSP_0_5;

            switch (repeatability)
            {
                case HIGH:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_0_5_H;
                    break;
                }
                case MEDIUM:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_0_5_M;
                    break;
                }
                case LOW:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_0_5_L;
                    break;
                }
                default:
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
                    break;
                }
            }
            break;
        }
        case MSP_1:
        {
            cmd[0] = SHT31_CMD_MEAS_PERIODIC_MSP_1;

            switch (repeatability)
            {
                case HIGH:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_1_H;
                    break;
                }
                case MEDIUM:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_1_M;
                    break;
                }
                case LOW:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_1_L;
                    break;
                }
                default:
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
                    break;
                }
            }
            break;
        }
        case MSP_2:
        {
            cmd[0] = SHT31_CMD_MEAS_PERIODIC_MSP_2;

            switch (repeatability)
            {
                case HIGH:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_2_H;
                    break;
                }
                case MEDIUM:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_2_M;
                    break;
                }
                case LOW:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_2_L;
                    break;
                }
                default:
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
                    break;
                }
            }
            break;
        }
        case MSP_4:
        {
            cmd[0] = SHT31_CMD_MEAS_PERIODIC_MSP_4;

            switch (repeatability)
            {
                case HIGH:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_4_H;
                    break;
                }
                case MEDIUM:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_4_M;
                    break;
                }
                case LOW:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_4_L;
                    break;
                }
                default:
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
                    break;
                }
            }
            break;
        }
        case MSP_10:
        {
            cmd[0] = SHT31_CMD_MEAS_PERIODIC_MSP_10;

            switch (repeatability)
            {
                case HIGH:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_10_H;
                    break;
                }
                case MEDIUM:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_10_M;
                    break;
                }
                case LOW:
                {
                    cmd[1] = SHT31_CMD_MEAS_PERIODIC_MSP_10_L;
                    break;
                }
                default:
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
                    break;
                }
            }
            break;
        }
        default:
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 - invalid input");
            break;
        }
    }


    if (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "SHT31 started measurement");
        return true;
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C write failed");
        return false;
    }
}

bool SHT31_measurementRead(void)
{
    uint8_t result[6];
    uint8_t cmd[2] = { SHT31_CMD_MEAS_PERIODIC_READ_MSB, SHT31_CMD_MEAS_PERIODIC_READ_LSB };

    HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS);
    HAL_Delay(SHT31_MEASUREMENT_DELAY_MS);

    if (HAL_I2C_Master_Receive(&SHT_I2C, SHT31_ADDR, result, 6, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        // Check CRC
        if (crc8(result, 2, 0xff) != result[2] || crc8(&result[3], 2, 0xff) != result[5])
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 invalid data (CRC)");
            return false;
        }
        else
        {
            int32_t t_conv = SHT31_TEMP_C(  (result[0] << 8) | (result[1] & 0xFF));
            int32_t h_conv = SHT31_HUMIDITY((result[3] << 8) | (result[4] & 0xFF));
            SHT31_printTempHumidity(t_conv, h_conv);
            return true;
        }
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C read failed");
        return false;
    }
}

bool SHT31_measurementAccelerated(void)
{
    // Same as measurement with 4 Hz
    //SHT31_measurementStart(MSP_4, HIGH);

    uint8_t cmd[2] = { SHT31_CMD_MEAS_PERIODIC_ART_MSB, SHT31_CMD_MEAS_PERIODIC_ART_LSB };

    if (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "SHT31 started measurement in ART mode");
        return true;
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C write failed");
        return false;
    }
}

bool SHT31_measurementStop(void)
{
    uint8_t cmd[2] = { SHT31_CMD_MEAS_PERIODIC_STOP_MSB, SHT31_CMD_MEAS_PERIODIC_STOP_LSB };

    if (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "SHT31 stopped measurement");
        return true;
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C write failed");
        return false;
    }
}

bool SHT31_heaterSetStatus(uint32_t enable)
{
    uint8_t cmd[2] = { SHT31_CMD_HEATER_MSB, 0 };

    if (enable)
    {
        cmd[1] = SHT31_CMD_HEATER_EN_LSB;
    }
    else
    {
        cmd[1] = SHT31_CMD_HEATER_DIS_LSB;
    }

    if (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "SHT31 set heater to %i", enable);
        return true;
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C write failed");
        return false;
    }
}

bool SHT31_getStatus(void)
{
    uint8_t result[3];
    uint8_t cmd[2] = { SHT31_CMD_STATUS_READ_MSB, SHT31_CMD_STATUS_READ_LSB };

    HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS);
    HAL_Delay(SHT31_MEASUREMENT_DELAY_MS);

    if (HAL_I2C_Master_Receive(&SHT_I2C, SHT31_ADDR, result, 3, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        // Check CRC
        if (crc8(result, 2, 0xff) != result[2])
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 invalid data (CRC)");
            return false;
        }
        else
        {
            DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status read");

            uint16_t status = (result[0] << 8) | result[1];
            if (status & SHT31_STATUS_ALERT_PENDING)
            {
                DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status - Alert pending");
            }
            if (status & SHT31_STATUS_HEATER_EN)
            {
                DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status - Heater on");
            }
            if (status & SHT31_STATUS_RH_ALERT)
            {
                DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status - RH tracking alert");
            }
            if (status & SHT31_STATUS_T_ALERT)
            {
                DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status - T tracking alert");
            }
            if (status & SHT31_STATUS_RESET_DETECTED)
            {
                DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status - System reset detected");
            }
            if (status & SHT31_STATUS_CMD_UNSUCCESSFUL)
            {
                DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status - Last command not processed");
            }
            if (status & SHT31_STATUS_CRC_FAILED)
            {
                DEBUG_PRINT(DBG_LVL_INFO, "SHT31 status - Checksum failed");
            }

            return true;
        }
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C read failed");
        return false;
    }
}

bool SHT31_clearStatus(void)
{
    uint8_t cmd[2] = { SHT31_CMD_STATUS_CLEAR_MSB, SHT31_CMD_STATUS_CLEAR_LSB };

    if (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "SHT31 status cleared");
        return true;
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C write failed");
        return false;
    }
}

void SHT31_printTempHumidity(int16_t temp, uint16_t humidity)
{
    DEBUG_PRINTF(DBG_LVL_VERBOSE, "SHT31 Temperature: %d.%02u" SHT31_DEGREE_CELSIUS_SIGN ", Humidity: %d.%02u%%", temp / 100, temp % 100, humidity / 100, humidity % 100);
}

bool SHT31_readTempHumidity(int16_t* temp, uint16_t* humidity)
{
    uint8_t cmd[2] = { SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS, SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS_H };
    if (HAL_I2C_Master_Transmit(&SHT_I2C, SHT31_ADDR, cmd, 2, SHT31_I2C_TIMEOUT_MS) != HAL_OK)
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "I2C write failed");
        return false;
    }
    HAL_Delay(SHT31_MEASUREMENT_DELAY_MS);

    uint8_t  rcvbuf[6];
    uint32_t tries = SHT31_MEASUREMENT_TRIES;
    while (tries)
    {
        if (HAL_I2C_Master_Receive(&SHT_I2C, SHT31_ADDR, rcvbuf, 6, SHT31_I2C_TIMEOUT_MS) == HAL_OK)
        {
            break;
        }
        DEBUG_PRINT(DBG_LVL_ERROR, "Failed to read values");
        tries--;
    }
    if (!tries)
    {
        return false;
    }
    int16_t  t = SHT31_TEMP_C((rcvbuf[0] << 8) | (rcvbuf[1] & 0xFF));
    uint16_t h = SHT31_HUMIDITY((rcvbuf[3] << 8) | (rcvbuf[4] & 0xFF));

    if (temp)     { *temp = t;     }
    if (humidity) { *humidity = h; }

    return true;
}



