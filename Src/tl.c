/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Functions -----------------------------------------------------------------*/

/* Function to create a new element */
static tl_t* TL_newElement(uint32_t start, uint32_t period, uint32_t argument, uint8_t task)
{
    tl_t* temp = (tl_t*)malloc(sizeof(tl_t));
    if (!temp)
    {
        return NULL;
    }
    temp->start     = start;
    temp->period    = period;
    temp->argument  = argument;
    temp->task      = task;
    temp->next      = NULL;

    return temp;
}

/* Removes the given element from the list (if it exists) */
static void TL_removeElement(tl_t** head, tl_t* elem)
{
    tl_t* curr = *head;
    tl_t* prev = NULL;
    while (curr)
    {
        if (curr == elem)
        {
            /* Update link */
            if (prev)
            {
                prev->next = curr->next;
            }
            else
            {
                *head = curr->next;
            }
            free(elem);

            break;
        }
        prev = curr;
        curr = curr->next;
    }
}

/* Removes the element with the highest priority from the list */
static void TL_remove(tl_t** head)
{
    tl_t* to_remove = (*head);

    (*head) = (*head)->next;

    free(to_remove);
}

/* Function to push according to priority */
bool TL_insert(tl_t** head, uint32_t start, uint32_t period, uint32_t argument, uint8_t task)
{
    tl_t* curr_elem  = (*head);

    /* Create new element */
    tl_t* new_elem = TL_newElement(start, period, argument, task);
    if (!new_elem)
    {
        return false;
    }

    /* When list is empty, place element at beginning */
    if (TL_isEmpty(head))
    {
        (*head) = new_elem;
    }
    /* Special Case: The head of list has higher start time than new element. So insert new element before head element and change head element. */
    else if ((*head)->start > start)
    {
        /* Insert new element before head */
        new_elem->next = *head;
        (*head)        = new_elem;
    }
    else
    {
        /* Traverse the list and find a position to insert new element */
        while (curr_elem->next && (curr_elem->next)->start < start)
        {
            curr_elem = curr_elem->next;
        }
        new_elem->next  = curr_elem->next;
        curr_elem->next = new_elem;
    }

    return true;
}

/* Function to remove highest priority element from queue and if it is periodic (period not 0), push next occurrence to queue
 * returns true if the task has been reinserted successfully, false otherwise */
bool TL_popPush(tl_t** head, uint32_t current_time)
{
    if (TL_isEmpty(head)) { return false; }

    uint32_t start    = (*head)->start;
    uint32_t period   = (*head)->period;
    uint32_t argument = (*head)->argument;
    uint8_t  task     = (*head)->task;

    /* Remove old task from queue */
    TL_remove(head);

    /* Add again for periodic measurements */
    if (period > 0)
    {
        /* If we are more than one period away from the next measurement (e.g. with an initial time set as 0), we do not want to step through every single period until then */
        if ( (start + period) <= current_time )
        {
            start += ((current_time - start) / period + 1) * period;
        }
        else
        {
            start += period;
        }
        /* Add new entry */
        return TL_insert(head, start, period, argument, task);
    }
    return false;
}

bool TL_isEmpty(tl_t** head)
{
    return (*head) == NULL;
}

bool TL_contains(tl_t** head, uint8_t task)
{
    tl_t* curr = *head;
    while (curr)
    {
        if (curr->task == task)
        {
            return true;
        }
        curr = curr->next;
    }
    return false;
}

void TL_clear(tl_t** head)
{
    while (*head)
    {
        TL_remove(head);
    }
}

/* move the start time of the given tasks to after 'until' (periodic tasks only, single execution tasks will be skipped)
 * returns the number of postponed tasks */
uint32_t TL_postpone(tl_t** head, uint8_t task, uint32_t until)
{
    uint32_t cnt  = 0;
    tl_t*    curr = *head;

    while (curr)
    {
        if ( (curr->task == task) && (until > curr->start) )  /* only one task per entry, therefore must be equal (also to avoid a match if the postpone flag is set) */
        {
            if (curr->period)
            {
                uint32_t start    = curr->start + ( (until - curr->start) / curr->period + 1 ) * curr->period;
                uint32_t period   = curr->period;
                uint32_t argument = curr->argument;
                TL_insert(head, start, period, argument, task);
            }
            tl_t* to_remove = curr;
            curr = curr->next;
            TL_removeElement(head, to_remove);
            cnt++;
        }
        else
        {
            curr = curr->next;
        }
    }

    return cnt;
}

uint32_t TL_setPeriod(tl_t** head, uint8_t task, uint32_t period)
{
    uint32_t cnt  = 0;
    tl_t*    curr = *head;

    while (curr)
    {
        if (curr->task == task)
        {
            curr->period = period;
            cnt++;
        }
        curr = curr->next;
    }

    return cnt;
}
