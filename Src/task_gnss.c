/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

#if USE_GNSS

/* Variables -----------------------------------------------------------------*/
static uint32_t     gnss_num_samples = 0;    /* Number of consecutive raw messages to receive when GNSS is enabled */
static uint32_t     gnss_num_tp      = 0;    /* Number of TP messages to collect */

/* External variables --------------------------------------------------------*/
extern GNSSStat_t   GNSS;
extern rb_t         rb_gnssdata;

/* Private Functions ---------------------------------------------------------*/
static uint32_t ProcessUBXMsg(gnss_ubx_msg_t* ubx_msg);

/* Functions -----------------------------------------------------------------*/

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    if (huart->Instance != USART1 || !huart->hdmarx) return;   /* Wrong handle or DMA not properly linked */

    BaseType_t     xHigherPriorityTaskWoken = pdFALSE;
    const uint16_t curr_cnt                 = __HAL_DMA_GET_COUNTER(huart->hdmarx);   /* Note: this counter contains the # remaining samples until 'DMA transfer complete' */
    uint16_t       prev_cnt                 = GNSS.dma_cntr;

    if (prev_cnt != curr_cnt)
    {
        /* New data has arrived */
        /* Check for overflow: if rx_len not yet cleared, then GNSS task is still processing the previous message */
        if (GNSS.rx_len)
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "RX buffer overflow");
            RTOS_event(EVENT_GEOPHONE3X_GNSS_ERR, GNSS_ERR_RXBUFFER_OVF, EVTLOG_SD | EVTLOG_COM);
        }
        /* Copy data to GNSS buffer and notify task */
        uint32_t ofs = GNSS_RX_BUFFER_SIZE - prev_cnt;     /* Start of the message in the DMA buffer */
        GNSS.rx_len  = 0;
        if (curr_cnt > prev_cnt)
        {
            /* Handle buffer wrap-around */
            memcpy(GNSS.rx_buf, &GNSS.dma_buf[ofs], prev_cnt);
            GNSS.rx_len += prev_cnt;
            prev_cnt     = GNSS_RX_BUFFER_SIZE;
            ofs          = 0;
        }
        uint32_t rem = prev_cnt - curr_cnt;
        memcpy(&GNSS.rx_buf[GNSS.rx_len], &GNSS.dma_buf[ofs], rem);
        GNSS.rx_len += rem;
        if (IS_RTOS_STARTED())
        {
            xTaskNotifyFromISR(xTaskHandle_GNSS, GNSS_NOTIFY_RX, eSetValueWithoutOverwrite, &xHigherPriorityTaskWoken);
        }
    }
    GNSS.dma_cntr = curr_cnt;

    /* If xHigherPriorityTaskWoken is now set to pdTRUE then a context switch
       should be performed to ensure the interrupt returns directly to the highest
       priority task */
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/* UART & DMA Error Callback -------------------------------------------------*/
void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    if (huart->Instance == USART1)
    {
        if (IS_RTOS_STARTED())
        {
            xTaskNotifyFromISR(xTaskHandle_GNSS, GNSS_NOTIFY_UARTERR, eSetValueWithoutOverwrite, &xHigherPriorityTaskWoken);
        }
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "UART error");
    }
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


/*** GNSS TASK ****************************************************************/
void vTaskGNSS(void* arg)
{
    (void)arg;

    DEBUG_PRINT(DBG_LVL_VERBOSE, "GNSS task started");

    while (true)
    {
        uint32_t notification = ulTaskNotifyTake(pdTRUE, portMAX_DELAY);   /* Wait for a non-zero notification */
        uint32_t start_ts     = TIM5_getCounterValue();

        switch (notification)
        {
        case GNSS_NOTIFY_SAMPLE:
            gnss_num_samples = GNSS_NUM_OF_SAMPLES;
            GNSS_enable();
            break;

        case GNSS_NOTIFY_TSYNC_INIT:
            /* Values need to be reset before enabling the GNSS module (erratic spikes can appear on TP pin) */
            GNSS.last_sync_ts = 0;
            GNSS.tpulse_cnt   = 0;
            gnss_num_tp       = GNSS_NUM_OF_TP;   /* At least GNSS_NUM_OF_TP TP messages are required */
            GNSS_enable();
            break;

        case GNSS_NOTIFY_TSYNC_ABORT:
            gnss_num_tp = 0;
            break;

        case GNSS_NOTIFY_UARTERR:
            DEBUG_PRINT(DBG_LVL_WARNING, "UART error");
            RTOS_event(EVENT_GEOPHONE3X_GNSS_ERR, GNSS_ERR_UART_DMA, EVTLOG_SD | EVTLOG_COM);
            GNSS_reinitUART(true);
            break;

        /* A UBX message is in the RX buffer and needs to be processed */
        case GNSS_NOTIFY_RX:
        {
            /* Verify validity */
            if (GNSS.rx_buf == NULL || GNSS.rx_len < GNSS_MSG_LEN_MIN)
            {
                DEBUG_PRINT(DBG_LVL_WARNING, "Invalid GNSS message");
                RTOS_event(EVENT_GEOPHONE3X_GNSS_ERR, GNSS_ERR_MSG_LEN, EVTLOG_SD | EVTLOG_COM);
                GNSS.rx_len = 0;
                break;
            }

            /* Buffer may contain more than one message */
            uint8_t* curr_msg = GNSS.rx_buf;
            while (GNSS.rx_len)
            {
                uint32_t proc_len = ProcessUBXMsg((gnss_ubx_msg_t*)curr_msg);
                if (!proc_len)
                {
                    GNSS.rx_len = 0;
                    break;
                }
                /* Go to the next message in the buffer */
                curr_msg += proc_len;
                taskENTER_CRITICAL();
                GNSS.rx_len -= proc_len;
                taskEXIT_CRITICAL();
            }
            break;
        }

        case GNSS_NOTIFY_STOP:
            gnss_num_samples = 0;
            gnss_num_tp      = 0;
            DEBUG_PRINT(DBG_LVL_INFO, "stop requested");
            break;

        default:
            /* invalid task notification value */
            break;
        }

        /* Turn off GNSS if there are no more samples to collect and time is synced (at least 2 TP received) */
        if (!gnss_num_samples && !gnss_num_tp)
        {
#if GNSS_PPS_LED_ON
            LED_G_OFF();
#endif
            GNSS_disable();
#if !GNSS_ALWAYS_ON
            GNSS_powerOff();
#endif
        }

        /* Check task runtime */
        uint32_t time_delta_ms = TIM5_TICKS_TO_MS(TIM5_getCounterValue() - start_ts);
        if (time_delta_ms > GNSS_RUNTIME_WARN_TH_MS)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "Task runtime longer than expected (%4lums)", time_delta_ms);
            RTOS_event(EVENT_GEOPHONE3X_TASK_RUNTIME, (time_delta_ms << 16) | RTOS_TASK_ID_GNSS, EVTLOG_SD | EVTLOG_COM);
        }

    } /* GNSS task loop */
}


static uint32_t ProcessUBXMsg(gnss_ubx_msg_t* ubx_msg)
{
    /* Verify UBX message format */
    if ( (ubx_msg->header[0] != GNSS_MSG_SYNC_CHAR_1) || (ubx_msg->header[1] != GNSS_MSG_SYNC_CHAR_2) )
    {
        DEBUG_PRINT(DBG_LVL_INFO, "Received unknown packet type (non-UBX)");
        RTOS_event(EVENT_GEOPHONE3X_GNSS_ERR, GNSS_ERR_MSG_HDR, EVTLOG_SD | EVTLOG_COM);
        return 0;
    }

    uint8_t  msg_class    = ubx_msg->class;
    uint8_t  msg_id       = ubx_msg->id;
    uint32_t payload_size = ubx_msg->len;
    uint32_t msg_len      = GNSS_MSG_LEN(ubx_msg);
    //DEBUG_PRINTF(DBG_LVL_VERBOSE, "UBX message (Class: 0x%02x, ID: 0x%02x, Len: %u)", msg_class, msg_id, msg_len);

    if (!GNSS_checkCRC(ubx_msg, msg_len))
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "Invalid CRC (Class: 0x%x, ID: 0x%x, len: %u)", msg_class, msg_id, payload_size);
        RTOS_event(EVENT_GEOPHONE3X_GNSS_CRC_ERR, (payload_size << 16) | (msg_id << 8) | msg_class, EVTLOG_SD | EVTLOG_COM);
        return 0;
    }

    switch (msg_class)
    {
    case GNSS_MSG_CLASS_ACK:
    {
        if (msg_id == GNSS_MSG_ID_ACK_NAK)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "NAK received (Class: 0x%02x, ID: 0x%02x)", msg_class, msg_id);
            RTOS_event(EVENT_GEOPHONE3X_GNSS_ERR, GNSS_ERR_NAK, EVTLOG_SD | EVTLOG_COM);
        }
        /* else: GNSS_MSG_ID_ACK_ACK (valid) */
        break;
    }
    case GNSS_MSG_CLASS_RXM:
    {
        if (msg_id == GNSS_MSG_ID_RXM_RAWX)
        {
            DEBUG_PRINTF(DBG_LVL_VERBOSE, "Time: %lu, TP: %u, numMeas: %u", GNSS.last_sync_ts, GNSS.tpulse_cnt, ubx_msg->rxm_rawx.numMeas);

            if ( gnss_num_samples && (ubx_msg->rxm_rawx.numMeas >= GNSS_NUM_MEAS_MIN) )
            {
                /* Log GNSS Data */
                memcpy(RB_insert(&rb_gnssdata, 0), (uint8_t*)ubx_msg, msg_len);
                RTOS_queueSendLog(LOGTYPE_GNSS);
                gnss_num_samples--;
            }
            GNSS_updateLeapSecs(ubx_msg);
        }
        break;
    }
    case GNSS_MSG_CLASS_TIM:
    {
        uint32_t timestamp = 0;
        if (msg_id == GNSS_MSG_ID_TIM_TP)
        {
            timestamp = GNSS_timestampFromTP(ubx_msg);
        }
        else if (msg_id == GNSS_MSG_ID_TIM_TM2)
        {
            timestamp = GNSS_timestampFromTM(ubx_msg);
        }
        else
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "Unknown GNSS_MSG_CLASS_TIM message ID");
        }
        if (timestamp && gnss_num_tp)
        {
            GNSS.last_sync_ts = timestamp;
#if TSYNC_METHOD_GNSS
            TSync_setNextEpoch(timestamp);
#endif
            //DEBUG_PRINTF(DBG_LVL_INFO, "GNSS sync: %lu", timestamp);
            gnss_num_tp--;
        }
        break;
    }
    case GNSS_MSG_CLASS_INF:
    {
        if (msg_id == GNSS_MSG_ID_INF_WARNING)
        {
            /* Print WARNING */
            ubx_msg->inf_warning.str[payload_size] = 0;  /* make sure string is zero-terminated */
            DEBUG_PRINTF(DBG_LVL_WARNING, "Warning message: %s", ubx_msg->inf_warning.str);
            RTOS_event(EVENT_GEOPHONE3X_GNSS_ERR, GNSS_ERR_WARNING, EVTLOG_SD | EVTLOG_COM);
        }
        break;
    }
    case GNSS_MSG_CLASS_MON:
    {
        /* Version is checked during self-testing; no action required */
        break;
    }
    default:
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Received unknown UBX packet over UART1 (Class: %u, ID: %u)", msg_class, msg_id);
        break;
    }

    return msg_len;
}

#endif /* USE_GNSS */
