/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
extern SysHealth_t    SysHealth;
extern Geophone_t     Geo;
extern ImuStat_t      Imu;

/* Ring Buffers --------------------------------------------------------------*/
extern rb_t           rb_event;
extern rb_t           rb_bolt;
extern rb_t           rb_cmd;

/* RTOS Task Handles ---------------------------------------------------------*/
TaskHandle_t          xTaskHandle_Geo     = NULL;
#if BOLT_ENABLE
TaskHandle_t          xTaskHandle_Bolt    = NULL;
#endif
TaskHandle_t          xTaskHandle_Health  = NULL;
TaskHandle_t          xTaskHandle_Imu     = NULL;
#if USE_INCLINO
TaskHandle_t          xTaskHandle_Inclino = NULL;
#endif
#if USE_GNSS
TaskHandle_t          xTaskHandle_GNSS    = NULL;
#endif
#if !TSYNC_METHOD_NONE || TSYNC_MASTER
TaskHandle_t          xTaskHandle_TSync   = NULL;
#endif
TaskHandle_t          xTaskHandle_Log     = NULL;
#if !STREAMING_TARGET_NONE
TaskHandle_t          xTaskHandle_Stream  = NULL;
#endif
TaskHandle_t          xTaskHandle_Schedule = NULL;

/* RTOS Semaphore Handles ----------------------------------------------------*/
#if SD_USE_SEMAPHORES
SemaphoreHandle_t     xSemaphore_SDRead   = NULL;
SemaphoreHandle_t     xSemaphore_SDWrite  = NULL;
#endif
SemaphoreHandle_t     xSemaphore_SD       = NULL;
SemaphoreHandle_t     xSemaphore_GeoStop  = NULL;
SemaphoreHandle_t     xSemaphore_ImuStop  = NULL;
SemaphoreHandle_t     xSemaphore_SPI1     = NULL;
SemaphoreHandle_t     xSemaphore_SPI2     = NULL;
SemaphoreHandle_t     xSemaphore_Schedule = NULL;

/* RTOS Queue Handles --------------------------------------------------------*/
QueueHandle_t         xQueue_Log          = NULL;

/* RTOS Timer Handles --------------------------------------------------------*/
TimerHandle_t         xTimer_Button       = NULL;
TimerHandle_t         xTimer_Buzzer       = NULL;
TimerHandle_t         xTimer_LED          = NULL;

/* Private variables ---------------------------------------------------------*/
static volatile bool  core_stopped        = false;
static volatile bool  err_pending         = false;
static uint16_t       buzzer_toggle_cnt   = 0;
static uint16_t       led_toggle_cnt      = 0;
static led_t          led_to_toggle;

/* RTOS Task function prototypes ---------------------------------------------*/
extern void vTaskGeo(void* arg);
extern void vTaskBolt(void* arg);
extern void vTaskHealth(void* arg);
extern void vTaskLog(void* arg);
extern void vTaskImu(void* arg);
extern void vTaskInclino(void* arg);
extern void vTaskGNSS(void* arg);
extern void vTaskTsync(void* arg);
extern void vTaskStream(void* arg);
extern void vTaskSchedule(void* arg);

/* RTOS function prototypes --------------------------------------------------*/
void vApplicationDaemonTaskStartupHook(void);
void vApplicationIdleHook(void);
void vApplicationMallocFailedHook(void);
void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName);
void vTimerBTNCallback(TimerHandle_t xTimer);
void vTimerBuzzerCallback(TimerHandle_t xTimer);
extern TickType_t  GetExpectedIdleTime(void);
extern UBaseType_t IsDelayedTaskListEmpty(void);

/* Private functions ---------------------------------------------------------*/
void RTOS_processCommands(void);
void RTOS_enterStopMode(void);
void RTOS_USBMSC(bool mount);

/*** RTOS Timer Daemon Startup Hook *******************************************/
void vApplicationDaemonTaskStartupHook(void)
{
    static dpp_node_info_t dpp_node_info = { 0 };   /* static to reduce stack usage */

    /* Compose Node Info message */
    dpp_node_info.component_id = DPP_COMPONENT_ID_GEO3X;
    dpp_node_info.rst_flag     = SysConf.SysHealth->ResetFlags;
    dpp_node_info.compiler_ver = INFO_COMPILER_VER;
    dpp_node_info.compile_date = INFO_FW_BUILD_TIME;
    dpp_node_info.fw_ver       = (INFO_FW_VER_MAJOR * 10000) + (INFO_FW_VER_MINOR * 100) + INFO_FW_VER_PATCH;
    dpp_node_info.sw_rev_id    = INFO_FW_REV;
    dpp_node_info.config       = (SysConf.OpMode & SYS_OPMODE_ALL)                        |   /* 4 bits for operating mode */
                                 ((uint32_t)(SysConf.SysHealth->ConfigFromSD & 0x1) << 4);    /* 1 bit for 'valid config' */
    memcpy(dpp_node_info.mcu_desc,      INFO_MCU_DESC, strlen(INFO_MCU_DESC));
    memcpy(dpp_node_info.compiler_desc, INFO_COMPILER_DESC, strlen(INFO_COMPILER_DESC));
    memcpy(dpp_node_info.fw_name,       INFO_FW_NAME, strlen(INFO_FW_NAME));

    Bolt_enqueueMessage(DPP_MSG_TYPE_NODE_INFO, (uint8_t*)&dpp_node_info);

    /* Set startup time for scheduler interrupt */
#if SYSTEM_WAKEUP_PERIOD || ADC_SYNC_PERIOD
    RTC_startWakeUpTimer(ADC_SYNC_PERIOD ? ADC_SYNC_PERIOD : SYSTEM_WAKEUP_PERIOD);    /* ADC_SYNC_PERIOD has priority */
#endif

    /* Application Start Event */
    RTOS_event(EVENT_GEOPHONE3X_START, dpp_node_info.rst_flag, EVTLOG_SD | EVTLOG_COM);

    DEBUG_PRINT(DBG_LVL_VERBOSE, "RTOS daemon task started");

    /* Application is started */
    RTOS_buzzer(BUZZER_TIME_STARTUP_MS, 1);
}

/*** RTOS Idle Task Hook ******************************************************/
void vApplicationIdleHook(void)
{
    WDT_POLL();

    /* Perform operations that need to be done when device is `idle`,
     * i.e. device is ready to go enter STOP mode */

#if DEBUG_USB_MSC_ON_DBGPIN2
    /* Check if SD card needs to be mounted via USB */
    if (DEBUG_PIN2_STATE())
    {
        RTOS_USBMSC(true);
        HAL_Delay(1000);
    }
    else
    {
        RTOS_USBMSC(false);
    }
#elif USB_MSC_AUTOMOUNT
    if (USB_IS_CONNECTED())
    {
        RTOS_USBMSC(true);
        HAL_Delay(1000);
    }
    else
    {
        RTOS_USBMSC(false);
    }
#endif

    /* Check for App Commands to execute */
    if (!RB_isEmpty(&rb_cmd))
    {
        RTOS_processCommands();
    }

    if (err_pending)
    {
        portENTER_CRITICAL();
        SD_eject();
        Error_Handler();
    }

#if USE_STOP2
    /* Check if device is ready to enter STOP mode */
    uint32_t exp_idle_time = GetExpectedIdleTime();
    if ( (exp_idle_time > pdMS_TO_TICKS(IDLE_TIME_BEFORE_SLEEP)) &&
         (SysConf.Geo->Status == GEO_ACQSTATUS_STOPPED) &&
         (!Imu.Status) &&
         (!SysConf.Inclino->Status) &&
         (SysConf.GNSS->state <= GNSS_DISABLED) &&
         (SysConf.USB->deviceClass == NONE) &&
         (IsDelayedTaskListEmpty()) )
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Enter stop mode");

        /* Update duty-cycle counter */
        RTOS_dutyCycle(DC_CNTR_STOP);

        /* Enter STOP2 (and re-enable interrupts) */
        RTOS_enterStopMode();

        /* Restore after exiting STOP2 */
        RTOS_resumeFromStopMode();

        /* Poll the watchdog */
        WDT_POLL();
    }
#endif
}

/*** TICKLESS IDLE HOOKS ******************************************************/
void vPreSleepProcessing(uint32_t ulExpectedIdleTime)
{
    (void)ulExpectedIdleTime;
    HAL_SuspendTick();
    RTOS_dutyCycle(DC_CNTR_PAUSE);
}

void vPostSleepProcessing(uint32_t ulExpectedIdleTime)
{
    (void)ulExpectedIdleTime;
    RTOS_dutyCycle(DC_CNTR_RESUME);
    HAL_ResumeTick();
}

/*** BTN Timer Callback *******************************************************/
void vTimerButtonCallback(TimerHandle_t xTimer)
{
    static uint8_t btn_duration = 0;

    btn_duration++;
    if ( BTN_IS_PRESSED() )
    {
        if ( (btn_duration > RTOS_TIMER_S_TO_TICKS(BTN_PRESSED_MAX_DURATION_S)) )
        {
            LED_ALL_ON();
        }
        xTimerStart(xTimer, pdMS_TO_TICKS(RTOS_TIMER_MAXWAIT_MS));
        return;
    }
    /* Button released */
    LED_ALL_OFF();

#if LPM_ON_USER_BUTTON
    if (SysConf.OpMode)
    {
        SysConf.OpMode = 0;     /* Stop all operations */
        Schedule_clear(false);  /* Remove all scheduled tasks */
        Geo_abort(false);
        NOTIFY_GNSS_TASK(GNSS_NOTIFY_STOP);
        TSync_timeout();
        DEBUG_PRINT(DBG_LVL_INFO, "Request to enter low-power mode");
        RTOS_event(EVENT_GEOPHONE3X_TASKS_STOPPED, 0, EVTLOG_SD | EVTLOG_COM);
    #if USE_BUZZER
        RTOS_buzzer(BUZZER_TIME_SLEEP_MS, 3);
    #else
        RTOS_blink(LED_GREEN, 300, 3);
    #endif
    }
#else
    if (btn_duration >= RTOS_TIMER_S_TO_TICKS(BTN_USBMOUNT_DURATION_S))
    {
        /* Mount USB mass storage device */
        RTOS_USBMSC(true);
    }
    else
    {
        /* Request Timesync */
        NOTIFY_TSYNC_TASK();

        /* Make sure USB MSC is unmounted */
        RTOS_USBMSC(false);

        LED_G_ON();
        HAL_Delay(50);
        LED_G_OFF();
        RTOS_buzzer(BUZZER_TIME_USBMSC_MS, 1);
    }
#endif
    btn_duration = 0;
}

/*** Buzzer Timer Callback ****************************************************/
void vTimerBuzzerCallback(TimerHandle_t xTimer)
{
    if (buzzer_toggle_cnt)
    {
        BUZZER_TG();
        buzzer_toggle_cnt--;
        xTimerStart(xTimer, pdMS_TO_TICKS(RTOS_TIMER_MAXWAIT_MS));
    }
    else
    {
        BUZZER_OFF();   /* should not happen; nevertheless, disable the buzzer just in case */
    }
}

/*** LED Timer Callback *******************************************************/
void vTimerLEDCallback(TimerHandle_t xTimer)
{
    if (led_toggle_cnt)
    {
        switch (led_to_toggle)
        {
        case LED_RED:    LED_R_TG();         break;
        case LED_YELLOW: LED_Y_TG();         break;
        case LED_GREEN:  LED_G_TG();         break;
        case LED_BLUE:   LED_B_TG();         break;
        default:         led_toggle_cnt = 0; break;
        }
        led_toggle_cnt--;
        xTimerStart(xTimer, pdMS_TO_TICKS(RTOS_TIMER_MAXWAIT_MS));
    }
    else
    {
        LED_ALL_OFF();   /* should not happen; nevertheless, turn off all LEDs just in case */
    }
}


/* RTOS functions ------------------------------------------------------------*/
void RTOS_init(void)
{
    /* RTOS Tasks */
    if (xTaskCreate(vTaskGeo,                           /* Task function */
                    "task_geo",                         /* Task name */
                    RTOS_TASK_STACK_GEO,                /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_GEO,                 /* Task priority */
                    &xTaskHandle_Geo) != pdPASS)        { Error_Handler(); }

    if (xTaskCreate(vTaskImu,                           /* Task function */
                    "task_imu",                         /* Task name */
                    RTOS_TASK_STACK_IMU,                /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_IMU,                 /* Task priority */
                    &xTaskHandle_Imu)!= pdPASS)         { Error_Handler(); }

#if USE_INCLINO
    if (xTaskCreate(vTaskInclino,                       /* Task function */
                    "task_inclino",                     /* Task name */
                    RTOS_TASK_STACK_INCLINO,            /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_INCLINO,             /* Task priority */
                    &xTaskHandle_Inclino)!= pdPASS)     { Error_Handler(); }
#endif

#if USE_GNSS
    if (xTaskCreate(vTaskGNSS,                          /* Task function */
                    "task_gnss",                        /* Task name */
                    RTOS_TASK_STACK_GNSS,               /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_GNSS,                /* Task priority */
                    &xTaskHandle_GNSS)!= pdPASS)        { Error_Handler(); }
#endif

    if (xTaskCreate(vTaskHealth,                        /* Task function */
                    "task_health",                      /* Task name */
                    RTOS_TASK_STACK_HEALTH,             /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_HEALTH,              /* Task priority */
                    &xTaskHandle_Health)!= pdPASS)      { Error_Handler(); }

#if BOLT_ENABLE
    if (xTaskCreate(vTaskBolt,                          /* Task function */
                    "task_bolt",                        /* Task name */
                    RTOS_TASK_STACK_BOLT,               /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_BOLT,                /* Task priority */
                    &xTaskHandle_Bolt)!= pdPASS)        { Error_Handler(); }
#endif

#if !TSYNC_METHOD_NONE || TSYNC_MASTER
    if (xTaskCreate(vTaskTsync,                         /* Task function */
                    "task_tsync",                       /* Task name */
                    RTOS_TASK_STACK_TSYNC,              /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_TSYNC,               /* Task priority */
                    &xTaskHandle_TSync)!= pdPASS)       { Error_Handler(); }
#endif

    if (xTaskCreate(vTaskLog,                           /* Task function */
                    "task_log",                         /* Task name */
                    RTOS_TASK_STACK_LOG,                /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_LOG,                 /* Task priority */
                    &xTaskHandle_Log) != pdPASS)        { Error_Handler(); }

#if !STREAMING_TARGET_NONE
    if (xTaskCreate(vTaskStream,                        /* Task function */
                    "task_stream",                      /* Task name */
                    RTOS_TASK_STACK_STREAM,             /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_STREAM,              /* Task priority */
                    &xTaskHandle_Stream) != pdPASS)     { Error_Handler(); }
#endif

    if (xTaskCreate(vTaskSchedule,                      /* Task function */
                    "task_schedule",                    /* Task name */
                    RTOS_TASK_STACK_SCHEDULE,           /* Stack size (32bit) */
                    NULL,                               /* Arguments */
                    RTOS_TASK_PRIO_SCHEDULE,            /* Task priority */
                    &xTaskHandle_Schedule) != pdPASS)   { Error_Handler(); }

    /* RTOS Semaphores */

#if SD_USE_SEMAPHORES
    xSemaphore_SDRead = xSemaphoreCreateBinary();        /* Used by the SD driver to signal DMA transfer complete (only if SD_USE_SEMAPHORES is enabled) */
    if (!xSemaphore_SDRead)  { Error_Handler(); }

    xSemaphore_SDWrite = xSemaphoreCreateBinary();       /* Used by the SD driver to signal DMA transfer complete (only if SD_USE_SEMAPHORES is enabled) */
    if (!xSemaphore_SDWrite) { Error_Handler(); }
#endif

    xSemaphore_SD = xSemaphoreCreateBinary();            /* Ensures exclusive access to the SD card */
    if (!xSemaphore_SD)  { Error_Handler(); }
    xSemaphoreGive(xSemaphore_SD);

    xSemaphore_GeoStop = xSemaphoreCreateBinary();
    if (!xSemaphore_GeoStop) { Error_Handler(); }

    xSemaphore_ImuStop = xSemaphoreCreateBinary();
    if (!xSemaphore_ImuStop) { Error_Handler(); }

    xSemaphore_SPI1 = xSemaphoreCreateBinary();
    if (!xSemaphore_SPI1)    { Error_Handler(); }
    /* ATTENTION: As the SPI bus is free after initialization, we give this semaphore directly after creation so the bus can be used */
    xSemaphoreGive(xSemaphore_SPI1);

    xSemaphore_SPI2 = xSemaphoreCreateBinary();
    if (!xSemaphore_SPI2)    { Error_Handler(); }
    xSemaphoreGive(xSemaphore_SPI2);

    xSemaphore_Schedule = xSemaphoreCreateBinary();
    if (!xSemaphore_Schedule) { Error_Handler(); }
    xSemaphoreGive(xSemaphore_Schedule);

    /* RTOS Queues */
    xQueue_Log = xQueueCreate(RTOS_QUEUE_LOG_SIZE, sizeof(LogType_t));
    if (!xQueue_Log) { Error_Handler(); }
    vQueueAddToRegistry(xQueue_Log, "queue_log");

    /* RTOS Timers */
    xTimer_Button = xTimerCreate("timer_button", pdMS_TO_TICKS(RTOS_TIMER_PERIOD_MS), pdFALSE, (void*)0, vTimerButtonCallback);
    xTimer_Buzzer = xTimerCreate("timer_buzzer", pdMS_TO_TICKS(RTOS_TIMER_PERIOD_MS), pdFALSE, (void*)0, vTimerBuzzerCallback);
    xTimer_LED    = xTimerCreate("timer_led",    pdMS_TO_TICKS(RTOS_TIMER_PERIOD_MS), pdFALSE, (void*)0, vTimerLEDCallback);
}

void RTOS_buzzer(uint16_t period_ms, uint16_t cnt)
{
    if (!pdMS_TO_TICKS(period_ms) || !cnt) { return; }

    if (xTimerIsTimerActive(xTimer_Buzzer) != pdFALSE)
    {
        xTimerStop(xTimer_Buzzer, pdMS_TO_TICKS(RTOS_TIMER_MAXWAIT_MS));
    }
    buzzer_toggle_cnt = cnt * 2 - 1;
    xTimerChangePeriod(xTimer_Buzzer, pdMS_TO_TICKS(period_ms), pdMS_TO_TICKS(RTOS_TIMER_MAXWAIT_MS));
    BUZZER_ON();
}

void RTOS_blink(led_t led, uint16_t period_ms, uint16_t cnt)
{
    if (led > NUM_LEDS || !pdMS_TO_TICKS(period_ms) || !cnt) { return; }

    if (xTimerIsTimerActive(xTimer_LED) != pdFALSE)
    {
        xTimerStop(xTimer_LED, pdMS_TO_TICKS(RTOS_TIMER_MAXWAIT_MS));
    }
    led_toggle_cnt = cnt * 2 - 1;
    led_to_toggle  = led;
    xTimerChangePeriod(xTimer_LED, pdMS_TO_TICKS(period_ms), pdMS_TO_TICKS(RTOS_TIMER_MAXWAIT_MS));

    switch (led)
    {
    case LED_RED:    LED_R_ON(); break;
    case LED_YELLOW: LED_Y_ON(); break;
    case LED_GREEN:  LED_G_ON(); break;
    case LED_BLUE:   LED_B_ON(); break;
    default: break;
    }
}

void RTOS_event(dpp_event_type_t type, uint32_t value, uint8_t flag)
{
    dpp_event_t evt;
    evt.type  = (dpp_event_type_t)type;
    evt.value = value;

    if (!IS_RTOS_STARTED())
    {
        return;
    }

    if (flag & EVTLOG_SD)
    {
        RB_insert(&rb_event, (uint8_t*)&evt);
        if (IS_ISR()) { RTOS_queueSendLogFromISR(LOGTYPE_EVENT, 0); }
        else          { RTOS_queueSendLog(LOGTYPE_EVENT);           }
    }

    if (flag & EVTLOG_COM)
    {
        Bolt_enqueueMessage(DPP_MSG_TYPE_EVENT, (uint8_t*)&evt);
    }

    DEBUG_PRINTF(DBG_LVL_INFO, "Event type: 0x%x, value: %u", evt.type, evt.value);
}

void RTOS_processCommands(void)
{
    uint32_t write_cfg = 0;        /* Flag: set to 1 when config needs to be updated on SD card */

    /* Process all commands */
    while (!RB_isEmpty(&rb_cmd))
    {
        /* Get Command from RB */
        dpp_command_t* command = (dpp_command_t *)RB_getTail(&rb_cmd);
        bool           unknown = false;

        /* Execute Command */
        switch (command->type)
        {
        case CMD_GEOPHONE_RESET:
            /* Reset */
            RTOS_event(EVENT_GEOPHONE3X_CMD_RESET, 0, EVTLOG_SD | EVTLOG_COM);
            Application_reset();
            break;

        case CMD_GEOPHONE_SELFTEST:
            /* Self-test (only if no acquisition ongoing) */
            if ((Geo.Status == GEO_ACQSTATUS_STOPPED) && !Imu.Status)   //TODO check this condition
            {
                RTOS_event(EVENT_GEOPHONE3X_CMD_SELFTEST, 0, EVTLOG_SD | EVTLOG_COM);
                SelfTest_quick();
                RTOS_event(EVENT_GEOPHONE3X_SELFTEST, SysConf.SysHealth->SelfTestResult, EVTLOG_SD | EVTLOG_COM);
            }
            break;

        case CMD_GEOPHONE_SYS_OPMODE:
            /* Operating mode */
            if (command->arg[0] <= SYS_OPMODE_ALL)
            {
                SysConf.OpMode = command->arg[0];
                RTOS_event(EVENT_GEOPHONE3X_CMD_SYS_OPMODE, command->arg[0], EVTLOG_SD | EVTLOG_COM);
                RTOS_buzzer(BUZZER_TIME_CMD_MS, 1);
                write_cfg = 1;
                DEBUG_PRINTF(DBG_LVL_INFO, "System OpMode set to %u", SysConf.OpMode);
                if (SysConf.OpMode & SYS_OPMODE_TRG)
                {
                    Geo_enableTrigger();
                }
                else
                {
                    Geo_disableTrigger();
                }
                if (SysConf.OpMode & SYS_OPMODE_CONT)
                {
                    Geo_initAcq(TRG_EXT, 0, 0);
                }
            }
            break;

        case CMD_GEOPHONE_EXTTRG:
            /* Launch manual trigger */
            if (SysConf.Geo->Status == GEO_ACQSTATUS_STOPPED)
            {
                Geo_initAcq(TRG_EXT, SysConf.Geo->PostTrg, 0);
                RTOS_event(EVENT_GEOPHONE3X_CMD_EXTTRG, 0, EVTLOG_SD | EVTLOG_COM);
            }
            /* Acquire is already running: reset post-trigger */
            else
            {
                /* Want to end the present chunk and add another PostTrg chunks */
                Geo.ChunksRemaining = (SysConf.Geo->PostTrg + 1) * GEO_NUM_AXES;
            }
            break;

        case CMD_GEOPHONE_TRG_GAIN:
            /* Set trigger gain */
            if (!command->arg[0])
            {
                STAGE2_SWOFF();
                Geo.TrgGain = TRG_GAIN_STAGE1;
                RTOS_event(EVENT_GEOPHONE3X_CMD_GAIN1, 0, EVTLOG_SD | EVTLOG_COM);
            }
            else
            {
                STAGE2_SWON();
                Geo.TrgGain = TRG_GAIN_STAGE2;
                RTOS_event(EVENT_GEOPHONE3X_CMD_GAIN2, 0, EVTLOG_SD | EVTLOG_COM);
            }
            write_cfg = 1;
            break;

        case CMD_GEOPHONE_TRG_TH_POS:
            /* Set positive trigger threshold */
            Geo_setTriggerThresholds(command->arg16[0], DPP_MSG_INV_UINT16);
            RTOS_event(EVENT_GEOPHONE3X_CMD_TRG_TH_POS, command->arg16[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        case CMD_GEOPHONE_TRG_TH_NEG:
            /* Set negative trigger threshold */
            Geo_setTriggerThresholds(DPP_MSG_INV_UINT16, command->arg16[0]);
            RTOS_event(EVENT_GEOPHONE3X_CMD_TRG_TH_NEG, command->arg16[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        case CMD_GEOPHONE_POSTTRG:
            /* Set ADC post-trigger */
            if (command->arg16[0])
            {
                Geo.PostTrg = command->arg16[0];
                write_cfg = 1;
            }
            RTOS_event(EVENT_GEOPHONE3X_CMD_POSTTRG, command->arg16[0], EVTLOG_SD | EVTLOG_COM);
            break;

        case CMD_GEOPHONE_TIMEOUT:
            /* Set ADC timeout */
            if (command->arg[0])
            {
                Geo.Timeout = command->arg16[0];
                write_cfg = 1;
            }
            RTOS_event(EVENT_GEOPHONE3X_CMD_TIMEOUT, command->arg16[0], EVTLOG_SD | EVTLOG_COM);
            break;

        case CMD_GEOPHONE_ADC_PGA:
            /* Set ADC PGA */
            Geo.PGA = command->arg[0];
            RTOS_event(EVENT_GEOPHONE3X_CMD_PGA, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        case CMD_GEOPHONE_ADC_FORMAT:
            /* Set ADC format */
            if (!command->arg[0])
            {
                Geo.Format = ADC_FORMAT_TWOSCOMPLEMENT;
            }
            else
            {
                Geo.Format = ADC_FORMAT_OFFSETBINARY;
            }
            RTOS_event(EVENT_GEOPHONE3X_CMD_FORMAT, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        /*case CMD_GEOPHONE_IMU_FREQ_LP:
            Imu.Freq_LP = command->arg[0];
            RTOS_event(EVENT_GEOPHONE3X_CMD_IMU_FREQ_LP, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
          break;*/

        case CMD_GEOPHONE_IMU_FREQ_HP:
            /* Set IMU frequency High-Power*/
            Imu.Freq_HP = command->arg[0];
            RTOS_event(EVENT_GEOPHONE3X_CMD_IMU_FREQ_HP, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        /*case CMD_GEOPHONE_IMU_FREQ_AA:
            Imu.Freq_AA = command->arg[0];
            RTOS_event(EVENT_GEOPHONE3X_CMD_IMU_FREQ_AA, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
        break;*/

        case CMD_GEOPHONE_IMU_OPMODE:
            /* Set IMU Operation Mode */
            Imu.OpMode = command->arg[0];
            RTOS_event(EVENT_GEOPHONE3X_CMD_IMU_OPMODE, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        case CMD_GEOPHONE_IMU_TRG_LVL:
            /* Set IMU Trigger Level */
            Imu.Level_Trg = command->arg[0];
            RTOS_event(EVENT_GEOPHONE3X_CMD_IMU_TRG_LVL, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        case CMD_GEOPHONE_IMU_DATA_DEC:
            /* Set IMU Data Decimation */
            Imu.Decimation = command->arg[0];
            RTOS_event(EVENT_GEOPHONE3X_CMD_IMU_DATA_DEC, command->arg[0], EVTLOG_SD | EVTLOG_COM);
            write_cfg = 1;
            break;

        case CMD_GEOPHONE_ADC_SPS:
            /* Set ADC SPS (sampling rate) */
            if (command->arg[0] < 4)
            {
                uint32_t sps = 1000 / (1 << command->arg[0]);
                if (sps <= ADC_MAX_SPS)
                {
                    Geo.SPS = sps;
                    write_cfg = 1;
                    RTOS_event(EVENT_GEOPHONE3X_CMD_SPS, command->arg[0], EVTLOG_SD | EVTLOG_COM);
                }
            }
            break;

        case CMD_GEOPHONE_RESET_CFG:
            /* Reset the configuration to the default values */
            if (SysConf.Geo->Status == GEO_ACQSTATUS_STOPPED)
            {
                Application_setConfigDefault();
                RTOS_event(EVENT_GEOPHONE3X_CMD_RESET_CFG, 0, EVTLOG_SD | EVTLOG_COM);
                write_cfg = 1;
            }
            break;

        case CMD_GEOPHONE_SCHED_CLEAR:
            /* Remove all elements in the scheduler and clear the scheduler file */
            Schedule_clear(false);
            RTOS_queueSendLog(LOGTYPE_SCHED_CLEAR);
            break;

        case CMD_GEOPHONE_SCHED_ADD:
            Schedule_addEntry(command->arg[0],        /* task ID */
                              command->arg32[1],      /* start time in UNIX epoch (32-bit) */
                              command->arg32[0] >> 8, /* period in minutes (24-bit) */
                              command->arg32[2],      /* duration in seconds (32-bit) */
                              false);                 /* don't make entry persistent */
            break;

        case CMD_GEOPHONE_SYS_WAKEUP_PERIOD: // TODO: replace with new command CMD_GEOPHONE_SCHED_DC
            /* Set a daily duty cycle for the GNSS and GEO tasks
             * arg[0] = duty cycle in percent (1 - 100)
             * arg[1] = offset in hours (0 - 23) */
            if (command->arg[0] > 0)
            {
                if (command->arg[0] >= 100)   /* back to 100% DC = continuous mode */
                {
                    Schedule_adjustTaskPeriod(TASK_GEO | TASK_POSTPONE, 0);
                    if (GNSS_PERIOD_S)
                    {
                        Schedule_adjustTaskPeriod(TASK_GNSS | TASK_POSTPONE, 0);
                    }
                }
                else
                {
                    uint32_t ontime_s = (86400UL * command->arg[0]) / 100;
                    uint32_t ofs_s    = 3600UL * (command->arg[1]);
                    uint8_t  tasks    = (GNSS_PERIOD_S ? (TASK_GEO | TASK_GNSS) : TASK_GEO);
                    Schedule_addEntry(tasks | TASK_POSTPONE,
                                      RTC_TIMESTAMP_2000_01_01 + ontime_s + ofs_s,    /* start time (UNIX epoch) */
                                      86400UL,                                        /* period in seconds (daily) */
                                      86400UL - ontime_s,                             /* postpone duration in seconds */
                                      false);
                }
            }
            break;

        case CMD_GEOPHONE_TSYNC_PERIOD:
            if (command->arg16[0] >= 300)   /* must be at least 5min */
            {
                Schedule_adjustTaskPeriod(TASK_TSYNC, command->arg16[0]);
            }
            break;

        default:
            /* Unknown command */
            unknown = true;
            break;
        }

        /* Pop from RB */
        RB_remove(&rb_cmd);

        if (!unknown)
        {
            RTOS_buzzer(BUZZER_TIME_CMD_MS, 1);
        }
        /* Increase Received Message Counter */
        SysHealth.CmdCnt++;

        DEBUG_PRINTF(DBG_LVL_INFO, "Command type 0x%x processed", command->type);
    }

    /* Write config to SD card */
    if (write_cfg)
    {
        RTOS_queueSendLog(LOGTYPE_CONF_WR);
    }
}

void RTOS_queueSendLog(LogType_t type)
{
    xQueueSendToBack(xQueue_Log, (void*)&type, pdMS_TO_TICKS(10));
}

void RTOS_queueSendLogFromISR(LogType_t type, BaseType_t* higherPriorityTaskWoken)
{
    xQueueSendToBackFromISR(xQueue_Log, (void*)&type, higherPriorityTaskWoken);
}

uint8_t RTOS_getHighestStackUsage(uint32_t* taskID)
{
    uint8_t critical = 0;

    *taskID = RTOS_TASK_ID_ERROR;

    if (!IS_RTOS_RUNNING()) { return critical; }

    /* NOTE: uxTaskGetStackHighWaterMark() returns the number of unused/free words on the stack */
    uint32_t temp = 100 - (uxTaskGetStackHighWaterMark(xTaskGetIdleTaskHandle()) * 100 / configMINIMAL_STACK_SIZE);
    DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Idle stack usage:     %2u%%", temp);
    if (temp > critical)
    {
        critical = temp;
        *taskID  = RTOS_TASK_ID_IDLE;
    }

    if (xTaskHandle_Geo)
    {
        temp = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Geo) * 100 / RTOS_TASK_STACK_GEO);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Geo stack usage:      %2u%%", temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_GEO;
        }
    }
#if BOLT_ENABLE
    if (xTaskHandle_Bolt)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Bolt) * 100 / RTOS_TASK_STACK_BOLT);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Bolt stack usage:     %2u%%", temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_BOLT;
        }
    }
#endif
    if (xTaskHandle_Health)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Health) * 100 / RTOS_TASK_STACK_HEALTH);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Health stack usage:   %2u%%", temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_HEALTH;
        }
    }
    if (xTaskHandle_Imu)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Imu) * 100 / RTOS_TASK_STACK_IMU);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Imu stack usage:      %2u%%", temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_IMU;
        }
    }
#if USE_INCLINO
    if (xTaskHandle_Inclino)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Inclino) * 100 / RTOS_TASK_STACK_INCLINO);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Inclino stack usage:  %2u%%", temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_INCLINO;
        }
    }
#endif
#if USE_GNSS
    if (xTaskHandle_GNSS)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_GNSS) * 100 / RTOS_TASK_STACK_GNSS);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task GNSS stack usage:     %2u%%", temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_GNSS;
        }
    }
#endif
    if (xTaskHandle_Log)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Log) * 100 / RTOS_TASK_STACK_LOG);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Log stack usage:      %2u%%", temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_LOG;
        }
    }
#if !TSYNC_METHOD_NONE || TSYNC_MASTER
    if (xTaskHandle_TSync)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_TSync) * 100 / RTOS_TASK_STACK_TSYNC);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task TSync stack usage:    %2u%%", 100 - temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_TSYNC;
        }
    }
#endif
#if !STREAMING_TARGET_NONE
    if (xTaskHandle_Stream)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Stream) * 100 / RTOS_TASK_STACK_STREAM);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Stream stack usage:   %2u%%", 100 - temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_STREAM;
        }
    }
#endif
    if (xTaskHandle_Schedule)
    {
        temp  = 100 - (uxTaskGetStackHighWaterMark(xTaskHandle_Schedule) * 100 / RTOS_TASK_STACK_SCHEDULE);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Task Schedule stack usage: %2u%%", 100 - temp);
        if (temp > critical)
        {
            critical = temp;
            *taskID  = RTOS_TASK_ID_SCHEDULE;
        }
    }

    return critical;
}

uint8_t RTOS_getHeapUsage(void)
{
    uint32_t temp = (configTOTAL_HEAP_SIZE - xPortGetFreeHeapSize()) * 100;
    return (uint8_t)(temp / configTOTAL_HEAP_SIZE);
}

uint16_t RTOS_dutyCycle(DCCounterAction_t action)
{
    static uint32_t dc_active_time       = 0;   /* Rollover after ~600h if not reset */
    static uint32_t dc_last_update_cntr  = 0;
    static uint32_t dc_last_reset_uptime = 0;
    static bool     dc_cntr_paused       = false;

    uint16_t retval = 0;
    uint32_t cntr   = __HAL_TIM_GET_COUNTER(&htim5);  /* Counter is in 0.5ms ticks */

    switch (action)
    {
    case DC_CNTR_START:
        HAL_TIM_Base_Start(&htim5);
        // fall through
    case DC_CNTR_RESUME:
        if (dc_cntr_paused)
        {
            dc_last_update_cntr = cntr;
            dc_cntr_paused      = false;
        }
        break;

    case DC_CNTR_STOP:
        HAL_TIM_Base_Stop(&htim5);
        __HAL_TIM_SET_COUNTER(&htim5, 0);
        // fall through
    case DC_CNTR_PAUSE:
        if (!dc_cntr_paused)    /* If not already paused */
        {
            dc_active_time     += (uint32_t)(cntr - dc_last_update_cntr);   /* Rollover is tolerated */
            dc_last_update_cntr = 0;
            dc_cntr_paused      = true;
        }
        break;

    case DC_CNTR_RESET:
        dc_active_time       = 0;
        dc_last_reset_uptime = RTC_getUptime();
        dc_last_update_cntr  = cntr;
        break;

    case DC_CNTR_GET:
        if (!dc_cntr_paused)    /* If not already paused */
        {
            dc_active_time     += (uint32_t)(cntr - dc_last_update_cntr);   /* Rollover is tolerated */
            dc_last_update_cntr = cntr;
        }
        /* Calculate duty cycle such that 100% = 10000:
         * 10000 * t / 2000 / elapsed_time_s = 5 * t / elapsed_time_s */
        retval = 5 * dc_active_time / (RTC_getUptime() - dc_last_reset_uptime);    /* Uptime in seconds */
        if (retval > 10000)     /* Limit to 100% */
        {
            retval = 10000;
        }
        break;

    default:
        break;
    }
    return retval;
}

#if USE_STOP2

/* Enter STOP2 ---------------------------------------------------------------*/
void RTOS_enterStopMode(void)
{
    /* Mask interrupts */
    __set_BASEPRI( (TICK_INT_PRIORITY + 1) << (8 - __NVIC_PRIO_BITS) );
    __DSB();
    __ISB();

    /* Stop Peripherals */
    PERIPH_prepareStop2();

    /* Poll the watchdog */
    WDT_POLL();

#if (PREFETCH_ENABLE != 0)
    __HAL_FLASH_PREFETCH_BUFFER_DISABLE();
#endif

    core_stopped = true;

    /* Suspend RTOS Systick */
    CLEAR_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);

    /* Reset system clock to HSI */
    SystemClock_reset();

    /* Suspend HAL tick interrupt */
    HAL_SuspendTick();

    /* Disable clocks */
    SystemClock_disable();

    /* Ensure that HSI is the wake-up system clock */
    __HAL_RCC_PWR_CLK_ENABLE();
    HAL_RCCEx_WakeUpStopCLKConfig(RCC_STOP_WAKEUPCLOCK_HSI);

    /* Re-enable interrupts */
    __set_BASEPRI(0);

    /* Enter Stop2 */
    __HAL_RCC_PWR_CLK_ENABLE();
    if (core_stopped)
    {
        HAL_PWREx_EnterSTOP2Mode(PWR_STOPENTRY_WFI);
    }
}

/* Resume from STOP2 ---------------------------------------------------------*/
void RTOS_resumeFromStopMode(void)
{
    if (core_stopped)
    {
        /* Mask interrupts */
        __set_BASEPRI( (TICK_INT_PRIORITY + 1) << (8 - __NVIC_PRIO_BITS) );
        __DSB();
        __ISB();

        /* Poll the watchdog timer */
        WDT_POLL();

        /* Enable clocks */
        SystemClock_enable();

        /* Restore clock configuration */
        SystemClock_config();

        /* Wait for RTC sync */
        RTC_sync();

        /* Resume HAL tick */
        /* Note: done by HAL_RCC_ClockConfig() in SystemClock_config() */

        /* Restore Peripherals */
        PERIPH_resumeFromStop2();

        /* Restart DutyCycle measurement */
        RTOS_dutyCycle(DC_CNTR_START);

        /* Resume SysTick */
        SET_BIT(SysTick->CTRL, SysTick_CTRL_ENABLE_Msk);

        core_stopped = false;

        /* Enable interrupts */
        __set_BASEPRI(0);

        //DEBUG_PRINT(DBG_LVL_VERBOSE, "Resume from stop mode");
    }
}

#endif /* USE_STOP2 */

/* Helper functions for mounting mass storage device (USB) -------------------*/
void RTOS_USBMSC(bool mount)
{
    static uint8_t op_mode   = false;
    static bool    usb_cdc   = false;
    static bool    mount_req = false;

    if (mount)
    {
        if ( (SysConf.USB->deviceClass != MSC) && USB_IS_CONNECTED() && !mount_req )
        {
            usb_cdc         = (SysConf.USB->deviceClass == CDC);
            op_mode         = SysConf.OpMode;
            SysConf.OpMode  = 0;
            Geo_abort(false);
            mount_req       = true;
            RTOS_queueSendLog(LOGTYPE_USB_MSC_MOUNT);
        }
    }
    else
    {
        if (SysConf.USB->deviceClass == MSC)
        {
            RTOS_queueSendLog(LOGTYPE_USB_MSC_EJECT);
            /* Restore previous USB mode */
            if (usb_cdc)
            {
                RTOS_queueSendLog(LOGTYPE_USB_CDC_INIT);
            }
            /* Restore previous operating mode */
            SysConf.OpMode = op_mode;
            /* Note: Cannot start geophone acquisition here since USB MSC is still mounted at this point, therefore the log task will take care of it.
             * Alternatively, one could also add a new (one-shot) schedule entry for the geophone task, but this only works if OPMODE_SCHED is enabled. */
        }
        mount_req = false;
    }
}

/* Tell idle task to enter error handler */
void RTOS_errorHandlerInIdle(const char* file, int line)
{
    DEBUG_PRINTF(DBG_LVL_ERROR, "Error_Handler requested from %s:%d", file, line);
    Geo_disableTrigger();
    Geo_abort(false);
    err_pending = true;
}

/* RTOS Error Handling -------------------------------------------------------*/
void vApplicationMallocFailedHook(void)
{
    DEBUG_PRINT(DBG_LVL_ERROR, "Malloc failed");
    Error_Handler();
}

void vApplicationStackOverflowHook(TaskHandle_t xTask, signed char *pcTaskName)
{
    (void)xTask;
    DEBUG_PRINTF(DBG_LVL_ERROR, "Application Stack overflow caused by task %s", pcTaskName);
    Error_Handler();
}

void vAssertCalled(const char *pcFile, uint16_t ulLine)
{
    DebugPrint(pcFile, ulLine, DBG_LVL_ERROR, "Assertion error");
    Error_Handler();
}
