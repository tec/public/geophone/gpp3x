/*
 * Copyright (c) 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"


/* Variables -----------------------------------------------------------------*/

static float       psd[PSD_FFT_FRAME_SIZE / 2 + 1]     = { 0 };
static uint32_t    psd_frame_cnt                       = 0;
static uint32_t    psd_sample_rate                     = 0;
static const float cosine_taper_norm                   = 896.75f;  /* = 1 / (window**2).sum() */
static const float cosine_taper[PSD_COSINE_TAPER_SIZE] =           /* Window function to apply to the data before the FFT */
{
    0.000000f, 0.000242f, 0.000967f, 0.002175f, 0.003865f, 0.006035f, 0.008682f, 0.011805f, 0.015401f, 0.019465f, 0.023993f, 0.028983f, 0.034428f, 0.040323f, 0.046664f, 0.053442f,
    0.060653f, 0.068289f, 0.076343f, 0.084806f, 0.093671f, 0.102929f, 0.112571f, 0.122589f, 0.132971f, 0.143708f, 0.154790f, 0.166206f, 0.177945f, 0.189995f, 0.202346f, 0.214984f,
    0.227898f, 0.241075f, 0.254503f, 0.268168f, 0.282058f, 0.296158f, 0.310456f, 0.324937f, 0.339587f, 0.354393f, 0.369339f, 0.384412f, 0.399596f, 0.414878f, 0.430242f, 0.445674f,
    0.461158f, 0.476680f, 0.492224f, 0.507776f, 0.523320f, 0.538842f, 0.554326f, 0.569758f, 0.585122f, 0.600404f, 0.615588f, 0.630661f, 0.645607f, 0.660413f, 0.675063f, 0.689544f,
    0.703842f, 0.717942f, 0.731832f, 0.745497f, 0.758925f, 0.772102f, 0.785016f, 0.797654f, 0.810005f, 0.822055f, 0.833794f, 0.845210f, 0.856292f, 0.867029f, 0.877411f, 0.887429f,
    0.897071f, 0.906329f, 0.915194f, 0.923657f, 0.931711f, 0.939347f, 0.946558f, 0.953336f, 0.959677f, 0.965572f, 0.971017f, 0.976007f, 0.980535f, 0.984599f, 0.988195f, 0.991318f,
    0.993965f, 0.996135f, 0.997825f, 0.999033f, 0.999758f, 1.000000f
};


/* Functions -----------------------------------------------------------------*/

static uint8_t PSD_quantize(float val)
{
    return (uint8_t)MIN(255.0f, -2.0f * val);
}


/* Frequency binning and quantization
 * Precondition: psd[] must contain the mean values in dB */
static uint32_t PSD_binnedQuantized(uint8_t* out_buffer, const uint32_t buffer_len)
{
    if (!psd_sample_rate || !out_buffer || !buffer_len) { return 0; }

    uint32_t sub_hz_idx = PSD_FFT_FRAME_SIZE / psd_sample_rate;
    uint32_t buf_idx    = 0;
    uint32_t prev_freq  = 0;
    uint32_t freq_cnt   = 0;
    float    freq_sum   = 0.0f;
    float    freq_step  = (float)psd_sample_rate / (float)PSD_FFT_FRAME_SIZE;

    for (uint32_t i = 1; i < PSD_FFT_FRAME_SIZE / 2; i++)   /* Start at index 1 (skip DC offset) */
    {
        if (buf_idx >= buffer_len)
        {
            return buf_idx;
        }
        if (i <= sub_hz_idx)
        {
            /* Keep all data below 1Hz */
            out_buffer[buf_idx++] = PSD_quantize(psd[i]);
        }
        else
        {
            /* above 1Hz: create 1Hz bins */
            uint32_t freq = freq_step * i + 0.5f;
            if (freq != prev_freq && freq_cnt)
            {
                out_buffer[buf_idx++] = PSD_quantize(freq_sum / freq_cnt);
                freq_sum = 0.0f;
                freq_cnt = 0;
            }
            prev_freq = freq;
            freq_sum += psd[i];
            freq_cnt++;
        }
    }
    if (freq_cnt && buf_idx < buffer_len)
    {
        out_buffer[buf_idx++] = PSD_quantize(freq_sum / freq_cnt);
    }
    return buf_idx;
}


void PSD_addSamples(const int32_t* samples, const uint32_t num_samples, const uint32_t sample_rate)
{
    static arm_rfft_fast_instance_f32 rfft = { 0 };
    static float                      fft_frame[PSD_FFT_FRAME_SIZE];
    static float                      fft_output[PSD_FFT_FRAME_SIZE];
    static float                      fft_overlap[PSD_FFT_OVERLAP_SIZE];
    static uint32_t                   last_idx = 0;

    if (!rfft.fftLenRFFT)
    {
        arm_rfft_fast_init_f32(&rfft, PSD_FFT_FRAME_SIZE);
    }
    if (!psd_frame_cnt)
    {
        memset(psd, 0, sizeof(psd));
    }
    const float output_scaling = 2.0f / ((float)sample_rate * cosine_taper_norm);
    psd_sample_rate            = sample_rate;

    for (uint32_t idx = 0; idx < num_samples; idx++)
    {
        fft_frame[last_idx++] = (float)samples[idx] * PSD_FFT_INPUT_SCALING;  /* Move into range -1..1 */

        if (last_idx == PSD_FFT_FRAME_SIZE)   /* Frame full? */
        {
            /* Keep a copy of the upper part of the buffer to achieve an overlap with the next calculation (fft_frame is modified by the rfft function) */
            memcpy(fft_overlap, &fft_frame[PSD_FFT_FRAME_SIZE - PSD_FFT_OVERLAP_SIZE], PSD_FFT_OVERLAP_SIZE * sizeof(float));

            /* Apply window function */
            for (uint32_t i = 0; i < PSD_FFT_FRAME_SIZE; i++)
            {
                if (i < PSD_COSINE_TAPER_SIZE)
                {
                    fft_frame[i] *= cosine_taper[i];
                }
                else if (i > (PSD_FFT_FRAME_SIZE - PSD_COSINE_TAPER_SIZE))
                {
                    fft_frame[i] *= cosine_taper[PSD_FFT_FRAME_SIZE - i];
                }
            }

            /* Compute FFT
             * The output is a series of N/2 complex values (or N real values):
             *   [real_0, real_N/2 - 1, real_1, im_1, real_2, im_2, ...] */
            arm_rfft_fast_f32(&rfft, fft_frame, fft_output, 0);

            /* Convert to power spectrum */
            float en_first = fft_output[0] * fft_output[0];
            float en_last  = fft_output[1] * fft_output[1];
            for (uint32_t i = 1; i < PSD_FFT_FRAME_SIZE / 2; i++)
            {
                float re      = fft_output[i * 2];
                float im      = fft_output[i * 2 + 1];
                fft_output[i] = re * re + im * im;

                /* Divide by the sampling frequency so that density function has units of 1/Hz */
                /* Scale the spectrum by the norm of the window to compensate for windowing loss; see Bendat & Piersol Sec 11.5.2. */
                fft_output[i] *= output_scaling;

                /* Aggregate */
                psd[i] += fft_output[i];
            }
            fft_output[0]                      = en_first;
            fft_output[PSD_FFT_FRAME_SIZE / 2] = en_last;
            psd[0]                            += en_first;
            psd[PSD_FFT_FRAME_SIZE / 2]       += en_last;
            psd_frame_cnt++;

            /* Prepare the fft frame for the next calculation */
            memcpy(fft_frame, fft_overlap, PSD_FFT_OVERLAP_SIZE * sizeof(float));
            last_idx = PSD_FFT_OVERLAP_SIZE;
        }
    }
}


/* get the PSD average (frequency-binned and quantized to 8-bit) */
uint32_t PSD_getAverage(uint8_t* out_buffer, uint32_t buffer_len)
{
    if (psd_frame_cnt)
    {
        for (uint32_t i = 0; i <= PSD_FFT_FRAME_SIZE / 2; i++)
        {
            /* Calculate the average */
            float avg = psd[i] / psd_frame_cnt;

            /* Logarithmic scaling to get dB */
            psd[i] = log10f(avg) * 10.0f;
        }
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "PSD averaged over %d frames", psd_frame_cnt);
        psd_frame_cnt = 0;
    }

    return PSD_binnedQuantized(out_buffer, buffer_len);
}


uint32_t PSD_getFrameCount(void)
{
    return psd_frame_cnt;
}
