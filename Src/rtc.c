/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Private Macros ------------------------------------------------------------*/
#define RTC_MS()        ((uint32_t)(rtc_time.SecondFraction - rtc_time.SubSeconds) * 1000 / (rtc_time.SecondFraction + 1))
#define RTC_US()        (uint32_t)((uint64_t)(rtc_time.SecondFraction - rtc_time.SubSeconds) * 1000000 / (rtc_time.SecondFraction + 1))

/* Private Variables ---------------------------------------------------------*/
static uint64_t         rtc_uptime_ms;
static RTC_DateTypeDef  rtc_date;
static RTC_TimeTypeDef  rtc_time;

/* Static Functions ----------------------------------------------------------*/
static void RTC_update(void)
{
    HAL_RTC_GetTime(&hrtc, &rtc_time, RTC_FORMAT_BIN);
    HAL_RTC_GetDate(&hrtc, &rtc_date, RTC_FORMAT_BIN);
}

/* RTC Functions -------------------------------------------------------------*/
void RTC_init(void)
{
    hrtc.Instance            = RTC;
    hrtc.Init.HourFormat     = RTC_HOURFORMAT_24;
    hrtc.Init.AsynchPrediv   = 0x03;
    hrtc.Init.SynchPrediv    = 0x1FFF;    /* Divider 8192 => ~122us subsecond resolution */
    hrtc.Init.OutPut         = RTC_OUTPUT_DISABLE;
    hrtc.Init.OutPutRemap    = RTC_OUTPUT_REMAP_NONE;
    hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
    hrtc.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;
    if (HAL_RTC_Init(&hrtc) != HAL_OK)
    {
        Error_Handler();
    }
    RTC_compensateDrift(0);     /* Reset drift compensation */

    HAL_NVIC_SetPriority(RTC_Alarm_IRQn, RTC_ALARM_IT_PRIO, 0);

    rtc_uptime_ms = RTC_getTimestampMS();
}

/* Resets the RTC value to 1.1.1970 */
void RTC_reset(void)
{
    /* Date: Jan 1 of 1900, 1970, 2000, 2100 are: Mon, Thu, Sat, Fri respectively */

    /* Default Time */
    rtc_time.Hours          = 0;
    rtc_time.Minutes        = 0;
    rtc_time.Seconds        = 0;
    rtc_time.SubSeconds     = 0;
    rtc_time.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
    rtc_time.StoreOperation = RTC_STOREOPERATION_RESET;
    if (HAL_RTC_SetTime(&hrtc, &rtc_time, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }

    /* Default Date */
    rtc_date.WeekDay = RTC_WEEKDAY_THURSDAY;
    rtc_date.Month   = RTC_MONTH_JANUARY;
    rtc_date.Date    = 1;
    rtc_date.Month   = 1;
    rtc_date.Year    = 70;
    if (HAL_RTC_SetDate(&hrtc, &rtc_date, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }

    rtc_uptime_ms = 0;
}

void RTC_sync(void)
{
    /* Wait for RTC sync */
    HAL_RTC_WaitForSynchro(&hrtc);
}

void RTC_startWakeUpTimer(uint32_t period_s)
{
    /* RTC_WAKEUPCLOCK_CK_SPRE_16BITS will set the granularity to 1s. Note that the counter value is zero-based, i.e. a value of 10 will result in a wakeup every 11 seconds */
    HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, (period_s - 1), RTC_WAKEUPCLOCK_CK_SPRE_16BITS);
    DEBUG_PRINTF(DBG_LVL_INFO, "RTC wakeup period is set to %us", period_s);

    /* Enable interrupt */
    HAL_NVIC_SetPriority(RTC_WKUP_IRQn, RTC_WAKEUP_IT_PRIO, 0);
    HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);
}

void RTC_stopWakeUpTimer(void)
{
    HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);

    /* Disable interrupt */
    HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);
}

const RTC_DateTypeDef* RTC_getDate(bool update)
{
    if (update) { RTC_update(); }
    return &rtc_date;
}

const RTC_TimeTypeDef* RTC_getTime(bool update)
{
    if (update) { RTC_update(); }
    return &rtc_time;
}

void RTC_getDateTime(const RTC_DateTypeDef** date, const RTC_TimeTypeDef** time)
{
    RTC_update();
    if (date)
    {
        *date = &rtc_date;
    }
    if (time)
    {
        *time = &rtc_time;
    }
}

uint32_t RTC_getDay(void)
{
    RTC_update();
    return ((rtc_date.Year < 70) ? (2000 + rtc_date.Year) : (1900 + rtc_date.Year)) * 10000 + rtc_date.Month * 100 + rtc_date.Date;
}

const char* RTC_getFormattedString(void)
{
    static char formattedDateTimeString[25];

    RTC_update();
    snprintf(formattedDateTimeString, sizeof(formattedDateTimeString), "%04u-%02u-%02u %02u:%02u:%02u", (rtc_date.Year < 70) ? (2000 + rtc_date.Year) : (1900 + rtc_date.Year), rtc_date.Month, rtc_date.Date, rtc_time.Hours, rtc_time.Minutes, rtc_time.Seconds);
    return formattedDateTimeString;
}

const char* RTC_getFormattedDate(void)
{
    static char formattedDateString[13];

    RTC_update();
    snprintf(formattedDateString, sizeof(formattedDateString), "%04u-%02u-%02u", (rtc_date.Year < 70) ? (2000 + rtc_date.Year) : (1900 + rtc_date.Year), rtc_date.Month, rtc_date.Date);

    return formattedDateString;
}

uint32_t RTC_getEpoch(void)
{
    uint8_t  a;
    uint8_t  m;
    uint16_t y;
    uint32_t JDN;

    RTC_update();

    a = (14 - rtc_date.Month) / 12;
    y = (rtc_date.Year >= 70) ? (rtc_date.Year + 1900 + 4800 - a) : (rtc_date.Year + 2000 + 4800 - a);
    m = rtc_date.Month + (12 * a) - 3;

    JDN  = rtc_date.Date;
    JDN += (153 * m + 2) / 5;
    JDN += 365 * y;
    JDN += y / 4;
    JDN += -y / 100;
    JDN += y / 400;
    JDN  = JDN - 32045;
    JDN  = JDN - 2440588;
    JDN *= 86400;
    JDN += rtc_time.Hours * 3600;
    JDN += rtc_time.Minutes * 60;
    JDN += rtc_time.Seconds;

    return (JDN <= 0x7FFFFFFF) ? JDN : 0;
}

uint64_t RTC_getTimestampUS(void)
{
    /* Note: granularity of subsecond part is 1 Sec / (SecondFraction + 1) => 1 / 8192 = ~122us */
    return (((uint64_t)RTC_getEpoch() * 1000000) + RTC_US());
}

uint64_t RTC_getTimestampMS(void)
{
    /* Note: granularity of subsecond part is 1 Sec / (SecondFraction + 1) => 1 / 8192 = ~122us */
    return (((uint64_t)RTC_getEpoch() * 1000) + RTC_MS());
}

void RTC_convertEpochToDatetime(uint32_t epoch, RTC_DateTypeDef* date, RTC_TimeTypeDef* time)
{
    if (!date || !time) { return; }

    uint64_t JD  = ((epoch + 43200) / (86400 >> 1)) + (2440587 << 1) + 1;
    uint64_t JDN = JD >> 1;

    uint32_t tm  = epoch;
    uint32_t t1  = tm / 60;
    int16_t sec  = tm - (t1 * 60);
    tm = t1;
    t1 = tm / 60;
    int16_t min  = tm - (t1 * 60);
    tm = t1;
    t1 = tm / 24;
    int16_t hour = tm - (t1 * 24);

    int16_t  dow   = (JDN % 7) + 1;
    uint32_t a     = JDN + 32044;
    uint32_t b     = ((4 * a) + 3) / 146097;
    uint32_t c     = a - ((146097 * b) / 4);
    uint32_t d     = ((4 * c) + 3) / 1461;
    uint32_t e     = c - ((1461 * d) / 4);
    uint32_t m     = ((5 * e) + 2) / 153;
    int16_t  mday  = e - (((153 * m) + 2) / 5) + 1;
    int16_t  month = m + 3 - (12 * (m / 10));
    int16_t  year  = (100 * b) + d - 4800 + (m / 10);

    date->Year       = year % 100;
    date->Month      = month;
    date->Date       = mday;
    date->WeekDay    = dow;
    time->Hours      = hour;
    time->Minutes    = min;
    time->Seconds    = sec;
    time->SubSeconds = 0;
}

uint32_t RTC_convertDatetimeToEpoch(RTC_DateTypeDef date, RTC_TimeTypeDef time)
{
    uint8_t  a;
    uint8_t  m;
    uint16_t y;
    uint32_t JDN;

    a = (14 - date.Month) / 12;
    y = (date.Year >= 70) ? (date.Year + 1900 + 4800 - a) : (date.Year + 2000 + 4800 - a);
    m = date.Month + (12 * a) - 3;

    JDN  = date.Date;
    JDN += (153 * m + 2) / 5;
    JDN += 365 * y;
    JDN += y / 4;
    JDN += -y / 100;
    JDN += y / 400;
    JDN  = JDN - 32045;
    JDN  = JDN - 2440588;
    JDN *= 86400;
    JDN += time.Hours * 3600;
    JDN += time.Minutes * 60;
    JDN += time.Seconds;

    return JDN;
}

void RTC_convertEpochToFatFSTimestamp(uint32_t epoch, uint32_t* fdatetime)
{
    uint32_t tm;
    uint32_t t1;
    uint32_t a;
    uint32_t b;
    uint32_t c;
    uint32_t d;
    uint32_t e;
    uint32_t m;
    int16_t  year  = 0;
    int16_t  month = 0;
    int16_t  mday  = 0;
    int16_t  hour  = 0;
    int16_t  min   = 0;
    int16_t  sec   = 0;
    uint64_t JD    = 0;
    uint64_t JDN   = 0;

    if (!fdatetime) return;

    if (epoch < 315532800)
    {
        epoch = 315532800;        /* must be >= 1.1.1980 */
    }

    JD  = ((epoch + 43200) / (86400 >> 1)) + (2440587 << 1) + 1;
    JDN = JD >> 1;

    tm = epoch; t1 = tm / 60; sec  = tm - (t1 * 60);
    tm = t1;    t1 = tm / 60; min  = tm - (t1 * 60);
    tm = t1;    t1 = tm / 24; hour = tm - (t1 * 24);

    a     = JDN + 32044;
    b     = ((4 * a) + 3) / 146097;
    c     = a - ((146097 * b) / 4);
    d     = ((4 * c) + 3) / 1461;
    e     = c - ((1461 * d) / 4);
    m     = ((5 * e) + 2) / 153;
    mday  = e - (((153 * m) + 2) / 5) + 1;
    month = m + 3 - (12 * (m / 10));
    year  = ((100 * b) + d - 4800 + (m / 10)) - 1980;

    *fdatetime = ((uint32_t)((year * 512U) | month * 32U | mday) << 16) | (uint32_t)(hour * 2048U | min * 32U | sec / 2U);
}

uint32_t RTC_convertToDayOfYear(uint16_t year, uint8_t month, uint8_t day)
{
    static const uint16_t days_in_month[14] = { 0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };

    uint32_t doy = day;

    if (month < 12)
    {
        doy += days_in_month[month];
    }
    /* Every fourth year, Feb has 29 days (starting from 1972) */
    if ( (month > 2) && ((year % 4) == 0) )
    {
        doy++;
    }
    return doy;
}

void RTC_adjustTimeZone(int8_t offset)
{
    uint32_t epoch = RTC_getEpoch();
    epoch += offset * 3600;
    RTC_setFromEpoch(epoch);
}

void RTC_setFromEpoch(uint32_t epoch)
{
    uint64_t curr_uptime_ms = RTC_getUptimeMS();

    RTC_convertEpochToDatetime(epoch, &rtc_date, &rtc_time);
    HAL_RTC_SetDate(&hrtc, &rtc_date, RTC_FORMAT_BIN);
    HAL_RTC_SetTime(&hrtc, &rtc_time, RTC_FORMAT_BIN);

    rtc_uptime_ms = RTC_getTimestampMS() - curr_uptime_ms;
}

void RTC_setAlarmFromEpoch(uint32_t epoch)
{
    RTC_AlarmTypeDef alarm;
    RTC_DateTypeDef date;
    RTC_TimeTypeDef time;

    RTC_convertEpochToDatetime(epoch, &date, &time);

    alarm.Alarm               = RTC_ALARM_A;
    alarm.AlarmDateWeekDay    = date.Date;
    alarm.AlarmDateWeekDaySel = RTC_ALARMDATEWEEKDAYSEL_DATE;
    alarm.AlarmMask           = RTC_ALARMMASK_NONE;
    alarm.AlarmSubSecondMask  = RTC_ALARMSUBSECONDMASK_ALL;
    alarm.AlarmTime.Hours     = time.Hours;
    alarm.AlarmTime.Minutes   = time.Minutes;
    alarm.AlarmTime.Seconds   = time.Seconds;
    if (HAL_RTC_SetAlarm_IT(&hrtc, &alarm, RTC_FORMAT_BIN) != HAL_OK)
    {
        Error_Handler();
    }
}

uint32_t RTC_getUptime(void)
{
    return (RTC_getEpoch() - rtc_uptime_ms / 1000);
}

uint64_t RTC_getUptimeMS(void)
{
    return (RTC_getTimestampMS() - rtc_uptime_ms);
}

bool RTC_compensateDrift(int32_t offset_ppm)
{
    if (offset_ppm > RTC_MAX_DRIFT_COMP_PPM || offset_ppm < -RTC_MAX_DRIFT_COMP_PPM)
    {
        return false;
    }
    /* calibration register: RTC_CALR
     * add ticks to slow down or mask (skip) ticks to speed up
     * set CALM[8:0] to set the #ticks to mask in a 32s cycle (2^20 pulses) or set the CALP bit (0x8000) to increase the frequency / slow down by 488.5ppm (16 pulses per second)
     * calibrated frequency: FCAL = 32768Hz x [1 + (CALP x 512 - CALM) / (2^20 + CALM - CALP x 512)]
     * extra pulses per second: ((512 * CALP) - CALM) / 32, 0.9537ppm granularity */

    offset_ppm = (int32_t)((float)offset_ppm * (1.0f / RTC_DRIFT_COMP_GRANULARITY_PPM));
    if (offset_ppm > 0)
    {
        offset_ppm = RTC_CALR_CALP | (512 - offset_ppm);
    }
    else
    {
        offset_ppm *= -1;
    }

    __HAL_RTC_WRITEPROTECTION_DISABLE(&hrtc);
    hrtc.Instance->CALR = offset_ppm;
    __HAL_RTC_WRITEPROTECTION_ENABLE(&hrtc);

    return true;
}

void HAL_RTC_AlarmAEventCallback(RTC_HandleTypeDef *_hrtc)
{
    (void)_hrtc;
}

void HAL_RTCEx_AlarmBEventCallback(RTC_HandleTypeDef *_hrtc)
{
    (void)_hrtc;
}
