/*
 * Copyright (c) 2021 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* External variables --------------------------------------------------------*/
extern Geophone_t         Geo;
extern rb_t*              adc_rb[];

/* Global Variables ----------------------------------------------------------*/
const char*               adc_channel_mapping[ADC_COUNT + 1] = { "", ADC_A1_NAME, ADC_A2_NAME, ADC_A3_NAME };   /* +1 due to 1-based index (e.g. ADC_A1 = 1) */

/* Private Variables ---------------------------------------------------------*/
static volatile ADCID_t   adc_active                   = 0;
static volatile uint8_t   adc_ready                    = 0;
static uint8_t            adc_buf[GEO_SAMPLE_SIZE + 1] = { 0 };     /* Buffer for the SPI transfer of 1 ADC sample (3 bytes, +1 byte for cmd) */
#if ADC_SYNC_PERIOD
static volatile uint8_t   adc_masked                   = 0;
#endif

/* Private Functions ---------------------------------------------------------*/
static void ADC_updateTriggerStats(uint32_t adc_val)
{
    /* Format: Offset binary */
    if (Geo.Format == ADC_FORMAT_OFFSETBINARY)
    {
        if (adc_val > ADC_IDLE_VALUE)
        {
            /* Positive range */
            if (adc_val > Geo.PeakPosVal)
            {
                Geo.PeakPosVal      = adc_val;
                Geo.PeakPosSampleNo = Geo.Samples_TrgADC;
            }
        }
        else
        {
            /* Negative range */
            if (adc_val < Geo.PeakNegVal)
            {
                Geo.PeakNegVal      = adc_val;
                Geo.PeakNegSampleNo = Geo.Samples_TrgADC;
            }
        }
    }
    /* Format: Two-s complement */
    else
    {
        if (adc_val < ADC_IDLE_VALUE)
        {
            /* Positive range */
            if (adc_val > Geo.PeakPosVal)
            {
                Geo.PeakPosVal      = adc_val;
                Geo.PeakPosSampleNo = Geo.Samples_TrgADC;
            }
        }
        else
        {
            /* Negative range */
            if (adc_val < Geo.PeakNegVal)
            {
                Geo.PeakNegVal      = adc_val;
                Geo.PeakNegSampleNo = Geo.Samples_TrgADC;
            }
        }
    }
}

static uint8_t ADC_getPGASetting(uint8_t PGA)
{
    switch (PGA)
    {
    case 0:     return 0x00; /* 0000 PGA      OFF */
    case 1:     return 0x08; /* 1000 PGA x1   ON  */
    case 2:     return 0x09;
    case 4:     return 0x0A; /* 1010 PGA x4   ON  */
    case 8:     return 0x0B;
    case 16:    return 0x0C;
    case 32:    return 0x0D;
    case 64:    return 0x0E; /* 1110 PGA x64  ON  */
    case 128:   return 0x0F; /* 1111 PGA x128 ON  */
    default:
        DEBUG_PRINT(DBG_LVL_WARNING, "Unknown PGA setting, default to 4x PGA");
        return (ADC_CTRL2_PGAEN | ADC_CTRL2_PGAG1); /* 1010 PGA x4 ON */
    }
}

static ADCConvRate_t ADC_getConvSetting(uint16_t sps)
{
    ADCConvRate_t rate = ADC_CONV_RATE_1000Hz;  /* default value */

    if      (sps == 500) { rate = ADC_CONV_RATE_500Hz; }
    else if (sps == 250) { rate = ADC_CONV_RATE_250Hz; }
    else if (sps == 125) { rate = ADC_CONV_RATE_125Hz; }

#if ADC_SYNC_PERIOD
    /* double the conversion rate in order to have slack for periodic ADC synchronization (effective datarate will be unchanged) */
    rate++;
#endif

    return rate;
}

static void ADC_assert(ADCID_t adc)
{
#if USE_INCLINO
    /* SCL is on the same SPI and must be deasserted regardless of whether it is switched on or off! SCL might have been temporarily charged through the SPI lines and will interfere if not deasserted. */
    SCL_DEASSERT();
#endif

    if      (adc == ADC_A1)  { ADC1_ASSERT(); }
    else if (adc == ADC_A2)  { ADC2_ASSERT(); }
    else if (adc == ADC_A3)  { ADC3_ASSERT(); }
    else if (adc == ADC_ALL) { ADC_ASSERT_ALL(); }
}

static void ADC_deassert(ADCID_t adc)
{
    if      (adc == ADC_A1)  { ADC1_DEASSERT(); }
    else if (adc == ADC_A2)  { ADC2_DEASSERT(); }
    else if (adc == ADC_A3)  { ADC3_DEASSERT(); }
    else if (adc == ADC_ALL) { ADC_DEASSERT_ALL(); }
}


/* Functions -----------------------------------------------------------------*/

void ADC_convertRAWToInt32(const uint8_t* in_data, uint32_t num_samples, int32_t* out_data)
{
    if (!in_data || !out_data) { return; }

    /* Convert from unsigned big endian to signed little endian */
    for (uint32_t i = 0; i < num_samples; i++)
    {
        out_data[i] = ADC_VAL_SIGNED(in_data);
        in_data    += GEO_SAMPLE_SIZE;
    }
}

/* Downsampling by linear interpolation
 * Input data must contain in_freq number of samples. Output buffer must be sufficiently large to hold out_freq samples. */
uint32_t ADC_downsample(const int32_t* in_data, uint32_t in_freq, uint32_t out_freq, int32_t* out_data)
{
    if (!in_data || !out_data) { return 0; }

    /* Make sure the selected frequency is valid */
    if ( (in_freq != 1000 && in_freq != 500 && in_freq != 250 && in_freq != 125) || (in_freq <= out_freq) ) { return 0; }

    /* Find upscale and downsample factors */
    uint32_t lcm_val   = lcm(in_freq, out_freq);
    int32_t  upscale   = lcm_val / in_freq;    /* Use int to ensure all operands for calculations below are signed */
    int32_t  downscale = lcm_val / out_freq;
    uint32_t sample_cnt;
    for (sample_cnt = 0; sample_cnt < out_freq; sample_cnt++)
    {
        /* Linear interpolation between the nearest two data points */
        int32_t tmp          = (sample_cnt * downscale);
        out_data[sample_cnt] = ( in_data[tmp / upscale]     * (upscale - (tmp % upscale)) +
                                 in_data[tmp / upscale + 1] * (tmp % upscale) ) / upscale;
    }

    return sample_cnt;
}

bool ADC_isReady(ADCID_t adc)
{
    if ((adc == ADC_ALL || adc == ADC_A1) && !ADC1_IS_RDY()) return false;
#if GEO_NUM_AXES > 1
    if ((adc == ADC_ALL || adc == ADC_A2) && !ADC2_IS_RDY()) return false;
#endif
#if GEO_NUM_AXES > 2
    if ((adc == ADC_ALL || adc == ADC_A3) && !ADC3_IS_RDY()) return false;
#endif
    return true;
}

void ADC_writeRegister(ADCID_t adc, uint8_t reg, uint8_t val)
{
    uint8_t send_buf[2] = { reg, val };
    ADC_assert(adc);
    HAL_SPI_Transmit(&ADC_SPI, send_buf, 2, ADC_SPI_TIMEOUT_MS);
    ADC_deassert(adc);
}

void ADC_sendCmd(ADCID_t adc, uint8_t cmd)
{
    ADC_assert(adc);
    HAL_SPI_Transmit(&ADC_SPI, &cmd, 1, ADC_SPI_TIMEOUT_MS);
    ADC_deassert(adc);
}

uint16_t ADC_readStatus(ADCID_t adc)
{
    uint8_t cmd_buf[3] = { 0 };
    cmd_buf[0] = ADC_REG_ADDR(ADC_REG_STAT, ADC_REG_READ);
    ADC_assert(adc);
    HAL_SPI_TransmitReceive(&ADC_SPI, cmd_buf, cmd_buf, 3, ADC_SPI_TIMEOUT_MS);
    ADC_deassert(adc);

    return (((uint16_t)cmd_buf[1] << 8) | cmd_buf[2]);  /* big endian */
}

/* reads the specified number of samples from a single ADC in polling mode */
bool ADC_readSamples(ADCID_t adc, uint16_t sample_rate, uint32_t num_samples, int32_t* out_samples)
{
    if (!num_samples || !out_samples || adc <= ADC_NONE || adc > ADC_ALL) { return false; }

    /* configure ADC */
#ifdef GPP3X
    uint8_t val = ADC_CTRL1_EXTCK | ADC_CTRL1_PD1;
#else
    /* GPP1X does not have an external reference clock */
    uint8_t val = ADC_CTRL1_PD1;
#endif
    if (Geo.Format == ADC_FORMAT_OFFSETBINARY) { val |= ADC_CTRL1_FORMAT; }
    ADC_writeRegister(adc, ADC_REG_ADDR(ADC_REG_CTRL1, ADC_REG_WRITE), val);
    ADC_writeRegister(adc, ADC_REG_ADDR(ADC_REG_CTRL2, ADC_REG_WRITE), ADC_getPGASetting(Geo.PGA));
    ADC_sendCmd(adc, ADC_CONV_COMMAND(ADC_getConvSetting(sample_rate)));

    /* sync pulse */
    while (!ADC_isReady(adc));
    ADC_SYNC_HIGH();
    Delay(5);           /* Wait at least 2/fclk from ADC (~0.4us) */
    ADC_SYNC_LOW();

    /* read the samples */
    while (num_samples)
    {
        /* wait until a sample is ready */
        while (!ADC_isReady(adc));

        /* read 1 sample */
        ADCID_t id = ((adc == ADC_ALL) ? ADC_A1 : adc);
        do
        {
            ADC_assert(id);
            adc_buf[0] = ADC_REG_ADDR(ADC_REG_DATA, ADC_REG_READ);
            if (HAL_SPI_TransmitReceive(&ADC_SPI, adc_buf, adc_buf, sizeof(adc_buf), ADC_SPI_TIMEOUT_MS) != HAL_OK)
            {
                ADC_deassert(id);
                return false;
            }
            ADC_deassert(id);
            *out_samples = (int32_t)ADC_DATA((uint32_t)adc_buf[1], (uint32_t)adc_buf[2], (uint32_t)adc_buf[3]) - ADC_IDLE_VALUE;
            out_samples++;
            id++;
        }
        while (id < adc);

        num_samples--;
    }
    /* stop conversion */
    ADC_sendCmd(adc, ADC_CONV_COMMAND(ADC_CONV_IMPD));

    return true;
}

/* Send a test command to the ADC and verify the response, returns true on success */
bool ADC_test(ADCID_t adc)
{
    if (Geo.Status != GEO_ACQSTATUS_STOPPED) return false;

    uint16_t adc_stat = ADC_readStatus(adc);
    if ( ((adc_stat >> 8)  != (ADC_STAT_DEFAULT | ADC_STAT_PDSTAT1)) ||
         ((adc_stat & 0xff) & (ADC_STAT_SYSGOR | ADC_STAT_DOR | ADC_STAT_MSTAT | ADC_STAT_RDY)) )
    {
        return false;
    }
    return true;
}

void ADC_enableInterrupts(void)
{
    __HAL_GPIO_EXTI_CLEAR_IT(ADC1_RDY_Pin);
    GPIO_EXTI_IT_ENABLE(ADC1_RDY_Pin);
    HAL_NVIC_EnableIRQ(EXTI0_IRQn);
#if GEO_NUM_AXES > 1
    __HAL_GPIO_EXTI_CLEAR_IT(ADC2_RDY_Pin);
    GPIO_EXTI_IT_ENABLE(ADC2_RDY_Pin);
    HAL_NVIC_EnableIRQ(EXTI3_IRQn);
#endif
#if GEO_NUM_AXES > 2
    __HAL_GPIO_EXTI_CLEAR_IT(ADC3_RDY_Pin);
    GPIO_EXTI_IT_ENABLE(ADC3_RDY_Pin);
    HAL_NVIC_EnableIRQ(EXTI4_IRQn);
#endif
}

void ADC_disableInterrupts(void)
{
    HAL_NVIC_DisableIRQ(EXTI0_IRQn);
    GPIO_EXTI_IT_DISABLE(ADC1_RDY_Pin);
    __HAL_GPIO_EXTI_CLEAR_IT(ADC1_RDY_Pin);
#if GEO_NUM_AXES > 1
    HAL_NVIC_DisableIRQ(EXTI3_IRQn);
    GPIO_EXTI_IT_DISABLE(ADC2_RDY_Pin);
    __HAL_GPIO_EXTI_CLEAR_IT(ADC2_RDY_Pin);
#endif
#if GEO_NUM_AXES > 2
    HAL_NVIC_DisableIRQ(EXTI4_IRQn);
    GPIO_EXTI_IT_DISABLE(ADC3_RDY_Pin);
    __HAL_GPIO_EXTI_CLEAR_IT(ADC3_RDY_Pin);
#endif
}

/* Note: runs in interrupt context! */
void ADC_transferSample(ADCID_t adc)
{
    if (Geo.Status == GEO_ACQSTATUS_STOPPED)
    {
        return;     /* abort, geophone task is not running */
    }
    if (!Geo.First)
    {
        Geo.First = RTC_TIMESTAMP();  /* take timestamp if this is the first sample */
    }
#if ADC_USE_DMA

    if (adc_active == ADC_NONE)
    {
        /* Start DMA to receive 1 measurement (24 bit) */
        ADC_DEASSERT_ALL();

        switch (adc)
        {
        case ADC_A1:
            ADC1_ASSERT();
            break;
        case ADC_A2:
            ADC2_ASSERT();
            break;
        case ADC_A3:
            ADC3_ASSERT();
            break;
        case ADC_ALL:
            return;   /* not supported */
        default:
            DEBUG_PRINT(DBG_LVL_ERROR, "Unexpected ADC type");
            Error_Handler();    /* Cannot recover since stalling the ISR by printing will lead to more errors */
            return;   /* abort */
        }
        adc_active = adc;
        adc_buf[0] = ADC_REG_ADDR(ADC_REG_DATA, ADC_REG_READ);
        if (HAL_SPI_TransmitReceive_DMA(&ADC_SPI, adc_buf, adc_buf, sizeof(adc_buf)) != HAL_OK)
        {
            DEBUG_PRINTF(DBG_LVL_ERROR, "Error in ADC%u SPI transmission", adc);
            Error_Handler();    /* Cannot recover since stalling the ISR by printing will lead to more errors */
        }
    }
    else
    {
        /* Another ADC has requested a DMA transfer, therefore we need to queue the request */
        if (adc_ready & (1 << adc))
        {
            /* Already set -> buffer overrun */
            DEBUG_PRINTF(DBG_LVL_WARNING, "ADC%u sample lost", adc);
            Error_Handler();    /* Cannot recover since stalling the ISR by printing will lead to more errors */
        }
        adc_ready |= (1 << adc);
        /* Request will be handled directly by the DMA */
    }
#else

    switch (adc)
    {
    case ADC_A1:
        ADC1_ASSERT();
        break;
    case ADC_A2:
        ADC2_ASSERT();
        break;
    case ADC_A3:
        ADC3_ASSERT();
        break;
    case ADC_ALL:
        return;   /* not supported */
    default:
        DEBUG_PRINT(DBG_LVL_ERROR, "Unexpected ADC type");
        Error_Handler();    /* Cannot recover since stalling the ISR by printing will lead to more errors */
        return;   /* abort */
    }
    adc_active = adc;
    adc_buf[0] = ADC_REG_ADDR(ADC_REG_DATA, ADC_REG_READ);
    if (HAL_SPI_TransmitReceive(&ADC_SPI, adc_buf, adc_buf, sizeof(adc_buf), ADC_SPI_TIMEOUT_MS) != HAL_OK)
    {
        DEBUG_PRINTF(DBG_LVL_ERROR, "Error in ADC%u SPI transmission", adc);
        Error_Handler();    /* Cannot recover since stalling the ISR by printing will lead to more errors */
    }
    else
    {
        HAL_SPI_TxRxCpltCallback(&ADC_SPI);
    }
    ADC_DEASSERT_ALL();
    adc_active = ADC_NONE;

#endif
}

void ADC_sync(void)
{
#if ADC_SYNC_PERIOD

    /* Make sure the following code cannot be interrupted by a higher priority interrupt (such as ADC ready).
     * Note that this function must not be called from an interrupt that has higher priority than the ADC ready or SPI TXCplt ISR. */
    BaseType_t intst = taskENTER_CRITICAL_FROM_ISR();

    /* Toggle ADC SYNC pin: this will abort the current sample conversion and restart sampling; the samples already collected (in the data register) will be cleared and the ADC ready signal deasserted) */
    ADC_SYNC_HIGH();
    Delay(5);           /* Wait at least 2/fclk from ADC (~0.4us) */
    ADC_SYNC_LOW();

    adc_masked = 0;     /* Unmask all ADCs */
    adc_ready  = 0;     /* Samples in data registers have just been cleared */
    adc_active = 0;

    taskEXIT_CRITICAL_FROM_ISR(intst);

#else

    ADC_SYNC_HIGH();
    Delay(5);           /* Wait at least 2/fclk from ADC (~0.4us) */
    ADC_SYNC_LOW();

#endif
}

/* Start sampling on all 3 channels */
void ADC_start(bool sync)
{
    /* Make sure the ADC is switched on */
    if (!ADC_IS_SWON())
    {
        /* Make sure ADC CS line is de-asserted before turning it on to prevent erroneous signalling */
        /* NOTE: Line should be asserted when waking up from sleep to prevent backdrive! */
        ADC_SWON();
        ADC_CLK_SWON();
        AVDD_SWON();
        Delay(ADC_POWER_ON_DELAY_US);  /* Wait for ADCs to be ready to receive commands */
    }

    /* ADC Config CTRL1 */
#ifdef GPP3X
    uint8_t val = ADC_CTRL1_EXTCK | ADC_CTRL1_PD1;
#else
    /* GPP1X does not have an external reference clock */
    uint8_t val = ADC_CTRL1_PD1;
#endif
    if (Geo.Format == ADC_FORMAT_OFFSETBINARY)
    {
        val |= ADC_CTRL1_FORMAT;
    }
    ADC_writeRegister(ADC_ALL, ADC_REG_ADDR(ADC_REG_CTRL1, ADC_REG_WRITE), val);

    /* ADC Config CTRL2 */
    ADC_writeRegister(ADC_ALL, ADC_REG_ADDR(ADC_REG_CTRL2, ADC_REG_WRITE), ADC_getPGASetting(Geo.PGA));

    /* ADC Config CTRL3 */
#if ADC_USE_FIR_FILTER
    ADC_writeRegister(ADC_ALL, ADC_REG_ADDR(ADC_REG_CTRL3, ADC_REG_WRITE), ADC_CTRL3_FILT1);    /* 10 = FIR, 11 = FIR + IIR */
#endif

    /* ADC Start */
    ADC_sendCmd(ADC_ALL, ADC_CONV_COMMAND(ADC_getConvSetting(Geo.SPS)));

    if (sync)
    {
        /* Wait until all used ADCs started conversion, otherwise pulling SYNC high has no effect (DS p.48 "pulse mode") */
        while (!ADC_isReady(ADC_ALL))  { __NOP(); }

        ADC_sync();
    }
}

void ADC_stop(bool switch_off)
{
    ADC_sendCmd(ADC_ALL, ADC_CONV_COMMAND(ADC_CONV_IMPD));    /* 1001 1010 stop conversion, power down using IMPD */

    if (switch_off)
    {
        /* Turn off ADC power supply and clock */
        ADC_SWOFF();    /* Also sets ADC CS lines and SYNC low and turns off the ADC clock */
        AVDD_SWOFF();
    }
}


/*** ADC SPI DMA Transfer Complete Callback ***********************************/
void HAL_SPI_TxRxCpltCallback(SPI_HandleTypeDef *hspi)
{
    /* Arrays require size +1 since the access is 1-based (e.g. ADC_A1 = 1) */
    static uint32_t   sample_ctr[ADC_COUNT + 1]  = { 0 };
    static ADCData_t* rb_head_ptr[ADC_COUNT + 1] = { 0 };
    BaseType_t        xHigherPriorityTaskWoken   = pdFALSE;

    (void)hspi;

#if ADC_USE_DMA
    ADC_DEASSERT_ALL();
#endif

    if (adc_active == ADC_NONE) { return; }

    /* Set Data ptr to current head in rb_adc (this contains the most recent ADC chunk) */
    if (!(rb_head_ptr[adc_active]))
    {
        /* Check for buffer overflow */
        if (RB_isFull(adc_rb[adc_active]))
        {
            ADC_disableInterrupts();
            DEBUG_PRINT(DBG_LVL_ERROR, "ADC buffer overflow");
            Error_Handler_Idle();
            xSemaphoreGiveFromISR(xSemaphore_GeoStop, &xHigherPriorityTaskWoken);
            return;
        }
        rb_head_ptr[adc_active] = (ADCData_t*)RB_insert(adc_rb[adc_active], NULL);
        sample_ctr[adc_active]  = 0;
        /* Write header */
        rb_head_ptr[adc_active]->acq_id    = Geo.ID;
        rb_head_ptr[adc_active]->len       = Geo.SPS * GEO_SAMPLE_SIZE;
        rb_head_ptr[adc_active]->timestamp = RTC_TIMESTAMP();
    }
    else if ((uint8_t*)rb_head_ptr[adc_active] != RB_getHead(adc_rb[adc_active]))
    {
        DEBUG_PRINTF(DBG_LVL_ERROR, "RB head for ADC %u does not correspond to stored head pointer!", adc_active);
        Error_Handler();
    }

#if GEO_STORE_TH_IN_LSB
    /* Use LSB to indicate whether the trigger threshold is exceeded (reduces effective ADC resolution by 1 bit) */
    if (PIN_GET(TRGP) || PIN_GET(TRGN))
    {
        adc_buf[GEO_SAMPLE_SIZE] |= 1;
        //LED_B_ON();
    }
    else
    {
        adc_buf[GEO_SAMPLE_SIZE] &= ~1;
        //LED_B_OFF();
    }
#endif

#if ADC_SYNC_PERIOD
    if (!(adc_masked & (1 << adc_active)))    /* Only keep the sample if not masked */
    {
#endif

    /* Copy 1 sample to ring buffer */
    memcpy((rb_head_ptr[adc_active]->data + sample_ctr[adc_active] * GEO_SAMPLE_SIZE), &adc_buf[1], GEO_SAMPLE_SIZE);

    sample_ctr[adc_active]++;

    Geo.Samples++;
    if (adc_active == ADC_A1)
    {
        Geo.Samples_TrgADC++;     /* Increase sample count for trigger Geophone */

        /* Trigger statistics */
        uint32_t curr_sample = ADC_DATA((uint32_t)adc_buf[1], (uint32_t)adc_buf[2], (uint32_t)adc_buf[3]);  /* Convert to little endian */
        ADC_updateTriggerStats(curr_sample);
    }

#if ADC_SYNC_PERIOD
    }
    adc_masked ^= (1 << adc_active);    /* Toggle bit to mask ADC (-> next sample will be skipped) */
#endif

    /* Check whether a chunk is fully received - chunk size is 1x Geo.SPS (1 second of data) */
    if (sample_ctr[adc_active] == Geo.SPS)
    {
        rb_head_ptr[adc_active] = 0;
        sample_ctr[adc_active]  = 0;

        Geo.ChunksSampled++;

        if ( !(SysConf.OpMode & SYS_OPMODE_CONT) && (Geo.ChunksRemaining > 0) )
        {
            Geo.ChunksRemaining--;
        }

        if ( ((Geo.ChunksSampled % GEO_NUM_AXES) == 0) &&                                                     /* Wait until chunks of all axes full, and */
             ( (Geo.Status == GEO_ACQSTATUS_ABORT)                             ||                             /* Abort signal sent, or */
               (Geo.ChunksSampled >= (GEO_CONT_ACQ_FILE_LEN_S * GEO_NUM_AXES)) ||                             /* Max file size reached, or */
               (Geo.ChunksRemaining == 0)                                      ||                             /* No more chunks to collect, or */
               ((SysConf.OpMode & SYS_OPMODE_TRG) && (Geo.ChunksSampled >= (Geo.Timeout * GEO_NUM_AXES))) ) ) /* In triggered mode and timeout reached. */
        {
            /* TODO: check whether it is necessary to disable ADC interrupts here already (ADC_disableInterrupts), especially with 1kHz sampling rate */
            Geo.End = RTC_TIMESTAMP();
            memset(sample_ctr,    0, sizeof(sample_ctr));
            memset(rb_head_ptr,   0, sizeof(rb_head_ptr));
            xSemaphoreGiveFromISR(xSemaphore_GeoStop, &xHigherPriorityTaskWoken);
        }

        /* Logging is initiated here to save a context switch and be faster */
        RTOS_queueSendLogFromISR(LOGTYPE_ADC1_CHUNK + adc_active - 1, &xHigherPriorityTaskWoken);

#if IMU_SAMPLE_WITH_GEO
        if (SysConf.Imu->Status)
        {
            /* Log IMUDATA */
            /* Only the `signal` to log task is initiated here.
             * Since rb_imudata contains multiple items (and maybe data with different IDs too),
             * the Log task handles the entire logging procedure (i.e. extracting the pointers to data to be logged). */
            RTOS_queueSendLogFromISR(LOGTYPE_IMUDATA, &xHigherPriorityTaskWoken);
        }
#endif /* IMU_SAMPLE_WITH_GEO */
    }

#if ADC_USE_DMA
    /* Release DMA - check first whether we have queued requests */
    if (adc_ready & (1 << adc_active))
    {
        DEBUG_PRINTF(DBG_LVL_ERROR, "ADC%u keeps SPI blocked for other ADCs! Current requests: %u", adc_active, adc_ready);
        Error_Handler();
    }
    adc_active = ADC_NONE;

    if (adc_ready & (1 << ADC_A1))
    {
        adc_ready &= ~(1 << ADC_A1);      /* Clear flag */
        ADC_transferSample(ADC_A1);
    }
    else if (adc_ready & (1 << ADC_A2))
    {
        adc_ready &= ~(1 << ADC_A2);      /* Clear flag */
        ADC_transferSample(ADC_A2);
    }
    else if (adc_ready & (1 << ADC_A3))
    {
        adc_ready &= ~(1 << ADC_A3);      /* Clear flag */
        ADC_transferSample(ADC_A3);
    }
#endif

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

/* SPI error occurred (maybe due to overrun) */
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
    (void)hspi;

    Error_Handler();
}

