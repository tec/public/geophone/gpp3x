/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Time synchronization
 *
 * via Bolt:
 * - TREQ line is pushed high and the timeout timer (LPTIM1) is started at the "same" time
 * - when a timesync message has been received by the Bolt task, the timestamp is forwarded to the TSync task
 * - the TSync task then reads the timeout counter value to determine the elapsed time since TREQ high and calculates the time delta to the next full second
 * - the timeout timer is started such that it overflows on the next full second and the RTC is updated within the overflow interrupt
 * - the TSync task estimates the drift and updates the RTC drift compensation
 *
 * via GNSS:
 * - TSync task polls the GNSS task, which enables the GNSS module and starts reading messages until a TP message has been received
 * - less than 1s after the TP message, an actual time pulse (PPS pin) will be sent by the GNSS module
 * - the pulse triggers an interrupt that executes TSync_capture(), which then stops the timeout counter and updates the RTC (since the interrupt is aligned to a full second)
 * - the TSync task estimates the drift and updates the RTC drift compensation
 *
 * via USB:
 * - TSync task polls the USB RX flag every TSYNC_USB_POLL_MS ms
 * - Once set, the UNIX time in microseconds is extracted from the received data (simple string parsing)
 * - Together with the reception timestamp (USB.rxTS), a rough time synchronization can be achieved.
 */

#include "main.h"

#if !TSYNC_METHOD_NONE || TSYNC_MASTER

/* Types ---------------------------------------------------------------------*/
typedef enum
{
    TSYNC_STATUS_STOPPED,
    TSYNC_STATUS_RUNNING,
    TSYNC_STATUS_UPDATING,
} TSyncStatus_t;

/* Variables -----------------------------------------------------------------*/
static TSyncStatus_t  tsync_status      = TSYNC_STATUS_STOPPED;
static uint32_t       tsync_duration_s  = 0;
static uint32_t       tsync_epoch       = 0;
static uint64_t       tsync_ts_us       = 0;
static uint64_t       last_tsync_uptime = 0;
static uint32_t       timeout_cnt       = 0;
static int8_t         avg_drift_ppm     = 0;

/* External variables --------------------------------------------------------*/
extern USB_t          USB;
extern rb_t           rb_tsync;

/* Functions -----------------------------------------------------------------*/

uint32_t TSync_getTimeoutCounter(void)
{
    uint64_t timestamp;
    uint16_t hw, hw2;

    /* make sure this routine runs atomically */
    bool isr = IS_ISR();
    if (!isr)
    {
        taskENTER_CRITICAL();
    }
    timestamp = tsync_duration_s;

    /* Read the timer counter */
    do
    {
        /* Since LPTIM1 counter clock is asynchronous, we need to read until two consecutive values match */
        hw  = __HAL_TIM_GET_COUNTER(&hlptim1);
        hw2 = __HAL_TIM_GET_COUNTER(&hlptim1);
    }
    while (hw != hw2);

    if (__HAL_LPTIM_GET_FLAG(&hlptim1, LPTIM_FLAG_ARRM))
    {
        timestamp++;
        do
        {
            hw  = __HAL_TIM_GET_COUNTER(&hlptim1);
            hw2 = __HAL_TIM_GET_COUNTER(&hlptim1);
        }
        while (hw != hw2);
    }
    /* The timer counter is the sub-second part with a resolution of 15 bits */
    timestamp <<= 15;
    timestamp |= (hw & 0x7fff);

    if (!isr)
    {
        taskEXIT_CRITICAL();
    }

    return LPTIMER_TICKS_TO_US(timestamp);
}


void TSync_sendTimestamp(void)
{
    /* Send the current timestamp via Bolt */
    COM_TREQ_LOW();
    COM_TREQ_HIGH();
    uint64_t timestamp = RTC_getTimestampUS();
    Bolt_enqueueMessage(DPP_MSG_TYPE_TIMESYNC, (uint8_t*)&timestamp);
    DEBUG_PRINTF(DBG_LVL_VERBOSE, "Timestamp %llu sent to Bolt", timestamp);
    COM_TREQ_LOW();
}


void TSync_request(void)
{
    /* Only trigger if not already pending */
    if (tsync_status == TSYNC_STATUS_STOPPED)
    {
#if TSYNC_METHOD_GNSS

        DEBUG_PRINT(DBG_LVL_INFO, "GNSS Tsync Req");
        taskENTER_CRITICAL();
        tsync_status     = TSYNC_STATUS_RUNNING;
        tsync_duration_s = 0;
        tsync_epoch      = 0;
        tsync_ts_us      = 0;
        HAL_LPTIM_Counter_Start_IT(&hlptim1, LPTIMER_FREQUENCY - 1);   /* count up to 32767 (-> 1 overflow interrupt per second) */
        taskEXIT_CRITICAL();
        /* Remark: Alternatively, the timeout feature of LPTIM1 could be used:
         * HAL_LPTIM_TimeOut_Start_IT(&hlptim1, LPTIMER_FREQUENCY - 1, LPTIMER_FREQUENCY - 1) to start and then __HAL_LPTIM_START_CONTINUOUS(&hlptim1) to reset */
        xTaskNotify(xTaskHandle_GNSS, GNSS_NOTIFY_TSYNC_INIT, eSetValueWithoutOverwrite);

#elif TSYNC_METHOD_BOLT

        if (!SysConf.ComBoard) { return; }    /* Abort, cannot sync */

        DEBUG_PRINT(DBG_LVL_INFO, "Bolt Tsync Req");
        taskENTER_CRITICAL();
        tsync_status     = TSYNC_STATUS_RUNNING;
        tsync_duration_s = 0;
        tsync_epoch      = 0;
        tsync_ts_us      = 0;
        HAL_LPTIM_Counter_Start_IT(&hlptim1, LPTIMER_FREQUENCY - 1);   /* count up to 32767 (-> 1 overflow interrupt per second) */
        COM_TREQ_HIGH();
        taskEXIT_CRITICAL();

        NOTIFY_BOLT_TASK();   /* Make sure the Bolt queue is empty */

#elif TSYNC_METHOD_NONE && TSYNC_MASTER

        TSync_sendTimestamp();

#endif
    }
}


void TSync_timeout(void)
{
    if (tsync_status == TSYNC_STATUS_STOPPED) { return; }

    DEBUG_PRINT(DBG_LVL_WARNING, "Tsync timeout");
    HAL_LPTIM_Counter_Stop_IT(&hlptim1);   /* Make sure the timer is stopped */

#if TSYNC_METHOD_GNSS

    xTaskNotify(xTaskHandle_GNSS, GNSS_NOTIFY_TSYNC_ABORT, eSetValueWithoutOverwrite);

#elif TSYNC_METHOD_BOLT

    COM_TREQ_LOW();
    /* Make sure the Bolt queue is empty (just in case the IND high interrupt got lost) */
    NOTIFY_BOLT_TASK();
#endif

    tsync_status = TSYNC_STATUS_STOPPED;
    tsync_epoch  = 0;
    timeout_cnt++;
    RTOS_event(EVENT_GEOPHONE3X_TSYNC_TIMEOUT, timeout_cnt, EVTLOG_ALL);

#if TSYNC_FALLBACK_TH
    if (timeout_cnt == TSYNC_FALLBACK_TH)
    {
        Schedule_adjustTaskPeriod(TASK_TSYNC, TSYNC_FALLBACK_PERIOD_S);
        NOTIFY_GNSS_TASK(GNSS_NOTIFY_STOP);
    }
    if (timeout_cnt >= TSYNC_FALLBACK_TH)
    {
        Schedule_postponeTask(TASK_GNSS, TSYNC_FALLBACK_PERIOD_S);
    }
#endif
}


static void TSync_update(void)
{
    TSyncInfo_t tsync_info      = { 0 };
    uint32_t    time_to_sync_us = 0;     /* Remaining time until the sync occurs */

#if TSYNC_METHOD_GNSS

    tsync_info.uptime         = RTC_getUptimeMS();
    tsync_info.rcvd_timestamp = S_TO_US((uint64_t)tsync_epoch);
    tsync_info.rtc_timestamp  = tsync_ts_us;
    tsync_info.duration       = tsync_duration_s;
    DEBUG_PRINTF(DBG_LVL_VERBOSE, "Tsync took %us", tsync_info.duration);

    /* Nothing else to do: RTC update has already been triggered in the capture callback function */
    tsync_status = TSYNC_STATUS_STOPPED;

#elif TSYNC_METHOD_BOLT

    COM_TREQ_LOW();

    tsync_info.rcvd_timestamp = tsync_ts_us;

    /* Check if reasonable */
  #if TSYNC_CHECK_MIN_VAL_S
    uint32_t rcvd_timestamp_s = US_TO_S(tsync_ts_us);
    if (rcvd_timestamp_s < TSYNC_CHECK_MIN_VAL_S)
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "Bolt Tsync potentially invalid timestamp (%lu)", rcvd_timestamp_s);
        RTOS_event(EVENT_GEOPHONE3X_TSYNC_INV_VAL, rcvd_timestamp_s, EVTLOG_ALL);
    }
  #endif /* TSYNC_CHECK_MIN_VAL_S */

    taskENTER_CRITICAL();

    /* Add elapsed time to timestamp received over Bolt */
    uint32_t elapsed_us       = TSync_getTimeoutCounter();
    tsync_info.duration       = US_TO_S(elapsed_us);
    HAL_LPTIM_Counter_Stop_IT(&hlptim1);
    tsync_ts_us              += elapsed_us;
    /* Add estimated processing (mostly LPTIM start and RTC time setting) */
    tsync_ts_us              += RTC_SETTIME_OVERHEAD_US + LPTIMER_TICKS_TO_US(2 * LPTIMER_RESTART_OVERHEAD);
    tsync_epoch               = (uint32_t)(US_TO_S(tsync_ts_us) + 1);    /* Sync will occur on the next full second */
    tsync_info.rtc_timestamp  = RTC_TIMESTAMP();
    tsync_info.uptime         = RTC_getUptimeMS();

    /* Calculate time difference to the next full second.
     * This will be the moment when the time needs to be set to the following value:
     * Received timestamp in seconds (without second fraction), plus one.
     * Reason: when time is set, the SSR (subsecond-register) of the RTC resets to PREDIV_S. */
    time_to_sync_us    = 1000000 - (uint32_t)(tsync_ts_us % 1000000);    /* timestamp is in us */
    uint32_t rem_ticks = LPTIMER_US_TO_TICKS((uint64_t)time_to_sync_us);
    if (rem_ticks <= 1)
    {
        /* Adjust the RTC now */
        RTC_setFromEpoch(tsync_epoch);
        tsync_status = TSYNC_STATUS_STOPPED;
    }
    else
    {
        /* Schedule RTC adjustment */
        HAL_LPTIM_Counter_Start_IT(&hlptim1, rem_ticks - 1);
    }
    taskEXIT_CRITICAL();

#elif TSYNC_METHOD_USB

    tsync_info.rcvd_timestamp = tsync_ts_us;
    uint32_t timestamp_s      = US_TO_S(tsync_ts_us);
    int32_t  elapsed_ms       = HAL_GetTick() - USB.rxTS;
    tsync_info.duration       = MS_TO_S(elapsed_ms);

  #if TSYNC_CHECK_MIN_VAL_S
    if ( ((timestamp_s > 0) && (timestamp_s < TSYNC_CHECK_MIN_VAL_S)) || (elapsed_ms <= 0) )
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "USB Tsync potentially invalid timestamp (%lu)", timestamp_s);
        RTOS_event(EVENT_GEOPHONE3X_TSYNC_INV_VAL, timestamp_s, EVTLOG_ALL);
        return;     /* Ignore timestamp (framing errors are quite common due to the way it is implemented) */
    }
  #endif

    taskENTER_CRITICAL();

    elapsed_ms               = HAL_GetTick() - USB.rxTS;                                                /* Update again */
    tsync_ts_us             += MS_TO_US(elapsed_ms);
    tsync_ts_us             += RTC_SETTIME_OVERHEAD_US + LPTIMER_TICKS_TO_US(LPTIMER_RESTART_OVERHEAD); /* Actually irrelevant considering the targeted accuracy, but add it anyways */
    tsync_epoch              = (uint32_t)(US_TO_S(tsync_ts_us) + 1);                                    /* Sync will occur on the next full second */
    tsync_info.rtc_timestamp = RTC_TIMESTAMP();
    tsync_info.uptime        = RTC_getUptimeMS();
    time_to_sync_us          = 1000000 - (uint32_t)(tsync_ts_us % 1000000);
    uint32_t rem_ticks       = LPTIMER_US_TO_TICKS((uint64_t)time_to_sync_us);
    if (rem_ticks <= 1)
    {
        /* Adjust the RTC now */
        RTC_setFromEpoch(tsync_epoch);
        tsync_status = TSYNC_STATUS_STOPPED;
    }
    else
    {
        /* Schedule RTC adjustment */
        HAL_LPTIM_Counter_Start_IT(&hlptim1, rem_ticks - 1);
    }
    taskEXIT_CRITICAL();

#endif

    /* RTC offset estimation */
    int64_t offset_us = (S_TO_US((int64_t)tsync_epoch) - (int64_t)(tsync_info.rtc_timestamp + time_to_sync_us));
    if (offset_us > INT32_MAX)      { tsync_info.offset = INT32_MAX; }
    else if (offset_us < INT32_MIN) { tsync_info.offset = INT32_MIN; }
    else                            { tsync_info.offset = offset_us; }

    bool first_sync = false;
    if (last_tsync_uptime)
    {
        /* Calculate the current clock offset (a positive value means the RTC is behind) and drift */
        int32_t diff_ms   = tsync_info.uptime - last_tsync_uptime;
        int32_t drift_ppm = US_TO_NS(offset_us) / diff_ms;      /* ns / ms = ppm */
        if ( (drift_ppm > TSYNC_MAX_DRIFT_PPM) || (drift_ppm < -TSYNC_MAX_DRIFT_PPM) )
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "Drift exceeds limit");
            if (drift_ppm > TSYNC_MAX_DRIFT_PPM) { drift_ppm =  TSYNC_MAX_DRIFT_PPM; }
            else                                 { drift_ppm = -TSYNC_MAX_DRIFT_PPM; }
        }
        avg_drift_ppm             += drift_ppm;
        tsync_info.drift_estimated = drift_ppm;
        tsync_info.drift_comp      = avg_drift_ppm;
        RTC_compensateDrift(avg_drift_ppm);
        DEBUG_PRINTF(DBG_LVL_INFO, "RTC synced (Offset: %ldus, Estimated drift: %ldppm, Applied compensation: %dppm)", tsync_info.offset, tsync_info.drift_estimated, tsync_info.drift_comp);
    }
    else
    {
        /* Print clock offset */
        DEBUG_PRINTF(DBG_LVL_INFO, "RTC synced (Offset: %ldus)", tsync_info.offset);
        first_sync = true;
    }
    last_tsync_uptime = tsync_info.uptime + US_TO_MS(time_to_sync_us);

    /* Store Tsync information on the SD card */
    RB_insert(&rb_tsync, (uint8_t*)&tsync_info);
    RTOS_queueSendLog(LOGTYPE_TSYNC);

#if TSYNC_FALLBACK_TH
    if (timeout_cnt)
    {
        Schedule_adjustTaskPeriod(TASK_TSYNC, TSYNC_PERIOD_S);    /* Reset to regular timesync period */
    }
#endif
    timeout_cnt = 0;

    /* Time adjustment has not yet occurred, wait until the RTC has been synced */
    if (tsync_status != TSYNC_STATUS_STOPPED)
    {
        vTaskDelay(pdMS_TO_TICKS(US_TO_MS(time_to_sync_us) + 1));
    }

#if TSYNC_ACQ_ID_CHANGE_OFS_S
    /* Check whether there was a jump in time */
    if (ABS(offset_us) > S_TO_US((uint64_t)TSYNC_ACQ_ID_CHANGE_OFS_S))
    {
        Geo_abort(true);    /* Tell the Geo task to restart the acquisition */
    }
#endif

#if TSYNC_MASTER
    TSync_sendTimestamp();
#endif

    if (first_sync)
    {
        RTOS_buzzer(BUZZER_TIME_TSYNC_MS, 1);    /* First sync: acoustic feedback */

#if GEO_WAIT_FOR_TSYNC
        /* Start geophone sampling if continuous mode is enabled and not yet running */
        if (SysConf.OpMode & SYS_OPMODE_CONT)
        {
            Geo_initAcq(TRG_EXT, 0, 0);
        }
#endif
    }
}


#if TSYNC_METHOD_GNSS

void TSync_capture(void)
{
    /* NOTE: runs in interrupt context! */
    if ( (tsync_status == TSYNC_STATUS_RUNNING) && tsync_epoch )    /* tsync_epoch != 0 means it has been set via TSync_setNextEpoch() */
    {
        UBaseType_t intst = taskENTER_CRITICAL_FROM_ISR();
        tsync_ts_us       = RTC_TIMESTAMP();
        tsync_status      = TSYNC_STATUS_UPDATING;
        RTC_setFromEpoch(tsync_epoch);
        taskEXIT_CRITICAL_FROM_ISR(intst);
        HAL_LPTIM_Counter_Stop_IT(&hlptim1);
        /* Important: The LPTIM interrupt flags must be cleared manually since this function runs in a context with higher priority than the LPTIM ISR.
         * If the LPTIM interrupt occurs while executing this function, the LPTIM handler (which only clears the ISR flag if the interrupt is actually
         * enabled) will be triggered infinitely after exiting this function. */
        __HAL_LPTIM_CLEAR_FLAG(&hlptim1, LPTIM_FLAG_CMPM | LPTIM_FLAG_ARRM);
        vTaskNotifyGiveFromISR(xTaskHandle_TSync, NULL);
    }
}

#endif


#if TSYNC_METHOD_GNSS

void TSync_setNextEpoch(uint32_t next_epoch)
{
    if (tsync_status == TSYNC_STATUS_RUNNING)
    {
        tsync_epoch = next_epoch;
    }
}

#endif


#if TSYNC_METHOD_BOLT

void TSync_setTimestamp(uint64_t ts_us)
{
    if (tsync_status == TSYNC_STATUS_RUNNING)
    {
        tsync_ts_us  = ts_us;
        tsync_status = TSYNC_STATUS_UPDATING;
        xTaskNotifyGive(xTaskHandle_TSync);
    }
}

#endif


/*** Tsync Task **********************************************************/
void vTaskTsync(void* arg)
{
    (void)arg;

    LPTIM1_init();

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Tsync task started");

    /* Request timesync at startup */
    NOTIFY_TSYNC_TASK();

    while (true)
    {
#if TSYNC_METHOD_USB

        ulTaskNotifyTake(pdTRUE, pdMS_TO_TICKS(TSYNC_USB_POLL_MS));

        if (USB.rxFlag)
        {
            tsync_ts_us = strtoll((const char*)USB.rcvdData, 0, 10);
            if (tsync_ts_us)
            {
                tsync_status = TSYNC_STATUS_UPDATING;
                TSync_update();
                DEBUG_PRINTF(DBG_LVL_VERBOSE, "Timestamp received: %llu", tsync_ts_us);
            }
            USB.rxFlag = 0;
        }
#else
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);            /* wait for a non-zero notification */

        DEBUG_PRINT(DBG_LVL_VERBOSE, "Tsync task is running");

        /* Is there a time-sync request pending? */
        switch (tsync_status)
        {
        case TSYNC_STATUS_RUNNING:
            if (tsync_duration_s >= TSYNC_TIMEOUT_S)
            {
                TSync_timeout();
            }
            break;

        case TSYNC_STATUS_UPDATING:
            TSync_update();
            break;

        case TSYNC_STATUS_STOPPED:
            TSync_request();
            break;
        }
#endif
    }
}

/* LP timer overflow interrupt */
void HAL_LPTIM_AutoReloadMatchCallback(LPTIM_HandleTypeDef *hlptim)
{
    if (hlptim->Instance == LPTIM1)
    {
        if (tsync_status == TSYNC_STATUS_RUNNING)
        {
            tsync_duration_s++;
            if (tsync_duration_s >= TSYNC_TIMEOUT_S)
            {
                /* Timeout -> stop timer and notify the time-sync task */
                HAL_LPTIM_Counter_Stop_IT(hlptim);
                vTaskNotifyGiveFromISR(xTaskHandle_TSync, NULL);
            }
        }
#if !TSYNC_METHOD_GNSS
        else if (tsync_status == TSYNC_STATUS_UPDATING)
        {
            if (tsync_epoch)
            {
                UBaseType_t intst = taskENTER_CRITICAL_FROM_ISR();
                RTC_setFromEpoch(tsync_epoch);
                tsync_status = TSYNC_STATUS_STOPPED;
                tsync_epoch  = 0;
                taskEXIT_CRITICAL_FROM_ISR(intst);
                HAL_LPTIM_Counter_Stop_IT(hlptim);
            }
        }
#endif
        else
        {
            HAL_LPTIM_Counter_Stop_IT(hlptim);
        }
    }
}

#endif /* TSYNC_METHOD_NONE */
