/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"


/* Functions -----------------------------------------------------------------*/

uint32_t MSeed_numBits(uint32_t val)
{
    uint32_t bits = 31;
    while (bits > 0)
    {
        uint32_t mask = (1 << bits);
        if (val & mask)
        {
            if (val > mask) { bits++; }
            break;
        }
        bits--;
    }
    return bits;
}


mseed_header_t* MSeed_initHeader(uint8_t* buffer)
{
    static mseed_header_t mseed_header;

    memset(&mseed_header, 0, sizeof(mseed_header_t));                       /* make sure all unused fields are set to 0 */
    memcpy(mseed_header.fsdh.seq_no,   "000000", MSEED_FSDH_SEQNO_LEN);     /* padded with zeros on the left */
    mseed_header.fsdh.data_quality     = 'R';                               /* 'R' for raw data */
#ifndef MSEED_HDR_STATION
    char MSEED_HDR_STATION[6];
    snprintf(MSEED_HDR_STATION, 6, "%05u", SysConf.DeviceID);
#endif
    memcpy(mseed_header.fsdh.station,  MSEED_HDR_STATION,  MSEED_FSDH_STATION_LEN);   /* padded with white spaces at the end */
    memcpy(mseed_header.fsdh.location, MSEED_HDR_LOCATION, MSEED_FSDH_LOCATION_LEN);  /* padded with white spaces at the end */
    memcpy(mseed_header.fsdh.channel,  "   ",              MSEED_FSDH_CHANNEL_LEN);   /* padded with white spaces at the end */
    memcpy(mseed_header.fsdh.network,  MSEED_HDR_NETWORK,  MSEED_FSDH_NETWORK_LEN);   /* padded with white spaces at the end */
    mseed_header.fsdh.start_time.year  = 1970;
    mseed_header.fsdh.start_time.day   = 1;
    mseed_header.fsdh.start_time.hour  = 12;
    mseed_header.fsdh.start_time.min   = 0;
    mseed_header.fsdh.start_time.sec   = 0;
    mseed_header.fsdh.start_time.fract = 0;
    mseed_header.fsdh.num_samples      = 0;                                 /* will be set later */
    mseed_header.fsdh.samprate_fact    = 0;                                 /* will be set later */
    mseed_header.fsdh.samprate_mult    = 1;                                 /* note: multiplier <0 will be treated as divider! */
    mseed_header.fsdh.act_flags        = MSEED_ACTFLAG_TIME_CORR;
    mseed_header.fsdh.io_flags         = 0;
    mseed_header.fsdh.dq_flags         = 0;
    mseed_header.fsdh.num_blockettes   = 1;
    mseed_header.fsdh.time_correct     = 0;                                 /* time correction to apply, in 100us steps */
    mseed_header.fsdh.data_ofs         = MSEED_HEADER_LEN;
    mseed_header.fsdh.blockette_ofs    = MSEED_FSDH_LEN;
    mseed_header.bl1k.type             = 1000;                              /* blockette 1000 */
    mseed_header.bl1k.next             = 0;                                 /* no more blockettes follow */
    mseed_header.bl1k.encoding         = MSEED_ENCODING;
    mseed_header.bl1k.endian           = MSEED_DATA_LITTLE_ENDIAN;
    mseed_header.bl1k.data_len         = MSEED_DATA_LEN_EXP;                /* exponent (2^x) of data record length, min. value is 8 (256 bytes) -> will be set later */

    /* If a valid buffer address is provided: copy data */
    if (buffer)
    {
        memcpy(buffer, &mseed_header, sizeof(mseed_header_t));
        return (mseed_header_t*)buffer;
    }
    return &mseed_header;
}


void MSeed_setHeaderFields(mseed_header_t* header, uint32_t seq_no, char channel, uint16_t num_samples, uint16_t sample_size, uint16_t sample_rate, uint64_t start_time)
{
    char seqno_buf[MSEED_FSDH_SEQNO_LEN + 1];

    if (!header) { return; }

    snprintf(seqno_buf, MSEED_FSDH_SEQNO_LEN + 1, "%06lu", seq_no);
    memcpy(header->fsdh.seq_no, seqno_buf, MSEED_FSDH_SEQNO_LEN);   /* padded with zeros on the left */

    if (start_time)
    {
        RTC_DateTypeDef date;
        RTC_TimeTypeDef time;
        uint32_t epoch = US_TO_S(start_time);
        RTC_convertEpochToDatetime(epoch, &date, &time);            /* as an alternative: use localtime() */
        header->fsdh.start_time.year  = (date.Year >= 70) ? (1900 + date.Year) : (2000 + date.Year);
        header->fsdh.start_time.day   = RTC_convertToDayOfYear(date.Year, date.Month, date.Date);
        header->fsdh.start_time.hour  = time.Hours;
        header->fsdh.start_time.min   = time.Minutes;
        header->fsdh.start_time.sec   = time.Seconds;
        header->fsdh.start_time.fract = (start_time - S_TO_US((uint64_t)epoch)) / 100;     /* subseconds in 100us steps */
        header->fsdh.num_samples      = num_samples;
        header->fsdh.samprate_fact    = sample_rate;
    }

    if (channel == 'Z' || channel == 'E' || channel == 'N')
    {
        /* Use standard channel names (EHZ, EHE, EHN) */
        header->fsdh.channel[0] = 'E';
        header->fsdh.channel[1] = 'H';
        header->fsdh.channel[2] = channel;
    }

    /* Find out how many bits are necessary to address 'data_len' bytes */
    if (sample_size)
    {
        uint32_t data_len     = num_samples * sample_size + sizeof(mseed_header_t);
        header->bl1k.data_len = MSeed_numBits(data_len);
    }
}


static uint32_t MSeed_steim1Minbits(int32_t diff)
{
    if      ( (diff >= -(1 <<  3)) && (diff < (1 <<  3)) ) return  4;
    else if ( (diff >= -(1 <<  4)) && (diff < (1 <<  4)) ) return  5;
    else if ( (diff >= -(1 <<  5)) && (diff < (1 <<  5)) ) return  6;
    else if ( (diff >= -(1 <<  7)) && (diff < (1 <<  7)) ) return  8;
    else if ( (diff >= -(1 <<  9)) && (diff < (1 <<  9)) ) return 10;
    else if ( (diff >= -(1 << 14)) && (diff < (1 << 14)) ) return 15;
    else if ( (diff >= -(1 << 15)) && (diff < (1 << 15)) ) return 16;
    else if ( (diff >= -(1 << 29)) && (diff < (1 << 29)) ) return 30;
    return 32;
}


/*
 * Steim-1 compression
 * source: libmseed (https://github.com/iris-edu/libmseed/blob/master/packdata.c)
 */
uint32_t MSeed_compressSteim1(const int32_t* input,
                              uint32_t sample_cnt,
                              int32_t* output,
                              uint32_t output_len,
                              int32_t diff0,
                              uint32_t* bytes_packed)
{
    int32_t* Xnp            = NULL;
    uint32_t diff_cnt       = 0;
    uint32_t input_idx      = 0;
    uint32_t output_samples = 0;
    uint32_t max_frames     = output_len / MSEED_STEIM1_FRAME_SIZE;
    uint32_t packed_samples = 0;
    uint32_t frame_idx;
    int32_t  diffs[4];
    int32_t  bitwidth[4];

    union dword
    {
        int8_t  d8[4];
        int16_t d16[2];
        int32_t d32;
    } * word;

    if (!input || !output || !output_len || !sample_cnt) { return 0; }

    /* Add first difference to buffers */
    diffs[0]    = diff0;
    bitwidth[0] = MSeed_steim1Minbits(diffs[0]);
    diff_cnt    = 1;

    for (frame_idx = 0; (frame_idx < max_frames) && (output_samples < sample_cnt); frame_idx++)
    {
        int32_t* frame_ptr = output + (MSEED_STEIM1_FRAME_SIZE / sizeof(int32_t) * frame_idx);
        memset(frame_ptr, 0, MSEED_STEIM1_FRAME_SIZE);

        /* Save forward integration constant (X0), pointer to reverse integration constant (Xn)
         * and set the starting nibble index depending on frame. */
        uint32_t start_nibble;
        if (frame_idx == 0)
        {
            frame_ptr[1] = input[0];
            Xnp          = &frame_ptr[2];
            start_nibble = 3; /* First frame: skip nibbles, X0, and Xn */
        }
        else
        {
            start_nibble = 1; /* Subsequent frames: skip nibbles */
        }

        for (uint32_t widx = start_nibble; (widx < (MSEED_STEIM1_FRAME_SIZE / sizeof(int32_t))) && (output_samples < sample_cnt); widx++)
        {
            if (diff_cnt < 4)
            {
                /* Shift diffs and related bit widths to beginning of buffers */
                for (uint32_t idx = 0; idx < diff_cnt; idx++)
                {
                    diffs[idx]    = diffs[packed_samples + idx];
                    bitwidth[idx] = bitwidth[packed_samples + idx];
                }
                /* Add new diffs and determine bit width needed to represent */
                for (uint32_t idx = diff_cnt; (idx < 4) && (input_idx < (sample_cnt - 1)); idx++, input_idx++)
                {
                    diffs[idx]    = *(input + input_idx + 1) - *(input + input_idx);
                    bitwidth[idx] = MSeed_steim1Minbits(diffs[idx]);
                    diff_cnt++;
                }
            }

            /* Determine optimal packing by checking, in-order:
             * 4 x 8-bit differences
             * 2 x 16-bit differences
             * 1 x 32-bit difference */
            word           = (union dword*)&frame_ptr[widx];
            packed_samples = 0;

            /* 4 x 8-bit differences */
            if ( (diff_cnt == 4) && (bitwidth[0] <= 8) && (bitwidth[1] <= 8) && (bitwidth[2] <= 8) && (bitwidth[3] <= 8) )
            {
                word->d8[0]    = diffs[0];
                word->d8[1]    = diffs[1];
                word->d8[2]    = diffs[2];
                word->d8[3]    = diffs[3];
                /* 2-bit nibble is 0b01 (0x1) */
                frame_ptr[0]  |= 0x1ul << (30 - 2 * widx);
                packed_samples = 4;
            }
            /* 2 x 16-bit differences */
            else if ( (diff_cnt >= 2) && (bitwidth[0] <= 16) && (bitwidth[1] <= 16) )
            {
                word->d16[0]   = diffs[0];
                word->d16[1]   = diffs[1];
                /* 2-bit nibble is 0b10 (0x2) */
                frame_ptr[0]  |= 0x2ul << (30 - 2 * widx);
                packed_samples = 2;
            }
            /* 1 x 32-bit difference */
            else
            {
                frame_ptr[widx] = diffs[0];
                /* 2-bit nibble is 0b11 (0x3) */
                frame_ptr[0]   |= 0x3ul << (30 - 2 * widx);
                packed_samples  = 1;
            }

            diff_cnt       -= packed_samples;
            output_samples += packed_samples;
        }
    }

    /* Set Xn (reverse integration constant) in first frame to last sample */
    if (Xnp)
    {
        *Xnp = *(input + output_samples - 1);
    }
    if (bytes_packed)
    {
        *bytes_packed = frame_idx * MSEED_STEIM1_FRAME_SIZE;
    }

    return output_samples;
}


/*
 * Steim-2 compression
 * source: libmseed (https://github.com/iris-edu/libmseed/blob/master/packdata.c)
 */
uint32_t MSeed_compressSteim2(const int32_t* input,
                              uint32_t sample_cnt,
                              int32_t* output,
                              uint32_t output_len,
                              int32_t diff0,
                              uint32_t* bytes_packed)
{
    int32_t*  Xnp            = NULL;
    uint32_t  diff_cnt       = 0;
    uint32_t  input_idx      = 0;
    uint32_t  output_samples = 0;
    uint32_t  max_frames     = output_len / MSEED_STEIM1_FRAME_SIZE;
    uint32_t  packed_samples = 0;
    uint32_t  frame_idx;
    int32_t   diffs[7];
    int32_t   bitwidth[7];

    union dword
    {
        int8_t  d8[4];
        int16_t d16[2];
        int32_t d32;
    } * word;

    if (!input || !output || !output_len || !sample_cnt) { return 0; }

    /* Add first difference to buffers */
    diffs[0]    = diff0;
    bitwidth[0] = MSeed_steim1Minbits(diffs[0]);
    diff_cnt    = 1;

    for (frame_idx = 0; (frame_idx < max_frames) && (output_samples < sample_cnt); frame_idx++)
    {
        uint32_t* frame_ptr = (uint32_t*)output + (MSEED_STEIM1_FRAME_SIZE / sizeof(int32_t) * frame_idx);
        memset (frame_ptr, 0, MSEED_STEIM1_FRAME_SIZE);

        /* Save forward integration constant (X0), pointer to reverse integration constant (Xn)
         * and set the starting nibble index depending on frame. */
        uint32_t start_nibble;
        if (frame_idx == 0)
        {
            frame_ptr[1] = input[0];
            Xnp          = (int32_t*)&frame_ptr[2];
            start_nibble = 3; /* First frame: skip nibbles, X0, and Xn */
        }
        else
        {
            start_nibble = 1; /* Subsequent frames: skip nibbles */
        }

        for (uint32_t widx = start_nibble; (widx < (MSEED_STEIM1_FRAME_SIZE / sizeof(int32_t))) && (output_samples < sample_cnt); widx++)
        {
            if (diff_cnt < 7)
            {
                /* Shift diffs and related bit widths to beginning of buffers */
                for (uint32_t idx = 0; idx < diff_cnt; idx++)
                {
                    diffs[idx]    = diffs[packed_samples + idx];
                    bitwidth[idx] = bitwidth[packed_samples + idx];
                }

                /* Add new diffs and determine bit width needed to represent */
                for (uint32_t idx = diff_cnt; (idx < 7) && (input_idx < (sample_cnt - 1)); idx++, input_idx++)
                {
                    diffs[idx]    = *(input + input_idx + 1) - *(input + input_idx);
                    bitwidth[idx] = MSeed_steim1Minbits(diffs[idx]);
                    diff_cnt++;
                }
            }

            /* Determine optimal packing by checking, in-order:
             * 7 x 4-bit differences
             * 6 x 5-bit differences
             * 5 x 6-bit differences
             * 4 x 8-bit differences
             * 3 x 10-bit differences
             * 2 x 15-bit differences
             * 1 x 30-bit difference */

            packed_samples = 0;

            /* 7 x 4-bit differences */
            if ( (diff_cnt == 7) && (bitwidth[0] <= 4) && (bitwidth[1] <= 4) && (bitwidth[2] <= 4) && (bitwidth[3] <= 4) && (bitwidth[4] <= 4) && (bitwidth[5] <= 4) && (bitwidth[6] <= 4) )
            {
                /* Mask the values, shift to proper location and set in word */
                frame_ptr[widx]  = ((uint32_t)diffs[6] & 0xFul);
                frame_ptr[widx] |= ((uint32_t)diffs[5] & 0xFul) << 4;
                frame_ptr[widx] |= ((uint32_t)diffs[4] & 0xFul) << 8;
                frame_ptr[widx] |= ((uint32_t)diffs[3] & 0xFul) << 12;
                frame_ptr[widx] |= ((uint32_t)diffs[2] & 0xFul) << 16;
                frame_ptr[widx] |= ((uint32_t)diffs[1] & 0xFul) << 20;
                frame_ptr[widx] |= ((uint32_t)diffs[0] & 0xFul) << 24;
                /* 2-bit decode nibble is 0b10 (0x2) */
                frame_ptr[widx] |= 0x2ul << 30;
                /* 2-bit nibble is 0b11 (0x3) */
                frame_ptr[0]    |= 0x3ul << (30 - 2 * widx);
                packed_samples   = 7;
            }
            /* 6 x 5-bit differences */
            else if ( (diff_cnt >= 6) && (bitwidth[0] <= 5) && (bitwidth[1] <= 5) && (bitwidth[2] <= 5) && (bitwidth[3] <= 5) && (bitwidth[4] <= 5) && (bitwidth[5] <= 5) )
            {
                /* Mask the values, shift to proper location and set in word */
                frame_ptr[widx]  = ((uint32_t)diffs[5] & 0x1Ful);
                frame_ptr[widx] |= ((uint32_t)diffs[4] & 0x1Ful) << 5;
                frame_ptr[widx] |= ((uint32_t)diffs[3] & 0x1Ful) << 10;
                frame_ptr[widx] |= ((uint32_t)diffs[2] & 0x1Ful) << 15;
                frame_ptr[widx] |= ((uint32_t)diffs[1] & 0x1Ful) << 20;
                frame_ptr[widx] |= ((uint32_t)diffs[0] & 0x1Ful) << 25;
                /* 2-bit decode nibble is 0b01 (0x1) */
                frame_ptr[widx] |= 0x1ul << 30;
                /* 2-bit nibble is 0b11 (0x3) */
                frame_ptr[0]    |= 0x3ul << (30 - 2 * widx);
                packed_samples   = 6;
            }
            /* 5 x 6-bit differences */
            else if ( (diff_cnt >= 5) && (bitwidth[0] <= 6) && (bitwidth[1] <= 6) && (bitwidth[2] <= 6) && (bitwidth[3] <= 6) && (bitwidth[4] <= 6) )
            {
                /* Mask the values, shift to proper location and set in word */
                frame_ptr[widx]  = ((uint32_t)diffs[4] & 0x3Ful);
                frame_ptr[widx] |= ((uint32_t)diffs[3] & 0x3Ful) << 6;
                frame_ptr[widx] |= ((uint32_t)diffs[2] & 0x3Ful) << 12;
                frame_ptr[widx] |= ((uint32_t)diffs[1] & 0x3Ful) << 18;
                frame_ptr[widx] |= ((uint32_t)diffs[0] & 0x3Ful) << 24;
                /* 2-bit decode nibble is 0b00, nothing to set */
                /* 2-bit nibble is 0b11 (0x3) */
                frame_ptr[0]    |= 0x3ul << (30 - 2 * widx);
                packed_samples   = 5;
            }
            /* 4 x 8-bit differences */
            else if ( (diff_cnt >= 4) && (bitwidth[0] <= 8) && (bitwidth[1] <= 8) && (bitwidth[2] <= 8) && (bitwidth[3] <= 8) )
            {
                word           = (union dword *)&frame_ptr[widx];
                word->d8[0]    = diffs[0];
                word->d8[1]    = diffs[1];
                word->d8[2]    = diffs[2];
                word->d8[3]    = diffs[3];
                /* 2-bit nibble is 0b01, only need to set 2nd bit */
                frame_ptr[0]  |= 0x1ul << (30 - 2 * widx);
                packed_samples = 4;
            }
            /* 3 x 10-bit differences */
            else if ( (diff_cnt >= 3) && (bitwidth[0] <= 10) && (bitwidth[1] <= 10) && (bitwidth[2] <= 10) )
            {
                /* Mask the values, shift to proper location and set in word */
                frame_ptr[widx]  = ((uint32_t)diffs[2] & 0x3FFul);
                frame_ptr[widx] |= ((uint32_t)diffs[1] & 0x3FFul) << 10;
                frame_ptr[widx] |= ((uint32_t)diffs[0] & 0x3FFul) << 20;
                /* 2-bit decode nibble is 0b11 (0x3) */
                frame_ptr[widx] |= 0x3ul << 30;
                /* 2-bit nibble is 0b10 (0x2) */
                frame_ptr[0]    |= 0x2ul << (30 - 2 * widx);
                packed_samples   = 3;
            }
            /* 2 x 15-bit differences */
            else if ( (diff_cnt >= 2) && (bitwidth[0] <= 15) && (bitwidth[1] <= 15) )
            {
                /* Mask the values, shift to proper location and set in word */
                frame_ptr[widx]  = ((uint32_t)diffs[1] & 0x7FFFul);
                frame_ptr[widx] |= ((uint32_t)diffs[0] & 0x7FFFul) << 15;
                /* 2-bit decode nibble is 0b10 (0x2) */
                frame_ptr[widx] |= 0x2ul << 30;
                /* 2-bit nibble is 0b10 (0x2) */
                frame_ptr[0]    |= 0x2ul << (30 - 2 * widx);
                packed_samples   = 2;
            }
            /* 1 x 30-bit difference */
            else if ( (diff_cnt >= 1) && (bitwidth[0] <= 30) )
            {
                /* Mask the value and set in word */
                frame_ptr[widx]  = ((uint32_t)diffs[0] & 0x3FFFFFFFul);
                /* 2-bit decode nibble is 0b01 (0x1) */
                frame_ptr[widx] |= 0x1ul << 30;
                /* 2-bit nibble is 0b10 (0x2) */
                frame_ptr[0]    |= 0x2ul << (30 - 2 * widx);
                packed_samples   = 1;
            }
            else
            {
                return -1;
            }
            diff_cnt       -= packed_samples;
            output_samples += packed_samples;
        }
    }

    /* Set Xn (reverse integration constant) in first frame to last sample */
    if (Xnp)
    {
        *Xnp = *(input + output_samples - 1);
    }
    if (bytes_packed)
    {
        *bytes_packed = frame_idx * MSEED_STEIM1_FRAME_SIZE;
    }

    return output_samples;
}
