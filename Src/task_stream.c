/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

#if !STREAMING_TARGET_NONE

/* Variables -----------------------------------------------------------------*/

/* External variables --------------------------------------------------------*/
extern rb_t   rb_stream;

/* Private variables ---------------------------------------------------------*/

/* Functions -----------------------------------------------------------------*/

bool Stream_sendPacket(SerialPacketType_t type, uint32_t arg, uint16_t len, const uint8_t* data)
{
    static SerialPacket_t pkt_buffer;
    static uint16_t       seq_no = 0;

    /* Compose the UART packet, potentially split into several chunks */
    pkt_buffer.type   = type;
    pkt_buffer.offset = 0;

    while (len)
    {
        pkt_buffer.seq_no      = seq_no++;
        pkt_buffer.payload_len = MIN(len, SERIAL_PKT_MAX_PAYLOAD);
        pkt_buffer.arg         = arg;
        memcpy(pkt_buffer.payload, data, pkt_buffer.payload_len);
        /* Calculate and append the CRC */
        uint32_t pkt_len = SERIAL_PKT_HDR_LEN + pkt_buffer.payload_len;
        uint32_t crc     = crc32((uint8_t*)&pkt_buffer, pkt_len, 0);
        memcpy(&pkt_buffer.payload[pkt_buffer.payload_len], &crc, SERIAL_PKT_CRC_LEN);
        pkt_len += SERIAL_PKT_CRC_LEN;

        /* Send the data through the selected serial channel */
    #if STREAMING_TARGET_UART
        if (!LPUART1_transmitBinary((uint8_t*)&pkt_buffer, pkt_len))
        {
            DEBUG_PRINT(DBG_LVL_ERROR, "Failed to transmit data via UART");
            return false;
        }
    #elif STREAMING_TARGET_USB
        if (!USB_transmitBinary((uint8_t*)&pkt_buffer, pkt_len))
        {
            return false;
        }
    #elif STREAMING_TARGET_SWO
        SWO_transmitBinary((uint8_t*)&pkt_buffer, pkt_len);
    #endif

        pkt_buffer.offset += pkt_buffer.payload_len;
        len               -= pkt_buffer.payload_len;
    }
    return true;
}


/*** Streaming Task ***********************************************************/
void vTaskStream(void* arg)
{
#if STREAMING_BACKLOG_MODE
    static uint8_t      read_buf[LOG_ADCDATA_MINISEED ? MSEED_DATA_LEN : (GEO_SAMPLE_SIZE * ADC_MAX_SPS)];
    static char         filename_buf[SD_FILE_MAX_PATH_LEN];
    static char         curr_filename[GEO_NUM_AXES][SD_FILE_MAX_PATH_LEN] = { 0 };
    uint32_t            curr_acq_id[GEO_NUM_AXES] = { 0 };
    uint32_t            curr_ofs[GEO_NUM_AXES]    = { 0 };
    uint32_t            task_period_ms            = STREAM_TASK_PERIOD_MAX_MS;

    for (uint32_t i = 0; i < GEO_NUM_AXES; i++)
    {
        curr_acq_id[i] = SysConf.Geo->ID;
    }
#endif

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Streaming task started");

    while (true)
    {
#if STREAMING_BACKLOG_MODE
        ulTaskNotifyTake(pdTRUE, pdMS_TO_TICKS(task_period_ms));
        task_period_ms = STREAM_TASK_PERIOD_MAX_MS;     /* Reset to default task run interval */

        if (xSemaphoreTake(xSemaphore_SD, pdMS_TO_TICKS(STREAM_TASK_SEM_WAIT_MS)) != pdFALSE)
        {
            uint32_t start_ts   = TIM5_getCounterValue();
            uint32_t sent_bytes = 0;

            if (SD_isMounted())
            {
                for (uint32_t ch = 0; ch < GEO_NUM_AXES; ch++)
                {
                    /* Determine filename */
                    if (!SD_fileExists(curr_filename[ch]))
                    {
                        snprintf(filename_buf, SD_FILE_MAX_PATH_LEN, SD_FILENAME_ADCDATA, curr_acq_id[ch], adc_channel_mapping[ch + 1]);
                        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Searching for file %s...", filename_buf);
                        if (!SD_findFile(filename_buf, curr_filename[ch]))
                        {
                            DEBUG_PRINT(DBG_LVL_WARNING, "File not found");
                            if (curr_acq_id[ch] < SysConf.Geo->ID)
                            {
                                curr_acq_id[ch]++;          /* Acquisition ID most likely doesn't exist -> skip it */
                                curr_ofs[ch]         = 0;
                                curr_filename[ch][0] = 0;   /* Clear filename */
                            }
                            continue;
                        }
                    }

                    /* Read the next chunk of data */
                    uint32_t chunk_size = ( LOG_ADCDATA_MINISEED ? MSEED_DATA_LEN          : (GEO_SAMPLE_SIZE * ADC_MAX_SPS) );
                    uint8_t  type       = ( LOG_ADCDATA_MINISEED ? SERIAL_PKT_ADC_MINISEED : (SERIAL_PKT_ADC1_RAW + ch)      );
                    uint32_t read_bytes = SD_readFile(curr_filename[ch], curr_ofs[ch], chunk_size, read_buf);
                    if (!read_bytes)
                    {
                        if (curr_acq_id[ch] < SysConf.Geo->ID)
                        {
                            DEBUG_PRINT(DBG_LVL_VERBOSE, "Acquisition ID has changed");
                            curr_acq_id[ch]++;
                            curr_ofs[ch] = 0;
                            /* Update filename (don't change the path, only the filename) */
                            char* first_slash = strchr(curr_filename[ch], '/');
                            if (first_slash)
                            {
                                snprintf(first_slash + 1, SD_FILE_MAX_PATH_LEN - (first_slash - curr_filename[ch] + 1), SD_FILENAME_ADCDATA, curr_acq_id[ch], adc_channel_mapping[ch + 1]);
                                DEBUG_PRINTF(DBG_LVL_VERBOSE, "Filename set to %s", curr_filename[ch]);
                            }
                        }
                        continue;
                    }
                    if (!Stream_sendPacket(type, curr_acq_id[ch], read_bytes, read_buf))
                    {
                        task_period_ms = STREAM_TASK_PERIOD_MAX_MS;
                        break;    /* Try again later */
                    }

                    task_period_ms = STREAM_TASK_PERIOD_MIN_MS;
                    curr_ofs[ch]  += read_bytes;
                    sent_bytes    += read_bytes;
                }
            }
            uint32_t time_delta_ms = TIM5_TICKS_TO_MS(TIM5_getCounterValue() - start_ts);

            xSemaphoreGive(xSemaphore_SD);

            if (time_delta_ms > STREAM_TASK_RUNTIME_TH_MS)
            {
                DEBUG_PRINTF(DBG_LVL_WARNING, "Task runtime %4lums exceeded expected access time", time_delta_ms);
            }
            if (sent_bytes)
            {
                DEBUG_PRINTF(DBG_LVL_VERBOSE, "%lu bytes sent", sent_bytes);
            }
        }
        else
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "SD card not mounted, cannot read from file");
        }
#else
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        /* Check if there are elements in the ring buffer */
        while (!RB_isEmpty(&rb_stream))
        {
            const StreamingData_t* item = (StreamingData_t*)RB_getTail(&rb_stream);
            SerialPacketType_t pkt_type = SERIAL_PKT_INVALID;

            switch (item->type)
            {
            case STREAMING_DTYPE_GEO_A1:
            case STREAMING_DTYPE_GEO_A2:
            case STREAMING_DTYPE_GEO_A3:
                pkt_type = (LOG_ADCDATA_MINISEED ? SERIAL_PKT_ADC_MINISEED : (SERIAL_PKT_ADC1_RAW + (item->type - STREAMING_DTYPE_GEO_A1)));
                break;

            case STREAMING_DTYPE_DEBUG:
                pkt_type = SERIAL_PKT_DEBUG_MSG;
                break;

            case STREAMING_DTYPE_HEALTH:
                pkt_type = SERIAL_PKT_APP_HEALTH;
                break;

            default:
                DEBUG_PRINTF(DBG_LVL_WARNING, "Unknown notification type %u", item->type);
                break;
            }

            if (pkt_type != SERIAL_PKT_INVALID)
            {
                Stream_sendPacket(pkt_type, item->arg, item->len, item->data);
            }

            RB_remove(&rb_stream);
        }
#endif
    }
}

#endif
