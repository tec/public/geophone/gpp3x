/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"
#include "fatfs.h"

/* Variables -----------------------------------------------------------------*/
static bool         sd_initialized;                     /* Whether SD card is initialized */
static bool         sd_mounted;                         /* Whether SD card is mounted */
static uint32_t     sd_size_total;                      /* KBytes */
static uint32_t     sd_size_free;                       /* KBytes */
static char         sd_file_buf[SD_FILE_MAX_LINE_LEN];  /* Buffer for reading/writing from/to (text) files */
static char         sd_cwd[SD_DIR_MAX_LEN];             /* Working directory path */
static uint32_t     sd_cwdi;                            /* Working directory in the format YYYYMMDD (as an integer) */
static FIL          sd_file;                            /* File object for SD */
static char         sd_path[4];                         /* SD logical drive path */
static FATFS        sd_fatfs;                           /* File system object for SD logical drive */
static const char*  sd_fs_types[] = { "unknown", "FAT12", "FAT16", "FAT32", "exFAT" };

/* External variables---------------------------------------------------------*/
extern Geophone_t   Geo;
extern ImuStat_t    Imu;
extern GNSSStat_t   GNSS;

/* Functions -----------------------------------------------------------------*/
void SD_init(void)
{
    /* FatFs Link Driver and retrieve the path */
    FATFS_Init(sd_path);

    /* Power-cycle */
    SD_SWOFF();
    HAL_Delay(10);
    SD_SWON();
    HAL_Delay(SD_POWER_ON_DELAY_MS);

    /* Mount card and update working directory */
    if (!SD_mount())
    {
        Error_Handler();    /* Application cannot continue */
    }

    /* Get SD card usage */
    SD_updateSize();

    /* Write application info */
    SD_writeInfo();

#ifdef SD_ARCHIVE_DIR
    /* If the current date is 1.1.2000 (default RTC value after a POR), then move the folder to a backup location to avoid accumulation of files in that folder (which will significantly slow down file access via FatFS) */
    if (strcmp(RTC_getFormattedDate(), SD_DEFAULT_DATE) == 0)
    {
        if (!SD_archiveFiles(SD_DEFAULT_DATE, SD_ARCHIVE_FILETYPE))
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "Failed to archive files in '" SD_DEFAULT_DATE "'");
        }
    }
#endif

    DEBUG_PRINTF(DBG_LVL_INFO, "SD card initialized (Filesystem: %s, Size: %uMB, Free: %uMB / %u%%)", sd_fs_types[sd_fatfs.fs_type], sd_size_total / 1024, sd_size_free / 1024, 100 - SD_getFillLevel());
}

bool SD_mount(void)
{
    FRESULT fr;

    /* If mounted before --> return */
    if (sd_mounted)
    {
        return true;
    }

    /* Check if power enabled */
    if (!SD_IS_SWON())
    {
        SD_SWON();
        HAL_Delay(SD_POWER_ON_DELAY_MS);   /* Give the SD card some time to start up */
    }

    DEBUG_PRINT(DBG_LVL_VERBOSE, "SD mount");

    /* Try to mount */
    for (uint32_t m_tries = 0; m_tries < SD_MAX_ACCESS_TRIES; ++m_tries)
    {
        /* Re-initialize after 2 tries */
        if (m_tries > 1)
        {
            BSP_SD_DeInit();
            sd_initialized = false;
            /* Re-initialize */
            if (!IS_ISR())
            {
                RTOS_TASK_DELAY(SD_REINIT_DELAY_MS);
            }
        }

        /* If SD hasn't been initialized */
        if (!sd_initialized)
        {
            if (BSP_SD_Init() == MSD_OK)
            {
                sd_initialized = true;
            }
            else
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "BSP_SD_Init failed");
                Error_Handler();
            }
        }

        /* Mount */
        if (sd_initialized)
        {
            fr = f_mount(&sd_fatfs, (TCHAR const*)sd_path, 1);
            if (fr == FR_OK)
            {
                sd_mounted = true;
                sd_cwdi    = 0;
                /* Check working directory */
                return SD_checkCWD();
            }
            else
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "f_mount failed");
                Error_Handler();
            }
        }
        HAL_Delay(SD_RETRIES_DELAY_MS);
    }

    sd_mounted = false;

    return false;
}

/* Pass root as both arguments to recursively create all necessary directories. */
bool SD_createDirRecursive(const char* root, char* next_dir)
{
    if (!root || !next_dir) { return true; }

    next_dir = strchr(next_dir, '/');
    if (next_dir)
    {
        *next_dir = 0;    /* Terminate string contained in curr_dir */
    }
    FRESULT fr = f_mkdir((char*)root);
    if (next_dir)
    {
        *next_dir = '/';  /* Revert change to string */
        next_dir++;
    }
    if ( (fr != FR_OK) && (fr != FR_EXIST) )
    {
        return false;
    }
    return SD_createDirRecursive(root, next_dir);
}

bool SD_checkCWD(void)
{
    if (sd_cwdi == RTC_getDay())
    {
        return true;    /* No need to update */
    }

    const RTC_DateTypeDef* date = RTC_getDate(true);
    snprintf(sd_cwd, SD_DIR_MAX_LEN, SD_DIR_FORMAT, (date->Year >= 70) ? (1900 + date->Year) : (2000 + date->Year), date->Month, date->Date);
    sd_cwdi = RTC_getDay();

    FRESULT fr = f_stat(sd_cwd, NULL);
    if (fr == FR_NO_FILE || fr == FR_NO_PATH)
    {
        /* Create directories recursively */
        for (uint32_t m_tries = 0; m_tries < SD_MAX_ACCESS_TRIES; m_tries++)
        {
            if (SD_createDirRecursive(sd_cwd, sd_cwd))
            {
                DEBUG_PRINTF(DBG_LVL_INFO, "SD mkdir success: %s", sd_cwd);
                return true;
            }
            HAL_Delay(SD_RETRIES_DELAY_MS);
        }
        DEBUG_PRINTF(DBG_LVL_ERROR, "SD mkdir failed: %s", sd_cwd);
        sd_cwdi = 0;
        return false;
    }
    else if (fr != FR_OK)
    {
        /* Fstat error */
        DEBUG_PRINTF(DBG_LVL_ERROR, "SD fstat for '%s' failed with error code %u", sd_cwd, fr);
        sd_cwdi = 0;
        return false;
    }

    DEBUG_PRINTF(DBG_LVL_INFO, "Working directory updated to '%s'", sd_cwd);

    return true;
}

const char* SD_getCWD(void)
{
    return sd_cwd;
}

void SD_unmount(void)
{
    f_mount(NULL, (TCHAR const*)sd_path, 0);
    sd_mounted = false;
}

bool SD_isMounted(void)
{
    return sd_mounted;
}

void SD_eject(void)
{
    if (sd_mounted)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "SD eject");
        SD_unmount();
    }
    BSP_SD_DeInit();
    sd_initialized = false;
}

bool SD_format(void)
{
    static BYTE work_buf[FF_MAX_SS];

    if (IS_RTOS_RUNNING())
    {
        return false;   /* Must not run while RTOS is running */
    }
    if (sd_mounted)
    {
        SD_unmount();
    }

    MKFS_PARM opt = { 0 };
    opt.fmt = FM_FAT32;

    FRESULT fr = f_mkfs("", &opt, work_buf, sizeof(work_buf));
    if (fr != FR_OK)
    {
        DEBUG_PRINTF(DBG_LVL_ERROR, "Failed to format SD card (error code %d)", fr);
        return false;
    }
    DEBUG_PRINT(DBG_LVL_INFO, "SD card formatted");
    return true;
}

void SD_updateSize(void)
{
    /* Get SD Card Info */
    HAL_SD_CardInfoTypeDef sd_card_info;
    BSP_SD_GetCardInfo(&sd_card_info);

    /* Calculate free space */
    FATFS* fs = &sd_fatfs;
    DWORD  free_clusters;
    if (f_getfree("", &free_clusters, &fs) == FR_OK)
    {
        sd_size_total = (uint32_t)((uint64_t)(fs->n_fatent - 2) * fs->csize * sd_card_info.LogBlockSize / 1024);
        sd_size_free  = (uint32_t)((uint64_t)(free_clusters) * fs->csize * sd_card_info.LogBlockSize / 1024);
    }
}

uint8_t SD_getFillLevel(void)
{
    /* Do not update the fill level here, this function can be called from tasks other than LOG */
    if (sd_size_total)
    {
        return (((uint64_t)(sd_size_total - sd_size_free) * 100) / sd_size_total);
    }
    return 0;
}

uint32_t SD_getSizeFree(void)
{
    return sd_size_free;
}

uint32_t SD_getSizeTotal(void)
{
    return sd_size_total;
}


/* Schedule: Read from SD ------------------------------------------------------*/
bool SD_readSchedule(void)
{
    /* Check if file exists */
    if (f_stat(SD_FILENAME_SCHEDULE, NULL) == FR_NO_FILE)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "No schedule found");
        return true;
    }
    if (f_open(&sd_file, SD_FILENAME_SCHEDULE, FA_READ | FA_OPEN_EXISTING) == FR_OK)
    {
        /* Read line by line */
        while (f_gets(sd_file_buf, SD_FILE_MAX_LINE_LEN, &sd_file) != NULL)
        {
            /* Remove CR, LF */
            char* tmp = strchr(sd_file_buf, 0x0D);
            if (tmp) { *tmp = 0; }
            tmp = strchr(sd_file_buf, 0x0A);
            if (tmp) { *tmp = 0; }
            /* Remove comments */
            tmp = strchr(sd_file_buf, '#');
            if (tmp) { *tmp = 0; }
            /* Ignore line if it contains a negative number */
            tmp = strchr(sd_file_buf, '-');
            if (tmp) { continue; }
            /* Check for empty line */
            if (!strlen(sd_file_buf)) { continue; }

            /* Expects schedule in the form: "[start_unixts] [period_s] [argument] [task_id]\n" */
            char* pEnd;
            uint32_t start    = (uint32_t)strtoull(sd_file_buf, &pEnd, 10);
            uint32_t period   = (uint32_t)strtoull(pEnd, &pEnd, 10);
            uint32_t argument = (uint32_t)strtoull(pEnd, &pEnd, 10);
            uint8_t  tasks    = (uint8_t)strtoull(pEnd, &pEnd, 10);

            /* Only add to scheduler if task bit valid */
            if ( (tasks & ~TASK_ALL) == 0 )
            {
                Schedule_addEntry(tasks, start, period, argument, false);
            }
            else
            {
                DEBUG_PRINTF(DBG_LVL_WARNING, "Invalid schedule detected: '%s'", sd_file_buf);
            }
        }
        f_close(&sd_file);

        return true;
    }
    /* else: file open error */

    return false;
}

/* Config: Read from SD ------------------------------------------------------*/
bool SD_readConfig(void)
{
    if (f_open(&sd_file, SD_FILENAME_CONFIG, FA_READ | FA_OPEN_EXISTING) == FR_OK)
    {
        bool result = true;

        while (f_gets(sd_file_buf, SD_FILE_MAX_LINE_LEN, &sd_file) != NULL)
        {
            /* Remove CR, LF */
            char* tmp = strchr(sd_file_buf, 0x0D);
            if (tmp) { *tmp = '\0'; }
            tmp = strchr(sd_file_buf, 0x0A);
            if (tmp) { *tmp = '\0'; }
            /* Check for comment */
            tmp = strchr(sd_file_buf, '#');
            if (tmp) { *tmp = '\0'; }
            /* Check for empty line */
            if (!strlen(sd_file_buf)) { continue; }

            /* Check for ':' location. This indicates the start position of the config value. */
            tmp = strchr(sd_file_buf, ':');
            if ((tmp == NULL) || (strlen(tmp + 1) == 0))
            {
                result = false;
                break;
            }

            /* Parse config value */
            uint32_t number = (uint32_t)strtol((tmp + 1), NULL, 10);
            if (strstr(sd_file_buf, "DEVICE_ID"))
            {
                if (number < 65536) { SysConf.DeviceID = (uint16_t)number; }
                else                { result           = false; }
            }
            else if (strstr(sd_file_buf, "SYS_OPMODE"))
            {
                if (number <= SYS_OPMODE_ALL) { SysConf.OpMode = (uint8_t)number; }
                else                          { result         = false; }
            }
            else if (strstr(sd_file_buf, "TRG_TH_POS"))
            {
                if ((number > (SYSTEM_VOLTAGE / 2)) && (number <= SYSTEM_VOLTAGE)) { Geo.TrgThPos = (uint16_t)number; }
                else                                                               { result       = false; }
            }
            else if (strstr(sd_file_buf, "TRG_TH_NEG"))
            {
                if (number < (SYSTEM_VOLTAGE / 2)) { Geo.TrgThNeg = (uint16_t)number; }
                else                               { result       = false; }
            }
            else if (strstr(sd_file_buf, "TRG_GAIN"))
            {
                if      (number == TRG_GAIN_STAGE1) { Geo.TrgGain = TRG_GAIN_STAGE1; }
                else if (number == TRG_GAIN_STAGE2) { Geo.TrgGain = TRG_GAIN_STAGE2; }
                else                                { result      = false; }
            }
            else if (strstr(sd_file_buf, "POSTTRG"))
            {
                if (number < 65536) { Geo.PostTrg = (uint16_t)number; }
                else                { result      = false; }
            }
            else if (strstr(sd_file_buf, "TIMEOUT"))
            {
                if (number < 65536) { Geo.Timeout = (uint16_t)number; }
                else                { result      = false; }
            }
            else if (strstr(sd_file_buf, "ADC_PGA"))
            {
                if (number <= 128) { Geo.PGA = (uint8_t)number; }
                else               { result  = false; }
            }
            else if (strstr(sd_file_buf, "ADC_FORMAT"))
            {
                if      (number == ADC_FORMAT_TWOSCOMPLEMENT) { Geo.Format = ADC_FORMAT_TWOSCOMPLEMENT; }
                else if (number == ADC_FORMAT_OFFSETBINARY)   { Geo.Format = ADC_FORMAT_OFFSETBINARY;   }
                else                                          { result     = false;                     }
            }
            else if (strstr(sd_file_buf, "ADC_SPS"))
            {
                if ((number == 1000 || number == 500 || number == 250 || number == 125) && (number <= ADC_MAX_SPS)) { Geo.SPS = number; }
                else                                                                                                { result  = false;  }
            }
            else if (strstr(sd_file_buf, "IMU_FREQ_LP"))
            {
                if ((number == 0) || (number == 10) || (number == 50) || (number == 100) || (number == 200) || (number == 400) || (number == 800)) { Imu.Freq_LP = (uint16_t)number; }
                else                                                                                                                               { result      = false; }
            }
            else if (strstr(sd_file_buf, "IMU_FREQ_HP"))
            {
                if ((number == 0) || (number == 10) || (number == 50) || (number == 100) || (number == 200) || (number == 400) || (number == 800)) { Imu.Freq_HP = (uint16_t)number; }
                else                                                                                                                               { result      = false; }
            }
            else if (strstr(sd_file_buf, "IMU_FREQ_AA"))
            {
                if ((number == 50) || (number == 100) || (number == 200) || (number == 400)) { Imu.Freq_AA = (uint16_t)number; }
                else                                                                         { result      = false; }
            }
            else if (strstr(sd_file_buf, "IMU_OPMODE"))
            {
                if (number <= 10) { Imu.OpMode = (uint8_t)number; }
                else              { result     = false; }
            }
            else if (strstr(sd_file_buf, "IMU_TRG_LVL"))
            {
                if (number <= 10) { Imu.Level_Trg = (uint8_t)number; }
                else              { result        = false;           }
            }
            else if (strstr(sd_file_buf, "IMU_DATA_DEC"))
            {
                if ((number == 1) || (number == 2) || (number == 4) || (number == 8))  { Imu.Decimation = (uint8_t)number; }
                else                                                                   { result         = false; }
            }
            else if (strstr(sd_file_buf, "GNSS_LEAP_S"))
            {
                if (number > 0) { GNSS.leap_s = (uint8_t)number; }
                else            { result      = false; }
            }
            else /* Line not recognized */
            {
                DEBUG_PRINTF(DBG_LVL_WARNING, "Config line '%s' not recognized", sd_file_buf);
                result = 0;
            }
        }
        f_close(&sd_file);

        return result;
    }
    /* else: file open error */

    return false;
}

/* Config: Write to SD -------------------------------------------------------*/
bool SD_writeConfig(void)
{
    if (f_open(&sd_file, SD_FILENAME_CONFIG, FA_WRITE | FA_CREATE_ALWAYS) == FR_OK)
    {
        f_printf(&sd_file, "DEVICE_ID:%u\n", SysConf.DeviceID);
        f_printf(&sd_file, "SYS_OPMODE:%u\n", SysConf.OpMode);
        f_printf(&sd_file, "TRG_TH_POS:%u\n", Geo.TrgThPos);
        f_printf(&sd_file, "TRG_TH_NEG:%u\n", Geo.TrgThNeg);
        f_printf(&sd_file, "TRG_GAIN:%u\n", Geo.TrgGain);
        f_printf(&sd_file, "POSTTRG:%u\n", Geo.PostTrg);
        f_printf(&sd_file, "TIMEOUT:%u\n", Geo.Timeout);
        f_printf(&sd_file, "ADC_PGA:%u\n", Geo.PGA);
        f_printf(&sd_file, "ADC_FORMAT:%u\n", Geo.Format);
        f_printf(&sd_file, "ADC_SPS:%u\n", Geo.SPS);
        f_printf(&sd_file, "IMU_FREQ_LP:%u\n", Imu.Freq_LP);
        f_printf(&sd_file, "IMU_FREQ_HP:%u\n", Imu.Freq_HP);
        f_printf(&sd_file, "IMU_FREQ_AA:%u\n", Imu.Freq_AA);
        f_printf(&sd_file, "IMU_OPMODE:%u\n",  Imu.OpMode);
        f_printf(&sd_file, "IMU_TRG_LVL:%u\n", Imu.Level_Trg);
        f_printf(&sd_file, "IMU_DATA_DEC:%u\n", Imu.Decimation);
        f_printf(&sd_file, "GNSS_LEAP_S:%u\n", GNSS.leap_s);
        f_truncate(&sd_file);
        f_close(&sd_file);
        return true;
    }

    return false;
}

/* Schedule: Clear on SD -------------------------------------------------------*/
bool SD_clearSchedule(void)
{
    /* Clear file */
    if (f_open(&sd_file, SD_FILENAME_SCHEDULE, FA_WRITE | FA_CREATE_ALWAYS) == FR_OK)
    {
        f_close(&sd_file);
        return true;
    }
    return false;
}

/* Schedule: Add entry on SD -------------------------------------------------------*/
bool SD_addToSchedule(const ScheduleEntry_t* schedEntry)
{
    if (!schedEntry) { return false; }

    /* Append new schedule entries to end of schedule file and push them to the schedule priority queue */
    if (f_open(&sd_file, SD_FILENAME_SCHEDULE, FA_WRITE | FA_OPEN_APPEND) == FR_OK)
    {
        f_printf(&sd_file, "%lu %lu %lu %u\n", schedEntry->first, schedEntry->period, schedEntry->argument, schedEntry->tasks);
        f_close(&sd_file);
        return true;
    }

    return false;
}

void SD_writeInfo(void)
{
    if (f_open(&sd_file, SD_FILENAME_INFO, FA_WRITE | FA_CREATE_ALWAYS) == FR_OK)
    {
        f_printf(&sd_file, "MCU:            " INFO_MCU_DESC "\n");
        f_printf(&sd_file, "MCU ID:         %08X %08X %08X\n", *(uint32_t*)MCU_UUID_ADDR, *((uint32_t*)MCU_UUID_ADDR + 1), *((uint32_t*)MCU_UUID_ADDR + 2));
        f_printf(&sd_file, "COMPILER_NAME:  " INFO_COMPILER_DESC "\n");
        uint16_t major = (INFO_COMPILER_VER / 1000000);
        uint16_t minor = (INFO_COMPILER_VER % 1000000) / 1000;
        uint16_t patch = (INFO_COMPILER_VER % 1000);
        f_printf(&sd_file, "COMPILER_VER:   %u.%u.%u\n", major, minor, patch);
        f_printf(&sd_file, "COMPILE_DATE:   " __DATE__ "\n");
        f_printf(&sd_file, "COMPILE_TIME:   " __TIME__ "\n");
        f_printf(&sd_file, "FIRMWARE_NAME:  " INFO_FW_NAME "\n");
        f_printf(&sd_file, "FIRMWARE_VER:   %u.%u.%u\n", INFO_FW_VER_MAJOR, INFO_FW_VER_MINOR, INFO_FW_VER_PATCH);
        f_printf(&sd_file, "FIRMWARE_REV:   %08x\n", INFO_FW_REV);
        f_printf(&sd_file, "DEVICE_ID:      %u\n", DPP_DEVICE_ID);
        f_close(&sd_file);
    }
}


bool SD_writeResetCounter(uint32_t cnt)
{
    if (f_open(&sd_file, SD_FILENAME_RESETS, FA_WRITE | FA_OPEN_ALWAYS) == FR_OK)
    {
        f_printf(&sd_file, "%lu", cnt);
        f_truncate(&sd_file);
        f_close(&sd_file);
        return true;
    }
    return false;
}

uint32_t SD_readResetCounter(void)
{
    uint32_t cnt = 0;

    if (f_open(&sd_file, SD_FILENAME_RESETS, FA_READ) == FR_OK)
    {
        if (f_gets(sd_file_buf, 8, &sd_file) != NULL)
        {
            cnt = (uint32_t)strtol((char*)&sd_file_buf, NULL, 10);
        }
        f_close(&sd_file);
    }
    return cnt;
}

bool SD_writeAcqID(uint32_t id)
{
    if (f_open(&sd_file, SD_FILENAME_ACQID, FA_WRITE | FA_OPEN_ALWAYS) == FR_OK)
    {
        f_printf(&sd_file, "%lu", id);
        f_truncate(&sd_file);
        f_close(&sd_file);
        return true;
    }
    return false;
}

uint32_t SD_readAcqID(void)
{
    uint32_t id = 0;

    /* Read the previous acquisition ID */
    if (f_open(&sd_file, SD_FILENAME_ACQID, FA_READ) == FR_OK)
    {
        if (f_gets(sd_file_buf, 10, &sd_file) != NULL)
        {
            id = (uint32_t)strtol(sd_file_buf, NULL, 10);
        }
        f_close(&sd_file);
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "Last acquisition ID not found, reset to 0");
    }
    return id;
}

bool SD_initFile(const char* filename, const char* header)
{
    if (!filename) { return false; }

    /* First check whether the file exists */
    if (f_stat(filename, NULL) == FR_NO_FILE)
    {
        /* Create file */
        FRESULT fr = f_open(&sd_file, filename, FA_WRITE | FA_CREATE_ALWAYS);
        if (fr != FR_OK)
        {
            DEBUG_PRINTF(DBG_LVL_ERROR, "File '%s' could not be created (error code %u)", filename, fr);
            return false;
        }
        if (header)
        {
            if ( (f_write(&sd_file, header, strlen(header), 0) != FR_OK) || (f_sync(&sd_file) != FR_OK) )
            {
                f_close(&sd_file);
                return false;
            }
        }
        f_close(&sd_file);
    }
    return true;
}

bool SD_writeFile(const char* filename, const void* data, uint32_t len)
{
    if (!filename || !data || !len) { return false; }

    /* Open file, move pointer to the end */
    if (f_open(&sd_file, filename, FA_WRITE | FA_OPEN_APPEND) != FR_OK)
    {
        return false;
    }

    UINT written_bytes = 0;
    if ( (f_write(&sd_file, data, len, &written_bytes) != FR_OK) || (written_bytes != len) )
    {
        f_close(&sd_file);
        return false;
    }
    f_close(&sd_file);

    return true;
}

bool SD_writeAtOffset(const char* filename, int32_t ofs, const uint8_t* data, uint16_t len)
{
    if (!filename || !data || !len) { return false; }

    if (f_open(&sd_file, filename, FA_WRITE) != FR_OK)
    {
        return false;
    }

    int32_t file_size = f_size(&sd_file);
    if (ofs > file_size)
    {
        f_close(&sd_file);
        return false;
    }
    if (ofs < 0)    /* Negative offset is treated as offset from the end of the file */
    {
        ofs += file_size;
        if (ofs < 0)
        {
            DEBUG_PRINTF(DBG_LVL_ERROR, "Negative file pointer (ofs: %ld, file_size: %ld)", ofs, file_size);
            f_close(&sd_file);
            return false;
        }
    }

    if (f_lseek(&sd_file, ofs) != FR_OK)
    {
        f_close(&sd_file);
        return false;
    }
    UINT written_bytes = 0;
    if ( (f_write(&sd_file, data, len, &written_bytes) != FR_OK) || (written_bytes != len) )
    {
        f_close(&sd_file);
        return false;
    }
    f_close(&sd_file);

    return true;
}

bool SD_appendPadding(const char* filename, uint8_t val, uint16_t num_bytes)
{
    const uint8_t buffer_size = 16;
    uint8_t       bytes_buffer[buffer_size];

    memset(bytes_buffer, val, buffer_size);

    if (f_open(&sd_file, filename, FA_WRITE | FA_OPEN_APPEND) != FR_OK)
    {
        return false;
    }
    while (num_bytes)
    {
        uint32_t bytes_to_write = MIN(num_bytes, buffer_size);
        UINT     written_bytes  = 0;
        if ( (f_write(&sd_file, bytes_buffer, bytes_to_write, &written_bytes) != FR_OK) || (written_bytes != bytes_to_write) )
        {
            f_close(&sd_file);
            return false;
        }
        num_bytes -= bytes_to_write;
    }
    f_close(&sd_file);

    return true;
}

uint32_t SD_readFile(const char* filename, uint32_t offset, uint16_t num_bytes, uint8_t* out_data)
{
    if (!num_bytes || !out_data) { return 0; }

    /* Open file, move pointer to the end */
    FRESULT fr = f_open(&sd_file, filename, FA_READ);
    if (fr != FR_OK)
    {
        DEBUG_PRINTF(DBG_LVL_ERROR, "File '%s' could not be opened (error code %u)", filename, fr);
        return 0;
    }
    /* Check file size */
    if (f_size(&sd_file) < (offset + num_bytes))
    {
        f_close(&sd_file);
        return 0;
    }
    if (offset > 0)
    {
        if (f_lseek(&sd_file, offset) != FR_OK)
        {
            f_close(&sd_file);
            return 0;
        }
    }

    UINT bytes_read = 0;
    f_read(&sd_file, out_data, num_bytes, &bytes_read);
    f_close(&sd_file);

    return bytes_read;
}

bool SD_fileExists(const char* filename)
{
    if (!filename || filename[0] == 0)
    {
        return false;
    }
    return (f_stat(filename, NULL) == FR_OK);
}

/* Searches the root directory and sub-directories (first level only) */
bool SD_findFile(const char* filename, char* out_path)
{
    static DIR      dir;
    static FILINFO  finfo;
    bool            found = false;

    if (!filename || !out_path) { return false; }

    if (SD_fileExists(filename))    /* Check if in the root directory */
    {
        return true;
    }
    out_path[0] = 0;

    /* Iterate through all sub-directories */
    if (f_opendir(&dir, "") == FR_OK)
    {
        while ( (f_readdir(&dir, &finfo) == FR_OK) && (finfo.fname[0] != 0) )
        {
            if (finfo.fattrib & AM_DIR)
            {
                snprintf(out_path, SD_FILE_MAX_PATH_LEN, "%s/%s" , finfo.fname, filename);
                if (f_stat(out_path, NULL) == FR_OK)
                {
                    found = true;
                    break;
                }
            }
        }
        f_closedir(&dir);
    }
    return found;
}

#ifdef SD_ARCHIVE_DIR
/* move all files in the given directory to an archive location (if file_ext is specified, only files matching the given extension will be moved) */
bool SD_archiveFiles(const char* dir, const char* file_ext)
{
    static DIR     dinfo;
    static FILINFO finfo;
    static char    curr_name[SD_FILE_MAX_PATH_LEN];
    static char    new_name[SD_FILE_MAX_PATH_LEN];
    bool           res = true;

    /* check if folder exists */
    if (!SD_fileExists(dir)) { return true; }   /* nothing to archive (no error) */

    /* make sure the archive directory exists */
    if ( !SD_fileExists(SD_ARCHIVE_DIR) && (f_mkdir(SD_ARCHIVE_DIR) != FR_OK) ) { return false; }

    if (f_opendir(&dinfo, dir) == FR_OK)
    {
        while ( (f_readdir(&dinfo, &finfo) == FR_OK) && (finfo.fname[0] != 0) )
        {
            /* file extension given and does not match -> skip file */
            if ( file_ext && strrchr(finfo.fname, '.') && (strcmp(strrchr(finfo.fname, '.') + 1, file_ext) != 0) ) { continue; }

            if (!(finfo.fattrib & AM_DIR))    /* skip subdirs */
            {
                snprintf(curr_name, SD_FILE_MAX_PATH_LEN, "%s/%s" , dir, finfo.fname);
                snprintf(new_name, SD_FILE_MAX_PATH_LEN, "%s/%s", SD_ARCHIVE_DIR, finfo.fname);
                //DEBUG_PRINTF(DBG_LVL_VERBOSE, "moving file '%s' to '%s'...", curr_name, new_name);
                FRESULT fr = f_rename(curr_name, new_name);
                if (fr != FR_OK)
                {
                    DEBUG_PRINTF(DBG_LVL_WARNING, "Failed to move file '%s' to archive (error code %d)", curr_name, fr);
                    res = false;
                    /* don't abort */
                }
                else
                {
                    DEBUG_PRINTF(DBG_LVL_VERBOSE, "File '%s' moved to archive", curr_name);
                }
            }
        }
        f_closedir(&dinfo);
    }
    else
    {
        res = false;
    }

    return res;
}
#endif

void SD_benchmark(uint32_t num_files, uint32_t chunks_per_file, uint32_t chunk_size, uint32_t delay_ms, uint32_t delay_mod)
{
    static FIL  file;
    static char fname_buf[32];
    const char* testdir    = "SDTEST";
    uint8_t*    write_buf  = 0;
    uint32_t    file_cnt   = 0;
    uint32_t    write_cnt  = 0;
    uint32_t    fopen_err  = 0;
    uint32_t    fwrite_err = 0;
    uint32_t    time_max   = 0;
    uint32_t    time_min   = UINT32_MAX;
    uint32_t    time_avg   = 0;

    if (!num_files || !chunks_per_file || !chunk_size) { return; }

    /* Initialize buffer */
    write_buf = malloc(chunk_size);
    if (!write_buf)
    {
        return;
    }
    memset(write_buf, 0xaa, chunk_size);

    /* Create directory */
    FRESULT fr = f_mkdir(testdir);
    if ( (fr != FR_OK) && (fr != FR_EXIST) )
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "Failed to create directory");
        return;
    }

    LED_Y_ON();
    DEBUG_PRINTF(DBG_LVL_INFO, "SD benchmark (Files: %lu, Chunks per file: %lu, Chunk size: %lu, Delay: %lums, Delay mod: %lu)", num_files, chunks_per_file, chunk_size, delay_ms, delay_mod);
    HAL_Delay(5000);

    while (file_cnt < num_files)
    {
        snprintf(fname_buf, sizeof(fname_buf), "%s/FILE%lu.DAT", testdir, file_cnt);
        //DEBUG_PRINTF(DBG_LVL_INFO, "Writing data to file %s...", fname_buf);

        uint32_t chunk_cnt = 0;
        while (chunk_cnt < chunks_per_file)
        {
            uint32_t start_time = HAL_GetTick();

            fr = f_open(&file, fname_buf, FA_WRITE | FA_OPEN_APPEND);
            if (fr != FR_OK)
            {
                DEBUG_PRINTF(DBG_LVL_ERROR, "f_open() failed with error code %u", fr);
                fopen_err++;
            }
            else
            {
                /* Write data to file */
                UINT written_bytes;
                fr = f_write(&file, write_buf, chunk_size, &written_bytes);
                f_close(&file);
                if (fr != FR_OK)
                {
                    DEBUG_PRINTF(DBG_LVL_ERROR, "f_write() failed with error code %u", fr);
                    fwrite_err++;
                }
                else if (written_bytes != chunk_size)
                {
                    DEBUG_PRINTF(DBG_LVL_ERROR, "Only %u of %u bytes written", written_bytes, chunk_size);
                    fwrite_err++;
                }
                else
                {
                    /* Print stats */
                    uint32_t elapsed_time = HAL_GetTick() - start_time;
                    if (elapsed_time > time_max)
                    {
                        time_max = elapsed_time;
                    }
                    if (elapsed_time < time_min)
                    {
                        time_min = elapsed_time;
                    }
                    time_avg += elapsed_time;
                    write_cnt++;
                    DEBUG_PRINTF(DBG_LVL_INFO, "%lu", elapsed_time);
                }
            }
            chunk_cnt++;
            if (delay_ms && (!delay_mod || ((chunk_cnt % delay_mod) == 0)))
            {
                HAL_Delay(delay_ms);
            }
            LED_Y_TG();
        }
        file_cnt++;
    }
    LED_Y_OFF();

    free(write_buf);

    DEBUG_PRINTF(DBG_LVL_INFO, "Benchmark finished (Chunks: %lu, Min: %lums, Max: %lums, Avg: %lums, Err: %lu/%lu)", write_cnt, time_min, time_max, time_avg / write_cnt, fopen_err, fwrite_err);
}
