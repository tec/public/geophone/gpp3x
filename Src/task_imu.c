/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Defines -------------------------------------------------------------------*/
#define IMU_SIZE_ID      4
#define IMU_SIZE_DATA    (2*3)

/* Variables -----------------------------------------------------------------*/
ImuStat_t                Imu = { 0 };

/* External variables --------------------------------------------------------*/
extern rb_t              rb_imustat;
extern rb_t              rb_imudata;

/* Functions -----------------------------------------------------------------*/

uint8_t Imu_readRegister(uint8_t devAddr, uint8_t regAddr)
{
    uint8_t val = 0;
    HAL_I2C_Master_Transmit(&IMU_I2C, devAddr, &regAddr, 1, 10);
    HAL_I2C_Master_Receive(&IMU_I2C, devAddr, &val, sizeof(uint8_t), 10);
    return val;
}

uint16_t Imu_readRegister16(uint8_t devAddr, uint8_t regAddr)
{
    uint16_t val = 0;
    HAL_I2C_Master_Transmit(&IMU_I2C, devAddr, &regAddr, 1, 10);
    HAL_I2C_Master_Receive(&IMU_I2C, devAddr, (uint8_t*)&val, sizeof(uint16_t), 10);
    return val;
}

uint8_t Imu_readMagStatus()
{
    return Imu_readRegister(IMU_MAG_ADDR, LSM303_MAG_STATUS);
}

void Imu_writeRegister(uint8_t devAddr, uint8_t regAddr, uint8_t val)
{
    uint16_t cmd = regAddr | (((uint16_t)val) << 8);
    HAL_I2C_Master_Transmit(&IMU_I2C, devAddr, (uint8_t*)&cmd, sizeof(uint16_t), 10);
}

void Imu_triggerMagSingleMeasurement()
{
    Imu_writeRegister(IMU_MAG_ADDR, LSM303_MAG_CTRL1, LSM303_MAG_CTRL1_MD_SINGLE | LSM303_MAG_CTRL1_TEMP_EN);
}

uint32_t Imu_getOrientation(bool swOff)
{
    if (!IMU_IS_SWON())
    {
        IMU_SWON();
        IMU_I2C_INIT();
        RTOS_TASK_DELAY(20);
    }
    Imu_writeRegister(IMU_MAG_ADDR, LSM303_MAG_CTRL2, LSM303_MAG_CTRL2_LPF);
    Imu_triggerMagSingleMeasurement();
    RTOS_TASK_DELAY(10);
    int16_t x = Imu_readRegister16(IMU_MAG_ADDR, LSM303_MAG_X_L | AUTO_INCREMENT_REG_ADDR);
    int16_t y = Imu_readRegister16(IMU_MAG_ADDR, LSM303_MAG_Y_L | AUTO_INCREMENT_REG_ADDR);
    //int16_t z = Imu_readRegister16(IMU_MAG_ADDR, LSM303_MAG_Z_L | AUTO_INCREMENT_REG_ADDR);

    if (swOff) { IMU_SWOFF(); }

    /*
     * TODO: convert to human readable orientation in degrees
     * However, the IMU is not working properly anyway due to the proximity to the geophone sensors (magnets).
     */
    return (uint32_t)y << 16 | x;
}

/*** IMU TASK *****************************************************************/
void vTaskImu(void* arg)
{
    (void)arg;

    DEBUG_PRINTF(DBG_LVL_VERBOSE, "IMU task started (ID: %lu, LPF: %i HPF: %i, OpMode: %i)", Imu.ID, Imu.Freq_LP, Imu.Freq_HP, Imu.OpMode);

    /* Configure IMU */
    IMU_triggerDisable();
    IMU_magDisable();
    IMU_accDisable();

    while (true)
    {
        if (ulTaskNotifyTake(pdTRUE, portMAX_DELAY))
        {
            Imu.Status          = 1;
            Imu.Samples_PostTrg = 0;
            Imu.ID              = SysConf.Geo->ID;

            /* Initial Delay */
            IMU_SWON();
            IMU_I2C_INIT();
            vTaskDelay(pdMS_TO_TICKS(20));

            /* Configure IMU to start acquiring data */
            IMU_configModeStream(0);
            IMU_magEnable();

            /* Delay */
            vTaskDelay(pdMS_TO_TICKS(20));

            /* Ignore 1st XL samples */
            uint8_t imu_buf[IMU_SIZE_DATA];
#if   defined(LSM303_C)
            imu_buf[0] = LSM303_ACC_X_L;
#elif defined(LSM303_AGR)
            imu_buf[0] = LSM303_ACC_X_L | AUTO_INCREMENT_REG_ADDR;
#endif
            HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);
            HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, imu_buf, IMU_SIZE_DATA, 10);
            /* Ignore 1st MAG samples */
            imu_buf[0] = LSM303_MAG_X_L | AUTO_INCREMENT_REG_ADDR;   /* Reg. Addr. auto-increment */
            HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 1, 10);
            HAL_I2C_Master_Receive( &IMU_I2C, IMU_MAG_ADDR, imu_buf, IMU_SIZE_DATA, 10);

            /* Timestamp */
            Imu.Start = RTC_TIMESTAMP();

            /* IMU Sampling: collects (Freq_HP / Decimation) samples per second until the Geo task signals to stop */
            do
            {
                /* Allocate a new element in the ring buffer */
                uint8_t* rb_imudata_head = RB_insert(&rb_imudata, NULL);

                /* Current event ID */
                memcpy(rb_imudata_head, (uint8_t*)&Imu.ID, IMU_SIZE_ID);

                /* XL */
#if   defined(LSM303_C)
                imu_buf[0] = LSM303_ACC_X_L;
#elif defined(LSM303_AGR)
                imu_buf[0] = LSM303_ACC_X_L | AUTO_INCREMENT_REG_ADDR;
#endif
                HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);
                HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, (rb_imudata_head + IMU_SIZE_ID), IMU_SIZE_DATA, 10);

                /* MAG */
                imu_buf[0] = LSM303_MAG_X_L | AUTO_INCREMENT_REG_ADDR;   /* Reg. Addr. auto-increment */
                HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 1, 10);
                HAL_I2C_Master_Receive( &IMU_I2C, IMU_MAG_ADDR, (rb_imudata_head + IMU_SIZE_ID + IMU_SIZE_DATA), IMU_SIZE_DATA, 10);

                Imu.Samples_PostTrg++;

                /* Trying to obtain semaphore (choose timeout such that the IMU sampling frequency is as desired) */
            }
            while (xSemaphoreTake(xSemaphore_ImuStop, pdMS_TO_TICKS((Imu.Decimation * 1000) / Imu.Freq_HP)) == pdFALSE);

            /* Log IMUSTAT to SD card */
            RB_insert(&rb_imustat, (uint8_t*)&Imu);
            RTOS_queueSendLog(LOGTYPE_IMUSTAT);

            /* Turn off Acc and Mag again */
            IMU_accDisable();
            IMU_magDisable();
            Imu.Status = 0;

            IMU_SWOFF();
        }
        else
        {
            /* IMU task notification timeout */
        }
    } /* IMU task loop */
}
