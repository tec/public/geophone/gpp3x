/*
 * Copyright (c) 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Functions -----------------------------------------------------------------*/

void DAC_init(void)
{
#ifdef DAC_INT_VREF
    /* Make sure the DAC is powered */
    if (!TRIGGER_IS_SWON())
    {
        TRIGGER_SWON();
        Delay(DAC_VREF_STARTUP_DELAY_US);
    }

    /* Select the internal reference voltage */
    DAC_sendCmd(DAC_CMD_ENTER_NORMAL, DAC_INT_VREF);
#endif
}

bool DAC_sendCmd(uint8_t cmd, uint16_t val)
{
    bool     res = true;
    uint8_t  dac_buf[2];
    uint16_t dac_cmd;

    /* Make sure it is a valid command (4-bit value) */
    if (cmd > 0xF) { return false; }

    if ( (cmd == DAC_CMD_ENTER_STANDBY) || (cmd == DAC_CMD_ENTER_NORMAL) || (cmd == DAC_CMD_ENTER_SHUTDOWN) )
    {
        dac_cmd = DAC_CMD_STATE(cmd, val);
    }
    else
    {
        dac_cmd = DAC_CMD_REG(cmd, val);
    }

    /* Convert to big endian */
    dac_buf[0] = dac_cmd >> 8;
    dac_buf[1] = dac_cmd & 0xFF;

    /* Acquire exclusive access to the SPI bus */
    if (IS_RTOS_RUNNING())
    {
        if (xSemaphoreTake(DAC_SPI_SEMAPHORE, pdMS_TO_TICKS(DAC_SPI_TIMEOUT_MS)) == pdFALSE)
        {
            DEBUG_PRINT(DBG_LVL_ERROR, "Could not acquire SPI bus");
            return false;
        }
    }

    DAC_ASSERT();
    if (HAL_SPI_Transmit(&DAC_SPI_DEVICE, dac_buf, 2, DAC_SPI_TIMEOUT_MS) != HAL_OK)
    {
        res = false;
    }
    DAC_DEASSERT();

    /* Release semaphore */
    if (IS_RTOS_RUNNING())
    {
        xSemaphoreGive(DAC_SPI_SEMAPHORE);
    }

    return res;
}
