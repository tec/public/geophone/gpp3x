/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Task scheduler, starts the RTOS tasks at the specified times
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
static tl_t*    schedule_head = 0;

/* External variables---------------------------------------------------------*/
extern rb_t     rb_schedule;

/* Functions -----------------------------------------------------------------*/

static void Schedule_addDefaultTasks(void)
{
#if TSYNC_PERIOD_S && !TSYNC_METHOD_NONE
    /* Add Tsync task to the scheduler (if not already included in the schedule) */
    if (!TL_contains(&schedule_head, TASK_TSYNC) &&
        TL_insert(&schedule_head, 0, TSYNC_PERIOD_S, 0, TASK_TSYNC))
    {
        DEBUG_PRINTF(DBG_LVL_INFO, "Added TSync task with default period %us", TSYNC_PERIOD_S);
    }
#endif

#if HEALTH_PERIOD_S
    /* Add Health task to the scheduler (if not already included in the schedule) */
    if (!TL_contains(&schedule_head, TASK_HEALTH) &&
        TL_insert(&schedule_head, 0, HEALTH_PERIOD_S, 0, TASK_HEALTH))
    {
        DEBUG_PRINTF(DBG_LVL_INFO, "Added Health task with default period %us", HEALTH_PERIOD_S);
    }
#endif

#if GNSS_PERIOD_S
    /* Add GNSS task to the scheduler (if not already included in the schedule) */
    if (!TL_contains(&schedule_head, TASK_GNSS) &&
        TL_insert(&schedule_head, 0, GNSS_PERIOD_S, 0, TASK_GNSS))
    {
        DEBUG_PRINTF(DBG_LVL_INFO, "Added GNSS task with default period %us", GNSS_PERIOD_S);
    }
#endif
}

static uint32_t Schedule_taskDuration(const tl_t* task)
{
    if (!task) { return 0; }

    if ( (task->task == TASK_GEO) || (task->task & TASK_POSTPONE) )
    {
        return task->argument;  /* argument is a duration in this case */
    }
    return 0;
}

static void Schedule_skipMissedTasks(void)
{
    uint32_t current_time = RTC_getEpoch();

    /* Skip task if start time is too far in the past */
    while ( !TL_isEmpty(&schedule_head) && ((schedule_head->start + SYSTEM_WAKEUP_PERIOD * 2) < current_time) )
    {
        /* For tasks with a duration, add it to the start time and check again */
        uint32_t duration = Schedule_taskDuration(schedule_head);
        if ( duration && (schedule_head->start + duration > current_time) )
        {
            duration = schedule_head->start + duration - current_time;
            TL_insert(&schedule_head, current_time, 0, duration, schedule_head->task);    /* do not insert a periodic task here */
            DEBUG_PRINTF(DBG_LVL_INFO, "Task 0x%x duration adjusted to %lu", schedule_head->task, duration);
        }
        else
        {
            DEBUG_PRINTF(DBG_LVL_INFO, "Skipping task 0x%x (start time is %lus in the past)", schedule_head->task, current_time - schedule_head->start);
        }
        TL_popPush(&schedule_head, current_time);
    }
}

bool Schedule_addEntry(uint8_t tasks, uint32_t start_time, uint32_t period, uint32_t arg, bool persistent)
{
    if ( IS_RTOS_STARTED() && (xSemaphoreTake(xSemaphore_Schedule, pdMS_TO_TICKS(10)) == pdFALSE) )
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "could not get semaphore");
        return false;
    }

    /* Check parameters */
    if (tasks & ~TASK_ALL) { return false; }

    /* Create a separate entry for each task */
    for (uint32_t bit = 0; bit < (TASK_BITS - 1); bit++)    /* Skip TASK_POSTPONE bit (0x80) */
    {
        uint8_t task_bitmask = (1 << bit);
        if (tasks & task_bitmask)
        {
            task_bitmask |= (tasks & TASK_POSTPONE);
            if (TL_insert(&schedule_head, start_time, period, arg, task_bitmask))
            {
                DEBUG_PRINTF(DBG_LVL_INFO, "Schedule entry added (first: %lu, period: %lu, argument: %lu, task: 0x%x)", start_time, period, arg, task_bitmask);
            }
            else
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "Failed to add entry to schedule");
            }
        }
    }

    if (persistent)
    {
        /* Notify the log task to write this new schedule entry to the SD card */
        ScheduleEntry_t new_task = { .first = start_time, .period = 0, .argument = arg, .tasks = tasks };
        RB_insert(&rb_schedule, (uint8_t *)&new_task);
        RTOS_queueSendLog(LOGTYPE_SCHED_ADD);
    }

    if (IS_RTOS_STARTED())
    {
        xSemaphoreGive(xSemaphore_Schedule);
    }

    return true;
}

bool Schedule_adjustTaskPeriod(uint8_t task, uint32_t period)
{
    if ( IS_RTOS_STARTED() && (xSemaphoreTake(xSemaphore_Schedule, pdMS_TO_TICKS(10)) == pdFALSE) )
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "could not get semaphore");
        return false;
    }

    uint32_t cnt = TL_setPeriod(&schedule_head, task, period);
    if (cnt)
    {
        DEBUG_PRINTF(DBG_LVL_INFO, "Period of task %u changed to %lus (%lu entries modified)", task, period, cnt);
    }

    if (IS_RTOS_STARTED())
    {
        xSemaphoreGive(xSemaphore_Schedule);
    }

    return cnt > 0;
}

/* Postpone the execution of a periodic task */
bool Schedule_postponeTask(uint8_t task, uint32_t duration_s)
{
    if ( IS_RTOS_STARTED() && (xSemaphoreTake(xSemaphore_Schedule, pdMS_TO_TICKS(10)) == pdFALSE) )
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "could not get semaphore");
        return false;
    }

    uint32_t cnt = TL_postpone(&schedule_head, task, RTC_getEpoch() + duration_s);
    if (cnt)
    {
        DEBUG_PRINTF(DBG_LVL_INFO, "Next start time of task %u postponed (%lu entries modified)", task, cnt);
    }

    if (IS_RTOS_STARTED())
    {
        xSemaphoreGive(xSemaphore_Schedule);
    }

    return cnt > 0;
}

void Schedule_clear(bool addDefaultTasks)
{
    if ( IS_RTOS_STARTED() && (xSemaphoreTake(xSemaphore_Schedule, pdMS_TO_TICKS(10)) == pdFALSE) ) { return; }

    /* Empty priority queue in which the schedule is stored */
    TL_clear(&schedule_head);

    if (addDefaultTasks)
    {
        Schedule_addDefaultTasks();
    }

    if (IS_RTOS_STARTED())
    {
        xSemaphoreGive(xSemaphore_Schedule);
    }
}

void vTaskSchedule(void* argument)
{
    (void)argument;

    /* Add default tasks to scheduler (must occur after loading the schedule from the SD card) */
    if (xSemaphoreTake(xSemaphore_Schedule, pdMS_TO_TICKS(10)) == pdTRUE)
    {
        Schedule_addDefaultTasks();
        xSemaphoreGive(xSemaphore_Schedule);
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "Failed to add default tasks");
    }

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Schedule task started");

    while (true)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);

        /* if SD card mounted as mass storage: skip all scheduling activity */
        if (SysConf.USB->deviceClass == MSC) { continue; }

        if (xSemaphoreTake(xSemaphore_Schedule, pdMS_TO_TICKS(10)) == pdFALSE)
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "Failed to acquire schedule semaphore");
            continue;
        }

        Schedule_skipMissedTasks();

        uint32_t current_time = RTC_getEpoch();
        bool     task_started = false;

        while ( !TL_isEmpty(&schedule_head) && (schedule_head->start <= current_time) )
        {
            uint32_t task = schedule_head->task;
            uint32_t arg  = schedule_head->argument;

            /* Postpone Task */
            if (task & TASK_POSTPONE)
            {
                DEBUG_PRINT(DBG_LVL_VERBOSE, "POSTPONE TASK");
                task = task & ~TASK_POSTPONE;

                uint32_t start     = schedule_head->start;
                bool     postponed = TL_postpone(&schedule_head, task, start + arg) > 0;

                /* Schedule wakeup in case it is the geophone task and continuous mode is enabled */
                if ( (task & TASK_GEO) && (SysConf.OpMode & SYS_OPMODE_CONT) )
                {
                    if (TL_insert(&schedule_head, start + arg, 0, 0, TASK_GEO))
                    {
                        Geo_abort(false);
                        DEBUG_PRINTF(DBG_LVL_INFO, "Schedule entry added to resume geo task in %lus", arg);
                    }
                    else
                    {
                        DEBUG_PRINT(DBG_LVL_WARNING, "Failed to add schedule entry to resume geo task");
                    }
                }
                else if (!postponed)
                {
                    DEBUG_PRINTF(DBG_LVL_WARNING, "Failed to postpone task 0x%x", task);
                    arg = 0;
                }
                RTOS_event(EVENT_GEOPHONE3X_TASK_POSTPONED, (arg << 8) | task, EVTLOG_SD | EVTLOG_COM);
                task = 0;    /* no further processing required */
            }

            /* Health Task */
            if (task & TASK_HEALTH)
            {
                if (IS_RTOS_STARTED())
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "HEALTH TASK");
                    xTaskNotifyGive(xTaskHandle_Health);
                }
            }

            /* Geophone Task */
            if ( (task & TASK_GEO) && !(SysConf.OpMode & SYS_OPMODE_CONT) )    /* Nothing to do if OpMode is 'continuous sampling' */
            {
                /* Launch Acquire */
                DEBUG_PRINT(DBG_LVL_VERBOSE, "GEOPHONE TASK");
                Geo_initAcq(TRG_SCHED, arg, 0);
            }

#if USE_GNSS
            /* GPS Task */
            if (task & TASK_GNSS)
            {
                DEBUG_PRINT(DBG_LVL_VERBOSE, "GNSS TASK");
                xTaskNotify(xTaskHandle_GNSS, GNSS_NOTIFY_SAMPLE, eSetValueWithoutOverwrite);
            }
#endif

#if USE_INCLINO
            /* Inclinometer Task */
            if (task & TASK_INCLINO)
            {
                DEBUG_PRINT(DBG_LVL_VERBOSE, "INCLINOMETER TASK");
                xTaskNotifyGive(xTaskHandle_Inclino);
            }
#endif

            /* Tsync Task */
            if (task & TASK_TSYNC)
            {
                DEBUG_PRINT(DBG_LVL_VERBOSE, "TSYNC TASK");
                NOTIFY_TSYNC_TASK();
            }

            task_started = true;

            /* Remove current task and re-schedule it later if periodic */
            TL_popPush(&schedule_head, current_time);
        }

        if ( task_started && !TL_isEmpty(&schedule_head) )
        {
            DEBUG_PRINTF(DBG_LVL_VERBOSE, "Start time of the next scheduled task (0x%02x) is in %lds", schedule_head->task, schedule_head->start - current_time);
        }

        xSemaphoreGive(xSemaphore_Schedule);
    }
}


