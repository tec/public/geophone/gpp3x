/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

#ifdef LSM303_AGR

/* Variables -----------------------------------------------------------------*/
#if !DBG_PRINT_TARGET_NONE
const uint8_t printMode = IMU_PRINT_DEBUG;
#else
const uint8_t printMode = IMU_PRINT_OFF;
#endif

/* External variables --------------------------------------------------------*/
extern ImuStat_t         Imu;
extern rb_t              rb_imudata;
extern rb_t              rb_imutemp;

/* Static functions ----------------------------------------------------------*/

static uint8_t IMU_getODRCodeFromFrequency(uint16_t frequency)
{
    if (frequency == 0)
        return LSM303_ACC_CTRL1_ODR_OFF;
    else if (frequency == 1)
        return LSM303_ACC_CTRL1_ODR_1Hz;
    else if (frequency == 10)
        return LSM303_ACC_CTRL1_ODR_10Hz;
    else if (frequency == 25)
        return LSM303_ACC_CTRL1_ODR_25Hz;
    else if (frequency == 50)
        return LSM303_ACC_CTRL1_ODR_50Hz;
    else if (frequency == 100)
        return LSM303_ACC_CTRL1_ODR_100Hz;
    else if (frequency == 200)
        return LSM303_ACC_CTRL1_ODR_200Hz;
    else if (frequency == 400)
        return LSM303_ACC_CTRL1_ODR_400Hz;
    else if (frequency == 1620)
        return LSM303_ACC_CTRL1_ODR_LP | LSM303_ACC_CTRL1_LP_EN;
    else if (frequency == 1344)
        return LSM303_ACC_CTRL1_ODR_HP;
    else if (frequency == 5376)
        return LSM303_ACC_CTRL1_ODR_HP | LSM303_ACC_CTRL1_LP_EN;
    else
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "IMU frequency invalid, default to 10Hz");
        return LSM303_ACC_CTRL1_ODR_10Hz;
    }
}

// Sort an array by insertion sort
static void insertionSort(uint16_t* array, uint32_t length)
{
    if (length != IMU_FILTER_SIZE)
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "Input length for sorting incorrect: %lu", length);
        return;
    }

    uint16_t key;
    uint32_t i, j;
    for (i = 1; i < length; i++)
    {
        key = array[i];
        j = i - 1;

        // Move elements of array[0..i-1], that are greater than key, to one position ahead of their current position
        while ( (j > 0) && (array[j] > key))
        {
            array[j + 1] = array[j];
            j--;
        }

        // Insert - j = 0 as special case to avoid using signed integers
        if (array[j] > key)
        {
            // Exit while loop because j = 0
            array[j + 1] = array[j];
            array[j] = key;
        }
        else
        {
            // Exit while loop because found j != 0 as correct index
            array[j + 1] = key;
        }
    }
}

/* Functions -----------------------------------------------------------------*/

void  IMU_init()
{
    /* Configure IMU to ensure no unintended interrupts are triggered */
    IMU_configModeStream(0);

    IMU_triggerDisable();
    IMU_magDisable();
    IMU_accDisable();
}

void IMU_configModeTriggeredHP()
{
    uint8_t imu_buf[2];

    /* CTRL1: Enable XYZ axes, low power ODR */
    imu_buf[0] = LSM303_ACC_CTRL1;
    imu_buf[1] = LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN |
                 LSM303_ACC_CTRL1_ZEN | IMU_getODRCodeFromFrequency(Imu.Freq_LP);
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL2: Enable lowest high-pass filter for interrupt generation */
    imu_buf[0] = LSM303_ACC_CTRL2;
    imu_buf[1] = LSM303_ACC_CTRL2_HPIS1 | LSM303_ACC_CTRL2_HPCF1 | LSM303_ACC_CTRL2_HPCF2;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL3: AOI1 on INT1 */
    imu_buf[0] = LSM303_ACC_CTRL3;
    imu_buf[1] = LSM303_ACC_CTRL3_AOI1;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL4: Normal self-test, High-resolution, +-2g, Little endian, Block data update */
    imu_buf[0] = LSM303_ACC_CTRL4;
    imu_buf[1] = LSM303_ACC_CTRL4_ST_N | LSM303_ACC_CTRL4_HR |
                 LSM303_ACC_CTRL4_FS_2g | LSM303_ACC_CTRL4_END_LITTLE | LSM303_ACC_CTRL4_BDU;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL5: FIFO enabled, Reboot acc memory */
    imu_buf[0] = LSM303_ACC_CTRL5;
    imu_buf[1] = LSM303_ACC_CTRL5_FIFO_EN | LSM303_ACC_CTRL5_REBOOT_MEM;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* FIFO_CTRL: Stream mode, 30 entries */
    imu_buf[0] = LSM303_ACC_FIFO_CTRL;
    imu_buf[1] = LSM303_ACC_FIFO_CTRL_MD_STREAM | 0x1E;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* IG1: XYZ high interrupt */
    imu_buf[0] = LSM303_ACC_IG1_CFG;
    imu_buf[1] = LSM303_ACC_IG1_CFG_XHIE | LSM303_ACC_IG1_CFG_YHIE | LSM303_ACC_IG1_CFG_ZHIE | LSM303_ACC_IG1_CFG_AOI_OR;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    imu_buf[0] = LSM303_ACC_IG1_THS;
    imu_buf[1] = Imu.Level_Trg;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Configured IMU in triggered mode");
}

void IMU_configModeTriggeredRef()
{
    uint8_t imu_buf[2];

    // Trigger soft reset
    imu_buf[0] = LSM303_MAG_CTRL1;
    imu_buf[1] = LSM303_MAG_CTRL1_SOFT_RST;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);
    vTaskDelay(pdMS_TO_TICKS(50));

    /* CTRL1: Enable XYZ axes, low power ODR */
    imu_buf[0] = LSM303_ACC_CTRL1;
    imu_buf[1] = LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN |
                 LSM303_ACC_CTRL1_ZEN | IMU_getODRCodeFromFrequency(Imu.Freq_LP);
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL2: Enable high-pass filter for triggering, enable reference signal for filter */
    imu_buf[0] = LSM303_ACC_CTRL2;
    imu_buf[1] = LSM303_ACC_CTRL2_HPIS1 | LSM303_ACC_CTRL2_HPM_REF;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL3: AOI1 on INT1 */
    imu_buf[0] = LSM303_ACC_CTRL3;
    imu_buf[1] = LSM303_ACC_CTRL3_AOI1;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL4: Normal self-test, High-resolution, +-2g, Little endian, Block data update */
    imu_buf[0] = LSM303_ACC_CTRL4;
    imu_buf[1] = LSM303_ACC_CTRL4_ST_N | LSM303_ACC_CTRL4_HR |
                 LSM303_ACC_CTRL4_FS_2g | LSM303_ACC_CTRL4_END_LITTLE | LSM303_ACC_CTRL4_BDU;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL4: Enable FIFO, Reboot acc memory */
    imu_buf[0] = LSM303_ACC_CTRL5;
    imu_buf[1] = LSM303_ACC_CTRL5_FIFO_EN | LSM303_ACC_CTRL5_REBOOT_MEM;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* FIFO_CTRL: Stream mode, 30 entries */
    imu_buf[0] = LSM303_ACC_FIFO_CTRL;
    imu_buf[1] = LSM303_ACC_FIFO_CTRL_MD_STREAM | 0x1E;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* IG1: Z high interrupt - reference only applicable to one axis*/
    imu_buf[0] = LSM303_ACC_IG1_CFG;
    imu_buf[1] = LSM303_ACC_IG1_CFG_ZHIE | LSM303_ACC_IG1_CFG_AOI_OR;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    // Compute reference signals
    IMU_setReferenceSignal();

    imu_buf[0] = LSM303_ACC_IG1_THS;
    imu_buf[1] = Imu.Level_Trg;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Configured IMU to triggered mode with reference signal");
}

void IMU_configModeStream(uint32_t signalFifoOverrun)
{
    uint8_t imu_buf[2];

    // Trigger soft reset
    imu_buf[0] = LSM303_MAG_CTRL1;
    imu_buf[1] = LSM303_MAG_CTRL1_SOFT_RST;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);
    vTaskDelay(pdMS_TO_TICKS(50));

    /* CTRL1: Enable XYZ axes, high power ODR */
    imu_buf[0] = LSM303_ACC_CTRL1;
    imu_buf[1] = LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN | LSM303_ACC_CTRL1_ZEN |
                 IMU_getODRCodeFromFrequency(Imu.Freq_HP);
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL2: Disable high-pass filter */

    /* CTRL3: Threshold interrupt signal */
    imu_buf[0] = LSM303_ACC_CTRL3;
    if (signalFifoOverrun)
        imu_buf[1] = LSM303_ACC_CTRL3_FIFO_OVERRUN | LSM303_ACC_CTRL3_FIFO_WTRMRK;
    else
        imu_buf[1] = LSM303_ACC_CTRL3_INT1_OFF;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL4: Normal self-test, High-resolution, +-2g, Little endian, Block data update */
    imu_buf[0] = LSM303_ACC_CTRL4;
    imu_buf[1] = LSM303_ACC_CTRL4_ST_N | LSM303_ACC_CTRL4_HR |
                 LSM303_ACC_CTRL4_FS_2g | LSM303_ACC_CTRL4_END_LITTLE | LSM303_ACC_CTRL4_BDU;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* CTRL5: FIFO enabled, Reboot acc memory */
    imu_buf[0] = LSM303_ACC_CTRL5;
    imu_buf[1] = LSM303_ACC_CTRL5_FIFO_EN | LSM303_ACC_CTRL5_REBOOT_MEM;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    /* FIFO_CTRL: Stream mode, 31 entries */
    imu_buf[0] = LSM303_ACC_FIFO_CTRL;
    imu_buf[1] = LSM303_ACC_FIFO_CTRL_MD_STREAM | LSM303_ACC_FIFO_CTRL_TR_INT1 | 0x1F;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    DEBUG_PRINT(DBG_LVL_INFO, "Configured IMU to stream mode");
}

/* Helper Functions ----------------------------------------------------------*/

// Sets Accelerometer to low sampling frequency
void IMU_setLowODR()
{
    uint8_t    imu_buf[2];

    // XYZ axes enable, low sampling frequency
    imu_buf[0] = LSM303_ACC_CTRL1;
    imu_buf[1] = LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN |
                 LSM303_ACC_CTRL1_ZEN | IMU_getODRCodeFromFrequency(Imu.Freq_LP);
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);
}

// Sets Accelerometer to high sampling frequency
void IMU_setHighODR()
{
    uint8_t imu_buf[2];

    // XYZ axes enable, high sampling frequency
    imu_buf[0] = LSM303_ACC_CTRL1;
    imu_buf[1] = LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN |
                 LSM303_ACC_CTRL1_ZEN | IMU_getODRCodeFromFrequency(Imu.Freq_HP);
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);
}

void IMU_triggerEnable()
{
    uint8_t imu_buf[2];

    // Enable ACC trigger
    imu_buf[0] = LSM303_ACC_CTRL3;
    if (Imu.OpMode == IMU_CONT)
        imu_buf[1] = LSM303_ACC_CTRL3_FIFO_OVERRUN | LSM303_ACC_CTRL3_FIFO_WTRMRK;
    else
        imu_buf[1] = LSM303_ACC_CTRL3_AOI1;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    // Enable MAG trigger
    imu_buf[0] = LSM303_MAG_IG_CFG;
    imu_buf[1] = LSM303_MAG_IG_CFG_EN | LSM303_MAG_IG_CFG_ACTIVE_H | LSM303_MAG_IG_CFG_ZIEN | LSM303_MAG_IG_CFG_YIEN | LSM303_MAG_IG_CFG_XIEN;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);
}

void IMU_triggerDisable()
{
    uint8_t imu_buf[2];

    // Disable ACC trigger
    imu_buf[0] = LSM303_ACC_CTRL3;
    imu_buf[1] = LSM303_ACC_CTRL3_INT1_OFF;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    // Disable MAG trigger
    imu_buf[0] = LSM303_MAG_IG_CFG;
    imu_buf[1] = LSM303_MAG_IG_CFG_ACTIVE_H;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);
}

// Sets the IMUs accelerometer reference signals based on the average of the last SAD_SIZE Ring Buffer data
void IMU_setReferenceSignal()
{
    uint8_t imu_buf[2];

    uint32_t SAD_SIZE = MIN(rb_imutemp.count, 20);

    ImuData_t* first_block = (ImuData_t*)rb_imutemp.data;
    ImuData_t* last_block  = (ImuData_t*)rb_imutemp.data + ((rb_imutemp.max - 1) * rb_imutemp.itemsize);
    ImuData_t* current     = (ImuData_t*)rb_imutemp.head;
    ImuData_t* tail        = (ImuData_t*)rb_imutemp.tail;

    uint32_t i         = SAD_SIZE;
    int32_t accSumX    = 0;
    int32_t accSumY    = 0;
    int32_t accSumZ    = 0;
    int32_t magSumX    = 0;
    int32_t magSumY    = 0;
    int32_t magSumZ    = 0;
    int16_t accAvgs[3] = {0};
    int8_t  imu_ref[6] = {0};

    if (rb_imudata.tail >= rb_imudata.head) //ring buffer wrap around
    {
        while ( (i > 0) && (current >= first_block))
        {
            accSumX += current->acc_x;
            accSumY += current->acc_y;
            accSumZ += current->acc_z;

            magSumX += current->mag_x;
            magSumY += current->mag_y;
            magSumZ += current->mag_z;

            current--;
            i--;
        }
        current = (ImuData_t*)last_block;
    }

    while ( (i > 0) && (current >= tail))
    {
        accSumX += current->acc_x;
        accSumY += current->acc_y;
        accSumZ += current->acc_z;

        magSumX += current->mag_x;
        magSumY += current->mag_y;
        magSumZ += current->mag_z;

        current--;
        i--;
    }

    accAvgs[0] = (int16_t)(accSumX / SAD_SIZE / 8); // Divide by  8, reference signal LSB corresponds to 8x higher acceleration
    accAvgs[1] = (int16_t)(accSumY / SAD_SIZE / 8);
    accAvgs[2] = (int16_t)(accSumZ / SAD_SIZE / 8);

    imu_ref[0] = (uint8_t)(accAvgs[0]);
    imu_ref[1] = (uint8_t)(accAvgs[0] >> 8);
    imu_ref[2] = (uint8_t)(accAvgs[1]);
    imu_ref[3] = (uint8_t)(accAvgs[1] >> 8);
    imu_ref[4] = (uint8_t)(accAvgs[2]);
    imu_ref[5] = (uint8_t)(accAvgs[2] >> 8);

    // Set reference signals - as we use the high-res mode (12bit), we must take the 8 most significant bits
    // The LSM303AGR only allows reference for one axis; we arbitrarily choose the Z axis as we expect tilting motions
    imu_buf[0] = LSM303_ACC_REF;
    imu_buf[1] = ((imu_ref[4] & 0x0F) << 4) | (imu_ref[5] >> 4);
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);

    if (printMode & IMU_PRINT_DEBUG)
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Average (X/Y/Z): %i / %i / %i", (int)(accSumX/SAD_SIZE), (int)(accSumY/SAD_SIZE), (int)(accSumZ/SAD_SIZE));
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Set new reference signal:");
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "ACC (X/Y/Z): %i / %i / %i", accAvgs[0], accAvgs[1], accAvgs[2]);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "ACC (X_L/Y_L/Z_L): %i / %i / %i", imu_ref[0], imu_ref[2], imu_ref[4]);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "ACC (X_H/Y_H/Z_H): %i / %i / %i", imu_ref[1], imu_ref[3], imu_ref[5]);
    }

    // Reset SAD ring buffer
    rb_imutemp.count = 0;
    rb_imutemp.head  = rb_imutemp.data;
    rb_imutemp.tail  = rb_imutemp.data;

    // Log new static state
    ImuData_t imuDataPoint;
    imuDataPoint.ID = Imu.ID;
    Imu.Samples_PostTrg++;

    imuDataPoint.acc_x = (int16_t)(accSumX / SAD_SIZE);
    imuDataPoint.acc_y = (int16_t)(accSumY / SAD_SIZE);
    imuDataPoint.acc_z = (int16_t)(accSumZ / SAD_SIZE);

    imuDataPoint.mag_x = (int16_t)(magSumX / SAD_SIZE);
    imuDataPoint.mag_y = (int16_t)(magSumY / SAD_SIZE);
    imuDataPoint.mag_z = (int16_t)(magSumZ / SAD_SIZE);

    // Add reference point (new stable position) as last data point in series
    RB_insert(&rb_imudata, (uint8_t*)&imuDataPoint);
}

// Measures whether signal is stable (SAD: sum of absolute differences)
uint32_t IMU_dataCalculateSAD(uint8_t numOfData)
{
    uint32_t SAD = 0;

    ImuData_t* first_block = (ImuData_t*)rb_imutemp.data;
    ImuData_t* last_block  = (ImuData_t*)rb_imutemp.data + ((rb_imutemp.max - 1) * rb_imutemp.itemsize);
    ImuData_t* current     = (ImuData_t*)rb_imutemp.head;
    ImuData_t* tail        = (ImuData_t*)rb_imutemp.tail;

    uint32_t i = MIN(rb_imutemp.count, numOfData);
    int16_t x_last = current->acc_x;
    int16_t y_last = current->acc_y;
    int16_t z_last = current->acc_z;
    int16_t x_diff, y_diff, z_diff;

    if (rb_imudata.tail > rb_imudata.head) // Ring buffer wrap around
    {
        while ( (i > 0) && (current >= first_block))
        {
            x_diff = current->acc_x - x_last;
            y_diff = current->acc_y - y_last;
            z_diff = current->acc_z - z_last;
            SAD   += ABS(x_diff);
            SAD   += ABS(y_diff);
            SAD   += ABS(z_diff);
            x_last = current->acc_x;
            y_last = current->acc_y;
            z_last = current->acc_z;

            current--;
            i--;
        }
        current = (ImuData_t*)last_block;
    }

    while ( (i > 0) && (current >= tail))
    {
        x_diff  = current->acc_x - x_last;
        y_diff  = current->acc_y - y_last;
        z_diff  = current->acc_z - z_last;
        SAD    += ABS(x_diff);
        SAD    += ABS(y_diff);
        SAD    += ABS(z_diff);
        x_last  = current->acc_x;
        y_last  = current->acc_y;
        z_last  = current->acc_z;

        current--;
        i--;
    }

    return SAD;
}

// Returns the median value of the contained data points as value inside the ring buffer
void IMU_calculateAvg(rb_t* rb)
{
    /* declare as static to reduce stack usage */
    static uint16_t data_acc_x[IMU_FILTER_SIZE];
    static uint16_t data_acc_y[IMU_FILTER_SIZE];
    static uint16_t data_acc_z[IMU_FILTER_SIZE];
    static uint16_t data_mag_x[IMU_FILTER_SIZE];
    static uint16_t data_mag_y[IMU_FILTER_SIZE];
    static uint16_t data_mag_z[IMU_FILTER_SIZE];

    uint32_t curr_ID = 0;

    // Read data into arrays
    if (rb->count != IMU_FILTER_SIZE)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "Incorrect rb count");
        // Last data value will be communicated over the network (as at head of rb)
        return;
    }

    uint32_t i;
    for (i = 0; i < IMU_FILTER_SIZE; i++)
    {
        ImuData_t* imuDataPoint = (ImuData_t*)RB_getTail(rb);
        if (!imuDataPoint)
        {
            DEBUG_PRINT(DBG_LVL_ERROR, "Invalid RB pointer");
            return;
        }
        curr_ID       = imuDataPoint->ID;
        data_acc_x[i] = imuDataPoint->acc_x;
        data_acc_y[i] = imuDataPoint->acc_y;
        data_acc_z[i] = imuDataPoint->acc_z;
        data_mag_x[i] = imuDataPoint->mag_x;
        data_mag_y[i] = imuDataPoint->mag_y;
        data_mag_z[i] = imuDataPoint->mag_z;
        RB_remove(rb);
    }

    // Sort arrays
    insertionSort(data_acc_x, IMU_FILTER_SIZE);
    insertionSort(data_acc_y, IMU_FILTER_SIZE);
    insertionSort(data_acc_z, IMU_FILTER_SIZE);
    insertionSort(data_mag_x, IMU_FILTER_SIZE);
    insertionSort(data_mag_y, IMU_FILTER_SIZE);
    insertionSort(data_mag_z, IMU_FILTER_SIZE);

    // Create new data point containing Median values and add it to the ring buffer
    uint32_t  m_idx = IMU_FILTER_SIZE / 2;
    ImuData_t imuMedianVal = { curr_ID, data_acc_x[m_idx], data_acc_y[m_idx], data_acc_z[m_idx], data_mag_x[m_idx], data_mag_y[m_idx], data_mag_z[m_idx]};

    if (rb->count == 0)
    {
        RB_insert(rb, (uint8_t*)&imuMedianVal);
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "Rb was not empty!");
    }
    // Median value is returned as only value inside ring buffer
}

void IMU_magEnable()
{
    uint8_t imu_buf[2];

    /* CTRL1: Continuous measurements, 50Hz ODR, Reboot mag memory */
    imu_buf[0] = LSM303_MAG_CTRL1;
    imu_buf[1] = LSM303_MAG_CTRL1_MD_CONT | LSM303_MAG_CTRL1_ODR50;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);

    /* CTRL2: Enable low-pass */
    imu_buf[0] = LSM303_MAG_CTRL2;
    imu_buf[1] = LSM303_MAG_CTRL2_LPF;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);

    /* CTRL3: Little endian, Block data update */
    imu_buf[0] = LSM303_MAG_CTRL3;
    imu_buf[1] = LSM303_MAG_CTRL3_END_LITTLE | LSM303_MAG_CTRL3_BDU | LSM303_MAG_CTRL3_INT_MAG;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);
}

void IMU_magDisable()
{
    uint8_t imu_buf[2];

    // Cannot turn off - reduce to minimum
    imu_buf[0] = LSM303_MAG_CTRL1;
    imu_buf[1] = LSM303_MAG_CTRL1_MD_IDLE | LSM303_MAG_CTRL1_ODR10 | LSM303_MAG_CTRL1_LP;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR, imu_buf, 2, 10);
}

void IMU_accEnable()
{
    uint8_t imu_buf[2];

    // ODR = FREQ_LP (Power on)
    imu_buf[0] = LSM303_ACC_CTRL1;
    imu_buf[1] = LSM303_ACC_CTRL1_XEN | LSM303_ACC_CTRL1_YEN |
                 LSM303_ACC_CTRL1_ZEN | IMU_getODRCodeFromFrequency(Imu.Freq_LP);
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);
}

void IMU_accDisable()
{
    uint8_t imu_buf[2];

    // ODR = 0 (Power off)
    imu_buf[0] = LSM303_ACC_CTRL1;
    imu_buf[1] = LSM303_ACC_CTRL1_ODR_OFF;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 2, 10);
}

/* Empties FIFO, pushes the data to given ring buffer and returns the number of read items
   Arg: notifyLogging - should notification be sent to logging queue?
*/
uint8_t IMU_emptyFifo(bool notifyLogging, rb_t* ringBuffer)
{
    uint8_t cnt = IMU_readFifoLevel();
    IMU_readFromFifo(cnt, notifyLogging, ringBuffer);

    DEBUG_PRINTF(DBG_LVL_VERBOSE, "FIFO level: %u", cnt);

    return cnt;
}

uint8_t IMU_readFifoLevel()
{
    uint8_t imu_buf[1];

    imu_buf[0] = LSM303_ACC_FIFO_SRC;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);

    if (printMode & IMU_PRINT_DEBUG)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "FIFO status:");
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "  Threshold:  \t%i", (imu_buf[0] & LSM303_ACC_FIFO_SRC_FTH  ) > 0);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "  Overrun:    \t%i", (imu_buf[0] & LSM303_ACC_FIFO_SRC_OVR  ) > 0);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "  Empty:      \t%i", (imu_buf[0] & LSM303_ACC_FIFO_SRC_EMPTY) > 0);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "  Data level: \t%i", (imu_buf[0] & 0x1F)        );
    }

    return (imu_buf[0] & 0x1F);
}

/* Reads the given number of data from FIFO, pushes the data to given ring buffer
   Arg: notifyLogging - should notification be sent to logging queue?
*/
uint8_t IMU_readFromFifo(uint8_t count, bool notifyLogging, rb_t* ringBuffer)
{
    uint8_t  imu_buf[1];

    for (uint32_t i = 0; i < count; i++)
    {
        ImuData_t imuDataPoint;
        imuDataPoint.ID = Imu.ID;

        imu_buf[0] = LSM303_ACC_X_L | AUTO_INCREMENT_REG_ADDR;
        HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR,                         imu_buf, 1, 10);
        HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, (uint8_t*)&(imuDataPoint.acc_x), 6, 10);

        imu_buf[0] = LSM303_MAG_X_L | AUTO_INCREMENT_REG_ADDR;
        HAL_I2C_Master_Transmit(&IMU_I2C, IMU_MAG_ADDR,                         imu_buf, 1, 10);
        HAL_I2C_Master_Receive( &IMU_I2C, IMU_MAG_ADDR, (uint8_t*)&(imuDataPoint.mag_x), 6, 10);

        IMU_printDataPoint(&imuDataPoint);

        RB_insert(ringBuffer, (uint8_t*)&imuDataPoint);
    }

    if (notifyLogging)
    {
        // Notify logging task that it should read from rb_imudata
        RTOS_queueSendLog(LOGTYPE_IMUDATA);
    }

    return count;
}

/* Reads all four interrupt registers and checks whether they are active */
uint32_t IMU_readInterrupts()
{
    uint8_t imu_buf[1];

    /* INT1 */
    imu_buf[0] = LSM303_ACC_IG1_SRC;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);

    if (imu_buf[0] & LSM303_ACC_IG1_SRC_ACTIVE)
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Accelerometer INT1 active: %u", imu_buf[0]);
    }
    /* INT2 */
    imu_buf[0] = LSM303_ACC_IG2_SRC;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);

    if (imu_buf[0] & LSM303_ACC_IG2_SRC_ACTIVE)
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Accelerometer INT2 active: %u", imu_buf[0]);
    }
    /* FIFO */
    imu_buf[0] = LSM303_ACC_FIFO_SRC;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);

    if (imu_buf[0] & LSM303_ACC_FIFO_SRC_OVR)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Accelerometer FIFO overflown");
    }
    if (imu_buf[0] & LSM303_ACC_FIFO_SRC_FTH)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Accelerometer FIFO threshold / watermark reached");
    }

    /* CLICK */
    imu_buf[0] = LSM303_ACC_CLCK_SRC;
    HAL_I2C_Master_Transmit(&IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);
    HAL_I2C_Master_Receive( &IMU_I2C, IMU_XL_ADDR, imu_buf, 1, 10);

    if (imu_buf[0] & LSM303_ACC_CLCK_SRC_ACTIVE)
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "Accelerometer CLICK active: %u", imu_buf[0]);
    }

    return 0; // Could be used to return interrupt type
}

void IMU_printDataPoint(ImuData_t* imu_buf)
{
    if (printMode & (IMU_PRINT_ACC | IMU_PRINT_MAG))
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "IMU data from ID %i, location %p", (int)imu_buf->ID, imu_buf);
    }

    if (printMode & IMU_PRINT_ACC)
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "ACC (X/Y/Z): %i / %i / %i", imu_buf->acc_x, imu_buf->acc_y, imu_buf->acc_z);
    }

    if (printMode & IMU_PRINT_MAG)
    {
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "MAG (X/Y/Z): %i / %i / %i", imu_buf->mag_x, imu_buf->mag_y, imu_buf->mag_z);
    }
}

#endif /* LSM303_AGR */
