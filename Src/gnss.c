/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* Variables -----------------------------------------------------------------*/
GNSSStat_t              GNSS = { 0 };

#if USE_GNSS

/* Static Variables ----------------------------------------------------------*/
static uint8_t          gnss_rxbuf[GNSS_RX_BUFFER_SIZE];
static uint8_t          gnss_dma_rxbuf[GNSS_RX_BUFFER_SIZE];
static gnss_ubx_msg_t   gnss_msg;

/* Private functions ----------------------------------------------------------*/
static void GNSS_config(void);                    /* Configure GNSS output: disable NMEA, set GNSS only, cold-start GNSS, enable UBX-RAWX */
static void GNSS_setHighBaud(void);               /* Set GNSS module to use GNSS_UART_HIGH_BAUDRATE */
static void GNSS_waitForIdleUART(void);           /* Wait for IDLE GNSS UART line manually, in blocking mode. Timeout: 3sec */
static void GNSS_configUBX_RAWX(void);            /* Configure UBX_RAWX message */
static void GNSS_enableUBX_RAWX(bool enable);     /* Enable UBX_RAWX message */
static void GNSS_configUBX_TIM(void);             /* Configure UBX_TIM message */
static void GNSS_enableUBX_TIM(bool enable);      /* Enable UBX_TIM message */
static void GNSS_disableNMEA_GGA(void);           /* Disable NMEA GGA message */
static void GNSS_disableNMEA_GLL(void);           /* Disable NMEA GLL message */
static void GNSS_disableNMEA_GSA(void);           /* Disable NMEA GSA message */
static void GNSS_disableNMEA_GSV(void);           /* Disable NMEA GSV message */
static void GNSS_disableNMEA_RMC(void);           /* Disable NMEA RMC message */
static void GNSS_disableNMEA_VTG(void);           /* Disable NMEA VTG message */
static void GNSS_disableNMEA_ZDA(void);           /* Disable NMEA ZDA message */
static void GNSS_setGPSOnly(void);                /* Set GNSS module to use GPS satellites only */
static void GNSS_coldstartGNSS(void);             /* Cold-Start command */
static uint16_t GNSS_calcCRC(const uint8_t* data, uint16_t size);  /* Computes a new CRC and returns it as a concatenation */


/* Functions -----------------------------------------------------------------*/

uint32_t GNSS_timestampFromRAWX(const gnss_ubx_msg_t* ubx_raw_msg)
{
    double   tow         = 0.0;
    uint32_t timestamp_s = 0;
    uint32_t week_no     = ubx_raw_msg->rxm_rawx.week;

    /* RAW message does not contain valid time values yet */
    if (week_no == 0) { return 0; }

    timestamp_s = WEEKS_TO_S(week_no);        /* Seconds in a week */

    memcpy((uint8_t*)&tow, ubx_raw_msg->rxm_rawx.rcvTow, sizeof(double));

    timestamp_s += (uint32_t)tow;
    timestamp_s += GNSS_GPS_TO_UNIX_TIME_OFS;

    /* Check whether leapS is valid */
    if (ubx_raw_msg->rxm_rawx.recStat & 0x01)
    {
        timestamp_s -= ubx_raw_msg->rxm_rawx.leapS;
    }
    else
    {
        timestamp_s -= GNSS.leap_s;
    }

    return timestamp_s;
}

uint32_t GNSS_timestampFromTP(const gnss_ubx_msg_t* ubx_tp_msg)
{
    uint32_t week_no   = ubx_tp_msg->tim_tp.week;
    bool     utc_based = (ubx_tp_msg->tim_tp.flags & 0x01);
    bool     utc_info  = (ubx_tp_msg->tim_tp.flags & 0x02);

    /* TP message does not contain valid time values or the number of leap seconds has not been determined yet */
    if (week_no == 0 || (!utc_based && !GNSS.leap_s)) { return 0; }

    uint32_t tow_ms      = ubx_tp_msg->tim_tp.towMS;
    uint32_t timestamp_s = WEEKS_TO_S(week_no);
    timestamp_s += tow_ms / 1000;
    timestamp_s += GNSS_GPS_TO_UNIX_TIME_OFS;

    /* Parse flags */
    if (utc_based)
    {
        /* UTC */
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "TP with UTC information from UTC standard %u", ubx_tp_msg->tim_tp.refInfo >> 4);
    }
    else
    {
        #define NUM_GNSS_CONSTELLATIONS   5
        const char* gnss_constellations[NUM_GNSS_CONSTELLATIONS + 1] = { "GPS", "GLONASS", "BeiDou", "Galileo", "NavIC", "UNKNOWN" };

        /* Correct for leap seconds if GNSS-based */
        timestamp_s -= GNSS.leap_s;

        uint8_t gnss_time_base = ubx_tp_msg->tim_tp.refInfo & 0x0F;
        if (gnss_time_base > NUM_GNSS_CONSTELLATIONS)
        {
            gnss_time_base = NUM_GNSS_CONSTELLATIONS;
        }
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "TP with GNSS info from %s", gnss_constellations[gnss_time_base]);

        if (utc_info)
        {
            DEBUG_PRINT(DBG_LVL_VERBOSE, "TP has UTC info available");
        }
    }

    return timestamp_s;
}

uint32_t GNSS_timestampFromTM(const gnss_ubx_msg_t* ubx_tm_msg)
{
    uint32_t week_no   = ubx_tm_msg->tim_tm2.wnR;
    bool     timeValid = (ubx_tm_msg->tim_tm2.flags & (1 << 6));

    /* TM2 message does not contain valid time values yet */
    if (week_no == 0 || !timeValid) { return 0; }

    uint32_t timestamp_s = WEEKS_TO_S(week_no);
    timestamp_s += ubx_tm_msg->tim_tm2.towMsR / 1000;
    timestamp_s += GNSS_GPS_TO_UNIX_TIME_OFS;

    /* Correct for leap seconds if GNSS-based */
    if (((ubx_tm_msg->tim_tm2.flags >> 3) & 0x3) == 1)
    {
        timestamp_s -= GNSS.leap_s;
    }
    if (ubx_tm_msg->tim_tm2.flags & (1 << 5))
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Time mark has UTC information available");
    }

    return timestamp_s;
}

/* Time pulse callback */
void GNSS_timePulseCallback(void)
{
    GNSS.tpulse_cnt++;
}

/* Get the number of leap seconds from a UBX-RXM-RAWX message */
void GNSS_updateLeapSecs(const gnss_ubx_msg_t* ubx_raw_msg)
{
    /* Check if there are some measurements */
    if ( ubx_raw_msg && (ubx_raw_msg->rxm_rawx.recStat & 0x01) )
    {
        /* Leap seconds have been determined */
        if (ubx_raw_msg->rxm_rawx.leapS != GNSS.leap_s)
        {
            GNSS.leap_s = ubx_raw_msg->rxm_rawx.leapS;
            RTOS_queueSendLog(LOGTYPE_CONF_WR);
            DEBUG_PRINTF(DBG_LVL_VERBOSE, "Leap seconds changed to %u", GNSS.leap_s);
        }
    }
}

static uint16_t GNSS_calcCRC(const uint8_t* data, uint16_t size)
{
    uint8_t sum1 = 0;
    uint8_t sum2 = 0;
    for (uint32_t i = 0; i < size; i++)
    {
        sum1 += data[i];
        sum2 += sum1;
    }

    return ( ((uint16_t)sum1 << 8) | sum2);
}

bool GNSS_checkCRC(const gnss_ubx_msg_t* msg, uint16_t size)
{
    if ( (size < GNSS_MSG_LEN_MIN) || (size > GNSS_MSG_LEN_MAX) )
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "Received invalid GNSS message for CRC check with size %u", size);
        return false;
    }

    uint16_t crc_calc = GNSS_calcCRC((uint8_t*)&msg->class, size - GNSS_MSG_LEN_CRC - GNSS_MSG_LEN_SYNC);
    uint16_t crc_msg  = ((uint16_t)msg->payload[size - GNSS_MSG_LEN_HDR - 2] << 8) | msg->payload[size - GNSS_MSG_LEN_HDR - 1];

    return (crc_calc == crc_msg);
}

void GNSS_enable(void)
{
    if (GNSS.state >= GNSS_ENABLED) { return; }

    if (GNSS.state == GNSS_OFF)
    {
        GNSS_powerOn();
    }
    GNSS.rx_len = 0;    /* Reset RX buffer */

    if (GNSS.state == GNSS_ON)
    {
        /* Powered-on but not yet configured */

        /* Set up DMA transfer */
        UART1_initDMA(GNSS.dma_buf, GNSS_RX_BUFFER_SIZE);

        /* Set configuration */
        DEBUG_PRINT(DBG_LVL_VERBOSE, "GNSS config");
        GNSS_config();
    }

    /* Enable the necessary UBX packets */
    GNSS_enableUBX_RAWX(true);
#if GNSS_TP_EN
    GNSS_enableUBX_TIM(true);
#endif

    /* Enable time pulse interrupt */
    __HAL_GPIO_EXTI_CLEAR_IT(GNSS_TPULSE_Pin);
    GPIO_EXTI_IT_ENABLE(GNSS_TPULSE_Pin);

    GNSS.state = GNSS_ENABLED;

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Enabled GNSS");
}

void GNSS_init(void)
{
    /* Power off */
    GNSS_SWOFF();     /* This also sets Reset and EXTINT low */

    /* Reset state (note: do not use memset here, some parameters may not be reset to zero) */
    GNSS.state        = GNSS_OFF;
    GNSS.rx_buf       = gnss_rxbuf;
    GNSS.dma_buf      = gnss_dma_rxbuf;
    GNSS.dma_cntr     = GNSS_RX_BUFFER_SIZE;
    GNSS.last_sync_ts = 0;
    GNSS.tpulse_cnt   = 0;
    memset(GNSS.rx_buf, 0x0, GNSS_RX_BUFFER_SIZE);
}

void GNSS_disable(void)
{
    if (GNSS.state <= GNSS_DISABLED) { return; }

    /* Disable all UBX packets */
#if GNSS_TP_EN
    GNSS_enableUBX_TIM(false);
#endif
    GNSS_enableUBX_RAWX(false);

    GPIO_EXTI_IT_DISABLE(GNSS_TPULSE_Pin);
    GNSS.state  = GNSS_DISABLED;

    DEBUG_PRINT(DBG_LVL_INFO, "Disabled GNSS");
}

/* Power on the GNSS receiver and configure the UART interface.
 * Call GNSS_enable() instead if the device should be configured as well. */
void GNSS_powerOn(void)
{
    if (GNSS.state != GNSS_OFF) { return; }

    GNSS_init();
    GNSS_SWON();
    GNSS_RST_HIGH();
    RTOS_TASK_DELAY(GNSS_PWRON_DELAY_MS);

    /* Make sure that GNSS is correctly configured for high-speed UART */
    GNSS_waitForIdleUART();
    UART1_init(GNSS_UART_DEFAULT_BAUDRATE);
    GNSS_setHighBaud();
    GNSS_waitForIdleUART();
    UART1_init(GNSS_UART_HIGH_BAUDRATE);

    GNSS.state = GNSS_ON;

    DEBUG_PRINT(DBG_LVL_INFO, "Powered on GNSS");
}

void GNSS_powerOff(void)
{
    if (GNSS.state == GNSS_OFF) { return; }

    GNSS_RST_LOW();
    GNSS_SWOFF();
    UART1_deInitDMA();
    HAL_UART_DeInit(&GNSS_UART);
    GNSS.state = GNSS_OFF;

    DEBUG_PRINT(DBG_LVL_INFO, "Powered off GNSS");
}

void GNSS_reinitUART(bool enable_dma)
{
    HAL_UART_DeInit(&GNSS_UART);
    GNSS_waitForIdleUART();
    UART1_init(GNSS_UART_HIGH_BAUDRATE);

    if (enable_dma)
    {
        GNSS.dma_cntr = GNSS_RX_BUFFER_SIZE;
        UART1_initDMA(GNSS.dma_buf, GNSS_RX_BUFFER_SIZE);
    }
}

static void GNSS_waitForIdleUART(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    uint32_t cntr              = 0;
    const uint32_t cntr_target = 5;       /* Number of times in a row the RX line has to be high */
    const uint32_t min_step_ms = 10;
    uint16_t timeout           = GNSS_UART_IDLE_TIMEOUT_MS / min_step_ms;

    GPIO_InitStruct.Pin   = GNSS_RX_Pin;
    GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull  = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    HAL_GPIO_Init(GNSS_RX_Port, &GPIO_InitStruct);

    while (timeout && (cntr != cntr_target))
    {
        if (HAL_GPIO_ReadPin(GNSS_RX_Port, GNSS_RX_Pin) == GPIO_PIN_SET)
        {
            cntr++;
        }
        else
        {
            cntr = 0;
        }
        RTOS_TASK_DELAY(min_step_ms);
        timeout--;
    }

    if (!timeout)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "Timeout while trying to detect idle UART");
    }

    HAL_GPIO_DeInit(GNSS_RX_Port, GNSS_RX_Pin);
}

/* GNSS module commands -------------------------------------------------------*/

static void GNSS_config(void)
{
    GNSS_disableNMEA_GGA();
    GNSS_disableNMEA_GLL();
    GNSS_disableNMEA_GSA();
    GNSS_disableNMEA_GSV();
    GNSS_disableNMEA_RMC();
    GNSS_disableNMEA_VTG();
    GNSS_disableNMEA_ZDA();
    GNSS_setGPSOnly();
    GNSS_coldstartGNSS();
    GNSS_configUBX_RAWX();
#if GNSS_TP_EN
    GNSS_configUBX_TIM();
#endif
}

static void GNSS_setHighBaud(void)
{
#ifdef GNSS_MODEL_M8

    // CFG-PRT
    memset(&gnss_msg, 0, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + GNSS_MSG_LEN_CFG_PRT);
    gnss_msg.class                = GNSS_MSG_CLASS_CFG;
    gnss_msg.id                   = GNSS_MSG_ID_CFG_PRT;
    gnss_msg.cfg_prt.portId       = 1;                           // UART
    gnss_msg.cfg_prt.txReady      = 0;                           // txReady disabled
    gnss_msg.cfg_prt.mode         = (0b100 << 9) | (0b11 << 6);  // 1 Stop bit, no parity, 8bit
    gnss_msg.cfg_prt.baudRate     = GNSS_UART_HIGH_BAUDRATE;
    gnss_msg.cfg_prt.inProtoMask  = 0x1;                         // enable UBX protocol
    gnss_msg.cfg_prt.outProtoMask = 0x1;
    gnss_msg.cfg_prt.flags        = 0;
    GNSS_sendUBXmsg(&gnss_msg, GNSS_MSG_LEN_CFG_PRT);

    // CFG-INF
    memset(&gnss_msg, 0, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + GNSS_MSG_LEN_CFG_INF(2));
    gnss_msg.class                    = GNSS_MSG_CLASS_CFG;
    gnss_msg.id                       = GNSS_MSG_ID_CFG_INF;
    gnss_msg.cfg_inf[0].protocolId    = 0;                       // UBX
    gnss_msg.cfg_inf[0].infMsgMask[1] = 0b11111;                 // enable all message types for port UART (port identifier 1), disable message output on all other ports (I2C, USB, SPI)
    gnss_msg.cfg_inf[1].protocolId    = 1;                       // NMEA - disable all ports
    GNSS_sendUBXmsg(&gnss_msg, GNSS_MSG_LEN_CFG_INF(2));

#endif
#ifdef GNSS_MODEL_F9P
    // CFG-PRT
    // UART1; 8bit No parity 1 Stop Bit; GNSS_UART_HIGH_BAUDRATE Baud; In UBX NMEA RTCM2; Out UBX NMEA
    uint8_t buf[4] = { 0 };

    buf[0] = 0x01; // Enable UART1
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1_ENABLED, buf, GNSS_UBX_CFG_UART1_ENABLED_LEN);

    buf[0] = 0x00; // 8bit
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1_DATABITS, buf, GNSS_UBX_CFG_UART1_DATABITS_LEN);

    buf[0] = 0x00; // No parity
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1_PARITY, buf, GNSS_UBX_CFG_UART1_PARITY_LEN);

    buf[0] = 0x01; // 1 Stop bit
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1_STOPBITS, buf, GNSS_UBX_CFG_UART1_STOPBITS_LEN);

    buf[0] = 0x01; // In UBX
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1INPROT_UBX, buf, GNSS_UBX_CFG_UART1INPROT_UBX_LEN);

    buf[0] = 0x00; // In NMEA - disabled
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1INPROT_NMEA, buf, GNSS_UBX_CFG_UART1INPROT_NMEA_LEN);

    buf[0] = 0x00; // In RTCM3X - disabled
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1INPROT_RTCM3X, buf, GNSS_UBX_CFG_UART1INPROT_RTCM3X_LEN);

    buf[0] = 0x01; // Out UBX
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1OUTPROT_UBX, buf, GNSS_UBX_CFG_UART1OUTPROT_UBX_LEN);

    buf[0] = 0x00; // Out NMEA - disabled
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1OUTPROT_NMEA, buf, GNSS_UBX_CFG_UART1OUTPROT_NMEA_LEN);

    buf[0] = 0x00; // Out RTCM3X - disabled
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1OUTPROT_RTCM3X, buf, GNSS_UBX_CFG_UART1OUTPROT_RTCM3X_LEN);

    buf[0] = GNSS_UBX_CFG_INFMSG_ERROR | GNSS_UBX_CFG_INFMSG_WARNING; // INFMSG-UBX: enable ERROR and WARNING
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_INFMSG_UBX_UART1, buf, GNSS_UBX_CFG_INFMSG_UBX_UART1_LEN);

    buf[0] = GNSS_UBX_CFG_INFMSG_ERROR | GNSS_UBX_CFG_INFMSG_WARNING; // INFMSG-NMEA: enable ERROR and WARNING
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_INFMSG_NMEA_UART1, buf, GNSS_UBX_CFG_INFMSG_NMEA_UART1_LEN);

    *(uint32_t*)buf = GNSS_UART_HIGH_BAUDRATE;
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_UART1_BAUDRATE, buf, GNSS_UBX_CFG_UART1_BAUDRATE_LEN);
#endif
}

static void GNSS_configUBX_RAWX(void)
{
#ifdef GNSS_MODEL_M8

    memset(&gnss_msg, 0, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + GNSS_MSG_LEN_CFG_RATE);
    gnss_msg.class             = GNSS_MSG_CLASS_CFG;
    gnss_msg.id                = GNSS_MSG_ID_CFG_RATE;
    gnss_msg.cfg_rate.measRate = GNSS_MEASUREMENT_PERIOD_MS;
    gnss_msg.cfg_rate.navRate  = 1;
    gnss_msg.cfg_rate.timeRef  = (GNSS_TIMEREF_UTC ? 0x0 : 0x1);
    GNSS_sendUBXmsg(&gnss_msg, GNSS_MSG_LEN_CFG_RATE);

#endif
#ifdef GNSS_MODEL_F9P
    // CFG-MSG
    uint8_t buf[2] = { 0 };

    buf[0] = GNSS_MEASUREMENT_PERIOD_MS & 0x00FF; // Measurement rate: 1000ms = 1 Hz = 0x03E8, 30'000ms = 1/30 Hz = 0x7530
    buf[1] = GNSS_MEASUREMENT_PERIOD_MS >> 8;
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_RATE_MEAS, buf, GNSS_UBX_CFG_RATE_MEAS_LEN);

    buf[0] = 0x01; // Navigation solution rate: 1 to 1
    buf[1] = 0x00;
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_RATE_NAV, buf, GNSS_UBX_CFG_RATE_NAV_LEN);

    buf[0] = (GNSS_TIMEREF_UTC ? 0x0 : 0x1); // Time reference: GPS (UTC conversion information only transmitted every 12.5min on GPS, which prevents conversion after cold start)
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_RATE_TIMEREF, buf, GNSS_UBX_CFG_RATE_TIMEREF_LEN);
#endif
}

static void GNSS_enableUBX_RAWX(bool enable)
{
#ifdef GNSS_MODEL_M8

    memset(&gnss_msg, 0, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + GNSS_MSG_LEN_CFG_MSG);
    gnss_msg.class            = GNSS_MSG_CLASS_CFG;
    gnss_msg.id               = GNSS_MSG_ID_CFG_MSG;
    gnss_msg.cfg_msg.msgClass = GNSS_MSG_CLASS_RXM;
    gnss_msg.cfg_msg.msgId    = GNSS_MSG_ID_RXM_RAWX;
    gnss_msg.cfg_msg.rate[1]  = (enable ? GNSS_MSG_OUTPUT_HZ : 0);    // UART (port identifier 1): 1Hz
    GNSS_sendUBXmsg(&gnss_msg, GNSS_MSG_LEN_CFG_MSG);

#endif
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = enable; // UBX-RMX-RAWX - enable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_UBX_RXM_RAWX_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_UBX_RXM_RAWX_UART1_LEN);
#endif
}


static void GNSS_configUBX_TIM(void)
{
#ifdef GNSS_MODEL_M8

    memset(&gnss_msg, 0, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + GNSS_MSG_LEN_CFG_TP5);
    gnss_msg.class                     = GNSS_MSG_CLASS_CFG;
    gnss_msg.id                        = GNSS_MSG_ID_CFG_TP5;
    gnss_msg.cfg_tp5.tpIdx             = 0;         // TIMEPULSE pin
    gnss_msg.cfg_tp5.version           = 0x01;
    gnss_msg.cfg_tp5.freqPeriod        = 0;         // No time pulse if not locked
    gnss_msg.cfg_tp5.freqPeriodLock    = 1000000;   // Lock period of 1s;
    gnss_msg.cfg_tp5.pulseLenRatio     = 100000;    // Pulse length 100ms;
    gnss_msg.cfg_tp5.pulseLenRatioLock = 100000;    // Pulse length 100ms;
    gnss_msg.cfg_tp5.userConfigDelay   = 0;         // No delay;
    gnss_msg.cfg_tp5.flags             = ((GNSS_TIMEREF_UTC ? 0x0 : 0x1) << 11) | (0x1 << 7) | 0b1110111; // syncMode switch, gridUtcGnss to GPS, rising polarity, align, is length, is not frequency, use lock setting, do lock, is active
    GNSS_sendUBXmsg(&gnss_msg, GNSS_MSG_LEN_CFG_TP5);

#endif
#ifdef GNSS_MODEL_F9P
    uint8_t buf[4] = { 0 };

    buf[0] = 0x00; // Pulse definition: interpretation as PERIOD
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_PULSE_DEF, buf, GNSS_UBX_CFG_TP_PULSE_DEF_LEN);

    buf[0] = 0x01; // Pulse length definition: interpretation as LENGTH (us)
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_PULSE_LENGTH_DEF, buf, GNSS_UBX_CFG_TP_PULSE_LENGTH_DEF_LEN);

    *(uint32_t*)buf = 0;        // No time pulse if not locked
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_PERIOD_TP1, buf, GNSS_UBX_CFG_TP_PERIOD_TP1_LEN);

    *(uint32_t*)buf = 1000000;  // Lock period of 1s
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_PERIOD_LOCK_TP1, buf, GNSS_UBX_CFG_TP_PERIOD_LOCK_TP1_LEN);

    *(uint32_t*)buf = 100000;   // Pulse length 100ms
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_LEN_TP1, buf, GNSS_UBX_CFG_TP_LEN_TP1_LEN);

    buf[0] = 0x01; // Sync to GNSS - enable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_SYNC_GNSS_TP1, buf, GNSS_UBX_CFG_TP_SYNC_GNSS_TP1_LEN);

    buf[0] = 0x01; // Use locked parameters when GNSS time is valid - enable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_USE_LOCKED_TP1, buf, GNSS_UBX_CFG_TP_USE_LOCKED_TP1_LEN);

    buf[0] = 0x01; // Edge at top of second: Rising
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_POL_TP1, buf, GNSS_UBX_CFG_TP_POL_TP1_LEN);

    buf[0] = (GNSS_TIMEREF_UTC ? 0x0 : 0x1); // Time grid to use (default: GPS)
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_TIMEGRID_TP1, buf, GNSS_UBX_CFG_TP_TIMEGRID_TP1_LEN);
#endif
}

static void GNSS_enableUBX_TIM(bool enable)
{
#ifdef GNSS_MODEL_M8

    memset(&gnss_msg, 0, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + GNSS_MSG_LEN_CFG_MSG);
    gnss_msg.class            = GNSS_MSG_CLASS_CFG;
    gnss_msg.id               = GNSS_MSG_ID_CFG_MSG;
    gnss_msg.cfg_msg.msgClass = GNSS_MSG_CLASS_TIM;
    gnss_msg.cfg_msg.msgId    = GNSS_MSG_ID_TIM_TP;
    gnss_msg.cfg_msg.rate[1]  = (enable ? GNSS_MSG_OUTPUT_HZ : 0);    // UART (port identifier 1): 1Hz
    GNSS_sendUBXmsg(&gnss_msg, GNSS_MSG_LEN_CFG_MSG);

#endif
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = (enable ? GNSS_MSG_OUTPUT_HZ : 0); // 1 Hz output rate on UART1
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_UBX_TIM_TP_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_UBX_TIM_TP_UART1_LEN);

    cmd_val = (enable ? GNSS_MSG_OUTPUT_HZ : 0);         // UBX-TIM-TP - enable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_TP_TP1_ENA, &cmd_val, GNSS_UBX_CFG_TP_TP1_ENA_LEN);
#endif
}

static void GNSS_disableNMEA_GGA(void)
{
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = 0x0; // NMEA-GGA - disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_NMEA_ID_GGA_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_NMEA_ID_GGA_UART1_LEN);
#endif
}

static void GNSS_disableNMEA_GLL(void)
{
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = 0x0; // NMEA-GLL - disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_NMEA_ID_GLL_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_NMEA_ID_GLL_UART1_LEN);
#endif
}

static void GNSS_disableNMEA_GSA(void)
{
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = 0x0; // NMEA-GSA - disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSA_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSA_UART1_LEN);
#endif
}

static void GNSS_disableNMEA_GSV(void)
{
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = 0x0; // NMEA-GSV - disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSV_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSV_UART1_LEN);
#endif
}

static void GNSS_disableNMEA_RMC(void)
{
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = 0x0; // NMEA-RMC - disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_NMEA_ID_RMC_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_NMEA_ID_RMC_UART1_LEN);
#endif
}

static void GNSS_disableNMEA_VTG(void)
{
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = 0x0; // NMEA-VTG - disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_NMEA_ID_VTG_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_NMEA_ID_VTG_UART1_LEN);
#endif
}

static void GNSS_disableNMEA_ZDA(void)
{
#ifdef GNSS_MODEL_F9P
    uint8_t cmd_val = 0x0; // NMEA-ZDA - disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_MSGOUT_NMEA_ID_ZDA_UART1, &cmd_val, GNSS_UBX_CFG_MSGOUT_NMEA_ID_ZDA_UART1_LEN);
#endif
}

static void GNSS_setGPSOnly(void)
{
#ifdef GNSS_MODEL_M8

    /*
     * Default config for NEO-M8T:
     * numTrkChHw = numTrkChUse = 32
     * numConfigBlocks = 7
     * gnssId/resTrkCh/maxTrkCh/flags:
     *   0 8 16 0x01010001
     *   1 1  3 0x01010000
     *   2 4  8 0x01010000
     *   3 8 16 0x01010000
     *   4 0  8 0x03010000
     *   5 0  3 0x05010001
     *   6 8 14 0x01010001
     */
    memset(&gnss_msg, 0, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + GNSS_MSG_LEN_CFG_GNSS(7));
    gnss_msg.class                     = GNSS_MSG_CLASS_CFG;
    gnss_msg.id                        = GNSS_MSG_ID_CFG_GNSS;
    gnss_msg.cfg_gnss.msgVer           = 0;
    gnss_msg.cfg_gnss.numTrkChUse      = 0xFF;  // use all tracking channels (default value for NEO-M8T is 32)
    gnss_msg.cfg_gnss.numConfigBlocks  = 7;
    gnss_msg.cfg_gnss.gnss[0].gnssId   = 0;     // GPS: enabled
    gnss_msg.cfg_gnss.gnss[0].resTrkCh = 8;
    gnss_msg.cfg_gnss.gnss[0].maxTrkCh = 16;
    gnss_msg.cfg_gnss.gnss[0].flags    = 0x00010001;  // NEO-M8T only supports L1
    gnss_msg.cfg_gnss.gnss[1].gnssId   = 1;     // SBAS: disabled
    gnss_msg.cfg_gnss.gnss[1].maxTrkCh = 4;     // min. value for all GNSS is 4
    gnss_msg.cfg_gnss.gnss[2].gnssId   = 2;     // Galileo: disabled
    gnss_msg.cfg_gnss.gnss[2].maxTrkCh = 4;
    gnss_msg.cfg_gnss.gnss[3].gnssId   = 3;     // BeiDou: disabled
    gnss_msg.cfg_gnss.gnss[3].maxTrkCh = 4;
    gnss_msg.cfg_gnss.gnss[4].gnssId   = 4;     // IMES: disabled
    gnss_msg.cfg_gnss.gnss[4].maxTrkCh = 4;
    gnss_msg.cfg_gnss.gnss[5].gnssId   = 5;     // QZSS: enabled (to avoid cross-correlation issues)
    gnss_msg.cfg_gnss.gnss[5].resTrkCh = 1;
    gnss_msg.cfg_gnss.gnss[5].maxTrkCh = 4;
    gnss_msg.cfg_gnss.gnss[5].flags    = 0x00010001;
    gnss_msg.cfg_gnss.gnss[6].gnssId   = 6;     // GLONASS: disabled
    gnss_msg.cfg_gnss.gnss[6].maxTrkCh = 4;
    GNSS_sendUBXmsg(&gnss_msg, GNSS_MSG_LEN_CFG_GNSS(7));

    // Wait for 0.5s for the configuration to take effect
    HAL_Delay(500);

#endif
#ifdef GNSS_MODEL_F9P
    // CFG-GNSS
    // Enabling GPS and QZSS, as they should both be on to avoid cross-correlation issues
    uint8_t buf[1] = { 0 };

    buf[0] = 0x01; // GPS: enable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_GPS_ENA,      buf, GNSS_UBX_CFG_SIGNAL_GPS_ENA_LEN);
    buf[0] = 0x01; // GPS: L1CA enable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_GPS_L1CA_ENA, buf, GNSS_UBX_CFG_SIGNAL_GPS_L1CA_ENA_LEN);
    buf[0] = 0x01; // GPS: L2C enable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_GPS_L2C_ENA,  buf, GNSS_UBX_CFG_SIGNAL_GPS_L2C_ENA_LEN);

    // NOTE: do not disable L1/L2 bands with the exception of BeiDou (generates warning messages)

    buf[0] = 0x00; // Galileo: disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_GAL_ENA,     buf, GNSS_UBX_CFG_SIGNAL_GAL_ENA_LEN);

    buf[0] = 0x00; // BeiDou: disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_BDS_ENA,    buf, GNSS_UBX_CFG_SIGNAL_BDS_ENA_LEN);
    buf[0] = 0x00; // BeiDou: B1 disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_BDS_B1_ENA, buf, GNSS_UBX_CFG_SIGNAL_BDS_B1_ENA_LEN);
    buf[0] = 0x00; // BeiDou: B2 disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_BDS_B2_ENA, buf, GNSS_UBX_CFG_SIGNAL_BDS_B2_ENA_LEN);

    buf[0] = 0x00; // QZSS: disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_QZSS_ENA,      buf, GNSS_UBX_CFG_SIGNAL_QZSS_ENA_LEN);

    buf[0] = 0x00; // GLONASS: disable
    GNSS_setUBXConfigValue(GNSS_UBX_CFG_SIGNAL_GLO_ENA,    buf, GNSS_UBX_CFG_SIGNAL_GLO_ENA_LEN);
#endif
}

static void GNSS_coldstartGNSS(void)
{
    // CFG-RST, length 4 - not acknowledged
    // Controlled Software reset GNSS
    uint8_t           cmd[] = { GNSS_MSG_SYNC_CHAR_1, GNSS_MSG_SYNC_CHAR_2, GNSS_MSG_CLASS_CFG, GNSS_MSG_ID_CFG_RST, 0x04, 0x00, 0xFF, 0xFF, 0x02, 0x00, 0x0E, 0x61 };
    HAL_StatusTypeDef res   = HAL_UART_Transmit(&GNSS_UART, cmd, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + 4, GNSS_UART_TX_TIMEOUT);
    if (res != HAL_OK)
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "GNSS UART Start Tx error: %u", res);
    }

    HAL_Delay(GNSS_UART_TX_DELAY);
}

bool GNSS_requestInfo(gnss_ubx_msg_t* out_info, uint32_t len)
{
    // MON-VER
    // Request Receiver/Software version
    uint8_t cmd[] = { GNSS_MSG_SYNC_CHAR_1, GNSS_MSG_SYNC_CHAR_2, GNSS_MSG_CLASS_MON, GNSS_MSG_ID_MON_VER, 0x00, 0x00, 0x0E, 0x34 };

    if (HAL_UART_Transmit(&GNSS_UART, cmd, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC, GNSS_UART_TX_TIMEOUT) != HAL_OK)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "GNSS UART Info TX error");
        return false;
    }

    memset(&gnss_msg, 0, sizeof(gnss_msg));
    HAL_UART_Receive(&GNSS_UART, (uint8_t*)&gnss_msg, GNSS_MSG_LEN_MAX, GNSS_UART_RX_TIMEOUT);
    if (!GNSS_checkCRC(&gnss_msg, GNSS_MSG_LEN_HDR + gnss_msg.len + GNSS_MSG_LEN_CRC))
    {
        HAL_Delay(GNSS_UART_RX_TIMEOUT);    // Wait some time to make sure the receiver has sent all data
        HAL_UART_AbortReceive(&GNSS_UART);
        DEBUG_PRINT(DBG_LVL_WARNING, "GNSS UART RX failed (invalid CRC)");
        return false;
    }
    if (out_info && len)
    {
        memcpy(out_info, &gnss_msg, MIN(len, GNSS_MSG_LEN_MAX));
    }

    return true;
}

const char* GNSS_requestModVer(void)
{
    static char gnss_mod_ver_str[16] = { 0 };

    if (gnss_mod_ver_str[0] == 0)   // if first call to this function
    {
        if (GNSS_requestInfo(0, 0))
        {
            uint32_t       num_ext_blocks = (gnss_msg.len > GNSS_MSG_LEN_MON_VER_MIN) ? ((gnss_msg.len - GNSS_MSG_LEN_MON_VER_MIN) / GNSS_MSG_LEN_MON_VER_EXT) : 0;
            const uint32_t max_ext_blocks = (GNSS_MSG_LEN_MAX - GNSS_MSG_LEN_HDR - GNSS_MSG_LEN_CRC - GNSS_MSG_LEN_MON_VER_MIN) / GNSS_MSG_LEN_MON_VER_EXT;
            for (uint32_t i = 0; (i < num_ext_blocks) && (i < max_ext_blocks); i++)
            {
                gnss_msg.mon_ver.extInfo[i].str[GNSS_MSG_LEN_MON_VER_EXT - 1] = 0;    // make sure the string is zero-terminated
                //DEBUG_PRINTF(DBG_LVL_VERBOSE, "extraInfo: %s", gnss_msg.mon_ver.extInfo[i].str);
                if (strstr(gnss_msg.mon_ver.extInfo[i].str, "MOD="))
                {
                    strncpy(gnss_mod_ver_str, &gnss_msg.mon_ver.extInfo[i].str[4], sizeof(gnss_mod_ver_str));
                    gnss_mod_ver_str[15] = 0;   // Make sure the string is zero-terminated
                }
            }
        }
    }

    return gnss_mod_ver_str;
}

bool GNSS_requestConfig(uint8_t msg_id, gnss_ubx_msg_t* out_cfg, uint32_t len)
{
    if (!out_cfg || !len) { return false; }

    memset(&gnss_msg, 0, sizeof(gnss_msg));
    gnss_msg.class = GNSS_MSG_CLASS_CFG;
    gnss_msg.id    = msg_id;
    GNSS_sendUBXmsg(&gnss_msg, 0);

    memset(&gnss_msg, 0, sizeof(gnss_msg));
    HAL_UART_Receive(&GNSS_UART, (uint8_t*)&gnss_msg, GNSS_MSG_LEN_MAX, GNSS_UART_RX_TIMEOUT);
    if (!GNSS_checkCRC(&gnss_msg, GNSS_MSG_LEN_HDR + gnss_msg.len + GNSS_MSG_LEN_CRC))
    {
        HAL_Delay(GNSS_UART_RX_TIMEOUT);    // Wait some time to make sure the receiver has sent all data
        HAL_UART_AbortReceive(&GNSS_UART);
        DEBUG_PRINT(DBG_LVL_WARNING, "GNSS UART RX failed (invalid CRC)");
    }
    memcpy(out_cfg, &gnss_msg, MIN(len, GNSS_MSG_LEN_MAX));

    return true;
}

// Send UBX messages to GNSS
bool GNSS_sendUBXmsg(gnss_ubx_msg_t* msg, uint16_t payload_len)
{
    if ( !msg || ((payload_len + GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC) > GNSS_MSG_LEN_MAX) )
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "Invalid UBX message length %u", payload_len);
        return false;
    }

    // Fill in the missing fields
    msg->header[0] = GNSS_MSG_SYNC_CHAR_1;
    msg->header[1] = GNSS_MSG_SYNC_CHAR_2;
    msg->len       = payload_len;

    // Append UBX checksum (calculated over whole message, without sync chars)
    uint16_t checksum             = GNSS_calcCRC((uint8_t*)&msg->class, GNSS_MSG_LEN_HDR - GNSS_MSG_LEN_SYNC + payload_len);
    msg->payload[payload_len]     = checksum >> 8;
    msg->payload[payload_len + 1] = checksum  & 0x00FF;

    // Transmit the message
    if (HAL_UART_Transmit(&GNSS_UART, (uint8_t*)msg, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + payload_len, GNSS_UART_TX_TIMEOUT) != HAL_OK)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "GNSS UART UBX message Tx error");
        return false;
    }

    return true;
}

// Write a 4byte key and up to 8byte value pair to the module
// Memory: 0x01 RAM, 0x02 BBR, 0x04 Flash; set to 0x07 to write to all
void GNSS_setUBXConfigValue(uint32_t key, uint8_t* value, uint8_t value_length)
{
    if (value_length > 8)
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "Value length %u > 8 not implemented", value_length);
        return;
    }

    uint8_t cmd[] = { GNSS_MSG_SYNC_CHAR_1, GNSS_MSG_SYNC_CHAR_2, GNSS_MSG_CLASS_CFG, GNSS_MSG_ID_CFG_VALSET, 0x08, 0x00, 0x00, 0x07,
                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                      0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

    // Fill in key-value pair
    cmd[GNSS_UBX_CFG_IDX_LENGTH] += value_length; // Must be smaller than (255 - 8), else must fill out second length field
    cmd[GNSS_UBX_CFG_IDX_KEY]     = (key & 0x000000FF);
    cmd[GNSS_UBX_CFG_IDX_KEY + 1] = (key & 0x0000FF00) >> 8;
    cmd[GNSS_UBX_CFG_IDX_KEY + 2] = (key & 0x00FF0000) >> 16;
    cmd[GNSS_UBX_CFG_IDX_KEY + 3] = (key & 0xFF000000) >> 24;

    for (uint32_t i = 0; i < value_length; i++)
    {
        cmd[GNSS_UBX_CFG_IDX_VALUE + i] = value[i];

        // Clear buffer
        value[i] = 0x00;
    }

    uint16_t currentCRC = GNSS_calcCRC(cmd + GNSS_MSG_LEN_SYNC, GNSS_MSG_LEN_HDR - GNSS_MSG_LEN_SYNC + 8 + value_length);
    cmd[GNSS_UBX_CFG_IDX_VALUE + value_length + 0] = (currentCRC >> 8);
    cmd[GNSS_UBX_CFG_IDX_VALUE + value_length + 1] = (currentCRC  & 0x00FF);

    HAL_StatusTypeDef response = HAL_UART_Transmit(&GNSS_UART, cmd, GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC + 8 + value_length, GNSS_UART_TX_TIMEOUT);
    if (response != HAL_OK)
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "GNSS UART ConfigValue Tx error: %u", response);
    }
    else
    {
        //DEBUG_PRINTF(DBG_LVL_VERBOSE, "UBX-CFG sent (Key: 0x%08x)", key);
    }

    RTOS_TASK_DELAY(GNSS_UART_TX_DELAY);
}

#endif /* USE_GNSS */
