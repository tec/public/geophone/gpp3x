/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

#if USE_INCLINO

/* Variables -----------------------------------------------------------------*/
extern Geophone_t   Geo;

static uint8_t      transmit_buf[4];
static uint8_t      receive_buf[4];

/* Static functions ----------------------------------------------------------*/

static void SCL_prepareCommand(uint8_t* tx_buf, uint32_t cmd)
{
    tx_buf[0] = (cmd & 0xFF000000) >> 3*8;
    tx_buf[1] = (cmd & 0x00FF0000) >> 2*8;
    tx_buf[2] = (cmd & 0x0000FF00) >> 1*8;
    tx_buf[3] = (cmd & 0x000000FF);
}

static uint32_t SCL_assembleResponse(uint8_t* rx_buf)
{
    uint32_t packet = 0;
    packet |= (rx_buf[0] << 3*8);
    packet |= (rx_buf[1] << 2*8);
    packet |= (rx_buf[2] << 1*8);
    packet |=  rx_buf[3];

    return packet;
}

/* Helper function for SCL_calculateCRC */
static uint8_t CRC8(uint8_t BitValue, uint8_t CRCValue)
{
    uint8_t Temp;
    Temp = (uint8_t)(CRCValue & 0x80);
    if (BitValue == 0x01)
    {
        Temp ^= 0x80;
    }
    CRCValue <<= 1;

    if (Temp)
    {
        CRCValue ^= 0x1D;
    }

    return CRCValue;
}

/* Helper function for SCL_convertAcceleration */
static uint16_t twoComplementToAbsolute(uint16_t value)
{
    /* For negative number (first bit set), return two's complement; positive values remain */
    if (value & 0x8000)
    {
        return ((~value) + 0x01);
    }
    else
    {
        return value;
    }
}

/* Functions -----------------------------------------------------------------*/

void SCL_init(void)
{
    /* Software Reset - always recommended by datasheet */
    SCL_sendCommand(SCL3300_RESET);
    HAL_Delay(1);

    /* Set Measurement mode */
    SCL_sendCommand(INCLINO_MEAS_MODE);

    /* Enable Angle Outputs */
    SCL_sendCommand(SCL3300_EN_ANGLE);
    HAL_Delay(1);

    /* Wait depending on measurement mode */
    if (INCLINO_MEAS_MODE == SCL3300_EN_MODE1)
    {
        HAL_Delay(25);
    }
    else if (INCLINO_MEAS_MODE == SCL3300_EN_MODE2)
    {
        HAL_Delay(15);
    }
    else if ( (INCLINO_MEAS_MODE == SCL3300_EN_MODE3) || (INCLINO_MEAS_MODE == SCL3300_EN_MODE4) )
    {
        HAL_Delay(100);
    }
    else
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "Invalid SCL3300 Mode");
        return;
    }

    /* Read STATUS */
    SCL_sendCommand(SCL3300_READ_STATUS);
    HAL_Delay(1);
    SCL_sendCommand(SCL3300_READ_STATUS);
    HAL_Delay(1);

    /* Verify correct startup */
    SCL_sendCommand(SCL3300_READ_STATUS);

    /* Make sure that data is correct - RS and CRC are checked */
    SCL_verifyStatus(1);
}

void SCL_up(void)
{
    /* Wake up the Inclinometer */
    SCL_sendCommand(SCL3300_WK_UP);

    /* Wait 1ms */
    HAL_Delay(1);
}

/* Important note: Never put the inclinometer into down mode twice in a row, breaks some functionality */
void SCL_down(void)
{
    /* Set Inclino to Power Down Mode */
    SCL_sendCommand(SCL3300_PW_DOWN);
}

void SCL_sendCommandToBuf(uint8_t* buf, uint32_t cmd)
{
    SCL_prepareCommand(transmit_buf, cmd);

    /* Implementation specific: SPI1 is shared between the Inclinometer and the ADCs, so we need to wait until the acquisition is done */
    if (IS_RTOS_STARTED() && (xSemaphoreTake(xSemaphore_SPI1, pdMS_TO_TICKS(2000)) == pdFALSE) )     //TODO handle properly, also for continuous mode
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "Could not acquire SPI bus because Geophone task doesnt yield");
        return;
    }

    ADC_DEASSERT_ALL();   /* Make sure ADCs dont interfere */
    SCL_ASSERT();
    HAL_SPI_TransmitReceive(&hspi1, transmit_buf, buf, 4, 10);
    SCL_DEASSERT();

    /* Allow others to use the SPI1 bus again */
    if (IS_RTOS_STARTED())
    {
        xSemaphoreGive(xSemaphore_SPI1);
    }

    /* Response is provided in argument buffer */
}

void SCL_sendCommand(uint32_t cmd)
{
    SCL_sendCommandToBuf(receive_buf, cmd);
}

uint16_t SCL_getResponseFromBuf(uint8_t* buf)
{
    return (buf[1] << 1*8) | buf[2];
}

uint16_t SCL_getResponse()
{
    return SCL_getResponseFromBuf(receive_buf);
}

uint32_t SCL_verifyResponseFromBuf(uint8_t* rx_buf)
{
    /* Verify RS */
    uint8_t RS_bits = rx_buf[0] & (SCL3300_MASK_RS >> SCL3300_OFFSET_RS);

    if (RS_bits != SCL3300_RS_NORMAL)
    {
        if      (RS_bits == SCL3300_RS_STARTUP)
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "Inclinometer still performing start-up");
        }
        else if (RS_bits == SCL3300_RS_ERROR)
        {
            DEBUG_PRINT(DBG_LVL_WARNING, "Inclinometer is reporting errors");
        }
        else
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "RS bits: 0x%x", RS_bits);
        }

        return 1;
    }

    /* Verify CRC */
    uint8_t CRC_rx   = rx_buf[3];
    uint8_t CRC_calc = SCL_calculateCRC(SCL_assembleResponse(rx_buf));

    if (CRC_rx != CRC_calc)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "CRC not computed correctly");
        return 1;
    }

    return 0;
}

uint32_t SCL_verifyResponse()
{
    return SCL_verifyResponseFromBuf(receive_buf);
}

uint32_t SCL_verifyStatus(uint8_t startup_check)
{
    uint8_t* rx_buf = receive_buf;

    /* Parse error */
    uint16_t error_msg = SCL_getResponseFromBuf(rx_buf);

    /* An error occurred if a bit is set; during startup, the mode change and power bit are expected behaviour */
    if ( (startup_check && (error_msg & ~(SCL3300_STATUS_PWR | SCL3300_STATUS_MODE)) ) || (!startup_check && error_msg) )
    {
        DEBUG_PRINTF(DBG_LVL_ERROR, "Inclinometer error: %s", error_msg);

        if (error_msg & SCL3300_STATUS_DIG1) { DEBUG_PRINT(DBG_LVL_VERBOSE, "DIGI1"); }
        if (error_msg & SCL3300_STATUS_DIG2) { DEBUG_PRINT(DBG_LVL_VERBOSE, "DIGI2"); }
        if (error_msg & SCL3300_STATUS_CLK)  { DEBUG_PRINT(DBG_LVL_VERBOSE, "CLOCK_ERROR"); }
        if (error_msg & SCL3300_STATUS_SAT)  { DEBUG_PRINT(DBG_LVL_VERBOSE, "SIG_SATURATED"); }
        if (error_msg & SCL3300_STATUS_TEMP) { DEBUG_PRINT(DBG_LVL_VERBOSE, "TEMP_SATURATED"); }
        if (error_msg & SCL3300_STATUS_PWR)  { DEBUG_PRINT(DBG_LVL_VERBOSE, "POWER_FAILURE"); }
        if (error_msg & SCL3300_STATUS_MEM)  { DEBUG_PRINT(DBG_LVL_VERBOSE, "MEMORY_FAILURE"); }
        if (error_msg & SCL3300_STATUS_PD)   { DEBUG_PRINT(DBG_LVL_VERBOSE, "POWER_DOWN"); }
        if (error_msg & SCL3300_STATUS_MODE) { DEBUG_PRINT(DBG_LVL_VERBOSE, "MODE_CHANGE"); }
        if (error_msg & SCL3300_STATUS_PIN)  { DEBUG_PRINT(DBG_LVL_VERBOSE, "INTERNAL_ERROR"); }

        return 1;
    }
        /* Verify RS and CRC - checked if no other error occurred before */
    else if (SCL_verifyResponseFromBuf(rx_buf))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

/* Calculate CRC for 24 MSB's of the 32 bit dword
 * (8 LSB's are the CRC field and are not included in CRC calculation) */
uint8_t SCL_calculateCRC(uint32_t Data)
{
    uint8_t BitIndex;
    uint8_t BitValue;
    uint8_t CRCValue = 0xFF; // ATTENTION: CRC is already defined as a Macro!

    for (BitIndex = 31; BitIndex > 7; BitIndex--)
    {
        BitValue = (uint8_t)((Data >> BitIndex) & 0x01);
        CRCValue = CRC8(BitValue, CRCValue);
    }

    CRCValue = (uint8_t)~CRCValue;
    return CRCValue;
}

/* Conversion functions */
uint16_t SCL_convertAcceleration(uint8_t* rx_buf, uint8_t mode)
{
    /* Verify integrity of the data */
    if (SCL_verifyResponseFromBuf(rx_buf)) { return 0; }

    /* Store 16bit number as 32bit for multiplication with another 16bit value further below */
    uint32_t acc = SCL_getResponseFromBuf(rx_buf);

    // Calculate absolute value
    acc = twoComplementToAbsolute(acc);

    switch (mode)
    {
    case 1:
        acc = (acc * SCL3300_ACC_SCALE) / SCL3300_ACC_SENSITIVITY_MODE1;
        break;
    case 2:
        acc = (acc * SCL3300_ACC_SCALE) / SCL3300_ACC_SENSITIVITY_MODE2;
        break;
    case 3:
        acc = (acc * SCL3300_ACC_SCALE) / SCL3300_ACC_SENSITIVITY_MODE3;
        break;
    case 4:
        acc = (acc * SCL3300_ACC_SCALE) / SCL3300_ACC_SENSITIVITY_MODE4;
        break;
    default:
        acc = 0;
        break;
    }

    /* Return value in mg (for SCL3300_ACC_SCALE = 1000) */
    return (uint16_t)acc;
}

uint16_t SCL_convertAngle(uint8_t* rx_buf)
{
    /* Verify integrity of the data */
    if (SCL_verifyResponseFromBuf(rx_buf)) { return 0; }

    /* Store 16bit number as 32bit for multiplication with another 16bit value further below */
    int32_t angle_twocomplement = SCL_getResponseFromBuf(rx_buf);

    /* Shift the interval of (-90,+90) to (0,180) so we can work with strictly positive numbers */
    uint32_t angle = angle_twocomplement + ( (SCL3300_ANG_OFFSET * SCL3300_ANG_SENSITIVITY_DENOM) / SCL3300_ANG_SENSITIVITY_NOM);

    /* Return value in 1/SCL3300_ANG_SCALE's of degree */
    return (uint16_t)( (angle * SCL3300_ANG_SCALE * SCL3300_ANG_SENSITIVITY_NOM) / SCL3300_ANG_SENSITIVITY_DENOM);
}

uint16_t SCL_convertTemperature(uint8_t* rx_buf)
{
    /* Verify integrity of the data */
    if (SCL_verifyResponseFromBuf(rx_buf)) { return 0; }

    uint32_t temp = SCL_getResponseFromBuf(rx_buf);

    /* Conversion not necessary from two's complement, as cannot have negative Kelvin */

    /* Return value in 1/SCL3300_TEMP_SCALE's of Kelvin (for Celsius, change SCL3300_TEMP_OFFSET to 273) */
    return (uint16_t)( (temp * SCL3300_TEMP_SCALE) / SCL3300_TEMP_SENSITIVITY) - SCL3300_TEMP_OFFSET;
}

#endif /* USE_INCLINO */
