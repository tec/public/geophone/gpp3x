/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

#if BOLT_ENABLE

/* External variables --------------------------------------------------------*/
extern rb_t   rb_bolt;
extern rb_t   rb_cmd;
extern rb_t   rb_comdata;
extern rb_t   rb_event;

/* Functions -----------------------------------------------------------------*/

bool Bolt_checkComBoard(void)
{
    bool detected = false;

    if (BOLT_IS_ASSERTED()) return false;   /* Another task is currently accessing Bolt */

    BOLT_MODE_WR();
    BOLT_ASSERT();
    Delay(BOLT_ACK_TIMEOUT_US);
    if (BOLT_IS_ACK())
    {
        detected = true;
    }
    /* Timed out: Check COM_IND pin */
    else if (COM_IS_IND())
    {
        /* There is data in the output queue -> queue is potentially full, assume that a ComBoard is installed */
        DEBUG_PRINT(DBG_LVL_VERBOSE, "ACK timeout but COM_IND high (queue full?)");
        detected = true;
    }
    BOLT_DEASSERT();
    BOLT_MODE_RD();

    if (!detected && SysConf.ComBoard)
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "ComBoard disconnected");
    }
    else if (detected && !SysConf.ComBoard)
    {
        DEBUG_PRINT(DBG_LVL_INFO, "ComBoard detected");
    }
    SysConf.ComBoard = detected;

    return detected;
}

/* Compose the message header ------------------------------------------------*/
static uint32_t Bolt_fillMsgHeader(dpp_header_t* hdr, dpp_message_type_t type, uint32_t len)
{
    static uint16_t dpp_header_seqnr = 0;

    uint32_t payload_len = len;
    if (!payload_len)
    {
        switch (type)
        {
        case DPP_MSG_TYPE_EVENT:
            payload_len = DPP_EVENT_LEN;
            break;
        case DPP_MSG_TYPE_NODE_INFO:
            payload_len = DPP_NODE_INFO_LEN;
            break;
        case DPP_MSG_TYPE_APP_HEALTH:
            payload_len = DPP_APP_HEALTH_LEN;
            break;
        case DPP_MSG_TYPE_GEOPHONE_ACQ:
            payload_len = DPP_GEOPHONE_ACQ_LEN;
            break;
        case DPP_MSG_TYPE_IMU:
            payload_len = DPP_IMU_LEN;
            break;
        case DPP_MSG_TYPE_ADCDATA:
            payload_len = DPP_GEOPHONE_ADC_LEN;
            break;
        case DPP_MSG_TYPE_INCLINO:
            payload_len = DPP_INCLINO_LEN;
            break;
        case DPP_MSG_TYPE_GNSS_SV:
            payload_len = DPP_GNSS_SV_LEN;
            break;
        case DPP_MSG_TYPE_TIMESYNC:
            payload_len = sizeof(uint64_t);
            break;
        default:
            DEBUG_PRINTF(DBG_LVL_WARNING, "Unknown message type %u", type);
            break;
        }
    }
    hdr->device_id       = SysConf.DeviceID;
    hdr->type            = type;
    hdr->payload_len     = payload_len;
    hdr->target_id       = DPP_DEVICE_ID_SINK;
    hdr->seqnr           = dpp_header_seqnr++;
    hdr->generation_time = RTC_TIMESTAMP();

    return payload_len;
}

/* Insert a message into the Bolt RB for transmission ------------------------*/
bool Bolt_enqueueMessageEx(dpp_message_type_t type, const uint8_t* payload, uint32_t payload_len)
{
    if (!SysConf.ComBoard || payload_len > DPP_MSG_PAYLOAD_LEN)
    {
        /* Can't be written to Bolt -> drop the message */
        return false;
    }
    if (RB_isFull(&rb_bolt))
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "Bolt RB full, message dropped");
        return false;
    }

    uint8_t* rb_msg = RB_insert(&rb_bolt, NULL);
    payload_len     = Bolt_fillMsgHeader((dpp_header_t*)rb_msg, type, payload_len);
    memcpy(rb_msg + DPP_MSG_HDR_LEN, (uint8_t*)payload, payload_len);

    if (IS_ISR()) { NOTIFY_BOLT_TASK_FROM_ISR(0); }
    else          { NOTIFY_BOLT_TASK();           }

    return true;
}

bool Bolt_enqueueMessage(dpp_message_type_t type, const uint8_t* payload)
{
    return Bolt_enqueueMessageEx(type, payload, 0);
}

/* Check a message -----------------------------------------------------------*/
static BoltErrType_t Bolt_processMsg(dpp_message_t* msg, uint32_t len)
{
    /* Buffer length check */
    if (len <= DPP_MSG_MIN_HDR_LEN)
    {
        return BOLT_ERR_RD_BUF_LEN;
    }

    /* Identify message header and extract payload */
    if (msg->header.type & 0x80)
    {
        /* Minimal header */
        return BOLT_ERR_RD_HDR_LEN;    /* Not supported */
    }

    /* Check payload length */
    if ((!msg->header.payload_len)                      ||
        (msg->header.payload_len > DPP_MSG_PAYLOAD_LEN) ||
        len < (uint32_t)(msg->header.payload_len + DPP_MSG_HDR_LEN + 2))
    {
        /* Message length error */
        return BOLT_ERR_RD_PAYLOAD_LEN;
    }

    /* CRC check */
    uint16_t calc_crc = crc16((uint8_t*)msg, msg->header.payload_len + DPP_MSG_HDR_LEN, 0);
    if (calc_crc != DPP_MSG_GET_CRC16(msg))
    {
        return BOLT_ERR_RD_CRC;
    }

    /* Check target ID */
    if ((msg->header.target_id != SysConf.DeviceID) &&      /* Not current device ID */
        (msg->header.target_id != DPP_DEVICE_ID_BROADCAST)) /* Not broadcast address */
    {
        /* Ignore this message */
        DEBUG_PRINTF(DBG_LVL_WARNING, "Bolt message of type %u ignored (Target: %u, Initiator: %u, Payload: %x %x %x %x)", msg->header.type, msg->header.target_id, msg->header.device_id, msg->payload[0], msg->payload[1], msg->payload[2], msg->payload[3]);
        return BOLT_ERR_OK;
    }

    DEBUG_PRINTF(DBG_LVL_INFO, "Bolt Read Type: 0x%x", msg->header.type);

    /* Process based on message type */
    switch (msg->header.type)
    {
    /* TIMESTAMP ---------------------------------*/
    case DPP_MSG_TYPE_TIMESYNC:
        DEBUG_PRINTF(DBG_LVL_INFO, "Timesync msg rcvd (%llu)", msg->timestamp);
#if TSYNC_METHOD_BOLT
        TSync_setTimestamp(msg->timestamp);
#endif
        break;

    /* APP CMD -----------------------------------*/
    case DPP_MSG_TYPE_CMD:
        if ( (msg->header.payload_len < DPP_COMMAND_HDR_LEN) || (msg->header.payload_len > RB_CMD_MAX_ITEM_LEN) )
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "Received command has an invalid length (%u bytes)", msg->header.payload_len);
            return BOLT_ERR_RD_CMD_LEN;
        }
        /* Just insert into queue, command processing is initiated from IDLE task */
        memcpy(RB_insert(&rb_cmd, NULL), msg->payload, msg->header.payload_len);
        break;

    /* EVENT -------------------------------------*/
    case DPP_MSG_TYPE_EVENT:
        /* event message received via BOLT -> log to SD card */
        RB_insert(&rb_event, (uint8_t*)&msg->evt);
        RTOS_queueSendLog(LOGTYPE_EVENT);
        break;

    /* HEALTH ------------------------------------*/
    case DPP_MSG_TYPE_HEALTH_MIN:
        /* log to SD card */
        RB_insert(&rb_comdata, (uint8_t*)msg);
        RTOS_queueSendLog(LOGTYPE_COMDATA);
        break;

    /* Unknown type ------------------------------*/
    default:
        DEBUG_PRINT(DBG_LVL_WARNING, "Received unknown DPP message type over Bolt");
        /* log to SD card */
        RB_insert(&rb_comdata, (uint8_t*)msg);
        RTOS_queueSendLog(LOGTYPE_COMDATA);
        break;
    }

    return BOLT_ERR_OK;   /* No error */
}

/* Acquire / Release BOLT ----------------------------------------------------*/
static bool Bolt_acquire(bool writeMode)
{
    if ( BOLT_IS_ACK() || BOLT_IS_ASSERTED() )
    {
        return false;
    }

    /* Acquire exclusive access to SPI */
    if ( IS_RTOS_RUNNING() && (xSemaphoreTake(BOLT_SPI_SEMAPHORE, pdMS_TO_TICKS(BOLT_TASK_SEM_WAIT_MS)) == pdFALSE) )
    {
        DEBUG_PRINT(DBG_LVL_ERROR, "Could not acquire SPI bus");
        return false;
    }

    /* Assert BOLT for WRITE */
    if (writeMode)
    {
        BOLT_MODE_WR();
    }
    else
    {
        BOLT_MODE_RD();
    }
    BOLT_ASSERT();

    const uint32_t step_size_us = 5;

    for (uint32_t i = BOLT_ACK_TIMEOUT_US / step_size_us; i > 0; i--)
    {
        Delay(step_size_us);

        if (BOLT_IS_ACK())
        {
            return true;
        }
    }

    /* BOLT did not respond within BOLT_ACK_TIMEOUT_US */
    if (IS_RTOS_RUNNING())
    {
        xSemaphoreGive(BOLT_SPI_SEMAPHORE);
    }
    return false;
}

/* only call Release() if Acquire() was successful! */
static void Bolt_release(void)
{
    BOLT_DEASSERT();

    if (IS_RTOS_RUNNING())
    {
        xSemaphoreGive(BOLT_SPI_SEMAPHORE);
    }

    /* Wait for ACK to go low */
    if (BOLT_IS_ACK())
    {
        RTOS_TASK_DELAY(10);
    }
}

static BoltErrType_t Bolt_read(uint8_t* out_buf)
{
    BoltErrType_t err = BOLT_ERR_OK;

    if (Bolt_acquire(false))
    {
        /* Read byte by byte until Bolt pulls the ACK line low */
        uint32_t bytes_rcvd = 0;
        while (BOLT_IS_ACK())
        {
            if (bytes_rcvd >= BOLT_BUF_SIZE)
            {
                err = BOLT_ERR_RD_BUF_OV;
                break;
            }
            if (HAL_SPI_Receive(&BOLT_SPI, &out_buf[bytes_rcvd], 1, BOLT_TASK_SPI_TIMEOUT_MS) != HAL_OK)
            {
                err = BOLT_ERR_RD_SPI_HAL;
                break;
            }
            bytes_rcvd++;
        }
        if (!err)
        {
            /* Check and process the received message */
            err = Bolt_processMsg((dpp_message_t*)out_buf, bytes_rcvd);
        }
        Bolt_release();
    }
    else
    {
        /* REQ ACK timeout */
        err = BOLT_ERR_RD_ACK;
    }
    return err;
}

static BoltErrType_t Bolt_write(uint8_t* data, uint32_t len)
{
    BoltErrType_t err = BOLT_ERR_OK;

    if (Bolt_acquire(true))
    {
        /* ACK received within timeout */
        if (HAL_SPI_Transmit(&BOLT_SPI, data, len, BOLT_TASK_SPI_TIMEOUT_MS) != HAL_OK)
        {
            err = BOLT_ERR_WR_SPI_HAL;
        }
        else if (!BOLT_IS_ACK())      /* Abort by Bolt */
        {
            err = BOLT_ERR_WR_SPI_ACK;
        }
        Bolt_release();
    }
    else
    {
        /* REQ ACK timeout */
        err = BOLT_ERR_WR_ACK;
    }
    return err;
}

void Bolt_flush(uint8_t* buffer)
{
    while (BOLT_IS_IND())
    {
        if (Bolt_acquire(false))
        {
            /* Read the max. amount of bytes */
            if (HAL_SPI_Receive(&BOLT_SPI, buffer, BOLT_BUF_SIZE, BOLT_TASK_SPI_TIMEOUT_MS) != HAL_OK)
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "Failed to read from Bolt");
            }
            Bolt_release();
        }
    }
}

/* Directly write an event into Bolt (useful if RTOS not running) */
bool Bolt_sendEvent(dpp_event_type_t type, uint32_t value, bool force)
{
    static uint8_t bolt_evt_buffer[DPP_MSG_HDR_LEN + DPP_EVENT_LEN + DPP_MSG_CRC_LEN];

    if (!SysConf.ComBoard) { return false; }

    if (force && BOLT_IS_ASSERTED())
    {
        BOLT_DEASSERT();
    }

    Bolt_fillMsgHeader((dpp_header_t*)bolt_evt_buffer, DPP_MSG_TYPE_EVENT, 0);
    ((dpp_message_t*)bolt_evt_buffer)->evt.type  = type;
    ((dpp_message_t*)bolt_evt_buffer)->evt.value = value;
    uint32_t msg_len = DPP_MSG_HDR_LEN + DPP_EVENT_LEN;
    uint16_t crc = crc16(bolt_evt_buffer, msg_len, 0);
    DPP_MSG_SET_CRC16((dpp_message_t*)bolt_evt_buffer, crc);
    msg_len += DPP_MSG_CRC_LEN;

    /* Acquire BOLT and wait for ACK  */
    if (Bolt_write(bolt_evt_buffer, msg_len) != BOLT_ERR_OK)
    {
        DEBUG_PRINTF(DBG_LVL_WARNING, "Failed to write event to Bolt (type: 0x%x, value: %u)", type, value);
        return false;
    }

    DEBUG_PRINTF(DBG_LVL_INFO, "Event written to Bolt (type: 0x%x, value: %u)", type, value);
    return true;
}

/*** BOLT TASK ****************************************************************/
void vTaskBolt(void* arg)
{
    static uint8_t bolt_buf[BOLT_BUF_SIZE] __attribute__((aligned(32)));
    static uint8_t bolt_err_cntr;

    (void)arg;

    Bolt_checkComBoard();

#if BOLT_FLUSH
    /* Flush the Bolt queue */
    if (SysConf.ComBoard)
    {
        Bolt_flush();
    }
#endif
    DEBUG_PRINT(DBG_LVL_VERBOSE, "Bolt task started");

    while (true)
    {
        ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
        uint32_t start_ts = TIM5_getCounterValue();

        /* Check if ComBoard available */
        if (!SysConf.ComBoard && !Bolt_checkComBoard())
        {
            continue;
        }

        /* Read all pending messages from Bolt */
        while (BOLT_IS_IND())
        {
            BoltErrType_t bolt_err = BOLT_ERR_OK;

            /* Make sure the notification token is cleared */
            ulTaskNotifyTake(pdTRUE, 0);

            /* Acquire Bolt for reading */
            bolt_err = Bolt_read(bolt_buf);

            /* Error handling */
            if (bolt_err)
            {
                DEBUG_PRINTF(DBG_LVL_ERROR, "Bolt read error %u", bolt_err);
                RTOS_event(EVENT_GEOPHONE3X_BOLT_READ_ERROR, bolt_err, EVTLOG_SD);     /* Don't log to COM / BOLT */
                /* Abort, try later */
                break;
            }
        }

        /* Write messages to Bolt */
        while (!RB_isEmpty(&rb_bolt))
        {
            uint32_t bolt_err = BOLT_ERR_OK;

            /* Prepare the data */
            dpp_message_t* msg = (dpp_message_t*)RB_getTail(&rb_bolt);
            uint32_t msg_len   = msg->header.payload_len + DPP_MSG_HDR_LEN;
            /* Calculate and add the CRC */
            uint16_t crc = crc16((uint8_t*)msg, msg_len, 0);
            DPP_MSG_SET_CRC16(msg, crc);
            msg_len += DPP_MSG_CRC_LEN;

            /* Make sure the notification token is cleared */
            ulTaskNotifyTake(pdTRUE, 0);

            /* Acquire BOLT and wait for ACK  */
            bolt_err = Bolt_write((uint8_t*)msg, msg_len);

            /* Error handling */
            if (bolt_err)
            {
                if (bolt_err_cntr < 3)
                {
                    DEBUG_PRINTF(DBG_LVL_ERROR, "Bolt write error %u", bolt_err);
                    RTOS_event(EVENT_GEOPHONE3X_BOLT_WRITE_ERROR, bolt_err, EVTLOG_SD);      /* Don't log to COM / BOLT */
                }
                bolt_err_cntr++;
                /* Abort and try later */
                break;
            }
            else
            {
                /* No error */
                RB_remove(&rb_bolt);           /* Remove element from ringbuffer */
                bolt_err_cntr = 0;
                DEBUG_PRINTF(DBG_LVL_INFO, "Bolt write msg type 0x%x", msg->header.type);
            }
        }

        /* Check task runtime */
        uint32_t time_delta_ms = TIM5_TICKS_TO_MS(TIM5_getCounterValue() - start_ts);
        if (time_delta_ms > RTOS_TASK_RUNTIME_TH_MS)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "Task runtime longer than expected (%4lums)", time_delta_ms);
            RTOS_event(EVENT_GEOPHONE3X_TASK_RUNTIME, (time_delta_ms << 16) | RTOS_TASK_ID_BOLT, EVTLOG_SD | EVTLOG_COM);
        }

    } /* Bolt task loop */
}

#endif /* BOLT_ENABLE */
