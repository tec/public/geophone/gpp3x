/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"

/* External Variables --------------------------------------------------------*/
extern rb_t*  adc_rb[];
extern rb_t   rb_geo;
extern rb_t   rb_event;
extern rb_t   rb_health;
extern rb_t   rb_imustat;
extern rb_t   rb_imudata;
extern rb_t   rb_gnssdata;
extern rb_t   rb_inclinodata;
extern rb_t   rb_comdata;
extern rb_t   rb_tsync;
extern rb_t   rb_stream;
extern rb_t   rb_schedule;

/* Variables -----------------------------------------------------------------*/
static char   log_buf[SD_FILE_MAX_LINE_LEN];
static char   log_filename[SD_FILE_MAX_PATH_LEN];

/* Private Functions ---------------------------------------------------------*/
#if LOG_ADCDATA_MINISEED
static LogErrType_t LOG_writeADCDataMiniseed(ADCID_t adc_id, const ADCData_t* adc_data, bool finish);
#else
static LogErrType_t LOG_writeADCChunk(ADCID_t adc_id, const ADCData_t* adc_data);
#endif
static LogErrType_t LOG_writeGNSSData(const gnss_ubx_msg_t* data);
static LogErrType_t LOG_writeACQ(const Geophone_t* data);
static LogErrType_t LOG_writeEvent(const dpp_event_t* data);
static LogErrType_t LOG_writeHealth(const dpp_app_health_t* data);
static LogErrType_t LOG_writeIMUStat(const ImuStat_t* data);
static LogErrType_t LOG_writeIMUData(const ImuData_t* data);
static LogErrType_t LOG_writeInclino(const dpp_inclino_t* data);
static LogErrType_t LOG_writeCOMData(const dpp_message_t* data);
static LogErrType_t LOG_writeTSync(const TSyncInfo_t* data);
static void         LOG_mountUSBMSC(void);
static void         LOG_checkNotifications(void);
bool                LOG_streamData(StreamingDataType_t type, const void* data, uint16_t len, uint32_t arg);

/*** LOG TASK *****************************************************************/
void vTaskLog(void* arg)
{
    bool prevent_chdir = false;

    (void)arg;

    DEBUG_PRINT(DBG_LVL_VERBOSE, "Log task started");

    while (true)
    {
        LED_Y_OFF();
        WDT_POLL();   /* This task can run for several seconds and consequently block the idle task, therefore we also need to reset the watchdog here */

        /* Wait indefinitely for the next notification */
        LogType_t type;
        if (xQueueReceive(xQueue_Log, &type, portMAX_DELAY) != pdTRUE)
        {
            continue;    /* Timeout */
        }

        if (SysConf.USB->deviceClass == MSC)
        {
            /* USB MSC mounted -> can't proceed with logging until unmounted */
            if (type == LOGTYPE_USB_MSC_EJECT)
            {
                /* Eject USB MSC */
                USB_DEVICE_DeInit();
                DEBUG_PRINT(DBG_LVL_INFO, "USB MSC ejected");
                RTOS_event(EVENT_GEOPHONE3X_MSC_EJECT, 0, EVTLOG_SD | EVTLOG_COM);

                /* Restart geophone task if necessary */
                if (SysConf.OpMode & SYS_OPMODE_CONT)
                {
                    Geo_initAcq(TRG_EXT, 0, 0);
                }
            }
            continue;
        }

        if (xSemaphoreTake(xSemaphore_SD, pdMS_TO_TICKS(LOG_TASK_SEM_WAIT_MS)) == pdFALSE)
        {
            DEBUG_PRINT(DBG_LVL_ERROR, "Failed to acquire SD semaphore");
            RTOS_event(EVENT_GEOPHONE3X_TASK_SEMAPHORE, 1, EVTLOG_SD | EVTLOG_COM);
            continue;   /* TODO: maybe do a system reset here (semaphore should always be granted if the wait time is sufficiently high) */
        }

        LogErrType_t log_err  = LOG_ERR_OK;
        uint32_t     start_ts = TIM5_getCounterValue();

        if (!(SysConf.OpMode & SYS_OPMODE_NOLEDS))
        {
            LED_Y_ON();
        }

        if (!SD_mount())
        {
            DEBUG_PRINT(DBG_LVL_ERROR, "LOG SD mount failed");
            //RTOS_event(EVENT_GEOPHONE3X_LOG_ERROR, LOG_ERR_MOUNT, EVTLOG_COM);
            Error_Handler();   /* Cannot recover -> reset */
        }

        /* Change working directory: only if prevent flag is not set */
        if (!prevent_chdir && !SD_checkCWD())
        {
            SD_eject();
            RTOS_event(EVENT_GEOPHONE3X_LOG_ERROR, LOG_ERR_DIR, EVTLOG_COM);
            continue;
        }

        /* Log message to SD card */
        switch (type)
        {
        case LOGTYPE_SD_PREVENTCHGDIR:
            /* SD Prevent changing working directory from now on */
            prevent_chdir = true;
            DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG prevent ChgDir");
            break;

        case LOGTYPE_SD_ALLOWCHGDIR:
            /* SD Allow changing working directory from now on */
            prevent_chdir = false;
            DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG allow ChgDir");
            SD_checkCWD();
            break;

        case LOGTYPE_SD_MOUNT:
            /* Not used actually, since SD card mounting is done in any case */
            break;

        case LOGTYPE_SD_EJECT:
            SD_eject();
            break;

        case LOGTYPE_SD_GETSIZE:
        {
            SD_updateSize();
            uint8_t fill_level = SD_getFillLevel();
            if (fill_level >= LOG_TASK_SD_FULL_WARN_TH)
            {
                RTOS_event(EVENT_GEOPHONE3X_SD_FULL, fill_level, EVTLOG_SD | EVTLOG_COM);
            }
            break;
        }

            /* LOG ADC ---------------------------------------------------*/
        case LOGTYPE_ADC1_CHUNK:
        case LOGTYPE_ADC2_CHUNK:
        case LOGTYPE_ADC3_CHUNK:
        {
            ADCID_t    adc_id   = type - LOGTYPE_ADC1_CHUNK + 1;
            ADCData_t* adc_data = (ADCData_t*)RB_getTail(adc_rb[adc_id]);
            if (adc_data)
            {
                for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
                {
#if LOG_ADCDATA_MINISEED
                    log_err = LOG_writeADCDataMiniseed(adc_id, adc_data, false);
#else
                    log_err = LOG_writeADCChunk(adc_id, adc_data);
#endif
                    if (log_err == LOG_ERR_OK)
                    {
                        DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG ADC ok");
                        break;
                    }
                    DEBUG_PRINTF(DBG_LVL_ERROR, "LOG ADC error %u", log_err);
                    vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
                }
#if !STREAMING_TARGET_NONE && !LOG_ADCDATA_MINISEED  /* Note: miniseed streaming is handled in LOG_writeADCDataMiniseed() */
                LOG_streamData(STREAMING_DTYPE_GEO_A1 + (adc_id - 1), adc_data->data, adc_data->len, adc_data->acq_id);
#endif
                /* Must be removed from the RB in any case, otherwise buffer might fill up */
                RB_remove(adc_rb[adc_id]);
            }
            break;
        }

        /* LOG GNSS --------------------------------------------------*/
        case LOGTYPE_GNSS:
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeGNSSData((gnss_ubx_msg_t*)RB_getTail(&rb_gnssdata));
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG GNSS ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG GNSS error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
            /* Pop from RB */
            RB_remove(&rb_gnssdata);
            break;

        /* LOG ACQ ---------------------------------------------------*/
        case LOGTYPE_ACQ:
        {
            Geophone_t* geo_acq = (Geophone_t*)RB_getTail(&rb_geo);
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeACQ(geo_acq);
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG ACQ ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG ACQ error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
            /* Finish miniSEED file */
#if LOG_ADCDATA_MINISEED
            for (ADCID_t adc = ADC_A1; adc < (GEO_NUM_AXES + 1); adc++)
            {
                /* No new data is added, just finish the current miniseed block */
                log_err = LOG_writeADCDataMiniseed(adc, 0, true);
                if (log_err != LOG_ERR_OK)
                {
                    DEBUG_PRINTF(DBG_LVL_ERROR, "LOG ACQ mseed error %u", log_err);
                    break;
                }
            }
#endif
            /* update the acquisition ID */
            if (geo_acq && !SD_writeAcqID(geo_acq->ID))
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "LOG ACQID error");
            }
            /* Pop from RB */
            RB_remove(&rb_geo);
            break;
        }

        /* LOG EVENT -------------------------------------------------*/
        case LOGTYPE_EVENT:
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeEvent((dpp_event_t*)RB_getTail(&rb_event));
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG EVT ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG EVT error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
            /* Pop from RB */
            RB_remove(&rb_event);
            break;

        /* LOG HEALTH ------------------------------------------------*/
        case LOGTYPE_HEALTH:
        {
            dpp_app_health_t* health = (dpp_app_health_t*)RB_getTail(&rb_health);
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeHealth(health);
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG HEALTH ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG HEALTH error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
#if !STREAMING_TARGET_NONE
            LOG_streamData(STREAMING_DTYPE_HEALTH, health, sizeof(dpp_app_health_t), DPP_DEVICE_ID);
#endif
            /* Pop from RB */
            RB_remove(&rb_health);
            break;
        }

        /* LOG IMUSTAT -----------------------------------------------*/
        case LOGTYPE_IMUSTAT:
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeIMUStat((ImuStat_t*)RB_getTail(&rb_imustat));
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG IMUSTAT ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG IMUSTAT error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
            /* Pop from RB */
            RB_remove(&rb_imustat);
            break;

        /* LOG IMUDATA -----------------------------------------------*/
        case LOGTYPE_IMUDATA:
        {
            /* TODO: this should be optimized to reduce the number of file accesses to the SD card
             *       -> collect 1s chunks of data and then notify the log task (similar to ADC raw data) */
            uint32_t cntr = rb_imudata.count;
            while (cntr && (log_err == LOG_ERR_OK))
            {
                cntr--;
                for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
                {
                    log_err = LOG_writeIMUData((ImuData_t*)RB_getTail(&rb_imudata));
                    if (log_err == LOG_ERR_OK)
                    {
                        DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG IMUDATA ok");
                        break;
                    }
                    DEBUG_PRINTF(DBG_LVL_ERROR, "LOG IMUDATA error %u", log_err);
                    vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
                }
                /* Pop from RB */
                RB_remove(&rb_imudata);    /* remove item from RB */
            }
            break;
        }

        /* LOG INCLINOMETER ------------------------------------------*/
        case LOGTYPE_INCLINO:
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeInclino((dpp_inclino_t*)RB_getTail(&rb_inclinodata));
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG INCLINO ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG INCLINO error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
            /* Pop from RB */
            RB_remove(&rb_inclinodata);
            break;

        /* COM data (arbitrary DPP message) --------------------------*/
        case LOGTYPE_COMDATA:
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeCOMData((dpp_message_t*)RB_getTail(&rb_comdata));
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG COMDATA ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG COMDATA error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
            /* Pop from RB */
            RB_remove(&rb_comdata);
            break;

        /* Timesync data ---------------------------------------------*/
        case LOGTYPE_TSYNC:
            for (uint32_t tries = 0; tries < LOG_TASK_ERR_MAX_TRIES; tries++)
            {
                log_err = LOG_writeTSync((TSyncInfo_t*)RB_getTail(&rb_tsync));
                if (log_err == LOG_ERR_OK)
                {
                    DEBUG_PRINT(DBG_LVL_VERBOSE, "LOG TSYNC ok");
                    break;
                }
                DEBUG_PRINTF(DBG_LVL_ERROR, "LOG TSYNC error %u", log_err);
                vTaskDelay(pdMS_TO_TICKS(LOG_TASK_ERR_RETRY_DELAY_MS));
            }
            /* Pop from RB */
            RB_remove(&rb_tsync);
            break;

        /* CONFIG: Read from SD --------------------------------------*/
        case LOGTYPE_CONF_RD:
            if (!SD_readConfig())
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "Read config error");
                RTOS_event(EVENT_GEOPHONE3X_CONFIG_RD_ERR, 0, EVTLOG_SD | EVTLOG_COM);
            }
            else
            {
                DEBUG_PRINT(DBG_LVL_VERBOSE, "Read Config ok");
            }
            break;

        /* CONFIG: Write to SD ---------------------------------------*/
        case LOGTYPE_CONF_WR:
            if (!SD_writeConfig())
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "Write config error");
                RTOS_event(EVENT_GEOPHONE3X_CONFIG_WR_ERR, 0, EVTLOG_SD | EVTLOG_COM);
            }
            else
            {
                DEBUG_PRINT(DBG_LVL_INFO, "LOG CFG WR ok");
            }
            break;

        /* SCHEDULE: Clear on SD -------------------------------------*/
        case LOGTYPE_SCHED_CLEAR:
            if (!SD_clearSchedule())
            {
                DEBUG_PRINT(DBG_LVL_ERROR, "LOG SCHED CLEAR error");
                RTOS_event(EVENT_GEOPHONE3X_SCHED_CL_ERR, 0, EVTLOG_SD | EVTLOG_COM);
            }
            else
            {
                DEBUG_PRINT(DBG_LVL_INFO, "LOG SCHED CLEAR ok");
            }
            break;

        /* SCHEDULE: Add entry on SD ---------------------------------*/
        case LOGTYPE_SCHED_ADD:
            if (!RB_isEmpty(&rb_schedule))
            {
                const ScheduleEntry_t* entry = (ScheduleEntry_t*)RB_getTail(&rb_schedule);
                if (!SD_addToSchedule(entry))
                {
                    DEBUG_PRINT(DBG_LVL_WARNING, "LOG SCHED ADD failed");
                    RTOS_event(EVENT_GEOPHONE3X_SCHED_WR_ERR, 0, EVTLOG_SD | EVTLOG_COM);
                }
                else
                {
                    DEBUG_PRINT(DBG_LVL_INFO, "LOG SCHED ADD ok");
                }
                RB_remove(&rb_schedule);
            }
            break;

        /* USB MSC ---------------------------------------------------*/
        case LOGTYPE_USB_MSC_MOUNT:
            LOG_mountUSBMSC();
            break;

        /* USB CDC ---------------------------------------------------*/
        case LOGTYPE_USB_CDC_INIT:
            if (SysConf.USB->deviceClass == NONE)
            {
                USB_DEVICE_Init(CDC);
                DEBUG_PRINT(DBG_LVL_INFO, "USB device initialized (CDC mode)");
            }
            else
            {
                DEBUG_PRINT(DBG_LVL_WARNING, "USB device is already initialized");
            }
            break;

        /* Invalid LogType */
        default:
            log_err = LOG_ERR_INVALID;
            break;
        }

        /* Check if there are missing notifications */
        LOG_checkNotifications();

        /* Check for error */
        if (log_err != LOG_ERR_OK)
        {
            RTOS_event(EVENT_GEOPHONE3X_LOG_ERROR, log_err, EVTLOG_ALL);    /* Try to write the error to all media including SD card (otherwise error will not be detected in logger mode) */
        }

        /* Calculate the runtime and warn if it exceeds a certain value */
        uint32_t time_delta_ms = TIM5_TICKS_TO_MS(TIM5_getCounterValue() - start_ts);
        if ( (time_delta_ms > LOG_TASK_RUNTIME_WARN_TH_MS) && (type != LOGTYPE_USB_MSC_MOUNT) )
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "LOG task runtime %4lums exceeded expected access time", time_delta_ms);

            if (time_delta_ms > LOG_TASK_RUNTIME_ERR_TH_MS)
            {
                /* Lower 16 bits contain error value, upper 16 bits contain access time */
                CLAMP_UINT16(time_delta_ms);
                RTOS_event(EVENT_GEOPHONE3X_TASK_RUNTIME, (time_delta_ms << 16) | RTOS_TASK_ID_LOG, EVTLOG_ALL);
            }
        }

        xSemaphoreGive(xSemaphore_SD);

    } /* Log task loop */
}


/* Logging functions ---------------------------------------------------------*/

#if !LOG_ADCDATA_MINISEED

static LogErrType_t LOG_writeADCChunk(ADCID_t adc_id, const ADCData_t* adc_data)
{
    if (!adc_data || !adc_data->len || !adc_data->acq_id) return LOG_ERR_INVALID;

    /* Compose filename and write data to file */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_ADCDATA, SD_getCWD(), adc_data->acq_id, adc_channel_mapping[adc_id]);

    return SD_writeFile(log_filename, adc_data->data, adc_data->len);
}

#endif


static LogErrType_t LOG_writeGNSSData(const gnss_ubx_msg_t* data)
{
    if ( !data || !data->len || (data->len > GNSS_MSG_PAYLOAD_LEN_MAX) ) return LOG_ERR_INVALID;

    /* Compose filename and create file if it does not exist */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_GNSSDATA, SD_getCWD());

#if LOG_GNSSDATA_BIN
    return (SD_writeFile(log_filename, data, GNSS_MSG_LEN(data)) ? LOG_ERR_OK : LOG_ERR_FWRITE);

#else
    /* Make sure the file has a header */
    if (!SD_initFile(log_filename, "sampleTime,class,id,len,payload\n"))
    {
        return LOG_ERR_FCREATE;
    }

    /* Buffer fill */
    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%lu,0x%02x,0x%02x,%u,", RTC_getEpoch(), data->class, data->id, data->len);
    buf_len += bytes_to_hexstr(data->payload, data->len, log_buf + buf_len, (SD_FILE_MAX_LINE_LEN - buf_len));
    log_buf[buf_len - 1] = '\n';    // -1 to overwrite the '0' character

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
#endif
}


static LogErrType_t LOG_writeACQ(const Geophone_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and create file if it does not exist */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_ACQ, SD_getCWD());
    if (!SD_initFile(log_filename, "ID,start_time,first_time,end_time,total_samples,peak_pos_val,peak_pos_sample,peak_neg_val,peak_neg_sample,trg_count_pos,trg_count_neg,trg_last_pos_sample,trg_last_neg_sample,trg_gain,trg_th_pos,trg_th_neg,trg_source,adc_pga,adc_format,adc_sps,channels\n"))
    {
        return LOG_ERR_FCREATE;
    }

#if GEO_NUM_AXES > 2
  #define CHANNEL_NAMES   ADC_A1_NAME ";" ADC_A2_NAME ";" ADC_A3_NAME
#elif GEO_NUM_AXES > 1
  #define CHANNEL_NAMES   ADC_A1_NAME ";" ADC_A2_NAME
#else
  #define CHANNEL_NAMES   ADC_A1_NAME
#endif

    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN,
                                "%07lu,%llu,%llu,%llu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%lu,%u,%u,%u,%u,%u,%u,%u," CHANNEL_NAMES "\n",
                                data->ID,
                                data->Start,
                                data->First,
                                data->End,
                                data->Samples,
                                data->PeakPosVal,
                                data->PeakPosSampleNo,
                                data->PeakNegVal,
                                data->PeakNegSampleNo,
                                data->TrgCountPos,
                                data->TrgCountNeg,
                                data->TrgLastPosSampleNo,
                                data->TrgLastNegSampleNo,
                                ((data->TrgGain == TRG_GAIN_STAGE2) ? TRG_GAIN_STAGE2 : TRG_GAIN_STAGE1),
                                data->TrgThPos,
                                data->TrgThNeg,
                                data->TrgSource,
                                data->PGA,
                                data->Format,
                                data->SPS);

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static LogErrType_t LOG_writeEvent(const dpp_event_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and create file if it doesn't exist */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_EVENT, SD_getCWD());
    if (!SD_initFile(log_filename, "timestamp,type,value\n"))
    {
        return LOG_ERR_FCREATE;
    }

    /* Buffer fill */
    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%lu,%u,%lu\n",
                                RTC_getEpoch(),
                                data->type,
                                data->value);

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static LogErrType_t LOG_writeHealth(const dpp_app_health_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and create file if it doesn't exist */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_HEALTH, SD_getCWD());
    if (!SD_initFile(log_filename, "timestamp,tasks_stackwm,card_usage,uptime,core_temp,core_vcc,cpu_dc,supply_vcc,supply_current,temperature,humidity,heap_usage,svc_stackwm,idle_stackwm\n"))
    {
        return LOG_ERR_FCREATE;
    }

    /* Buffer fill */
    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%lu,%u,%u,%lu,%d,%u,%u,%u,%u,%d,%u,%u,%u,%u\n",
                                RTC_getEpoch(),
                                data->stack,
                                data->nv_mem,
                                data->uptime,
                                data->core_temp,
                                data->core_vcc,
                                data->cpu_dc,
                                data->supply_vcc,
                                data->supply_current,
                                data->temperature,
                                data->humidity,
                                SysConf.SysHealth->HeapUsedP,
                                SysConf.SysHealth->StackTmrSvcWMP,
                                SysConf.SysHealth->StackIdleWMP);

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static LogErrType_t LOG_writeIMUStat(const ImuStat_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and create file if it doesn't exist */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_IMUSTAT, SD_getCWD());
    if (!SD_initFile(log_filename, "ID,start_time,samples_preTRG,samples_postTRG,freq_LP,freq_HP,freq_AA,OpMode,level_trg,decimation\n"))
    {
        return LOG_ERR_FCREATE;
    }

    /* Buffer fill */
    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%07lu,%llu,%u,%u,%u,%u,%u,%u,%u,%u\n",
                                data->ID,
                                data->Start,
                                data->Samples_PreTrg,
                                data->Samples_PostTrg,
                                data->Freq_LP,
                                data->Freq_HP,
                                data->Freq_AA,
                                data->OpMode,
                                data->Level_Trg,
                                data->Decimation);

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static LogErrType_t LOG_writeIMUData(const ImuData_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and write data to file */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_IMUDATA, SD_getCWD(), data->ID);

    return (SD_writeFile(log_filename, ((uint8_t*)data + 4), (rb_imudata.itemsize - 4)) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static LogErrType_t LOG_writeInclino(const dpp_inclino_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and create file if it doesn't exist */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_INCLINO, SD_getCWD());
    if (!SD_initFile(log_filename, "timestamp,sample,acc_x,acc_y,acc_z,ang_x,ang_y,ang_z,temp\n"))
    {
        return LOG_ERR_FCREATE;
    }

    /* Buffer fill */
    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%lu,%u,%u,%u,%u,%u,%u,%u\n",
                                RTC_getEpoch(),
                                data->acc_x,
                                data->acc_y,
                                data->acc_z,
                                data->ang_x,
                                data->ang_y,
                                data->ang_z,
                                data->temperature);

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static LogErrType_t LOG_writeCOMData(const dpp_message_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and create file if it doesn't exist */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_COMDATA, SD_getCWD());
    if (!SD_initFile(log_filename, "timestamp,type,data\n"))
    {
        return LOG_ERR_FCREATE;
    }

    /* Buffer fill */
    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%llu,%u,", data->header.generation_time, data->header.type);
    /* Append message payload as hex string (is already zero-terminated) */
    buf_len += bytes_to_hexstr((uint8_t*)data->payload, data->header.payload_len, log_buf + buf_len, (SD_FILE_MAX_LINE_LEN - buf_len));
    log_buf[buf_len - 1] = '\n';    // -1 to overwrite the '0' character

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static LogErrType_t LOG_writeTSync(const TSyncInfo_t* data)
{
    if (!data) return LOG_ERR_INVALID;

    /* Compose filename and open file in append mode */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_TSYNC, SD_getCWD());
    if (!SD_initFile(log_filename, "rtc_uptime_ms,rtc_timestamp_us,rcvd_timestamp_us,duration_s,rtc_offset_us,drift_estimated_ppm,drift_comp_ppm,tsync_method\n"))
    {
        return LOG_ERR_FCREATE;
    }

    /* Buffer fill */
    uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%llu,%llu,%llu,%u,%ld,%d,%d,%u\n",
                                data->uptime,
                                data->rtc_timestamp,
                                data->rcvd_timestamp,
                                data->duration,
                                data->offset,
                                data->drift_estimated,
                                data->drift_comp,
                                TSYNC_METHOD);

    /* Write data to file */
    return (SD_writeFile(log_filename, log_buf, buf_len) ? LOG_ERR_OK : LOG_ERR_FWRITE);
}


static void LOG_mountUSBMSC(void)
{
    static bool msc_mount_delayed = false;                              /* Helper state to ensure the mounting is only delayed once (avoid infinite loop) */

    if (SysConf.Geo->Status != GEO_ACQSTATUS_STOPPED)
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "Waiting for Geo task to finish");
        vTaskDelay(pdMS_TO_TICKS(1000));                                /* Give the Geo task 1s to finish the current sampling */
        RTOS_queueSendLog(LOGTYPE_USB_MSC_MOUNT);
    }
    else if (uxQueueMessagesWaiting(xQueue_Log) && !msc_mount_delayed)  /* There are still items in the log queue */
    {
        DEBUG_PRINT(DBG_LVL_VERBOSE, "USB MSC postponed");
        RTOS_queueSendLog(LOGTYPE_USB_MSC_MOUNT);
        msc_mount_delayed = true;
    }
    else
    {
        if (SysConf.USB->deviceClass != NONE)
        {
            USB_DEVICE_DeInit();
        }
        SD_eject();
        BSP_SD_Init();
        USB_DEVICE_Init(MSC);
        RTOS_event(EVENT_GEOPHONE3X_MSC_MOUNT, 0, EVTLOG_COM);
        RTOS_buzzer(BUZZER_TIME_USBMSC_MS, 1);
        LED_ALL_BLINK();
        DEBUG_PRINT(DBG_LVL_INFO, "USB MSC mounted");
        msc_mount_delayed = false;
    }
}


static void LOG_checkNotifications(void)
{
    static uint32_t call_cnt = 0;

    if (call_cnt < 1000)
    {
        call_cnt++;
        return;
    }

    RTOS_SUSPEND();

    if (!uxQueueMessagesWaiting(xQueue_Log))
    {
        uint32_t val = (!RB_isEmpty(&rb_geo)             ) |
                       (!RB_isEmpty(&rb_event)       << 1) |
                       (!RB_isEmpty(&rb_health)      << 2) |
                       (!RB_isEmpty(&rb_imustat)     << 3) |
                       (!RB_isEmpty(&rb_imudata)     << 4) |
                       (!RB_isEmpty(&rb_gnssdata)    << 5) |
                       (!RB_isEmpty(&rb_inclinodata) << 6) |
                       (!RB_isEmpty(&rb_comdata)     << 7) |
                       (!RB_isEmpty(&rb_tsync)       << 8) |
                       (!RB_isEmpty(&rb_schedule)    << 9);
        if (val)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "LOG task notification discrepancy detected (0x%x)", val);
            RTOS_event(EVENT_GEOPHONE3X_LOG_ERROR, (val << 8) | LOG_ERR_NOTIFY, EVTLOG_ALL);
            //TODO proper handling
        }
        call_cnt = 0;
    }
    RTOS_RESUME();
}


bool LOG_streamData(StreamingDataType_t type, const void* data, uint16_t len, uint32_t arg)
{
#if !STREAMING_BACKLOG_MODE
    if (!data || !len || len > STREAMING_DATA_MAX_LEN) { return false; }

    if (RB_isFull(&rb_stream))
    {
        DEBUG_PRINT(DBG_LVL_WARNING, "Streaming buffer is full");
        return false;
    }

    StreamingData_t* pkt = (StreamingData_t*)RB_insert(&rb_stream, 0);
    pkt->type            = type;
    pkt->len             = len;
    pkt->arg             = arg;
    memcpy(pkt->data, data, len);

    NOTIFY_STREAM_TASK();

#endif
    return true;
}

#if LOG_ADCDATA_CALC_PSD

static void LOG_calcPSD(const int32_t* samples, const uint32_t num_samples, const uint32_t sample_rate)
{
    if (!samples || !num_samples || !sample_rate) { return; }

    PSD_addSamples(samples, num_samples, sample_rate);    /* Takes ~2ms whenever PSD_FFT_FRAME_SIZE samples have been collected */

    if (PSD_getFrameCount() > (LOG_PSD_AVG_PERIOD_S * sample_rate * 2 / PSD_FFT_FRAME_SIZE))
    {
        static dpp_geophone_spec_t spec;
        memset(&spec, 0, sizeof(spec));
        spec.info = DPP_GEO_SPEC_INFO_NFFT_1024 | DPP_GEO_SPEC_INFO_RATE_125HZ | DPP_GEO_SPEC_INFO_TYPE_PSD;
        spec.enc  = DPP_GEO_SPEC_ENC_BINS_1HZ_SUB | DPP_GEO_SPEC_ENC_TYPE_UINT8;
        uint32_t len = PSD_getAverage(spec.data, sizeof(spec.data));
        Bolt_enqueueMessageEx(DPP_MSG_TYPE_GEO_SPEC, (uint8_t*)&spec, len + DPP_GEO_SPEC_HDR_LEN);

        /* Write to SD card */
        uint32_t buf_len = snprintf(log_buf, SD_FILE_MAX_LINE_LEN, "%lu", RTC_getEpoch());
        for (uint32_t i = 0; i < len; i++)
        {
            buf_len += snprintf(&log_buf[buf_len], SD_FILE_MAX_LINE_LEN - buf_len, ",%d", spec.data[i]);
        }
        log_buf[buf_len++] = '\n';
        snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_PSD, SD_getCWD());
        SD_writeFile(log_filename, log_buf, buf_len);
    }
}

#endif

#if LOG_ADCDATA_MINISEED

#if LOG_TASK_BUFFERED_MSEED

/* Write compressed miniseed file, requires an intermediate buffer to collect sufficient samples before writing to the card */
static LogErrType_t LOG_writeADCDataMiniseed(ADCID_t adc_id, const ADCData_t* adc_data, bool flush)
{
    typedef struct
    {
        uint64_t    timestamp;
        uint32_t    acq_id;
        uint32_t    sample_cnt;
        uint16_t    sample_rate;
        int32_t     samples[MSEED_MAX_SAMPLES_PER_BLOCK];
    #if LOG_ADCDATA_APPEND_CHECKSUM
        uint32_t    checksum;
    #endif
    } LOGMSeedBuf_t;

    /* requires GEO_NUM_AXES buffers, one for each file (channel) */
    static LOGMSeedBuf_t mseed_buffer[GEO_NUM_AXES]      = { 0 };
    static uint8_t       mseed_compr_buf[MSEED_DATA_LEN] = { 0 };

    LogErrType_t   res             = LOG_ERR_OK;
    LOGMSeedBuf_t* mseed_buf       = &mseed_buffer[adc_id - 1];
    uint32_t       num_new_samples = 0;

    if (adc_data)
    {
        if ( (mseed_buf->acq_id != adc_data->acq_id) && (mseed_buf->sample_cnt != 0) )
        {
            DEBUG_PRINTF(DBG_LVL_ERROR, "Acq ID changed, but sample_cnt is %lu", mseed_buf->sample_cnt);
            res = LOG_ERR_BUFFER;
        }
        mseed_buf->acq_id = adc_data->acq_id;
        num_new_samples   = adc_data->len / GEO_SAMPLE_SIZE;
        if (num_new_samples == 0)
        {
            return LOG_ERR_INV_DATA;                  /* Potentially corrupted data */
        }
        mseed_buf->sample_rate = num_new_samples;     /* One chunk contains data of 1 second */
        if (mseed_buf->sample_cnt == 0)
        {
            mseed_buf->timestamp = adc_data->timestamp;
        }
        if ((mseed_buf->sample_cnt + num_new_samples) > MSEED_MAX_SAMPLES_PER_BLOCK)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "buffer overflow, tried to add %u samples to %u already stored for ADC %u", num_new_samples, mseed_buf->sample_cnt, adc_id);
            num_new_samples = MSEED_MAX_SAMPLES_PER_BLOCK - mseed_buf->sample_cnt;
            res = LOG_ERR_BUFFER;
        }
        ADC_convertRAWToInt32(adc_data->data, num_new_samples, &mseed_buf->samples[mseed_buf->sample_cnt]);

  #if LOG_ADCDATA_CALC_PSD
        if (adc_id == ADC_A1)
        {
            LOG_calcPSD(&mseed_buf->samples[mseed_buf->sample_cnt], num_new_samples, mseed_buf->sample_rate);
        }
  #endif

  #if LOG_ADCDATA_DOWNSAMPLE_FREQ
        static int32_t downsample_buf[LOG_ADCDATA_DOWNSAMPLE_FREQ];
        num_new_samples        = ADC_downsample(&mseed_buf->samples[mseed_buf->sample_cnt], num_new_samples, num_new_samples, LOG_ADCDATA_DOWNSAMPLE_FREQ, downsample_buf);
        mseed_buf->sample_rate = LOG_ADCDATA_DOWNSAMPLE_FREQ;
        memcpy(&mseed_buf->samples[mseed_buf->sample_cnt], downsample_buf, num_new_samples * sizeof(int32_t));
  #endif

        mseed_buf->sample_cnt += num_new_samples;
    }

    /* Convert to miniSEED format */
    while (mseed_buf->sample_cnt)
    {
        memset(mseed_compr_buf, 0, MSEED_DATA_LEN);
        const uint32_t packed_samples = MSeed_compressSteim1(mseed_buf->samples, mseed_buf->sample_cnt, (int32_t*)(mseed_compr_buf + sizeof(mseed_header_t)), MSEED_DATA_LEN - sizeof(mseed_header_t), 0, 0);
        if (packed_samples == 0)
        {
            DEBUG_PRINTF(DBG_LVL_WARNING, "LOG mseed compression failed (%lu, %lu)", packed_samples, mseed_buf->sample_cnt);
            res = LOG_ERR_MSEED;
            mseed_buf->sample_cnt = 0;
            break;
        }
        /* If buffer full or 'flush', write to SD */
        if ( (packed_samples < mseed_buf->sample_cnt) || flush )
        {
            /* Compose header */
            MSeed_initHeader(mseed_compr_buf);
            MSeed_setHeaderFields((mseed_header_t*)mseed_compr_buf, adc_data->acq_id, adc_channel_mapping[adc_id][0], packed_samples, 0, mseed_buf->sample_rate, mseed_buf->timestamp);

            /* Adjust the number of remaining samples and copy them to the beginning of the buffer */
            mseed_buf->sample_cnt -= packed_samples;
            memcpy(mseed_buf->samples, &mseed_buf->samples[packed_samples], mseed_buf->sample_cnt * sizeof(int32_t));

            /* Adjust the timestamp */
            if (mseed_buf->sample_cnt && adc_data)
            {
                mseed_buf->timestamp = adc_data->timestamp + (S_TO_US((uint64_t)(num_new_samples - mseed_buf->sample_cnt)) / mseed_buf->sample_rate);
            }

            /* Write to file */
            snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_ADCDATA, SD_getCWD(), mseed_buf->acq_id, adc_channel_mapping[adc_id]);
            if (!SD_writeFile(log_filename, mseed_compr_buf, MSEED_DATA_LEN))
            {
                res = LOG_ERR_FWRITE;
            }
  #if LOG_ADCDATA_APPEND_CHECKSUM
            /* Update the checksum */
            mseed_buf->checksum = crc32(mseed_compr_buf, MSEED_DATA_LEN, mseed_buf->checksum);
            if (!mseed_buf->sample_cnt && flush)    /* End of file? */
            {
                /* Write the checksum */
                if (!SD_writeFile(log_filename, &mseed_buf->checksum, sizeof(uint32_t)))
                {
                    res = LOG_ERR_FWRITE;
                }
                mseed_buf->checksum = 0;
            }
  #endif

  #if !STREAMING_TARGET_NONE
            LOG_streamData(STREAMING_DTYPE_GEO_A1 + (adc_id - 1), mseed_compr_buf, MSEED_DATA_LEN, mseed_buf->acq_id);
  #endif
        }
        else
        {
            /* Buffer not yet full */
            break;
        }
    }

    return res;
}

#else /* LOG_TASK_BUFFERED_MSEED */

static LogErrType_t LOG_writeADCDataMiniseed(ADCID_t adc_id, const ADCData_t* adc_data, bool finish_block)
{
    typedef struct
    {
        uint32_t    bytes_written;
        uint32_t    samples_written;
        int32_t     last_sample;
        uint32_t    acq_id;
    } LOGMSeedState_t;

    static LOGMSeedState_t mseed_state[GEO_NUM_AXES] = { 0 };
    static mseed_sample_t  mseed_conv_buf[ADC_MAX_SPS];
  #if MSEED_USE_COMPRESSION
    static uint8_t         mseed_compr_buf[MSEED_DATA_LEN - sizeof(mseed_header_t)];
  #endif

    LogErrType_t     res         = LOG_ERR_OK;
    LOGMSeedState_t* mseed       = &mseed_state[adc_id - 1];
    uint32_t         num_samples = 0;

    if (adc_data)
    {
        num_samples = adc_data->len / GEO_SAMPLE_SIZE;     /* Equals the sampling rate since each adc_data chunk is 1s long */
        if (num_samples == 0)
        {
            return LOG_ERR_INV_DATA;      /* Potentially corrupted data */
        }
        if ( (mseed->acq_id != adc_data->acq_id) && (mseed->bytes_written != 0) )
        {
            DEBUG_PRINTF(DBG_LVL_ERROR, "Acq ID changed, but bytes_written is %lu", mseed->bytes_written);
            mseed->samples_written = 0;
            mseed->bytes_written   = 0;
            mseed->last_sample     = 0;
            return LOG_ERR_BUFFER;
        }
        mseed->acq_id = adc_data->acq_id;
        ADC_convertRAWToInt32(adc_data->data, num_samples, mseed_conv_buf);

  #if LOG_ADCDATA_CALC_PSD
        if (adc_id == ADC_A1)
        {
            LOG_calcPSD(&mseed_buf->samples[mseed_buf->sample_cnt], num_new_samples, mseed_buf->sample_rate);
        }
  #endif

  #if LOG_ADCDATA_DOWNSAMPLE_FREQ
        static int32_t downsample_buf[LOG_ADCDATA_DOWNSAMPLE_FREQ];
        num_samples = ADC_downsample(mseed_conv_buf, num_samples, num_samples, LOG_ADCDATA_DOWNSAMPLE_FREQ, downsample_buf);
        memcpy(mseed_conv_buf, downsample_buf, num_samples * sizeof(int32_t));
  #endif
    }

    /* Compose filename and open file */
    snprintf(log_filename, SD_FILE_MAX_PATH_LEN, "%s/" SD_FILENAME_ADCDATA, SD_getCWD(), mseed->acq_id, adc_channel_mapping[adc_id]);

    /* Loop until all samples have been written to file */
    uint32_t rem_samples = num_samples;
    while (rem_samples)
    {
        /* First data frame? */
        if (!mseed->bytes_written)
        {
            /* Write miniSEED header */
            mseed_header_t* header = MSeed_initHeader(0);
            MSeed_setHeaderFields(header, adc_data->acq_id, adc_channel_mapping[adc_id][0], 0, 0, num_samples, adc_data->timestamp + S_TO_US((uint64_t)(num_samples - rem_samples)) / num_samples);
            if (!SD_writeFile(log_filename, (uint8_t*)header, sizeof(mseed_header_t)))
            {
                res = LOG_ERR_FWRITE;
            }
            mseed->bytes_written   = sizeof(mseed_header_t);
            mseed->last_sample     = 0;
            mseed->samples_written = 0;
        }

        /* Compress samples */
  #if MSEED_USE_COMPRESSION
        uint32_t packed_bytes   = 0;
        int32_t  initial_diff   = (mseed->samples_written ? (mseed_conv_buf[num_samples - rem_samples] - mseed->last_sample) : 0);
        uint32_t packed_samples = MSeed_compressSteim1(&mseed_conv_buf[num_samples - rem_samples], rem_samples, (int32_t*)mseed_compr_buf, MSEED_DATA_LEN - mseed->bytes_written, initial_diff, &packed_bytes);
        rem_samples            -= packed_samples;
        mseed->samples_written += packed_samples;
        mseed->bytes_written   += packed_bytes;
        mseed->last_sample      = mseed_conv_buf[num_samples - rem_samples - 1];
        if ( ((packed_bytes == 0) && (mseed->bytes_written < MSEED_DATA_LEN)) || (mseed->bytes_written > MSEED_DATA_LEN) )
        {
            /* TODO: figure out why this happens */
            DEBUG_PRINTF(DBG_LVL_WARNING, "LOG mseed compression failed (%lu, %lu)", packed_samples, mseed->bytes_written);
            res = LOG_ERR_MSEED;
            mseed->bytes_written = 0;   /* drop current buffer */
            break;
        }

        /* Write to file */
        if (!SD_writeFile(log_filename, mseed_compr_buf, packed_bytes))
        {
            res = LOG_ERR_FWRITE;
        }
        //DEBUG_PRINTF(DBG_LVL_VERBOSE, "LOG %lu samples packed into %lu bytes", packed_samples, packed_bytes);

  #else
        /* Write to file */
        uint32_t samples_to_write = MIN(rem_samples, (MSEED_DATA_LEN - mseed->bytes_written) / MSEED_SAMPLE_SIZE);
        if (!SD_writeFile(log_filename, &mseed_conv_buf[num_samples - rem_samples], samples_to_write * MSEED_SAMPLE_SIZE))
        {
            res = LOG_ERR_FWRITE;
        }
        mseed->bytes_written   += samples_to_write * MSEED_SAMPLE_SIZE;
        mseed->samples_written += samples_to_write;
        rem_samples            -= samples_to_write;
  #endif

        /* Block full? */
        if (mseed->bytes_written == MSEED_DATA_LEN)
        {
            /* Set number of samples */
            if (!SD_writeAtOffset(log_filename, -(int32_t)MSEED_DATA_LEN + MSEED_FSDH_NUMSAMPLES_OFS, (uint8_t*)&(mseed->samples_written), sizeof(uint16_t)))
            {
                res = LOG_ERR_FWRITE_OFS;
            }
  #if MSEED_USE_COMPRESSION
            /* Fix first data frame to avoid data integrity check errors */
            if (!SD_writeAtOffset(log_filename, -(int32_t)MSEED_DATA_LEN + sizeof(mseed_header_t) + MSEED_STEIM1_FRAME_LAST_OFS, (uint8_t*)&(mseed->last_sample), sizeof(int32_t)))
            {
                res = LOG_ERR_FWRITE_OFS;
            }
  #endif
            /* Reset */
            mseed->samples_written = 0;
            mseed->bytes_written   = 0;
            mseed->last_sample     = 0;
        }
    }
    /* If requested: close the current miniseed block */
    if (finish_block && mseed->bytes_written)
    {
        /* Set number of samples */
        if (!SD_writeAtOffset(log_filename, -(int32_t)mseed->bytes_written + MSEED_FSDH_NUMSAMPLES_OFS, (uint8_t*)&(mseed->samples_written), sizeof(uint16_t)))
        {
            res = LOG_ERR_FWRITE_OFS;
        }
  #if MSEED_USE_COMPRESSION
        /* Fix first data frame to avoid data integrity check errors */
        if (!SD_writeAtOffset(log_filename, -(int32_t)mseed->bytes_written + sizeof(mseed_header_t) + MSEED_STEIM1_FRAME_LAST_OFS, (uint8_t*)&(mseed->last_sample), sizeof(int32_t)))
        {
            res = LOG_ERR_FWRITE_OFS;
        }
  #endif
        /* Fill the remainder of the block with zeros */
        SD_appendPadding(log_filename, 0, MSEED_DATA_LEN - mseed->bytes_written);
        DEBUG_PRINTF(DBG_LVL_VERBOSE, "LOG mseed block closed (samples: %lu, padding: %lu)", mseed->samples_written, MSEED_DATA_LEN - mseed->bytes_written);
        /* Reset */
        mseed->samples_written = 0;
        mseed->bytes_written   = 0;
        mseed->last_sample     = 0;
    }

    return res;
}

#endif /* LOG_TASK_BUFFERED_MSEED */

#endif /* LOG_ADCDATA_MINISEED */
