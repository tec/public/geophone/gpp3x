/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "main.h"


/******************************************************************************/
/*            Cortex-M4 Processor Interruption and Exception Handlers         */
/******************************************************************************/

/**
 * @brief This function handles Non maskable interrupt.
 */
void NMI_Handler(void)
{

}

/**
 * @brief This function handles Hard fault interrupt.
 */
void HardFault_Handler(void)
{
    Error_Handler();
}

/**
 * @brief This function handles Memory management fault.
 */
void MemManage_Handler(void)
{
    Error_Handler();
}

/**
 * @brief This function handles Prefetch fault, memory access fault.
 */
void BusFault_Handler(void)
{
    Error_Handler();
}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void)
{
    Error_Handler();
}

/**
 * @brief This function handles Debug monitor.
 */
void DebugMon_Handler(void)
{

}

/******************************************************************************/
/*            Cortex-M4 Peripheral Interrupt Handlers                         */
/******************************************************************************/

/**
 * @brief DMA1 Channel2 ISR
 * @note  SPI1 DMA Rx
 */
void DMA1_Channel2_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_DMA_IRQHandler(hspi1.hdmarx);
    __ISR_EXIT();
}

/**
 * @brief DMA1 Channel3 ISR
 * @note  SPI1 DMA Tx
 */
void DMA1_Channel3_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_DMA_IRQHandler(hspi1.hdmatx);
    __ISR_EXIT();
}

/**
 * @brief DMA1 Channel4 ISR
 * @note  SPI2 DMA Rx
 */
void DMA1_Channel4_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_DMA_IRQHandler(hspi2.hdmarx);
    __ISR_EXIT();
}

/**
 * @brief DMA1 Channel5 ISR
 * @note  SPI2 DMA Tx
 */
void DMA1_Channel5_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_DMA_IRQHandler(hspi2.hdmatx);
    __ISR_EXIT();
}

/**
 * @brief DMA2 Channel5 ISR
 * @note  SDMMC DMA Tx, Rx
 */
void DMA2_Channel5_IRQHandler(void)
{
    __ISR_ENTER();
    if ((hsd1.Context == (SD_CONTEXT_DMA | SD_CONTEXT_READ_SINGLE_BLOCK)) ||
        (hsd1.Context == (SD_CONTEXT_DMA | SD_CONTEXT_READ_MULTIPLE_BLOCK)))
    {
        HAL_DMA_IRQHandler(hsd1.hdmarx);
    }
    else if ((hsd1.Context == (SD_CONTEXT_DMA | SD_CONTEXT_WRITE_SINGLE_BLOCK)) ||
             (hsd1.Context == (SD_CONTEXT_DMA | SD_CONTEXT_WRITE_MULTIPLE_BLOCK)))
    {
        HAL_DMA_IRQHandler(hsd1.hdmatx);
    }
    __ISR_EXIT();
}

void DMA2_Channel6_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_DMA_IRQHandler(&hdma_lpuart_tx);
    __ISR_EXIT();
}

/**
 * @brief DMA2 Channel7 ISR
 * @note  GNSS DMA Rx
 */
void DMA2_Channel7_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_DMA_IRQHandler(&hdma_usart1_rx);
    __ISR_EXIT();
}

/**
 * @brief EXTI_0 ISR
 * @note  ADC1_RDY
 */
void EXTI0_IRQHandler(void)
{
    __ISR_ENTER();

    if (__HAL_GPIO_EXTI_GET_IT(ADC1_RDY_Pin) != RESET)
    {
        ADC_transferSample(ADC_A1);
        __HAL_GPIO_EXTI_CLEAR_IT(ADC1_RDY_Pin);
    }

    __ISR_EXIT();
}

/**
 * @brief EXTI_1 ISR
 * @note  TRG+
 */
void EXTI1_IRQHandler(void)
{
    __ISR_ENTER();
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    RTOS_resumeFromStopMode();

    if (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_1) != RESET)
    {
        if (IS_RTOS_STARTED())
        {
            if (SysConf.OpMode & SYS_OPMODE_TRG)
            {
                Geo_initAcq(TRG_POS, SysConf.Geo->PostTrg, &xHigherPriorityTaskWoken);
            }
            /* Launch timer */
            if (!(htim3.Instance->CR1 & TIM_CR1_CEN))
            {
                HAL_TIM_Base_Start_IT(&htim3);
            }
        }
        /* Clear IT flag */
        __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_1);
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    __ISR_EXIT();
}

/**
 * @brief EXTI_2 ISR
 * @note  TRG-
 */
void EXTI2_IRQHandler(void)
{
    __ISR_ENTER();
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    RTOS_resumeFromStopMode();

    if (__HAL_GPIO_EXTI_GET_IT(GPIO_PIN_2) != RESET)
    {
        if (IS_RTOS_STARTED())
        {
            if (SysConf.OpMode & SYS_OPMODE_TRG)
            {
                Geo_initAcq(TRG_NEG, SysConf.Geo->PostTrg, &xHigherPriorityTaskWoken);
            }
            /* Launch timer */
            if (!(htim4.Instance->CR1 & TIM_CR1_CEN))
            {
                HAL_TIM_Base_Start_IT(&htim4);
            }
        }
        /* Clear IT flag */
        __HAL_GPIO_EXTI_CLEAR_IT(GPIO_PIN_2);
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    __ISR_EXIT();
}

/**
 * @brief EXTI_3 ISR
 * @note  ADC2_RDY
 */
void EXTI3_IRQHandler(void)
{
    __ISR_ENTER();

    if (__HAL_GPIO_EXTI_GET_IT(ADC2_RDY_Pin) != RESET)
    {
        ADC_transferSample(ADC_A2);
        __HAL_GPIO_EXTI_CLEAR_IT(ADC2_RDY_Pin);
    }

    __ISR_EXIT();
}

/**
 * @brief EXTI_4 ISR
 * @note  ADC3_RDY
 */
void EXTI4_IRQHandler(void)
{
    __ISR_ENTER();

    if (__HAL_GPIO_EXTI_GET_IT(ADC3_RDY_Pin) != RESET)
    {
        ADC_transferSample(ADC_A3);
        __HAL_GPIO_EXTI_CLEAR_IT(ADC3_RDY_Pin);
    }

    __ISR_EXIT();
}

/**
 * @brief EXTI_5_9 ISR
 * @note  6: GNSS_TPULSE
 *        8: BTN
 */
void EXTI9_5_IRQHandler(void)
{
    __ISR_ENTER();
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    RTOS_resumeFromStopMode();

#if USE_GNSS
    /* GNSS Timepulse */
    if (__HAL_GPIO_EXTI_GET_IT(GNSS_TPULSE_Pin) != RESET)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(GNSS_TPULSE_Pin);
        if (GNSS_TPULSE_STATE())
        {
  #if TSYNC_METHOD_GNSS
            TSync_capture();
            GNSS_timePulseCallback();
  #endif
  #if GNSS_PPS_LED_ON
            if (!(SysConf.OpMode & SYS_OPMODE_NOLEDS))
            {
                LED_G_ON();
            }
  #endif
        }
        else
        {
  #if GNSS_PPS_LED_ON
            LED_G_OFF();
  #endif
        }
    }
#endif

    /* User button */
    if (__HAL_GPIO_EXTI_GET_IT(BTN_Pin) != RESET)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(BTN_Pin);

        if (IS_RTOS_STARTED())
        {
            xTimerStartFromISR(xTimer_Button, &xHigherPriorityTaskWoken);
        }
    }

    /* DPP reset (reed switch or button) */
#ifdef DPP_RST_Pin
    if (__HAL_GPIO_EXTI_GET_IT(DPP_RST_Pin) != RESET)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(DPP_RST_Pin);

        if (IS_RTOS_STARTED())
        {
            xTimerStartFromISR(xTimer_Button, &xHigherPriorityTaskWoken);
        }
        //DEBUG_PRINT(DBG_LVL_VERBOSE, "DPP reset");
    }
#endif

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    __ISR_EXIT();
}

/**
 * @brief EXTI_10_15 ISR
 * @note  10: USB_DETECT
 *        12: BOLT_IND
 *        13: IMU_INT_XL
 *        14: IMU_INT_MAG
 *        15: BOLT_ACK
 */
void EXTI15_10_IRQHandler(void)
{
    __ISR_ENTER();
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    RTOS_resumeFromStopMode();

    /* USB_DETECT */
    /* Not implemented, as no action required */

    /* BOLT_IND */
    if (__HAL_GPIO_EXTI_GET_IT(BOLT_IND_Pin) != RESET)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(BOLT_IND_Pin);

        if (IS_RTOS_STARTED())
        {
            NOTIFY_BOLT_TASK_FROM_ISR(&xHigherPriorityTaskWoken);
        }
    }

    /* IMU_INT_XL */
    if (__HAL_GPIO_EXTI_GET_IT(IMU_INT_XL_Pin) != RESET)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(IMU_INT_XL_Pin);

        if (IS_RTOS_STARTED())
        {
            // FIXME: INT1 triggers despite all accelerometer interrupts being turned off and interrupt registers not indicating an active interrupt
            // xTaskNotifyFromISR(xTaskHandle_Imu, 1, eSetValueWithOverwrite, &xHigherPriorityTaskWoken);
        }
    }

    /* IMU_INT_MAG */
    if (__HAL_GPIO_EXTI_GET_IT(IMU_INT_MAG_Pin) != RESET)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(IMU_INT_MAG_Pin);

        if (IS_RTOS_STARTED())
        {
            xTaskNotifyFromISR(xTaskHandle_Imu, 1, eSetValueWithOverwrite, &xHigherPriorityTaskWoken);
        }
    }

    /* BOLT_ACK */
    if (__HAL_GPIO_EXTI_GET_IT(BOLT_ACK_Pin) != RESET)
    {
        __HAL_GPIO_EXTI_CLEAR_IT(BOLT_ACK_Pin);

        if (IS_RTOS_STARTED())
        {
            //if (BOLT_IS_ASSERTED())
            //{
            //    /* Use BOLT_ACK as an interrupt source from BOLT - currently done with busy-waiting */
            //    xTaskNotifyFromISR(xTaskHandle_Bolt, 1, eSetValueWithOverwrite, &xHigherPriorityTaskWoken);
            //}
        }
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    __ISR_EXIT();
}

/**
 * @brief RTC WKUP ISR
 */
void RTC_WKUP_IRQHandler(void)
{
    __ISR_ENTER();
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    RTOS_resumeFromStopMode();

    HAL_RTCEx_WakeUpTimerIRQHandler(&hrtc);

#if ADC_SYNC_PERIOD
    ADC_sync();
#endif

    /* Check if we should do a scheduled measurement */
    if (SysConf.OpMode & SYS_OPMODE_SCHED)
    {
        xTaskNotifyFromISR(xTaskHandle_Schedule, 1, eSetValueWithOverwrite, &xHigherPriorityTaskWoken);
    }

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
    __ISR_EXIT();
}

/**
 * @brief RTC Alarm ISR
 */
void RTC_Alarm_IRQHandler(void)
{
    __ISR_ENTER();
    //RTOS_resumeFromStopMode();
    HAL_RTC_AlarmIRQHandler(&hrtc);
    __ISR_EXIT();
}

/**
 * @brief SDMMC ISR
 */
void SDMMC1_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_SD_IRQHandler(&hsd1);
    __ISR_EXIT();
}

/**
 * @brief SPI1 ISR
 */
void SPI1_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_SPI_IRQHandler(&hspi1);
    __ISR_EXIT();
}

/**
 * @brief SPI2 ISR
 */
void SPI2_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_SPI_IRQHandler(&hspi2);
    __ISR_EXIT();
}

/**
 * @brief USART1 ISR
 */
void USART1_IRQHandler(void)
{
    __ISR_ENTER();

    /* Instead of using the default handler (HAL_UART_IRQHandler), wait for receiver timeout and then trigger transfer complete callback manually */
    if (__HAL_UART_GET_FLAG(&huart1, USART_ISR_RTOF))
    {
        __HAL_UART_CLEAR_FLAG(&huart1, USART_ICR_RTOCF);
        HAL_UART_RxCpltCallback(&huart1);
    }
    /* Check error flags */
    if ( __HAL_UART_GET_FLAG(&huart1, USART_ISR_ORE) ||
         __HAL_UART_GET_FLAG(&huart1, USART_ISR_FE)  ||
         __HAL_UART_GET_FLAG(&huart1, UART_CLEAR_NEF))
    {
        __HAL_UART_CLEAR_FLAG(&huart1, UART_CLEAR_OREF | UART_CLEAR_FEF | UART_CLEAR_NEF);
        HAL_NVIC_DisableIRQ(USART1_IRQn);
        HAL_NVIC_DisableIRQ(DMA2_Channel7_IRQn);
        HAL_UART_ErrorCallback(&huart1);
    }

    __ISR_EXIT();
}

/**
 * @brief LPUART1 ISR
 */
void LPUART1_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_UART_IRQHandler(&hlpuart1);
    __ISR_EXIT();
}

/**
 * @brief TIM3 ISR, used to debounce the counter for the positive triggers. RTOS system calls allowed.
 */
void TIM3_IRQHandler(void)
{
    __ISR_ENTER();
    if (__HAL_TIM_GET_FLAG(&htim3, TIM_FLAG_UPDATE) != RESET)
    {
        HAL_TIM_Base_Stop_IT(&htim3);

        /* Rising */
        if (TRG_IS_P())
        {
            /* Toggle LED */
#if !GNSS_PPS_LED_ON || !USE_GNSS
            if (!(SysConf.OpMode & SYS_OPMODE_NOLEDS))
            {
                LED_G_ON();
            }
#endif
        }
        /* Falling */
        else
        {
            /* Toggle LED */
#if !GNSS_PPS_LED_ON || !USE_GNSS
            LED_G_OFF();
#endif
        }

        /* Clear flag */
        __HAL_TIM_CLEAR_IT(&htim3, TIM_IT_UPDATE);
    }
    __ISR_EXIT();
}

/**
 * @brief TIM4 ISR, used to debounce the counter for the negative triggers. RTOS system calls allowed.
 */
void TIM4_IRQHandler(void)
{
    __ISR_ENTER();
    if (__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_UPDATE) != RESET)
    {
        HAL_TIM_Base_Stop_IT(&htim4);

        /* Rising */
        if (TRG_IS_N())
        {
            /* Toggle LED */
            if (!(SysConf.OpMode & SYS_OPMODE_NOLEDS))
            {
                LED_B_ON();
            }
        }
        /* Falling */
        else
        {
            /* Toggle LED */
            LED_B_OFF();
        }

        /* Clear flag */
        __HAL_TIM_CLEAR_IT(&htim4, TIM_IT_UPDATE);
    }
    __ISR_EXIT();
}

/**
 * @brief This function handles LPTIM1 global interrupt.
 */
void LPTIM1_IRQHandler(void)
{
  __ISR_ENTER();
  HAL_LPTIM_IRQHandler(&hlptim1);
  __ISR_EXIT();
}

/**
 * @brief USB OTG FS ISR
 */
void OTG_FS_IRQHandler(void)
{
    __ISR_ENTER();
    HAL_PCD_IRQHandler(&hpcd_USB_OTG_FS);
    __ISR_EXIT();
}

/**
 * @brief TIM17 ISR for HAL Timebase
 */
void TIM1_TRG_COM_TIM17_IRQHandler(void)
{
    if (__HAL_TIM_GET_FLAG(&htim17, TIM_FLAG_UPDATE) != RESET)
    {
        if (__HAL_TIM_GET_IT_SOURCE(&htim17, TIM_IT_UPDATE) != RESET)
        {
            __HAL_TIM_CLEAR_IT(&htim17, TIM_IT_UPDATE);
            HAL_IncTick();
        }
    }
}
