/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __LSM303C_H_
#define __LSM303C_H_


/* I2C Addresses -------------------------------------------------------------*/
#define IMU_XL_ADDR                     (0x1D << 1)
#define IMU_MAG_ADDR                    (0x1E << 1)

/* For magnetometer: Adjust register value MSB to auto increment */
#define AUTO_INCREMENT_REG_ADDR         0x80

/* Accelerometer Who Am I register -------------------------------------------*/
#define LSM303_ACC_WHO_AM_I             0x0F
#define LSM303_ACC_WHO_AM_I_VAL         0x41

/* Accelerometer Activity registers ------------------------------------------*/
#define LSM303_ACC_ACT_THS              0x1E // Threshold
#define LSM303_ACC_ACT_DUR              0x1E // Duration

/* Accelerometer Control register 1 ------------------------------------------*/
#define LSM303_ACC_CTRL1                0x20
#define LSM303_ACC_CTRL1_XEN            0x01 // X enable
#define LSM303_ACC_CTRL1_YEN            0x02 // Y enable
#define LSM303_ACC_CTRL1_ZEN            0x04 // Z enable
#define LSM303_ACC_CTRL1_BDU            0x08 // Block data update
#define LSM303_ACC_CTRL1_ODR_OFF        0x00 // ODR:   Off
#define LSM303_ACC_CTRL1_ODR_10Hz       0x10 //      10 Hz
#define LSM303_ACC_CTRL1_ODR_50Hz       0x20 //      50 Hz
#define LSM303_ACC_CTRL1_ODR_100Hz      0x30 //     100 Hz
#define LSM303_ACC_CTRL1_ODR_200Hz      0x40 //     200 Hz
#define LSM303_ACC_CTRL1_ODR_400Hz      0x50 //     400 Hz
#define LSM303_ACC_CTRL1_ODR_800Hz      0x60 //     800 Hz
#define LSM303_ACC_CTRL1_HR             0x80 // High resolution mode

/* Accelerometer Control register 2 ------------------------------------------*/
#define LSM303_ACC_CTRL2                0x21
#define LSM303_ACC_CTRL2_HPIS           0x03 // Enable filter on interrupt generation
#define LSM303_ACC_CTRL2_FDS            0x04 // Enable filter on data
#define LSM303_ACC_CTRL2_HPM_REF        0x08 // Enable reference signal for filtering
#define LSM303_ACC_CTRL2_CO50           0x00 // High-pass filter cutoff:  50Hz
#define LSM303_ACC_CTRL2_CO100          0x20 //                          100Hz
#define LSM303_ACC_CTRL2_CO9            0x40 //                            9Hz
#define LSM303_ACC_CTRL2_CO400          0x60 //                          400Hz

/* Accelerometer Control register 3 ------------------------------------------*/
#define LSM303_ACC_CTRL3                0x22
#define LSM303_ACC_CTRL3_INT_XL_DRDY    0x01 // INT_XL: Data ready
#define LSM303_ACC_CTRL3_INT_XL_FTH     0x02 // INT_XL: FIFO threshold signal
#define LSM303_ACC_CTRL3_INT_XL_OVR     0x04 // INT_XL: FIFO overrun signal
#define LSM303_ACC_CTRL3_INT_XL_IG1     0x08 // INT_XL: Interrupt generator 1
#define LSM303_ACC_CTRL3_INT_XL_IG2     0x10 // INT_XL: Interrupt generator 2
#define LSM303_ACC_CTRL3_INT_XL_INACT   0x20 // INT_XL: Inactivity interrupt
#define LSM303_ACC_CTRL3_STOP_FTH       0x40 // Enable FIFO threshold level
#define LSM303_ACC_CTRL3_FIFO_EN        0x80 // Enable FIFO

/* Accelerometer Control register 4 ------------------------------------------*/
#define LSM303_ACC_CTRL4                0x23
#define LSM303_ACC_CTRL4_SIM            0x01 // Enable SPI read (write always on)
#define LSM303_ACC_CTRL4_I2C_DISABLE    0x02 // Disable I2C interface
#define LSM303_ACC_CTRL4_IF_ADD_INC     0x04 // Enable Register address auto increment
#define LSM303_ACC_CTRL4_BW_SELECT      0x08 // 0: automatically selected according to ODR, 1: according to BW reg
#define LSM303_ACC_CTRL4_FS_2g          0x00 // Full scale: +-2g
#define LSM303_ACC_CTRL4_FS_4g          0x20 //             +-4g
#define LSM303_ACC_CTRL4_FS_8g          0x30 //             +-8g
#define LSM303_ACC_CTRL4_AA_400Hz       0x00 // AA filter BW: 400Hz
#define LSM303_ACC_CTRL4_AA_200Hz       0x40 //               200Hz
#define LSM303_ACC_CTRL4_AA_100Hz       0x80 //               100Hz
#define LSM303_ACC_CTRL4_AA_50Hz        0xC0 //                50Hz

/* Accelerometer Control registers 5 - 7 -------------------------------------*/
#define LSM303_ACC_CTRL5                0x24
#define LSM303_ACC_CTRL5_PP_OD          0x01 // Interrupt: open-drain (0: push-pull)
#define LSM303_ACC_CTRL5_IR_ACTIVE_LOW  0x02 //            active low (0: active high)
#define LSM303_ACC_CTRL5_ST_POS         0x04 // Enable self-test: positive
#define LSM303_ACC_CTRL5_ST_NEG         0x08 //                   negative
#define LSM303_ACC_CTRL5_DEC1           0x00 // Averaging length: 1 sample
#define LSM303_ACC_CTRL5_DEC2           0x10 //                   2 samples
#define LSM303_ACC_CTRL5_DEC4           0x20 //                   4 samples
#define LSM303_ACC_CTRL5_DEC8           0x30 //                   8 samples
#define LSM303_ACC_CTRL5_SOFT_RESET     0x40 // Reset
#define LSM303_ACC_CTRL5_DEBUG          0x80 // Enable debug stepping

#define LSM303_ACC_CTRL6                0x25 // Force reboot
#define LSM303_ACC_CTRL7                0x26 // Duration, latched interrupt, 4D interrupt

/* Accelerometer Status register ---------------------------------------------*/
#define LSM303_ACC_STATUS               0x27
#define LSM303_ACC_STATUS_ZYXDA         0x08 // Data available for X, Y and Z axis
#define LSM303_ACC_STATUS_ZYXOR         0x80 // Data overrun   for X, Y and Z axis

/* Accelerometer Data register -----------------------------------------------*/
#define LSM303_ACC_X_L                  0x28 // X low:  X[1: 8]
#define LSM303_ACC_X_H                  0x29 // X high: X[9:16]
#define LSM303_ACC_Y_L                  0x2A // Y low:  Y[1: 8]
#define LSM303_ACC_Y_H                  0x2B // Y high: Y[9:16]
#define LSM303_ACC_Z_L                  0x2C // Z low:  Z[1: 8]
#define LSM303_ACC_Z_H                  0x2D // Z high: Z[8:16]

/* Accelerometer FIFO registers ----------------------------------------------*/
#define LSM303_ACC_FIFO_CTRL            0x2E // FIFO Control register
#define LSM303_ACC_FIFO_CTRL_BYPASS     0x00 // Bypass mode - FIFO turned off
#define LSM303_ACC_FIFO_CTRL_FIFO       0x20 // FIFO mode   - Stops collecting data when FIFO is full.
#define LSM303_ACC_FIFO_CTRL_STREAM     0x40 // Stream mode - Overwrites data when FIFO is full.
#define LSM303_ACC_FIFO_CTRL_SF         0x60 // Stream mode until trigger deasserted, then FIFO   mode
#define LSM303_ACC_FIFO_CTRL_BS         0x80 // Bypass mode until trigger deasserted, then Stream mode
#define LSM303_ACC_FIFO_CTRL_BF         0xE0 // Bypass mode until trigger deasserted, then FIFO   mode

#define LSM303_ACC_FIFO_SRC             0x2F // FIFO Status register
#define LSM303_ACC_FIFO_SRC_EMPTY       0x20 // FIFO empty
#define LSM303_ACC_FIFO_SRC_OVR         0x40 // FIFO full
#define LSM303_ACC_FIFO_SRC_FTH         0x80 // FIFO reached threshold

/* Accelerometer Interrupt Generator registers 1 - 2 -------------------------*/
#define LSM303_ACC_IG1_CFG              0x30
#define LSM303_ACC_IG1_CFG_XLIE         0x01 // Enable interrupt generation on event: X low
#define LSM303_ACC_IG1_CFG_XHIE         0x02 //                                       X high
#define LSM303_ACC_IG1_CFG_YLIE         0x04 //                                       Y low
#define LSM303_ACC_IG1_CFG_YHIE         0x08 //                                       Y high
#define LSM303_ACC_IG1_CFG_ZLIE         0x10 //                                       Z low
#define LSM303_ACC_IG1_CFG_ZHIE         0x20 //                                       Z high
#define LSM303_ACC_IG1_CFG_6D           0x40 // Enable 6 direction detection
#define LSM303_ACC_IG1_CFG_AOI          0x80 // Combinatory logic of Interrupt event (0: OR, 1: AND)

#define LSM303_ACC_IG1_SRC              0x31 // IG 1: status
#define LSM303_ACC_IG1_SRC_ACTIVE       0x40 // IG 1: active (interrupt generated)

#define LSM303_ACC_IG1_THX              0x32 // IG 1: threshold X
#define LSM303_ACC_IG1_THY              0x33 // IG 1: threshold Y
#define LSM303_ACC_IG1_THZ              0x34 // IG 1: threshold Z
#define LSM303_ACC_IG1_DUR              0x35 // IG 1: duration

#define LSM303_ACC_IG2_CFG              0x36
#define LSM303_ACC_IG2_CFG_XLIE         0x01 // Enable interrupt generation on event: X low
#define LSM303_ACC_IG2_CFG_XHIE         0x02 //                                       X high
#define LSM303_ACC_IG2_CFG_YLIE         0x04 //                                       Y low
#define LSM303_ACC_IG2_CFG_YHIE         0x08 //                                       Y high
#define LSM303_ACC_IG2_CFG_ZLIE         0x10 //                                       Z low
#define LSM303_ACC_IG2_CFG_ZHIE         0x20 //                                       Z high
#define LSM303_ACC_IG2_CFG_6D           0x40 // Enable 6 direction detection
#define LSM303_ACC_IG2_CFG_AOI          0x80 // Combinatory logic of Interrupt event (0: OR, 1: AND)

#define LSM303_ACC_IG2_SRC              0x37 // IG 2: status
#define LSM303_ACC_IG2_SRC_ACTIVE       0x40 // IG 2: active (interrupt generated)

#define LSM303_ACC_IG2_THS              0x38 // IG 2: threshold
#define LSM303_ACC_IG2_DUR              0x39 // IG 2: duration

/* Accelerometer Reference registers -----------------------------------------*/
#define LSM303_ACC_REF_XL           0x3A // Reference X low
#define LSM303_ACC_REF_XH           0x3B //           X high
#define LSM303_ACC_REF_YL           0x3C //           Y low
#define LSM303_ACC_REF_YH           0x3D //           Y high
#define LSM303_ACC_REF_ZL           0x3E //           Z low
#define LSM303_ACC_REF_ZH           0x3F //           Z high


/* Magnetometer Who Am I register --------------------------------------------*/
#define LSM303_MAG_WHO_AM_I         0x0F
#define LSM303_MAG_WHO_AM_I_VAL     0x3D

/* Magnetometer Control register 1 -------------------------------------------*/
#define LSM303_MAG_CTRL1            0x20
#define LSM303_MAG_CTRL1_ST         0x01 // Enable self-test
#define LSM303_MAG_CTRL1_ODR0_625   0x00 // ODR: 0.625Hz
#define LSM303_MAG_CTRL1_ODR1_25    0x04 //      1.25Hz
#define LSM303_MAG_CTRL1_ODR2_5     0x08 //      2.5Hz
#define LSM303_MAG_CTRL1_ODR5       0x0C //      5Hz
#define LSM303_MAG_CTRL1_ODR10      0x10 //     10Hz
#define LSM303_MAG_CTRL1_ODR20      0x14 //     20Hz
#define LSM303_MAG_CTRL1_ODR40      0x18 //     40Hz
#define LSM303_MAG_CTRL1_ODR80      0x1C //     80Hz
#define LSM303_MAG_CTRL1_OM_LP      0x00 // Op mode: Low power
#define LSM303_MAG_CTRL1_OM_MP      0x20 // Op mode: Medium performance
#define LSM303_MAG_CTRL1_OM_HP      0x40 // Op mode: High performance
#define LSM303_MAG_CTRL1_OM_UP      0x60 // Op mode: Ultra-high performance
#define LSM303_MAG_CTRL1_TEMP_EN    0x80 // Enable temperature sensor

/* Magnetometer Control register 2 -------------------------------------------*/
#define LSM303_MAG_CTRL2            0x21
#define LSM303_MAG_CTRL2_SOFT_RST   0x04 // Reset config and user registers
#define LSM303_MAG_CTRL2_REBOOT     0x08 // Reboot memory content
#define LSM303_MAG_CTRL2_FS1_16g    0x60 // Full-scale configuration: +-16 gauss

/* Magnetometer Control register 3 -------------------------------------------*/
#define LSM303_MAG_CTRL3            0x22
#define LSM303_MAG_CTRL3_CC         0x00 // Continuous-conversion mode (sample data with given frequency)
#define LSM303_MAG_CTRL3_SC         0x01 // Single-conversion mode (sample once and wait for read; mandatory for ODR 0.625 - 80 Hz (manual error?))
#define LSM303_MAG_CTRL3_PD         0x02 // Power-down
#define LSM303_MAG_CTRL3_SIM        0x04 // Enable SPI read (write always on)
#define LSM303_MAG_CTRL3_LP         0x20 // Low-power mode
#define LSM303_MAG_CTRL3_I2C_DIS    0x80 // Disable I2C

/* Magnetometer Control registers 4 - 5 --------------------------------------*/
#define LSM303_MAG_CTRL4            0x23
#define LSM303_MAG_CTRL4_LE         0x02 // Little endian data selection
#define LSM303_MAG_CTRL4_OM_LP      0x00 // Z-axis Op mode: Low power
#define LSM303_MAG_CTRL4_OM_MP      0x04 // Z-axis Op mode: Medium performance
#define LSM303_MAG_CTRL4_OM_HP      0x08 // Z-axis Op mode: High performance
#define LSM303_MAG_CTRL4_OM_UP      0x0C // Z-axis Op mode: Ultra-high performance

#define LSM303_MAG_CTRL5            0x24
#define LSM303_MAG_CTRL5_BDU        0x40 // Block data update

/* Magnetometer Status register ----------------------------------------------*/
#define LSM303_MAG_STATUS           0x27
#define LSM303_MAG_STATUS_ZYXDA     0x08 // Data available for X, Y and Z axis
#define LSM303_MAG_STATUS_ZYXOR     0x80 // Data overrun   for X, Y and Z axis

/* Magnetometer Data registers -----------------------------------------------*/
#define LSM303_MAG_X_L              0x28 // X low:  X[1: 8]
#define LSM303_MAG_X_H              0x29 // X high: X[9:16]
#define LSM303_MAG_Y_L              0x2A // Y low:  Y[1: 8]
#define LSM303_MAG_Y_H              0x2B // Y high: Y[9:16]
#define LSM303_MAG_Z_L              0x2C // Z low:  Z[1: 8]
#define LSM303_MAG_Z_H              0x2D // Z high: Z[9:16]

#define LSM303_MAG_TEMP_L           0x2E // Temp low
#define LSM303_MAG_TEMP_H           0x2F // Temp high

/* Magnetometer Interrupt Generator registers --------------------------------*/
#define LSM303_MAG_IG_CFG           0x30 // IG config
#define LSM303_MAG_IG_CFG_INT_MAG   0x09 // Enable INT_MAG interrupt
#define LSM303_MAG_IG_CFG_ACTIVE_H  0x0C // Active high (0: low)
#define LSM303_MAG_IG_CFG_ZIEN      0x28 // Enable on axis: Z
#define LSM303_MAG_IG_CFG_YIEN      0x48 //                 Y
#define LSM303_MAG_IG_CFG_XIEN      0x88 //                 X

#define LSM303_MAG_IG_SRC           0x31 // IG status
#define LSM303_MAG_IG_SRC_ACTIVE    0x01 // IG: active (interrupt generated)

#define LSM303_MAG_IG_THS_L         0x32 // IG threshold low
#define LSM303_MAG_IG_THS_H         0x33 //              high

/* Debug Printing ------------------------------------------------------------*/
#define IMU_PRINT_OFF               (0x00)    // Default: Printing off
#define IMU_PRINT_ACC               (1 << 0)  // Bit 0: Print accelerometer data
#define IMU_PRINT_MAG               (1 << 1)  // Bit 1: Print magnetometer data
#define IMU_PRINT_DEBUG             (1 << 2)  // Bit 2: Print debug messages

/* Enumerations --------------------------------------------------------------*/
typedef enum
{
    IMU_SINGLE_SHOT   = 0, // Single-shot: only record when ACG is triggered
    IMU_TRIGGERED_HP  = 1, // Triggered mode, with high-pass
    IMU_TRIGGERED_REF = 2, // Triggered mode, with reference
    IMU_CONT          = 3  // Stream mode (always-on)
} ImuOpMode_t;

/* Functions -----------------------------------------------------------------*/
void        IMU_init();
void        IMU_configModeTriggeredHP();
void        IMU_configModeTriggeredRef();
void        IMU_configModeStream(uint8_t signalFifoOverrun);

void        IMU_setHighODR();
void        IMU_setLowODR();
void        IMU_triggerEnable();
void        IMU_triggerDisable();
void        IMU_setReferenceSignal();
uint16_t    IMU_dataCalculateSAD(uint8_t numOfData);
void        IMU_calculateAvg(rb_t* rb);
void        IMU_magEnable();
void        IMU_magDisable();
void        IMU_accEnable();
void        IMU_accDisable();

uint8_t     IMU_emptyFifo(bool notifyLogging, rb_t* ringBuffer);
uint8_t     IMU_readFifoLevel();
uint8_t     IMU_readFromFifo(uint8_t count, bool notifyLogging, rb_t* ringBuffer);

void        IMU_printConfigs();
void        IMU_printDataPoint(ImuData_t* imu_buf);

#endif /* __LSM303C_H_ */
