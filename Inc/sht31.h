/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SHT31_H
#define __SHT31_H


/* Definitions ---------------------------------------------------------------*/

#ifndef SHT31_ADDR
// Address A - ADDR to VSS
#define SHT31_ADDR                          (0x44 << 1)
// Address B - ADDR to VDD
//#define SHT31_ADDR (0x45 << 1)
#endif /* SHT31_ADDR */

#if !DBG_PRINT_TARGET_SWO || DEBUG_PRINT_BASEBOARD
#define SHT31_DEGREE_CELSIUS_SIGN           "°C"
#else
#define SHT31_DEGREE_CELSIUS_SIGN           "\260C"
#endif

#define SHT31_MEASUREMENT_TRIES             2
#define SHT31_I2C_TIMEOUT_MS                20

/* The following delay have been determined experimentally */
#define SHT31_POWERON_DELAY_MS              10
#define SHT31_RESET_DELAY_MS                10
#define SHT31_MEASUREMENT_DELAY_MS          30

/* Commands ------------------------------------------------------------------*/

// Single-shot
#define SHT31_CMD_MEAS_SINGLE_CLK_STR_EN    0x2C
#define SHT31_CMD_MEAS_SINGLE_CLK_STR_EN_H  0x06
#define SHT31_CMD_MEAS_SINGLE_CLK_STR_EN_M  0x0D
#define SHT31_CMD_MEAS_SINGLE_CLK_STR_EN_L  0x10

#define SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS   0x24
#define SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS_H 0x00
#define SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS_M 0x0B
#define SHT31_CMD_MEAS_SINGLE_CLK_STR_DIS_L 0x16

// Periodic
#define SHT31_CMD_MEAS_PERIODIC_MSP_0_5     0x20
#define SHT31_CMD_MEAS_PERIODIC_MSP_0_5_H   0x32
#define SHT31_CMD_MEAS_PERIODIC_MSP_0_5_M   0x24
#define SHT31_CMD_MEAS_PERIODIC_MSP_0_5_L   0x2F

#define SHT31_CMD_MEAS_PERIODIC_MSP_1       0x21
#define SHT31_CMD_MEAS_PERIODIC_MSP_1_H     0x30
#define SHT31_CMD_MEAS_PERIODIC_MSP_1_M     0x26
#define SHT31_CMD_MEAS_PERIODIC_MSP_1_L     0x2D

#define SHT31_CMD_MEAS_PERIODIC_MSP_2       0x22
#define SHT31_CMD_MEAS_PERIODIC_MSP_2_H     0x36
#define SHT31_CMD_MEAS_PERIODIC_MSP_2_M     0x20
#define SHT31_CMD_MEAS_PERIODIC_MSP_2_L     0x2B

#define SHT31_CMD_MEAS_PERIODIC_MSP_4       0x23
#define SHT31_CMD_MEAS_PERIODIC_MSP_4_H     0x34
#define SHT31_CMD_MEAS_PERIODIC_MSP_4_M     0x22
#define SHT31_CMD_MEAS_PERIODIC_MSP_4_L     0x29

#define SHT31_CMD_MEAS_PERIODIC_MSP_10      0x27
#define SHT31_CMD_MEAS_PERIODIC_MSP_10_H    0x37
#define SHT31_CMD_MEAS_PERIODIC_MSP_10_M    0x21
#define SHT31_CMD_MEAS_PERIODIC_MSP_10_L    0x2A

#define SHT31_CMD_MEAS_PERIODIC_READ_MSB    0xE0
#define SHT31_CMD_MEAS_PERIODIC_READ_LSB    0x00

#define SHT31_CMD_MEAS_PERIODIC_ART_MSB     0x2B
#define SHT31_CMD_MEAS_PERIODIC_ART_LSB     0x32

#define SHT31_CMD_MEAS_PERIODIC_STOP_MSB    0x30
#define SHT31_CMD_MEAS_PERIODIC_STOP_LSB    0x93

// Reset
#define SHT31_CMD_RESET_SOFT_MSB            0x30
#define SHT31_CMD_RESET_SOFT_LSB            0xA2

#define SHT31_CMD_RESET_GENERAL_ADDR        0x00
#define SHT31_CMD_RESET_GENERAL             0x06

// Heater
#define SHT31_CMD_HEATER_MSB                0x30
#define SHT31_CMD_HEATER_EN_LSB             0x6D
#define SHT31_CMD_HEATER_DIS_LSB            0x66

// Status
#define SHT31_CMD_STATUS_READ_MSB           0xF3
#define SHT31_CMD_STATUS_READ_LSB           0x2D

#define SHT31_STATUS_ALERT_PENDING          (1 << 15)
#define SHT31_STATUS_HEATER_EN              (1 << 13)
#define SHT31_STATUS_RH_ALERT               (1 << 11)
#define SHT31_STATUS_T_ALERT                (1 << 10)
#define SHT31_STATUS_RESET_DETECTED         (1 <<  4)
#define SHT31_STATUS_CMD_UNSUCCESSFUL       (1 <<  1)
#define SHT31_STATUS_CRC_FAILED             (1 <<  0)

#define SHT31_CMD_STATUS_CLEAR_MSB          0x30
#define SHT31_CMD_STATUS_CLEAR_LSB          0x41


/* Macros --------------------------------------------------------------------*/
#define SHT31_HUMIDITY(hum_d)               (( (int32_t)(hum_d)  * 10000 / 65535)       )
#define SHT31_TEMP_C(temp_d)                (( (int32_t)(temp_d) * 17500 / 65535) - 4500)
#define SHT31_TEMP_F(temp_d)                (( (int32_t)(temp_d) * 31500 / 65535) - 4900)

/* Enumerations --------------------------------------------------------------*/
typedef enum
{
    HIGH   = 0,
    MEDIUM = 1,
    LOW    = 2
} repeatability_mode_t;

typedef enum
{
    MSP_0_5 = 0,
    MSP_1   = 1,
    MSP_2   = 2,
    MSP_4   = 3,
    MSP_10  = 4
} measurements_per_second_t;

/* Functions -----------------------------------------------------------------*/

bool SHT31_init(void);
bool SHT31_reset(bool soft);
bool SHT31_measurementSingleShot(repeatability_mode_t repeatability, bool clock_stretching, uint8_t* result);
bool SHT31_measurementStart(repeatability_mode_t repeatability, measurements_per_second_t mps);
bool SHT31_measurementRead(void);
bool SHT31_measurementAccelerated(void);
bool SHT31_measurementStop(void);
bool SHT31_heaterSetStatus(uint32_t enable);
bool SHT31_getStatus(void);
bool SHT31_clearStatus(void);
void SHT31_printTempHumidity(int16_t temp, uint16_t humidity);
bool SHT31_readTempHumidity(int16_t* temp, uint16_t* humidity);


#endif /* __SHT31_H */
