/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/*** Application Information **************************************************/
#define INFO_FW_VER_MAJOR           2               /* Max:  5 */
#define INFO_FW_VER_MINOR           7               /* Max: 99 */
#define INFO_FW_VER_PATCH           5               /* Max: 99 */

/*** Application Configuration ************************************************/
#define DPP_DEVICE_ID               22015           /* Device ID (220xx for GPP3X and 210xx for GPP1X) */

/* ADC Config */
#define ADC_PGA                     4               /* PGA */
#define ADC_SPS                     125             /* Default sampling rate (samples per second) */
#define ADC_MAX_SPS                 ADC_SPS         /* Max. configurable sampling rate (a lower value will result in lower SRAM usage) */
#define ADC_A1_NAME                 "Z"             /* ADC mapping: A1 = Z (vertical) */
#define ADC_A2_NAME                 "E"             /* ADC mapping: A2 = E (east) */
#define ADC_A3_NAME                 "N"             /* ADC mapping: A3 = N (north) */
#define ADC_SYNC_PERIOD             0               /* ADC SYNC period in seconds, if set to a value > 0 the samples will be aligned to the RTC clock (sampling starts at the full second) */

/* Geophone Task Config */
#define GEO_POSTTRG_S               1               /* Acquisition time in seconds after the last trigger (only relevant in triggered mode) */
#define GEO_TRG_POS_TH              1700U           /* Positive trigger threshold in mV */
#define GEO_TRG_NEG_TH              1300U           /* Negative trigger threshold in mV */
#define GEO_TIMEOUT_S               60              /* Acquisition timeout in seconds (only relevant in triggered mode) */
#define GEO_SAMPLE_SIZE             ADC_SAMPLE_SIZE /* Size of one sample in bytes (1, 2 or 3) */
#define GEO_CONT_ACQ_FILE_LEN_S     3600            /* In continuous mode, a new Acq ID (and file) will be used every x seconds */
#define GEO_STORE_TH_IN_LSB         0               /* Whether to use the LSB of each ADC sample to indicate whether the threshold has been exceeded */
#define GEO_WAIT_FOR_TSYNC          0               /* If set to 1: at startup, do not start sampling before the time has been synchronized (in continuous mode only) */

/* GNSS Task Config */
#define GNSS_PERIOD_S               900             /* If set to a value >0, the GNSS receiver will collect GNSS_NUM_OF_SAMPLES every GNSS_PERIOD_S seconds */
#define GNSS_NUM_OF_SAMPLES         930             /* Number of consecutive GNSS samples to collect (if PERIOD_S is enabled) */
#define GNSS_PPS_LED_ON             0               /* If set to 1, the PPS will be shown on the green LED */
#define GNSS_DEFAULT_LEAP_S         18              /* Default value for leap seconds */
#define GNSS_ALWAYS_ON              (!USE_STOP2)    /* Whether the GNSS receiver should be permanently powered */
#define GNSS_RUNTIME_WARN_TH_MS     3000            /* Print warning if task runs longer than the specified ms */

/* IMU Task Config */
#define IMU_FREQ                    100             /* Sampling frequency [Hz] */
#define IMU_FREQ_AA                 50              /* Anti-aliasing frequency [Hz] */
#define IMU_OP_MODE                 IMU_SINGLE_SHOT /* Operation mode (see library) */
#define IMU_TRG_LEVEL               1               /* Threshold level [16mg @ FS = 2g] */
#define IMU_DATA_DEC                2               /* Data decimation [number of samples / data point] */
#define IMU_FILTER_SIZE             11              /* Filter size for median filtering */
#define IMU_SAD_STABILITY_TH        150             /* Max data delta that determines that sensor is stable again after excitation */
#define IMU_SAD_SIZE                100             /* Number of data points that SAD should be calculated on */
#define IMU_SAMPLE_WITH_GEO         0               /* Whether IMU should be sampled together with the geophone */

/* Inclinometer Task Config */
#define INCLINO_MEAS_MODE           SCL3300_EN_MODE3 /* Mode 1: 1.2g full-scale; Mode 2: 2.4g full-scale, Mode 3: Inclination mode; Mode 4: Inclination mode, low noise mode */

/* Timesync Task Config */
#define TSYNC_PERIOD_S              1200            /* Default period for time synchronization task [s], only used if TSync task is not included in the schedule */
#define TSYNC_FALLBACK_TH           10              /* How many times the timesync has to time out before switching the period to TSYNC_FALLBACK_PERIOD_S (set to zero to disable the fallback mechanism) */
#define TSYNC_FALLBACK_PERIOD_S     7200            /* Fallback period, used if timesync times out */
#define TSYNC_METHOD                TSYNC_GNSS      /* How the time should be synced (TSYNC_GNSS, TSYNC_BOLT or TSYNC_USB) */
#define TSYNC_MAX_DRIFT_PPM         100             /* Max. expected drift that should be compensated */
#define TSYNC_TIMEOUT_S             (TSYNC_METHOD_GNSS ? 300 : 30)    /* Timesync timeout [s] */
#define TSYNC_ACQ_ID_CHANGE_OFS_S   86400           /* If the timesync offset is larger than this value, then an acquisition ID change will be requested (in continuous mode only; set to zero to disable) */
#define TSYNC_MASTER                0               /* If set to 1, the timestamp will be sent over bolt periodically */

/* Health Task Config */
#define HEALTH_PERIOD_S             600             /* Default health task period (if not included in schedule) */
#define HEALTH_HUMIDITY_WARN_TH     90              /* Warning threshold for humidity [%] */
#define HEALTH_TEMP_LOW_WARN_TH     -30             /* Lower warning threshold for temperature [°C] */
#define HEALTH_TEMP_HIGH_WARN_TH    60              /* Upper warning threshold for temperature [°C] */
#define HEALTH_VBAT_LOW_WARN_TH     3100            /* Lower warning threshold for the battery voltage [mV] */
#define HEALTH_VBAT_HIGH_WARN_TH    3800            /* Upper warning threshold for the battery voltage [mV] */
#define HEALTH_VSYS_LOW_WARN_TH     2900            /* Lower warning threshold for the battery voltage [mV] */
#define HEALTH_VSYS_HIGH_WARN_TH    3100            /* Upper warning threshold for the battery voltage [mV] */

/* Log Task Config */
#define LOG_ADCDATA_MINISEED        1               /* If set to 1, the ADC data will be stored in miniSEED format (otherwise as raw 24-bit ADC samples) */
#define LOG_ADCDATA_DOWNSAMPLE_FREQ 0               /* Downsample ADC data to the specified frequency (this option only works in if LOG_ADCDATA_MINISEED is enabled) */
#define LOG_ADCDATA_APPEND_CHECKSUM 0               /* If set to 1, a 32-bit CRC will be appended at the end of the ADC data file (only applies when using miniseed format in buffered mode) */
#define LOG_GNSSDATA_BIN            1               /* If set to 1, GNSS raw data will be logged in binary format (UBX). If set to 0, the data will be logged in text format (CSV). */
#define LOG_ADCDATA_CALC_PSD        0               /* Set to 1 to calculate the power spectral density on the raw ADC samples (ADC channel 1 only) */
#define LOG_PSD_AVG_PERIOD_S        600             /* If PSD calculation is enabled: the period (in seconds) over which the PSD is averaged */

/* Bolt Task Config */
#define BOLT_ENABLE                 1               /* Whether to initialize and use the Bolt task */
#define BOLT_FLUSH                  0               /* Whether to flush (empty) the Bolt read queue on startup */

/* Streaming Task Config */
#define STREAMING_TARGET            STREAMING_NONE  /* Port for ADC data streaming (set to STREAMING_NONE to disable streaming) */
#define STREAMING_BACKLOG_MODE      0               /* If enabled, the streaming task will be paused when the USB connection is interrupted */

/* RingBuffer Config */
#define RB_ADC_MAX_ITEMS            20
#define RB_GEO_MAX_ITEMS            10
#define RB_BOLT_MAX_ITEMS           100
#define RB_CMD_MAX_ITEMS            10
#define RB_EVENT_MAX_ITEMS          20
#define RB_HEALTH_MAX_ITEMS         10
#define RB_IMUSTAT_MAX_ITEMS        10
#define RB_IMUDATA_MAX_ITEMS        100
#define RB_SCHEDULE_MAX_ITEMS       3
#define RB_INCLINODATA_MAX_ITEMS    10
#define RB_GNSSDATA_MAX_ITEMS       20
#define RB_COMDATA_MAX_ITEMS        10
#define RB_TSYNC_MAX_ITEMS          3
#define RB_STREAM_MAX_ITEMS         10

/* System Config */
#define SYSTEM_OPMODE               (SYS_OPMODE_SCHED | SYS_OPMODE_CONT)    /* Operating mode (bitwise combination of SYS_OPMODE_* flags) */
#define SYSTEM_WAKEUP_PERIOD        30              /* Wakeup period in seconds; determines the frequency with which the schedule task runs, and how long the watchdog period must be. Note: this parameter is ignored if ADC_SYNC_PERIOD is set. */
#define USE_CONFIG_FROM_SD          0               /* If set to 0, the config on the SD card will be ignored */
#define WRITE_DEFAULT_CONFIG        0               /* If enabled, the default config will be used and written to the SD card, existing schedules will be erased */
#define USE_STOP2                   1               /* Whether the MCU should go into STOP2 low-power mode */
#define USE_TICKLESS_IDLE           1               /* FreeRTOS tickless idle mode (stops the CPU when no task other than the Idle Task can run) */
#define USE_BUZZER                  0               /* If set to 0, the buzzer will not be used */
#define USB_MSC_AUTOMOUNT           0               /* Automatically mount mass storage device when USB cable is connected */
#define LPM_ON_USER_BUTTON          0               /* Enter LPM if user button is pressed */

/* Debug */
#define DEBUG_OUTPUT_LEVEL          DBG_LVL_VERBOSE
#define DEBUG_PRINT_TARGET          DBG_PRINT_SWO   /* Debug print output device */
#define DEBUG_ASSERT_ENABLED        1
#define DEBUG_MONITOR_ISR_RUNTIME   0               /* If enabled, a warning is printed if an interrupt takes longer than 1ms to execute (cannot be used together with SYSTEMVIEW). */
#define DEBUG_PRINTF_BUFFER_SIZE    256             /* Max. length of one debug print line */
#define DEBUG_USB_MSC_ON_DBGPIN2    0               /* Mount SD card via USB when debug pin 2 (PC1) is high */
#define DEBUG_PRINT_BASEBOARD       0               /* Format SWO debug prints for simple parsing on the baseboard */
#define USE_SYSTEMVIEW              0               /* Whether to use SEGGER SystemView (adds an overhead in terms of execution time and memory footprint) */

#endif /* __CONFIG_H */
