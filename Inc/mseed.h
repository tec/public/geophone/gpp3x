/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * miniSEED (aka data-only SEED)
 *
 * see SEED format specification for more info: http://www.fdsn.org/pdf/SEEDManual_V2.4.pdf (p.112 as well as Appendix G on p.199)
 */

#ifndef __MSEED_H
#define __MSEED_H

/* Defines -------------------------------------------------------------------*/

/* Config */
#define MSEED_DATA_LEN_EXP              10                      /* length of a data record / miniseed block; log2 value, must be >= 8 (= 256 bytes) */
#define MSEED_USE_COMPRESSION           1                       /* If set to 1, Steim-1 compression will be used */
//#define MSEED_HDR_STATION             "GPP3X"                 /* 5 characters (if not defined/commented-out, the 5-digit device ID will be used instead) */
#define MSEED_HDR_LOCATION              "  "                    /* 2 characters */
#define MSEED_HDR_NETWORK               "  "                    /* 2 characters */

/* Fixed Section of Data Header length */
#define MSEED_HEADER_LEN                (MSEED_FSDH_LEN + MSEED_BL1K_LEN + 8)   /* = sizeof(mseed_header_t) */
#define MSEED_FSDH_LEN                  48                                      /* = sizeof(mseed_fsdh_t) */
#define MSEED_FSDH_NUMSAMPLES_OFS       30
#define MSEED_FSDH_SAMPLERATE_OFS       32
#define MSEED_FSDH_SEQNO_LEN            6
#define MSEED_FSDH_STATION_LEN          5
#define MSEED_FSDH_LOCATION_LEN         2
#define MSEED_FSDH_CHANNEL_LEN          3
#define MSEED_FSDH_NETWORK_LEN          2
#define MSEED_BL1K_DATALEN_OFS          (MSEED_FSDH_LEN + 6)
#define MSEED_BL1K_LEN                  8                                       /* = sizeof(mseed_bl1k_t) */

/* Activity flags */
#define MSEED_ACTFLAG_TIME_CORR         0x02    /* Time correction already applied */

/* I/O flags */
#define MSEED_IOFLAG_SHORT_RECORD       0x04    /* Padded record due to missed samples */
#define MSEED_IOFLAG_START              0x08    /* Start of time series */
#define MSEED_IOFLAG_END                0x10    /* End of time series */

/* Data quality flags */
#define MSEED_DQFLAG_AMP_SAT            0x01    /* Amplifier saturation detected */
#define MSEED_DQFLAG_DIG_CLIP           0x02    /* Digitizer clipping detected */
#define MSEED_DQFLAG_SPIKES             0x04    /* Spikes detected */
#define MSEED_DQFLAG_GLITCHES           0x08    /* Glitches detected */
#define MSEED_DQFLAG_MISSING            0x10    /* Missing samples (padded) */

/* Data record encodings */
#define MSEED_DATA_ENCODING_INT16       0x01    /* 16-bit integers */
#define MSEED_DATA_ENCODING_INT24       0x02    /* 24-bit integers (deprecated, not supported by most parsers) */
#define MSEED_DATA_ENCODING_INT32       0x03    /* 32-bit integers */
#define MSEED_DATA_ENCODING_STEIM1      0x0a    /* Steim-1 compression for 32-bit integers */
#define MSEED_DATA_ENCODING_STEIM2      0x0b    /* Steim-2 compression */

/* Endianness */
#define MSEED_DATA_LITTLE_ENDIAN        0x0
#define MSEED_DATA_BIG_ENDIAN           0x1

/* Steim-1 compression */
#define MSEED_STEIM1_FRAME_SIZE         64
#define MSEED_STEIM1_FRAME_LAST_OFS     8

/* Various */
#define MSEED_SAMPLE_SIZE               ((GEO_SAMPLE_SIZE > 2) ? sizeof(int32_t) : sizeof(int16_t))     /* Size of one sample in uncompressed format */
#define MSEED_ENCODING                  (MSEED_USE_COMPRESSION ? MSEED_DATA_ENCODING_STEIM1 : ((GEO_SAMPLE_SIZE > 2) ? MSEED_DATA_ENCODING_INT32 : MSEED_DATA_ENCODING_INT16))
#define MSEED_DATA_LEN                  (1 << MSEED_DATA_LEN_EXP)
#if MSEED_USE_COMPRESSION
  #define MSEED_MAX_SAMPLES_PER_BLOCK   (2 * (MSEED_DATA_LEN - sizeof(mseed_header_t)))   /* Max. compression yields a bit less than 2 samples per byte */
#else
  #define MSEED_MAX_SAMPLES_PER_BLOCK   ((MSEED_DATA_LEN - sizeof(mseed_header_t)) / MSEED_SAMPLE_SIZE)
#endif


/* Structures ----------------------------------------------------------------*/

/* miniSEED v2.4 FSDH (Fixed Section of Data Header)
 * Note: SEED uses big endian word order as its standard, but libmseed can work with both
 *       (automatically determines endianness based on the 'year' field of the start time */
typedef struct __attribute__((__packed__))
{
    char          seq_no[6];
    char          data_quality;
    char          reserved;
    char          station[5];
    char          location[2];
    char          channel[3];
    char          network[2];
    /* Record start time (10 bytes) */
    struct {                        /* offset: 20 */
      uint16_t    year;
      uint16_t    day;
      uint8_t     hour;
      uint8_t     min;
      uint8_t     sec;
      uint8_t     unused;
      uint16_t    fract;
    } start_time;
    uint16_t      num_samples;      /* offset: 30 */
    int16_t       samprate_fact;
    int16_t       samprate_mult;
    uint8_t       act_flags;
    uint8_t       io_flags;
    uint8_t       dq_flags;
    uint8_t       num_blockettes;
    int32_t       time_correct;     /* offset: 40 */
    uint16_t      data_ofs;
    uint16_t      blockette_ofs;
} mseed_fsdh_t;

/* miniSEED v2.4 Blockette 1000 */
typedef struct __attribute__((__packed__))
{
    uint16_t      type;
    uint16_t      next;
    uint8_t       encoding;
    uint8_t       endian;
    uint8_t       data_len;
    uint8_t       reserved;
} mseed_bl1k_t;

typedef struct __attribute__((__packed__))
{
    mseed_fsdh_t  fsdh;
    mseed_bl1k_t  bl1k;
    uint8_t       padding[8];       /* padding bytes to increase header struct size to 64 bytes */
} mseed_header_t;

#if GEO_SAMPLE_SIZE > 2 || MSEED_USE_COMPRESSION
typedef int32_t mseed_sample_t;
#else
typedef int16_t mseed_sample_t;
#endif


/* Functions -----------------------------------------------------------------*/

mseed_header_t* MSeed_initHeader(uint8_t* buffer);    /* initialize the header fields (buffer is optional) */
void            MSeed_setHeaderFields(mseed_header_t* header, uint32_t seq_no, char channel, uint16_t num_samples, uint16_t sample_size, uint16_t sample_rate, uint64_t start_time);
uint32_t        MSeed_compressSteim1(const int32_t* input, uint32_t sample_cnt, int32_t* output, uint32_t output_len, int32_t diff0, uint32_t* bytes_packed);
uint32_t        MSeed_compressSteim2(const int32_t* input, uint32_t sample_cnt, int32_t* output, uint32_t output_len, int32_t diff0, uint32_t* bytes_packed);
uint32_t        MSeed_numBits(uint32_t val);          /* returns ceil(Log2(val)) */


#endif
