/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Task List: implementation of a priority queue using a linked list
 */

#ifndef __TASK_LIST_H
#define __TASK_LIST_H


/* Structures ----------------------------------------------------------------*/
typedef struct task_list            /* Task list struct */
{
    uint32_t start;                 /* First occurrence of the measurement */
    uint32_t period;                /* Period of the measurement */
    uint32_t argument;              /* Task-specific argument */
    uint8_t  task;                  /* ID of the task to be executed */
    struct task_list* next;         /* Pointer to the next node. The last node points to the first node */
} tl_t;

/* Functions -----------------------------------------------------------------*/

bool     TL_insert(tl_t** head,
                   uint32_t start,
                   uint32_t period,
                   uint32_t argument,
                   uint8_t task);                     /* Function to insert element according to priority */
bool     TL_popPush(tl_t** head,
                    uint32_t current_time);           /* Function to remove highest priority element from queue and if it is periodic (period not 0) push next occurrence to queue */
uint32_t TL_postpone(tl_t** head,
                     uint8_t task,
                     uint32_t until);                 /* Move the start time of the specified tasks to after 'until' (periodic tasks only, single execution tasks will be skipped) */
uint32_t TL_setPeriod(tl_t** head,
                      uint8_t task,
                      uint32_t period);
bool     TL_isEmpty(tl_t** head);                     /* Function to check if list is empty */
bool     TL_contains(tl_t** head,
                     uint8_t task);                   /* Checks whether the given task is contained in the list */
void     TL_clear(tl_t** head);                       /* Clear the list */

#endif /* __TASK_LIST_H */
