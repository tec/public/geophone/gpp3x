/*
 * Copyright (c) 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Power Spectral Density
 *
 * The calculation in this file is comparable to the following in Python:
 *   matplotlib.pyplot.psd(inputData, Fs=125, NFFT=1024, window=cosine_taper(1024, 0.2))
 */

#ifndef __PSD_H
#define __PSD_H

#include "main.h"

/* Defines -------------------------------------------------------------------*/

#define PSD_FFT_FRAME_SIZE        1024      /* Should be a power of two (128..4096) */
#define PSD_FFT_OVERLAP_SIZE      (PSD_FFT_FRAME_SIZE / 2)
#define PSD_FFT_INPUT_SCALING     (1.0f / (1 << 23))
#define PSD_COSINE_TAPER_SIZE     102       /* Suitable taper function for nFFT=1024 */


/* Functions -----------------------------------------------------------------*/

void     PSD_addSamples(const int32_t* samples, const uint32_t num_samples, const uint32_t sample_rate);
uint32_t PSD_getAverage(uint8_t* out_buffer, uint32_t buffer_len);
uint32_t PSD_getFrameCount(void);

#endif /* __PSD_H */
