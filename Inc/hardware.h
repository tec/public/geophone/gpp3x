/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __HARDWARE_H
#define __HARDWARE_H


/* Defines -------------------------------------------------------------------*/
#define WATCHDOG_PRESCALER          256             /* Possible values are 4, 8, 16, 32, 64, 128, 256 */
#define WATCHDOG_FREQ               32768           /* Watchdog has own clock */
#define WATCHDOG_MAX_RELOAD_VALUE   4095            /* Watchdog has only a 12-bit counter, higher values are not possible */
#define WATCHDOG_TIMEOUT            32              /* Watchdog timeout in seconds (set to 0 to disable IWDT) */
#define TIM5_PRESCALER              39999           /* General purpose counter; 1 tick corresponds to Prescaler/(80MHz) = 0.5ms */
#define TIM5_FREQUENCY              2000            /* Resulting timer 5 frequency when using prescaler TIM5_PRESCALER */
#define LPTIMER_FREQUENCY           32768           /* Low-power timer frequency */
#define LPTIMER_RESTART_OVERHEAD    4               /* Overhead for restarting the LP timer (in ticks) */
#define MCU_HSE_CRYSTAL             1               /* Whether to use the HSE crystal (if set to 0, the MSI will be used instead) */
#define MCU_ADC_OFFSET              5               /* measurement offset for the internal ADC of the MCU, in mV */
#define MCU_ADC_MAX_VALUE           4095            /* 12-bit ADC */
#define MCU_ADC_POWER_ON_DELAY_MS   10
#define MCU_ADC_VREF_CHARAC         3000            /* Voltage used during characterization at the factory, in mV */
#define MCU_ADC_VREFINT_CAL         *((uint16_t*)0x1FFF75AA)    /* VREFINT raw ADC calibration value measured at the factory */
#define MCU_UUID_ADDR               0x1FFF7590      /* Factory-programmed 96-bit unique identifier address */
#define SYSTEM_VOLTAGE              3000            /* mV (an incorrect value will lead to wrong DAC/trigger voltages) */
#define BOLT_ACK_TIMEOUT_US         100             /* Max time that is waited for BOLT ACK */
#define LPUART_USE_DMA              1
#define LPUART_DMA_TIMEOUT_MS       100             /* How long to wait until previous DMA transfer complete */
#define LPUART_TIMEOUT_MS           100             /* How long to wait for a transfer to complete */
#define SPI2_USE_DMA                0
#define GEO_STAGE2_PWRON_DELAY_MS   100

#ifdef GPP3X
  /* 3-axis geophone (HW rev2.0) */
  #define GEO_NUM_AXES              3
  #define USE_GNSS                  1
  #ifdef GPP3X_REV_2_1
    #define USE_INCLINO             0               /* Not installed by default */
    #define TRG_GAIN_VALUE_STAGE1   34
    #define TRG_GAIN_VALUE_STAGE2   1190
    #define GNSS_MODEL_M8
  #else
    #define USE_INCLINO             1
    #define TRG_GAIN_VALUE_STAGE1   20
    #define TRG_GAIN_VALUE_STAGE2   220
    #define GNSS_MODEL_F9P
  #endif
  #define LSM303_AGR

#else
  /* 1-axis geophone (HW rev1.03) */
  #define GEO_NUM_AXES              1
  #define USE_GNSS                  0
  #define USE_INCLINO               0
  #define TRG_GAIN_VALUE_STAGE1     20
  #define TRG_GAIN_VALUE_STAGE2     220
  #define LSM303_C
  #if TSYNC_METHOD_GNSS
    #error "TSYNC_GNSS is only available on GPP3X"
  #endif
  #if GEO_NUM_AXES > 1
    #error "GEO_NUM_AXES must be set to 1"
  #endif
#endif /* GPP3X */

#define ADC1_CS_Port                GPIOA
#define ADC1_CS_Pin                 GPIO_PIN_4
#define ADC1_RDY_Port               GPIOB
#define ADC1_RDY_Pin                GPIO_PIN_0

#define ADC2_CS_Port                GPIOD
#define ADC2_CS_Pin                 GPIO_PIN_0
#define ADC2_RDY_Port               GPIOD
#define ADC2_RDY_Pin                GPIO_PIN_3

#define ADC3_CS_Port                GPIOD
#define ADC3_CS_Pin                 GPIO_PIN_1
#define ADC3_RDY_Port               GPIOD
#define ADC3_RDY_Pin                GPIO_PIN_4

#define ADC_CLK_EN_Port             GPIOD
#define ADC_CLK_EN_Pin              GPIO_PIN_6

#define AVDD_ON_Port                GPIOE
#define AVDD_ON_Pin                 GPIO_PIN_9

#define TRIGGER_ON_Port             GPIOE
#define TRIGGER_ON_Pin              GPIO_PIN_11

#define ADC_ON_Port                 GPIOA
#define ADC_ON_Pin                  GPIO_PIN_3
#ifdef GPP3X_REV_2_1
  #define ADC_SYNC_Port             GPIOE
  #define ADC_SYNC_Pin              GPIO_PIN_3
  #define STAGE2_ON_Port            GPIOB
  #define STAGE2_ON_Pin             GPIO_PIN_12
#else
  #define ADC_SYNC_Port             GPIOC
  #define ADC_SYNC_Pin              GPIO_PIN_5
  #define STAGE2_ON_Port            GPIOC
  #define STAGE2_ON_Pin             GPIO_PIN_4
#endif

#define ADC_SPI_SCK_Port            GPIOA
#define ADC_SPI_SCK_Pin             GPIO_PIN_5
#define ADC_SPI_MISO_Port           GPIOA
#define ADC_SPI_MISO_Pin            GPIO_PIN_6
#define ADC_SPI_MOSI_Port           GPIOA
#define ADC_SPI_MOSI_Pin            GPIO_PIN_7
#define ADC_SPI                     hspi1
#define ADC_SPI_SEMAPHORE           xSemaphore_SPI1

#define COM_TREQ_Port               GPIOD
#define COM_TREQ_Pin                GPIO_PIN_10
#define COM_IND_Port                GPIOD
#define COM_IND_Pin                 GPIO_PIN_11
#define BOLT_IND_Port               GPIOD
#define BOLT_IND_Pin                GPIO_PIN_12
#define BOLT_MODE_Port              GPIOD
#define BOLT_MODE_Pin               GPIO_PIN_13
#define BOLT_REQ_Port               GPIOD
#define BOLT_REQ_Pin                GPIO_PIN_14
#define BOLT_ACK_Port               GPIOD
#define BOLT_ACK_Pin                GPIO_PIN_15
#define BOLT_SPI_SCK_Port           GPIOB
#define BOLT_SPI_SCK_Pin            GPIO_PIN_13
#define BOLT_SPI_MISO_Port          GPIOB
#define BOLT_SPI_MISO_Pin           GPIO_PIN_14
#define BOLT_SPI_MOSI_Port          GPIOB
#define BOLT_SPI_MOSI_Pin           GPIO_PIN_15
#define BOLT_SPI                    hspi2
#define BOLT_SPI_SEMAPHORE          xSemaphore_SPI2

#ifdef GPP3X
  #define BTN_Port                  GPIOE
  #define BTN_Pin                   GPIO_PIN_8
#else
  #define BTN_Port                  GPIOB
  #define BTN_Pin                   GPIO_PIN_8
#endif

#define BUZZER_Port                 GPIOE
#define BUZZER_Pin                  GPIO_PIN_0

#define DAC_CS_Port                 GPIOA
#define DAC_CS_Pin                  GPIO_PIN_0
#ifdef GPP3X_REV_2_1
  #define DAC_SPI_DEVICE            hspi1
  #define DAC_SPI_SEMAPHORE         xSemaphore_SPI1
  #define DAC_INT_VREF              DAC_VREF_2425_MV
  #define DAC_VREF                  DAC_CONV_VREF(DAC_INT_VREF)
#else
  #define DAC_SPI_DEVICE            hspi2
  #define DAC_SPI_SEMAPHORE         xSemaphore_SPI2
  #define DAC_VREF                  SYSTEM_VOLTAGE      /* Use external (supply) voltage as reference */
#endif

#define IMU_ON_Port                 GPIOE
#define IMU_ON_Pin                  GPIO_PIN_12
#define IMU_INT_XL_Port             GPIOE
#define IMU_INT_XL_Pin              GPIO_PIN_13
#define IMU_INT_MAG_Port            GPIOE
#define IMU_INT_MAG_Pin             GPIO_PIN_14
#ifdef GPP3X
  #define IMU_SCL_Port              GPIOB
  #define IMU_SCL_Pin               GPIO_PIN_8
  #define IMU_SDA_Port              GPIOB
  #define IMU_SDA_Pin               GPIO_PIN_9
#else
  #define IMU_SCL_Port              SHT_SCL_Port
  #define IMU_SCL_Pin               SHT_SCL_Pin
  #define IMU_SDA_Port              SHT_SDA_Port
  #define IMU_SDA_Pin               SHT_SDA_Pin
#endif

#define SHT_ON_Port                 GPIOE
#define SHT_ON_Pin                  GPIO_PIN_15
#define SHT_SCL_Port                GPIOB
#define SHT_SCL_Pin                 GPIO_PIN_10
#define SHT_SDA_Port                GPIOB
#define SHT_SDA_Pin                 GPIO_PIN_11
#define SHT_I2C                     hi2c2

#ifdef GPP3X
  #define SCL_ON_Port               GPIOE
  #define SCL_ON_Pin                GPIO_PIN_10
  #define SCL_CS_Port               GPIOE
  #define SCL_CS_Pin                GPIO_PIN_6

  #define GNSS_ON_Port              GPIOD
  #define GNSS_ON_Pin               GPIO_PIN_8
  #define GNSS_RX_Port              GPIOA
  #define GNSS_RX_Pin               GPIO_PIN_9
  #define GNSS_TX_Port              GPIOA
  #define GNSS_TX_Pin               GPIO_PIN_10
  #define GNSS_TPULSE_Port          GPIOC
  #define GNSS_TPULSE_Pin           GPIO_PIN_6
  #define GNSS_EXTINT_Port          GPIOD
  #define GNSS_EXTINT_Pin           GPIO_PIN_7
  #define GNSS_RST_Port             GPIOD
  #define GNSS_RST_Pin              GPIO_PIN_9
  #define GNSS_UART_RXD_Port        GPIOA
  #define GNSS_UART_RXD_Pin         GPIO_PIN_9
  #define GNSS_UART_TXD_Port        GPIOA
  #define GNSS_UART_TXD_Pin         GPIO_PIN_10
  #define GNSS_UART                 huart1
#endif

#define LED_R_Port                  GPIOB
#define LED_R_Pin                   GPIO_PIN_4
#define LED_Y_Port                  GPIOB
#define LED_Y_Pin                   GPIO_PIN_5
#define LED_G_Port                  GPIOB
#define LED_G_Pin                   GPIO_PIN_6
#define LED_B_Port                  GPIOB
#define LED_B_Pin                   GPIO_PIN_7

#define SD_ON_Port                  GPIOC
#define SD_ON_Pin                   GPIO_PIN_7
#define SD_D0_Port                  GPIOC
#define SD_D0_Pin                   GPIO_PIN_8
#define SD_D1_Port                  GPIOC
#define SD_D1_Pin                   GPIO_PIN_9
#define SD_D2_Port                  GPIOC
#define SD_D2_Pin                   GPIO_PIN_10
#define SD_D3_Port                  GPIOC
#define SD_D3_Pin                   GPIO_PIN_11
#define SD_CLK_Port                 GPIOC
#define SD_CLK_Pin                  GPIO_PIN_12

#define TRGP_Port                   GPIOA
#define TRGP_Pin                    GPIO_PIN_1
#define TRGN_Port                   GPIOA
#define TRGN_Pin                    GPIO_PIN_2

#define USB_DETECT_Port             GPIOA
#define USB_DETECT_Pin              GPIO_PIN_8

#define VSENSE_Port                 GPIOB
#define VSENSE_Pin                  GPIO_PIN_1
#define VSENSE_Channel              ADC_CHANNEL_16

#define VSENSE_ON_Port              GPIOB
#define VSENSE_ON_Pin               GPIO_PIN_2

#ifdef GPP3X_REV_2_1
  /* Rev2.1 has a 2x3 extension header */
  #define EXT_HDR_PIN1_Port         GPIOC
  #define EXT_HDR_PIN1_Pin          GPIO_PIN_2
  #define EXT_HDR_PIN2_Port         GPIOC
  #define EXT_HDR_PIN2_Pin          GPIO_PIN_0
  #define EXT_HDR_PIN3_Port         GPIOC
  #define EXT_HDR_PIN3_Pin          GPIO_PIN_3
  #define EXT_HDR_PIN4_Port         GPIOC
  #define EXT_HDR_PIN4_Pin          GPIO_PIN_1
  #define EXT_HDR_P3V_Port          GPIOA
  #define EXT_HDR_P3V_Pin           GPIO_PIN_15
  /* Use pins 1 and 3 for debugging */
  #define DEBUG_PIN1_Port           EXT_HDR_PIN1_Port
  #define DEBUG_PIN1_Pin            EXT_HDR_PIN1_Pin
  #define DEBUG_PIN2_Port           EXT_HDR_PIN3_Port
  #define DEBUG_PIN2_Pin            EXT_HDR_PIN3_Pin
  #define DEBUG_PIN3_Port           EXT_HDR_PIN2_Port
  #define DEBUG_PIN3_Pin            EXT_HDR_PIN2_Pin
  #define DEBUG_PIN4_Port           EXT_HDR_PIN4_Port
  #define DEBUG_PIN4_Pin            EXT_HDR_PIN4_Pin
#elif defined(GPP3X)
  #define DEBUG_PIN1_Port           GPIOC
  #define DEBUG_PIN1_Pin            GPIO_PIN_0
  #define DEBUG_PIN2_Port           GPIOC
  #define DEBUG_PIN2_Pin            GPIO_PIN_1
#endif

#ifdef GPP3X_REV_2_1
  #define DPP_RST_Port              GPIOE
  #define DPP_RST_Pin               GPIO_PIN_7
  /* COM_APP_RXD and COM_APP_TXD */
  #define DPP_RXD_Port              GPIOC
  #define DPP_RXD_Pin               GPIO_PIN_4
  #define DPP_TXD_Port              GPIOC
  #define DPP_TXD_Pin               GPIO_PIN_5
#endif

#define SWDIO_Port                  GPIOA
#define SWDIO_Pin                   GPIO_PIN_13
#define SWCLK_Port                  GPIOA
#define SWCLK_Pin                   GPIO_PIN_14
#define SWO_Port                    GPIOB
#define SWO_Pin                     GPIO_PIN_3

/* All unused GPIOs will be configured in analog mode */
#ifdef GPP3X_REV_2_1
  #define GPIOC_UNUSED_PINS         (GPIO_PIN_13 | DPP_RXD_Pin | DPP_TXD_Pin)     /* RXD/TXD currently unused */
  #define GPIOD_UNUSED_PINS         (GPIO_PIN_5)
  #define GPIOE_UNUSED_PINS         (GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_4 | GPIO_PIN_5)
#elif defined(GPP3X)
  #define GPIOA_UNUSED_PINS         (GPIO_PIN_15)
  #define GPIOB_UNUSED_PINS         (GPIO_PIN_12)
  #define GPIOC_UNUSED_PINS         (GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_13)
  #define GPIOD_UNUSED_PINS         (GPIO_PIN_5)
  #define GPIOE_UNUSED_PINS         (GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_7)
#else
  #define GPIOA_UNUSED_PINS         (GPIO_PIN_15)
  #define GPIOB_UNUSED_PINS         (GPIO_PIN_9 | GPIO_PIN_12)
  #define GPIOC_UNUSED_PINS         (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_6 | GPIO_PIN_13)
  #define GPIOD_UNUSED_PINS         (GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9)
  #define GPIOE_UNUSED_PINS         (GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7 | GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11)
#endif


/* Macros --------------------------------------------------------------------*/
#define ADC1_ASSERT()               HAL_GPIO_WritePin(ADC1_CS_Port, ADC1_CS_Pin, GPIO_PIN_RESET)
#define ADC1_DEASSERT()             HAL_GPIO_WritePin(ADC1_CS_Port, ADC1_CS_Pin, GPIO_PIN_SET)
#define ADC1_IS_RDY()               (HAL_GPIO_ReadPin(ADC1_RDY_Port, ADC1_RDY_Pin) == GPIO_PIN_RESET)
#define ADC2_ASSERT()               HAL_GPIO_WritePin(ADC2_CS_Port, ADC2_CS_Pin, GPIO_PIN_RESET)
#define ADC2_DEASSERT()             HAL_GPIO_WritePin(ADC2_CS_Port, ADC2_CS_Pin, GPIO_PIN_SET)
#define ADC2_IS_RDY()               (HAL_GPIO_ReadPin(ADC2_RDY_Port, ADC2_RDY_Pin) == GPIO_PIN_RESET)
#define ADC3_ASSERT()               HAL_GPIO_WritePin(ADC3_CS_Port, ADC3_CS_Pin, GPIO_PIN_RESET)
#define ADC3_DEASSERT()             HAL_GPIO_WritePin(ADC3_CS_Port, ADC3_CS_Pin, GPIO_PIN_SET)
#define ADC3_IS_RDY()               (HAL_GPIO_ReadPin(ADC3_RDY_Port, ADC3_RDY_Pin) == GPIO_PIN_RESET)

#define ADC_SYNC_HIGH()             HAL_GPIO_WritePin(ADC_SYNC_Port, ADC_SYNC_Pin, GPIO_PIN_SET)
#define ADC_SYNC_LOW()              HAL_GPIO_WritePin(ADC_SYNC_Port, ADC_SYNC_Pin, GPIO_PIN_RESET)
#define ADC_CLK_SWON()              HAL_GPIO_WritePin(ADC_CLK_EN_Port, ADC_CLK_EN_Pin, GPIO_PIN_SET)
#define ADC_CLK_SWOFF()             HAL_GPIO_WritePin(ADC_CLK_EN_Port, ADC_CLK_EN_Pin, GPIO_PIN_RESET)
#define ADC_SWON()                  ADC_DEASSERT_ALL(); HAL_GPIO_WritePin(ADC_ON_Port, ADC_ON_Pin, GPIO_PIN_RESET); TRIGGER_SWON()
#define ADC_SWOFF()                 HAL_GPIO_WritePin(ADC_ON_Port, ADC_ON_Pin, GPIO_PIN_SET); ADC_CLK_SWOFF(); ADC_SYNC_LOW(); ADC_ASSERT_ALL()       /* when off, SYNC and SPI CS lines must be low */
#define ADC_IS_SWON()               (HAL_GPIO_ReadPin(ADC_ON_Port, ADC_ON_Pin) == GPIO_PIN_RESET)

#define AVDD_SWON()                 HAL_GPIO_WritePin(AVDD_ON_Port, AVDD_ON_Pin, GPIO_PIN_SET)
#define AVDD_SWOFF()                HAL_GPIO_WritePin(AVDD_ON_Port, AVDD_ON_Pin, GPIO_PIN_RESET)
#define AVDD_IS_SWON()              (HAL_GPIO_ReadPin(AVDD_ON_Port, AVDD_ON_Pin) == GPIO_PIN_SET)

#ifdef GPP3X
  #ifdef GPP3X_REV_2_1
    #define TRIGGER_SWON()          DAC_DEASSERT(); HAL_GPIO_WritePin(TRIGGER_ON_Port, TRIGGER_ON_Pin, GPIO_PIN_SET)
    #define TRIGGER_SWOFF()         HAL_GPIO_WritePin(TRIGGER_ON_Port, TRIGGER_ON_Pin, GPIO_PIN_RESET); DAC_ASSERT()
    #define STAGE2_SWON()           HAL_GPIO_WritePin(STAGE2_ON_Port, STAGE2_ON_Pin, GPIO_PIN_RESET)
    #define STAGE2_SWOFF()          HAL_GPIO_WritePin(STAGE2_ON_Port, STAGE2_ON_Pin, GPIO_PIN_SET)
  #else
    /* Stage 2 cannot be used as trigger input due to a hardware problem with rev2.0.
     * When stage 2 is enabled (pin low), the trigger selection switch (U1) chooses the input S1 (= stage 1)
     * Consequently, stage 2 must always be enabled when stage 1 is enabled. */
    #define TRIGGER_SWON()          DAC_DEASSERT(); HAL_GPIO_WritePin(TRIGGER_ON_Port, TRIGGER_ON_Pin, GPIO_PIN_SET); HAL_GPIO_WritePin(STAGE2_ON_Port, STAGE2_ON_Pin, GPIO_PIN_RESET)
    #define TRIGGER_SWOFF()         HAL_GPIO_WritePin(TRIGGER_ON_Port, TRIGGER_ON_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(STAGE2_ON_Port, STAGE2_ON_Pin, GPIO_PIN_SET); DAC_ASSERT()      /* DAC is on same power rail -> SPI CS must be low */
    #define STAGE2_SWON()
    #define STAGE2_SWOFF()
  #endif
  #define ADC_ASSERT_ALL()          ADC1_ASSERT();   ADC2_ASSERT();   ADC3_ASSERT()
  #define ADC_DEASSERT_ALL()        ADC1_DEASSERT(); ADC2_DEASSERT(); ADC3_DEASSERT()
  #define TRIGGER_IS_SWON()         (HAL_GPIO_ReadPin(TRIGGER_ON_Port, TRIGGER_ON_Pin) == GPIO_PIN_SET)
#else
  #define TRIGGER_SWON()            /* on GPP1X, trigger cannot be switched off */
  #define TRIGGER_SWOFF()
  #define TRIGGER_IS_SWON()         1
  #define STAGE2_SWON()             HAL_GPIO_WritePin(STAGE2_ON_Port, STAGE2_ON_Pin, GPIO_PIN_RESET)
  #define STAGE2_SWOFF()            HAL_GPIO_WritePin(STAGE2_ON_Port, STAGE2_ON_Pin, GPIO_PIN_SET)
  #define ADC_ASSERT_ALL()          ADC1_ASSERT()
  #define ADC_DEASSERT_ALL()        ADC1_DEASSERT()
#endif

#define TRG_IS_P()                  (HAL_GPIO_ReadPin(TRGP_Port, TRGP_Pin) == GPIO_PIN_SET)
#define TRG_IS_N()                  (HAL_GPIO_ReadPin(TRGN_Port, TRGN_Pin) == GPIO_PIN_SET)

#define DAC_ASSERT()                HAL_GPIO_WritePin(DAC_CS_Port, DAC_CS_Pin, GPIO_PIN_RESET)
#define DAC_DEASSERT()              HAL_GPIO_WritePin(DAC_CS_Port, DAC_CS_Pin, GPIO_PIN_SET)

#define BOLT_ASSERT()               HAL_GPIO_WritePin(BOLT_REQ_Port, BOLT_REQ_Pin, GPIO_PIN_SET)
#define BOLT_DEASSERT()             HAL_GPIO_WritePin(BOLT_REQ_Port, BOLT_REQ_Pin, GPIO_PIN_RESET)
#define BOLT_IS_ASSERTED()          (HAL_GPIO_ReadPin(BOLT_REQ_Port, BOLT_REQ_Pin) == GPIO_PIN_SET)
#define BOLT_IS_ACK()               (HAL_GPIO_ReadPin(BOLT_ACK_Port, BOLT_ACK_Pin) == GPIO_PIN_SET)
#define BOLT_IS_IND()               (HAL_GPIO_ReadPin(BOLT_IND_Port, BOLT_IND_Pin) == GPIO_PIN_SET)
#define BOLT_MODE_RD()              HAL_GPIO_WritePin(BOLT_MODE_Port, BOLT_MODE_Pin, GPIO_PIN_RESET)
#define BOLT_MODE_WR()              HAL_GPIO_WritePin(BOLT_MODE_Port, BOLT_MODE_Pin, GPIO_PIN_SET)
#define COM_TREQ_HIGH()             HAL_GPIO_WritePin(COM_TREQ_Port, COM_TREQ_Pin, GPIO_PIN_SET)
#define COM_TREQ_LOW()              HAL_GPIO_WritePin(COM_TREQ_Port, COM_TREQ_Pin, GPIO_PIN_RESET)
#define COM_IS_TREQ()               (HAL_GPIO_ReadPin(COM_TREQ_Port, COM_TREQ_Pin) == GPIO_PIN_SET)
#define COM_IS_IND()                (HAL_GPIO_ReadPin(COM_IND_Port, COM_IND_Pin) == GPIO_PIN_SET)

#define BTN_IS_PRESSED()            (HAL_GPIO_ReadPin(BTN_Port, BTN_Pin) == GPIO_PIN_RESET)

#if USE_BUZZER
  #define BUZZER_ON()               HAL_GPIO_WritePin(BUZZER_Port, BUZZER_Pin, GPIO_PIN_SET)
  #define BUZZER_OFF()              HAL_GPIO_WritePin(BUZZER_Port, BUZZER_Pin, GPIO_PIN_RESET)
  #define BUZZER_TG()               HAL_GPIO_TogglePin(BUZZER_Port, BUZZER_Pin)
  #define BUZZER(ms)                BUZZER_ON(); HAL_Delay(ms); BUZZER_OFF()
#else /* USE_BUZZER */
  #define BUZZER_ON()
  #define BUZZER_OFF()
  #define BUZZER_TG()
  #define BUZZER(ms)
#endif /* USE_BUZZER */

#ifdef GPP3X
  #define SCL_ASSERT()              HAL_GPIO_WritePin(SCL_CS_Port, SCL_CS_Pin, GPIO_PIN_RESET)
  #define SCL_DEASSERT()            HAL_GPIO_WritePin(SCL_CS_Port, SCL_CS_Pin, GPIO_PIN_SET)
  #define SCL_SWON()                SCL_DEASSERT(); HAL_GPIO_WritePin(SCL_ON_Port, SCL_ON_Pin, GPIO_PIN_RESET)
  #define SCL_SWOFF()               HAL_GPIO_WritePin(SCL_ON_Port, SCL_ON_Pin, GPIO_PIN_SET); HAL_GPIO_WritePin(SCL_CS_Port, SCL_CS_Pin, GPIO_PIN_RESET)
  #define SCL_IS_SWON()             (HAL_GPIO_ReadPin(SCL_ON_Port, SCL_ON_Pin) == GPIO_PIN_RESET)
  #define IMU_SWON()                HAL_GPIO_WritePin(IMU_ON_Port, IMU_ON_Pin, GPIO_PIN_RESET)
  #define IMU_SWOFF()               HAL_GPIO_WritePin(IMU_ON_Port, IMU_ON_Pin, GPIO_PIN_SET)
  #define IMU_I2C                   hi2c1
  #define IMU_I2C_INIT()            I2C1_init()
#else
  /* On the old geophone (rev1.03), SHT must be switched on together with the IMU since they share the same I2C bus */
  #define IMU_SWON()                HAL_GPIO_WritePin(IMU_ON_Port, IMU_ON_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(SHT_ON_Port, SHT_ON_Pin, GPIO_PIN_RESET)
  #define IMU_SWOFF()               HAL_GPIO_WritePin(IMU_ON_Port, IMU_ON_Pin, GPIO_PIN_SET); HAL_GPIO_WritePin(SHT_ON_Port, SHT_ON_Pin, GPIO_PIN_SET)
  #define IMU_I2C_DEVICE            hi2c2
  #define IMU_I2C_INIT()            I2C2_init()
  #define SCL_SWOFF()               /* does not have an SCL sensor */
#endif
#define IMU_IS_SWON()               (HAL_GPIO_ReadPin(IMU_ON_Port, IMU_ON_Pin) == GPIO_PIN_RESET)

#define LED_R_ON()                  HAL_GPIO_WritePin(LED_R_Port, LED_R_Pin, GPIO_PIN_SET)
#define LED_R_OFF()                 HAL_GPIO_WritePin(LED_R_Port, LED_R_Pin, GPIO_PIN_RESET)
#define LED_R_TG()                  HAL_GPIO_TogglePin(LED_R_Port, LED_R_Pin)
#define LED_Y_ON()                  HAL_GPIO_WritePin(LED_Y_Port, LED_Y_Pin, GPIO_PIN_SET)
#define LED_Y_OFF()                 HAL_GPIO_WritePin(LED_Y_Port, LED_Y_Pin, GPIO_PIN_RESET)
#define LED_Y_TG()                  HAL_GPIO_TogglePin(LED_Y_Port, LED_Y_Pin)
#define LED_G_ON()                  HAL_GPIO_WritePin(LED_G_Port, LED_G_Pin, GPIO_PIN_SET)
#define LED_G_OFF()                 HAL_GPIO_WritePin(LED_G_Port, LED_G_Pin, GPIO_PIN_RESET)
#define LED_G_TG()                  HAL_GPIO_TogglePin(LED_G_Port, LED_G_Pin)
#define LED_B_ON()                  HAL_GPIO_WritePin(LED_B_Port, LED_B_Pin, GPIO_PIN_SET)
#define LED_B_OFF()                 HAL_GPIO_WritePin(LED_B_Port, LED_B_Pin, GPIO_PIN_RESET)
#define LED_B_TG()                  HAL_GPIO_TogglePin(LED_B_Port, LED_B_Pin)
#define LED_ALL_ON()                do { LED_R_ON(); LED_Y_ON(); LED_G_ON(); LED_B_ON(); } while (0)
#define LED_ALL_OFF()               do { LED_R_OFF(); LED_Y_OFF(); LED_G_OFF(); LED_B_OFF(); } while (0)
#define LED_ALL_TG()                do { LED_R_TG(); LED_Y_TG(); LED_G_TG(); LED_B_TG(); } while (0)
#define LED_ALL_BLINK()             do { LED_R_ON(); HAL_Delay(100); LED_R_OFF(); \
                                         LED_Y_ON(); HAL_Delay(100); LED_Y_OFF(); \
                                         LED_G_ON(); HAL_Delay(100); LED_G_OFF(); \
                                         LED_B_ON(); HAL_Delay(100); LED_B_OFF(); } while (0)

#ifdef GPP3X
  #define DEBUG_PIN1_HIGH()         HAL_GPIO_WritePin(DEBUG_PIN1_Port, DEBUG_PIN1_Pin, GPIO_PIN_SET)
  #define DEBUG_PIN1_LOW()          HAL_GPIO_WritePin(DEBUG_PIN1_Port, DEBUG_PIN1_Pin, GPIO_PIN_RESET)
  #define DEBUG_PIN1_TOGGLE()       HAL_GPIO_TogglePin(DEBUG_PIN1_Port, DEBUG_PIN1_Pin)
  #define DEBUG_PIN1_STATE()        (HAL_GPIO_ReadPin(DEBUG_PIN1_Port, DEBUG_PIN1_Pin) == GPIO_PIN_SET)
  #define DEBUG_PIN2_HIGH()         HAL_GPIO_WritePin(DEBUG_PIN2_Port, DEBUG_PIN2_Pin, GPIO_PIN_SET)
  #define DEBUG_PIN2_LOW()          HAL_GPIO_WritePin(DEBUG_PIN2_Port, DEBUG_PIN2_Pin, GPIO_PIN_RESET)
  #define DEBUG_PIN2_TOGGLE()       HAL_GPIO_TogglePin(DEBUG_PIN2_Port, DEBUG_PIN2_Pin)
  #define DEBUG_PIN2_STATE()        (HAL_GPIO_ReadPin(DEBUG_PIN2_Port, DEBUG_PIN2_Pin) == GPIO_PIN_SET)
  #ifdef GPP3X_REV_2_1
    #define EXT_SWON()              HAL_GPIO_WritePin(EXT_HDR_P3V_Port, EXT_HDR_P3V_Pin, GPIO_PIN_RESET)
    #define EXT_SWOFF()             HAL_GPIO_WritePin(EXT_HDR_P3V_Port, EXT_HDR_P3V_Pin, GPIO_PIN_SET)
    #define DEBUG_PIN3_HIGH()       HAL_GPIO_WritePin(DEBUG_PIN3_Port, DEBUG_PIN3_Pin, GPIO_PIN_SET)
    #define DEBUG_PIN3_LOW()        HAL_GPIO_WritePin(DEBUG_PIN3_Port, DEBUG_PIN3_Pin, GPIO_PIN_RESET)
    #define DEBUG_PIN3_TOGGLE()     HAL_GPIO_TogglePin(DEBUG_PIN3_Port, DEBUG_PIN3_Pin)
    #define DEBUG_PIN3_STATE()      (HAL_GPIO_ReadPin(DEBUG_PIN3_Port, DEBUG_PIN3_Pin) == GPIO_PIN_SET)
    #define DEBUG_PIN4_HIGH()       HAL_GPIO_WritePin(DEBUG_PIN4_Port, DEBUG_PIN4_Pin, GPIO_PIN_SET)
    #define DEBUG_PIN4_LOW()        HAL_GPIO_WritePin(DEBUG_PIN4_Port, DEBUG_PIN4_Pin, GPIO_PIN_RESET)
    #define DEBUG_PIN4_TOGGLE()     HAL_GPIO_TogglePin(DEBUG_PIN4_Port, DEBUG_PIN4_Pin)
    #define DEBUG_PIN4_STATE()      (HAL_GPIO_ReadPin(DEBUG_PIN4_Port, DEBUG_PIN4_Pin) == GPIO_PIN_SET)
  #else
    #define EXT_SWON()
    #define EXT_SWOFF()
  #endif
#else
  #define DEBUG_PIN1_LOW()
  #define DEBUG_PIN2_LOW()
  #define EXT_SWOFF()
#endif

#define SD_SWON()                   HAL_GPIO_WritePin(SD_ON_Port, SD_ON_Pin, GPIO_PIN_RESET)
#define SD_SWOFF()                  HAL_GPIO_WritePin(SD_ON_Port, SD_ON_Pin, GPIO_PIN_SET)
#define SD_IS_SWON()                (HAL_GPIO_ReadPin(SD_ON_Port, SD_ON_Pin) == GPIO_PIN_RESET)

#ifdef GPP3X
  #define SHT_SWON()                HAL_GPIO_WritePin(SHT_ON_Port, SHT_ON_Pin, GPIO_PIN_RESET)
  #define SHT_SWOFF()               HAL_GPIO_WritePin(SHT_ON_Port, SHT_ON_Pin, GPIO_PIN_SET)
#else
  /* On the old geophone (rev1.03), SHT must be switched together with the IMU since they share the same I2C bus */
  #define SHT_SWON()                HAL_GPIO_WritePin(SHT_ON_Port, SHT_ON_Pin, GPIO_PIN_RESET); HAL_GPIO_WritePin(IMU_ON_Port, IMU_ON_Pin, GPIO_PIN_RESET)
  #define SHT_SWOFF()               /* In case IMU task is still running: do not switch off SHT here, will we powered off later together with the IMU */
#endif
#define SHT_IS_SWON()               (HAL_GPIO_ReadPin(SHT_ON_Port, SHT_ON_Pin) == GPIO_PIN_RESET)

#ifdef GPP3X
  #define GNSS_SWON()               HAL_GPIO_WritePin(GNSS_ON_Port, GNSS_ON_Pin, GPIO_PIN_SET)
  #define GNSS_SWOFF()              HAL_GPIO_WritePin(GNSS_ON_Port, GNSS_ON_Pin, GPIO_PIN_RESET); GNSS_RST_LOW(); GNSS_EXTINT_LOW()     /* All connected lines must be low when switched off */
  #define GNSS_IS_SWON()            (HAL_GPIO_ReadPin(GNSS_ON_Port, GNSS_ON_Pin) == GPIO_PIN_SET)
  #define GNSS_RST_HIGH()           HAL_GPIO_WritePin(GNSS_RST_Port, GNSS_RST_Pin, GPIO_PIN_SET)
  #define GNSS_RST_LOW()            HAL_GPIO_WritePin(GNSS_RST_Port, GNSS_RST_Pin, GPIO_PIN_RESET)
  #define GNSS_TPULSE_STATE()       HAL_GPIO_ReadPin(GNSS_TPULSE_Port, GNSS_TPULSE_Pin)
  #define GNSS_EXTINT_HIGH()        HAL_GPIO_WritePin(GNSS_EXTINT_Port, GNSS_EXTINT_Pin, GPIO_PIN_SET)
  #define GNSS_EXTINT_LOW()         HAL_GPIO_WritePin(GNSS_EXTINT_Port, GNSS_EXTINT_Pin, GPIO_PIN_RESET)
#else
  #define GNSS_SWOFF()
#endif

#define USB_IS_CONNECTED()          (HAL_GPIO_ReadPin(USB_DETECT_Port, USB_DETECT_Pin) == GPIO_PIN_SET)

/* Supply (battery) voltage sensing */
#define VSENSE_SWON()               HAL_GPIO_WritePin(VSENSE_ON_Port, VSENSE_ON_Pin, GPIO_PIN_SET)
#define VSENSE_SWOFF()              HAL_GPIO_WritePin(VSENSE_ON_Port, VSENSE_ON_Pin, GPIO_PIN_RESET)
#define VSENSE_IS_SWON()            (HAL_GPIO_ReadPin(VSENSE_ON_Port, VSENSE_ON_Pin) == GPIO_PIN_SET)
#ifdef GPP3X_REV_2_1
  #define VSENSE_CONV(v)            ((val) * (47 + 47) / 47)    /* Scale according to external voltage divider */
#else
  #define VSENSE_CONV(v)            ((val) * (12 + 47) / 47)    /* Scale according to external voltage divider */
#endif

#define GPIO_EXTI_CLR(x)            do {\
                                        EXTI->IMR1  &= ~((uint32_t)x);\
                                        EXTI->EMR1  &= ~((uint32_t)x);\
                                        EXTI->RTSR1 &= ~((uint32_t)x);\
                                        EXTI->FTSR1 &= ~((uint32_t)x);\
                                    } while (0)
#define GPIO_EXTI_IT_DISABLE(x)     (EXTI->IMR1  &= ~((uint32_t)x))
#define GPIO_EXTI_IT_ENABLE(x)      (EXTI->IMR1  |= (uint32_t)x)
#define GPIO_EXTI_IT_IS_EN(x)       (EXTI->IMR1 & ((uint32_t)x))

#define LPTIMER_TICKS_TO_S(t)       ((t) / LPTIMER_FREQUENCY)
#define LPTIMER_TICKS_TO_MS(t)      ((t) * 1000 / LPTIMER_FREQUENCY)
#define LPTIMER_TICKS_TO_US(t)      ((uint64_t)(t) * 1000000 / LPTIMER_FREQUENCY)
#define LPTIMER_S_TO_TICKS(t)       ((t) * LPTIMER_FREQUENCY)
#define LPTIMER_MS_TO_TICKS(t)      ((t) * LPTIMER_FREQUENCY / 1000)
#define LPTIMER_US_TO_TICKS(t)      ((uint64_t)(t) * LPTIMER_FREQUENCY / 1000000)

#define TIM5_TICKS_TO_MS(t)         ((t) * 1000 / TIM5_FREQUENCY)
#define TIM5_MS_TO_TICKS(t)         ((t) * TIM5_FREQUENCY / 1000)

#define UART_WAIT_TXE(uart)         while (!__HAL_UART_GET_FLAG(uart, UART_FLAG_TXE)) {}           /* Wait until transmit register empty */
#define UART_SEND_BYTE(uart, b)     UART_WAIT_TXE(uart); ((uart)->Instance->TDR = ((b) & 0xff))

#if WATCHDOG_TIMEOUT
  #define WDT_POLL()                __HAL_IWDG_RELOAD_COUNTER(&hiwdg)
#else /* WATCHDOG_TIMEOUT */
  #define WDT_POLL()
#endif /* WATCHDOG_TIMEOUT */

#define PIN_SET(p)                  p##_Port->BSRR = p##_Pin
#define PIN_CLR(p)                  p##_Port->BRR = p##_Pin
#define PIN_GET(p)                  ((p##_Port->IDR & p##_Pin) != 0)


/* Enumerations --------------------------------------------------------------*/
typedef enum
{
    LED_RED,
    LED_YELLOW,
    LED_GREEN,
    LED_BLUE,
    NUM_LEDS,
} led_t;


/* Handles -------------------------------------------------------------------*/
extern SPI_HandleTypeDef            hspi1;          /* ADCs, Inclinometer, and DAC) */
extern SPI_HandleTypeDef            hspi2;          /* BOLT (and DAC) */
extern SD_HandleTypeDef             hsd1;           /* SD card */
extern UART_HandleTypeDef           huart1;         /* GNSS */
extern UART_HandleTypeDef           hlpuart1;       /* Debug print */
extern I2C_HandleTypeDef            hi2c1;          /* IMU */
extern I2C_HandleTypeDef            hi2c2;          /* SHT */
extern TIM_HandleTypeDef            htim3;          /* Debounce timer */
extern TIM_HandleTypeDef            htim4;          /* Debounce timer */
extern TIM_HandleTypeDef            htim5;          /* Measure task runtime */
extern TIM_HandleTypeDef            htim17;         /* HAL tick */
extern LPTIM_HandleTypeDef          hlptim1;        /* Timesync timeout */
extern IWDG_HandleTypeDef           hiwdg;
extern RTC_HandleTypeDef            hrtc;
extern DMA_HandleTypeDef            hdma_usart1_rx;
extern DMA_HandleTypeDef            hdma_lpuart_tx;
extern PCD_HandleTypeDef            hpcd_USB_OTG_FS;


/* Functions -----------------------------------------------------------------*/
void        ADC1_init(void);
void        ADC1_configChannel(uint32_t channel);
uint16_t    ADC1_getBatteryVoltage(void);
uint16_t    ADC1_getSystemVoltage(void);
bool        Button_pressedFor(uint32_t duration_s); /* Returns true if the button was pressed for the given duration */
void        GPIO_init(void);
void        I2C1_init(void);
void        I2C2_init(void);
void        PERIPH_prepareStop2(void);
void        PERIPH_resumeFromStop2(void);
void        SPI1_init(void);
void        SPI2_init(void);
void        UART1_init(uint32_t baudrate);
void        UART1_initDMA(uint8_t* rx_buffer, uint32_t len);
void        UART1_deInitDMA(void);
void        LPUART1_init(void);
bool        LPUART1_transmit(uint8_t* data, uint16_t len);
bool        LPUART1_transmitBinary(const uint8_t* data, uint16_t len);
bool        USB_transmitBinary(const uint8_t* data, uint16_t len);
bool        USB_printf(const char* str, ...);
bool        SWO_transmitBinary(const uint8_t* data, uint16_t len);
void        TIM2_init(void);                /* Time-sync */
void        TIM3_init(void);                /* TRG+ de-bounce */
void        TIM4_init(void);                /* TRG- de-bounce */
void        TIM5_init(void);                /* General purpose counter, used e.g. to calculate the duty cycle */
uint32_t    TIM5_getCounterValue(void);
void        LPTIM1_init(void);              /* Time-sync timeout */
void        WDT_init(void);                 /* independent watchdog timer */
const char* System_getResetCause(uint8_t* out_reset_flag);

void        SystemClock_config(void);
void        SystemClock_reset(void);
void        SystemClock_disable(void);
void        SystemClock_enable(void);
void        SysTick_init(void);
void        SysTick_reset(void);

#endif /* __HARDWARE_H */
