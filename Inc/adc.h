/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __ADC_H_
#define __ADC_H_

/*
 * Driver for the MAX11214 24-bit ADC
 *
 * This library supports Conversion and Register mode
 *
 * Conversion mode:
 * Bit  | B7 (MSB) | B6     | B5  | B4   | B3    | B2    | B1    | B0    |
 * Name | START=1  | MODE=0 | CAL | IMPD | RATE3 | RATE2 | RATE1 | RATE0 |
 *
 * Register mode:
 * Bit  | B7 (MSB) | B6     | B5  | B4  | B3  | B2  | B1  | B0  |
 * Name | START=1  | MODE=1 | RS4 | RS3 | RS2 | RS1 | RS0 | R/W |
 */

/* Conversion access ---------------------------------------------------------*/
#define ADC_CONV_START              (1 << 7)
#define ADC_CONV_MODE               (0 << 6)
#define ADC_CONV_CAL                (1 << 5)
#define ADC_CONV_IMPD               (1 << 4)
#define ADC_CONV_RATE_BIT3          (1 << 3)
#define ADC_CONV_RATE_BIT2          (1 << 2)
#define ADC_CONV_RATE_BIT1          (1 << 1)
#define ADC_CONV_RATE_BIT0          (1 << 0)

/* Register access -----------------------------------------------------------*/
#define ADC_REG_START               (1 << 7)
#define ADC_REG_MODE                (1 << 6)
#define ADC_REG_READ                1
#define ADC_REG_WRITE               0

/* Register addresses --------------------------------------------------------*/
#define ADC_REG_STAT                0x00
#define ADC_REG_CTRL1               0x01
#define ADC_REG_CTRL2               0x02
#define ADC_REG_CTRL3               0x03
#define ADC_REG_CTRL4               0x04
#define ADC_REG_CTRL5               0x05
#define ADC_REG_DATA                0x06
#define ADC_REG_SOC_SPI             0x07
#define ADC_REG_SGC_SPI             0x08
#define ADC_REG_SCOC_SPI            0x09
#define ADC_REG_SCGC_SPI            0x0A
#define ADC_REG_HPF                 0x0B
#define ADC_REG_RAM                 0x0C
#define ADC_REG_SYNC_SPI            0x0D
#define ADC_REG_SOC_ADC             0x15
#define ADC_REG_SGC_ADC             0x16
#define ADC_REG_SCOC_ADC            0x17
#define ADC_REG_SCGC_ADC            0x18

/* Status register -----------------------------------------------------------*/
#define ADC_STAT_DEFAULT            0x30
#define ADC_STAT_AOR                0x01
#define ADC_STAT_RDERR              0x02
#define ADC_STAT_PDSTAT0            0x04
#define ADC_STAT_PDSTAT1            0x08
#define ADC_STAT_ERROR              0x40
#define ADC_STAT_INRESET            0x80

#define ADC_STAT_RDY                0x01
#define ADC_STAT_MSTAT              0x02
#define ADC_STAT_DOR                0x04
#define ADC_STAT_SYSGOR             0x08
#define ADC_STAT_RATE0              0x10
#define ADC_STAT_RATE1              0x20
#define ADC_STAT_RATE2              0x40
#define ADC_STAT_RATE3              0x80

/* CTRL1 register ------------------------------------------------------------*/
#define ADC_CTRL1_CONTSC            0x01
#define ADC_CTRL1_SCYCLE            0x02
#define ADC_CTRL1_FORMAT            0x04
#define ADC_CTRL1_UNIPOLAR          0x08
#define ADC_CTRL1_PD0               0x10
#define ADC_CTRL1_PD1               0x20
#define ADC_CTRL1_SYNC              0x40
#define ADC_CTRL1_EXTCK             0x80

/* CTRL2 register ------------------------------------------------------------*/
#define ADC_CTRL2_PGAG0             0x01
#define ADC_CTRL2_PGAG1             0x02
#define ADC_CTRL2_PGAG2             0x04
#define ADC_CTRL2_PGAEN             0x08
#define ADC_CTRL2_LPMODE            0x10
#define ADC_CTRL2_BUFEN             0x20
#define ADC_CTRL2_DGAIN0            0x40
#define ADC_CTRL2_DGAIN1            0x80

/* CTRL3 register ------------------------------------------------------------*/
#define ADC_CTRL3_FILT0             0x01
#define ADC_CTRL3_FILT1             0x02
#define ADC_CTRL3_PHASE             0x04
#define ADC_CTRL3_DATA32            0x08
#define ADC_CTRL3_MODBITS           0x10
#define ADC_CTRL3_ENMSYNC           0x20

/* Defines -------------------------------------------------------------------*/
#define ADC_FORMAT                  ADC_FORMAT_OFFSETBINARY
#define ADC_IDLE_VALUE              0x00800000      /* Idle (zero) value */
#define ADC_MAX_AMPLITUDE           0x007fffff      /* Max. amplitude */
#define ADC_BITS                    24              /* ADC bits per sample */
#define ADC_SAMPLE_SIZE             (ADC_BITS / 8)  /* Raw sample size in bytes */
#ifdef GPP3X
#define ADC_POWER_ON_DELAY_US       3000            /* More time required on GPP3X */
#else
#define ADC_POWER_ON_DELAY_US       500
#endif
#define ADC_SPI_TIMEOUT_MS          5
#define ADC_USE_DMA                 1
#define ADC_USE_FIR_FILTER          0
#define ADC_COUNT                   3               /* Number of ADCs */

/* Config Checks -------------------------------------------------------------*/
#if GEO_SAMPLE_SIZE < 1 || GEO_SAMPLE_SIZE > 3      /* Current downsampling method only supports GEO_SAMPLE_SIZE 1, 2 or 3 */
#error "Invalid GEO_SAMPLE_SIZE"
#endif
#if (ADC_SPS != 1000U) && (ADC_SPS != 500U) && (ADC_SPS != 250U) && (ADC_SPS != 125U)
#error "Invalid value for ADC_SPS"
#endif
#if (ADC_MAX_SPS > 1000) || (ADC_MAX_SPS < ADC_SPS)
#error "Invalid value for ADC_MAX_SPS"
#endif

/* Macros --------------------------------------------------------------------*/
#define ADC_CONV_COMMAND(cmd)       (ADC_CONV_START | ADC_CONV_MODE | (cmd))
#define ADC_REG_ADDR(reg, read)     (ADC_REG_START  | ADC_REG_MODE  | ((reg) << 1) | (read))

#define ADC_DATA(u1, u2, u3)        ( (((u1) << 16) & 0x00FF0000) + (((u2) << 8) & 0x0000FF00) + ((u3) & 0x000000FF))
#define ADC_CONVERT(u)              ( (u) / 0x100 * 3000 / 0x00010000)

/* Convert unsigned big endian to signed little endian */
#if GEO_SAMPLE_SIZE > 2
#define ADC_VAL_SIGNED(ptr)         ( (int32_t)( (((uint32_t)ptr[0]) << 16) + (((uint32_t)ptr[1]) << 8) + (ptr[2]) ) - ADC_IDLE_VALUE )
#elif GEO_SAMPLE_SIZE > 1
#define ADC_VAL_SIGNED(ptr)         ( (int32_t)( (((uint32_t)ptr[0]) << 8) + (ptr[1]) ) - (ADC_IDLE_VALUE >> 8) )
#else
#define ADC_VAL_SIGNED(ptr)         ( (int32_t)(*ptr) - (ADC_IDLE_VALUE >> 16) )
#endif

/* Enumerations ---------------------------------------------------------------*/
typedef enum                                /* ADCs for Geophone measurements */
{
    ADC_NONE = 0,
    ADC_A1   = 1,                           /* The ADC that triggers the measurement (like on old board with only 1 axis measurement); also referred to as Z-axis */
    ADC_A2   = 2,                           /* ADC 2, also referred to as X-axis */
    ADC_A3   = 3,                           /* ADC 3, also referred to as Y-axis */
    ADC_ALL,
} ADCID_t;

typedef enum                                /* ADC Data Format Types */
{
    ADC_FORMAT_TWOSCOMPLEMENT = 0,
    ADC_FORMAT_OFFSETBINARY,
    NUM_OF_ADC_FORMAT_TYPES
} ADCFormatType_t;

typedef enum                                /* Trigger Types */
{
    TRG_EXT = 0,
    TRG_POS,
    TRG_NEG,
    TRG_SCHED,
    NUM_OF_TRG_TYPES
} TRGType_t;

typedef enum                                /* Trigger Gain Stages */
{
    TRG_GAIN_STAGE1 = 0,
    TRG_GAIN_STAGE2,
    NUM_OF_TRG_GAIN_STAGES
} TRGGainStage_t;

typedef enum
{
    ADC_CONV_RATE_62Hz    = ADC_CONV_RATE_BIT2 | ADC_CONV_RATE_BIT1,                      /* 0110 */
    ADC_CONV_RATE_125Hz   = ADC_CONV_RATE_BIT2 | ADC_CONV_RATE_BIT1 | ADC_CONV_RATE_BIT0, /* 0111 */
    ADC_CONV_RATE_250Hz   = ADC_CONV_RATE_BIT3,                                           /* 1000 */
    ADC_CONV_RATE_500Hz   = ADC_CONV_RATE_BIT3 | ADC_CONV_RATE_BIT0,                      /* 1001 */
    ADC_CONV_RATE_1000Hz  = ADC_CONV_RATE_BIT3 | ADC_CONV_RATE_BIT1,                      /* 1010 */
    ADC_CONV_RATE_2000Hz  = ADC_CONV_RATE_BIT3 | ADC_CONV_RATE_BIT1 | ADC_CONV_RATE_BIT0, /* 1011 */
    ADC_CONV_RATE_4000Hz  = ADC_CONV_RATE_BIT3 | ADC_CONV_RATE_BIT2,                      /* 1100 */
    ADC_CONV_RATE_8000Hz  = ADC_CONV_RATE_BIT3 | ADC_CONV_RATE_BIT2 | ADC_CONV_RATE_BIT0, /* 1101 */
} ADCConvRate_t;

typedef enum
{
    ADC_SPS_1000 = 0,
    ADC_SPS_500  = 1,
    ADC_SPS_250  = 2,
    ADC_SPS_125  = 3,
} ADCSPS_t;

typedef struct
{
    uint64_t timestamp;                           /* Start timestamp [us] */
    uint32_t acq_id;                              /* Acquisition ID */
    uint16_t len;                                 /* Number of bytes */
    uint8_t  data[ADC_MAX_SPS * GEO_SAMPLE_SIZE]; /* Raw ADC data (Note: dimension for the max. sampling rate) */
} ADCData_t;

/* Exported Variables --------------------------------------------------------*/
extern const char* adc_channel_mapping[];         /* mapping channel number (1 - 3) to channel name */

/* Functions -----------------------------------------------------------------*/
void     ADC_convertRAWToInt32(const uint8_t* in_data, uint32_t num_samples, int32_t* out_data);
uint32_t ADC_downsample(const int32_t* in_data, uint32_t in_freq, uint32_t out_freq, int32_t* out_data);
bool     ADC_test(ADCID_t adc);
void     ADC_writeRegister(ADCID_t adc, uint8_t reg, uint8_t val);
void     ADC_sendCmd(ADCID_t adc, uint8_t cmd);
bool     ADC_readSamples(ADCID_t adc, uint16_t sample_rate, uint32_t num_samples, int32_t* out_samples);
uint16_t ADC_readStatus(ADCID_t adc);
bool     ADC_isReady(ADCID_t adc);
void     ADC_transferSample(ADCID_t adc);
void     ADC_start(bool sync);
void     ADC_stop(bool switch_off);
bool     ADC_isRunning(void);
void     ADC_enableInterrupts(void);
void     ADC_disableInterrupts(void);
void     ADC_sync(void);

#endif /* __ADC_H_ */
