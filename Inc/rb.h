/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __RB_H
#define __RB_H

/* Structures ----------------------------------------------------------------*/
typedef struct          /* RingBuffer Struct */
{
    uint32_t count;                         /* Current number of items in RB */
    uint32_t max;                           /* Maximum number of items allowed in RB */
    uint32_t itemsize;                      /* Size of item in bytes */
    uint8_t* head;                          /* Pointer to head item */
    uint8_t* tail;                          /* Pointer to tail item */
    uint8_t* data;                          /* RB data buffer */
} rb_t;

/* Functions -----------------------------------------------------------------*/
void     RB_init(rb_t* rb, uint32_t max, uint32_t itemsize, uint8_t* data);
uint8_t* RB_insert(rb_t* rb, const uint8_t* item);  /* Returns a pointer to the new head. If item is NULL, then data can be added manually after. */
uint8_t* RB_getHead(rb_t* rb);              /* Returns pointer to head. NULL if empty */
uint8_t* RB_getTail(rb_t* rb);              /* Returns pointer to tail. NULL if empty */
bool     RB_remove(rb_t* rb);               /* Removes the tail and returns true if successful */
bool     RB_isEmpty(rb_t* rb);              /* Returns true if RB is empty */
bool     RB_isFull(rb_t* rb);               /* Returns true if the RB is full */
uint32_t RB_spaceLeft(rb_t* rb);            /* Returns space that is left in the buffer, in number of elements. */
void     RB_flush(rb_t* rb);                /* Flushes the buffer (remove all items) */
void     RB_print(rb_t* rb);                /* Prints the buffer content */

#endif /* __RB_H */
