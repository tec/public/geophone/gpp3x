/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __LSM303AGR_H_
#define __LSM303AGR_H_


/* I2C Addresses -------------------------------------------------------------*/
#define IMU_XL_ADDR                     (0x19 << 1)
#define IMU_MAG_ADDR                    (0x1E << 1)

// For accelerometer and magnetometer: Adjust register value MSB to auto increment
#define AUTO_INCREMENT_REG_ADDR         0x80

/* Accelerometer Temperature registers ---------------------------------------*/
#define LSM303_ACC_STATUS_AUX           0x07
#define LSM303_ACC_STATUS_AUX_TDA       0x04
#define LSM303_ACC_STATUS_AUX_TOR       0x40

#define LSM303_ACC_TEMP_OUT_L           0x0C
#define LSM303_ACC_TEMP_OUT_H           0x0D

#define LSM303_ACC_TEMP_CFG             0x1F
#define LSM303_ACC_TEMP_CFG_EN          0xC0

/* Accelerometer Interrupt counter -------------------------------------------*/
#define LSM303_INT_CNTR                 0x0E

/* Accelerometer Who Am I register -------------------------------------------*/
#define LSM303_ACC_WHO_AM_I             0x0F
#define LSM303_ACC_WHO_AM_I_VAL         0x33

/* Accelerometer Control register 1 ------------------------------------------*/
#define LSM303_ACC_CTRL1                0x20
#define LSM303_ACC_CTRL1_XEN            0x01 // X enable
#define LSM303_ACC_CTRL1_YEN            0x02 // Y enable
#define LSM303_ACC_CTRL1_ZEN            0x04 // Z enable
#define LSM303_ACC_CTRL1_LP_EN          0x08 // Low-power mode
#define LSM303_ACC_CTRL1_ODR_OFF        0x00 // ODR:   Off
#define LSM303_ACC_CTRL1_ODR_1Hz        0x10 //       1 Hz
#define LSM303_ACC_CTRL1_ODR_10Hz       0x20 //      10 Hz
#define LSM303_ACC_CTRL1_ODR_25Hz       0x30 //      25 Hz
#define LSM303_ACC_CTRL1_ODR_50Hz       0x40 //      50 Hz
#define LSM303_ACC_CTRL1_ODR_100Hz      0x50 //     100 Hz
#define LSM303_ACC_CTRL1_ODR_200Hz      0x60 //     200 Hz
#define LSM303_ACC_CTRL1_ODR_400Hz      0x70 //     400 Hz
#define LSM303_ACC_CTRL1_ODR_LP         0x80 // Low-power (1620 Hz)
#define LSM303_ACC_CTRL1_ODR_HP         0x90 // HR/Normal (1344 Hz), Low-power (5376 Hz)

/* Accelerometer Control register 2 ------------------------------------------*/
#define LSM303_ACC_CTRL2                0x21
#define LSM303_ACC_CTRL2_HPIS1          0x01 // Enable filter on interrupt 1
#define LSM303_ACC_CTRL2_HPIS2          0x02 // Enable filter on interrupt 2
#define LSM303_ACC_CTRL2_HPCLCK         0x04 // Enable filter for click detection
#define LSM303_ACC_CTRL2_FDS            0x08 // Filtered data selection
#define LSM303_ACC_CTRL2_HPCF1          0x10 // High-pass filter cutoff, depending on ODR: f_t = ln(1 - 1/HPc) * f_s /(2*pi) = f_s /(6 * HPc) (approx)
#define LSM303_ACC_CTRL2_HPCF2          0x20 //
#define LSM303_ACC_CTRL2_HPM_N_RESET    0x00 // High-pass: Normal (reset when reading reference)
#define LSM303_ACC_CTRL2_HPM_REF        0x40 // High-pass: Reference signal
#define LSM303_ACC_CTRL2_HPM_N          0x80 // High-pass: Normal
#define LSM303_ACC_CTRL2_HPM_AUTO       0xC0 // High-pass: Autoreset on interrupt

/* Accelerometer Control register 3 ------------------------------------------*/
#define LSM303_ACC_CTRL3                0x22
#define LSM303_ACC_CTRL3_INT1_OFF       0x00
#define LSM303_ACC_CTRL3_FIFO_OVERRUN   0x20 // FIFO overrun on INT1
#define LSM303_ACC_CTRL3_FIFO_WTRMRK    0x40 // FIFO watermark interrupt on INT1
#define LSM303_ACC_CTRL3_DRDY2          0x80 // Data-ready 2 interrupt on INT1
#define LSM303_ACC_CTRL3_DRDY1          0x10 // Data-ready 1 interrupt on INT1
#define LSM303_ACC_CTRL3_AOI2           0x20 // And/Or 2  interrupt on INT1
#define LSM303_ACC_CTRL3_AOI1           0x40 // And/Or 1  interrupt on INT1
#define LSM303_ACC_CTRL3_CLCK           0x80 // Click interrupt on INT1

/* Accelerometer Control register 4 ------------------------------------------*/
#define LSM303_ACC_CTRL4                0x23
#define LSM303_ACC_CTRL4_SPI_EN         0x01 // Enable SPI
#define LSM303_ACC_CTRL4_ST_N           0x00 // Self-test: normal
#define LSM303_ACC_CTRL4_ST_0           0x02 // Self-test: mode 0
#define LSM303_ACC_CTRL4_ST_1           0x04 // Self-test: mode 1
#define LSM303_ACC_CTRL4_HR             0x08 // High-resolution mode
#define LSM303_ACC_CTRL4_FS_2g          0x00 // Full scale: +- 2g
#define LSM303_ACC_CTRL4_FS_4g          0x10 //             +- 4g
#define LSM303_ACC_CTRL4_FS_8g          0x20 //             +- 8g
#define LSM303_ACC_CTRL4_FS_16g         0x30 //             +-16g
#define LSM303_ACC_CTRL4_END_LITTLE     0x00 // Little-endian (only for HR)
#define LSM303_ACC_CTRL4_END_BIG        0x40 // Big-endian    (only for HR)
#define LSM303_ACC_CTRL4_BDU            0x80 // Block data update

/* Accelerometer Control register 5 ------------------------------------------*/
#define LSM303_ACC_CTRL5                0x24
#define LSM303_ACC_CTRL5_D4D_INT2       0x01 // 4D detection enabled on INT2
#define LSM303_ACC_CTRL5_LIR_INT2       0x02 // Latched interrupt request on INT2
#define LSM303_ACC_CTRL5_D4D_INT1       0x04 // 4D detection enabled on INT1
#define LSM303_ACC_CTRL5_LIR_INT1       0x08 // 4D detection enabled on INT2
#define LSM303_ACC_CTRL5_FIFO_EN        0x40 // FIFO enabled
#define LSM303_ACC_CTRL5_REBOOT_MEM     0x80 // Reboot accelerometer memory content

/* Accelerometer Control register 6 ------------------------------------------*/
#define LSM303_ACC_CTRL6                0x25
#define LSM303_ACC_CTRL6_ACTIVE_HIGH    0x00
#define LSM303_ACC_CTRL6_ACTIVE_LOW     0x02
#define LSM303_ACC_CTRL6_ACT_INT2       0x08 // Activity interrupt on INT2
#define LSM303_ACC_CTRL6_BOOT_INT2      0x10 // Boot on INT2
#define LSM303_ACC_CTRL6_IR2_INT2       0x20 // Interrupt 2 on INT2
#define LSM303_ACC_CTRL6_IR1_INT2       0x40 // Interrupt 1 on INT2
#define LSM303_ACC_CTRL6_CLCK_INT2      0x80 // Click interrupt on INT2

/* Accelerometer Reference ---------------------------------------------------*/
#define LSM303_ACC_REF                  0x26

/* Accelerometer Status register ---------------------------------------------*/
#define LSM303_ACC_STATUS               0x27
#define LSM303_ACC_STATUS_ZYXDA         0x08 // Data available for X, Y and Z axis
#define LSM303_ACC_STATUS_ZYXOR         0x80 // Data overrun   for X, Y and Z axis

/* Accelerometer Data register -----------------------------------------------*/
#define LSM303_ACC_X_L                  0x28 // X low:  X[1: 8]
#define LSM303_ACC_X_H                  0x29 // X high: X[9:16]
#define LSM303_ACC_Y_L                  0x2A // Y low:  Y[1: 8]
#define LSM303_ACC_Y_H                  0x2B // Y high: Y[9:16]
#define LSM303_ACC_Z_L                  0x2C // Z low:  Z[1: 8]
#define LSM303_ACC_Z_H                  0x2D // Z high: Z[8:16]

/* Accelerometer FIFO registers ----------------------------------------------*/
#define LSM303_ACC_FIFO_CTRL            0x2E // FIFO Control register
#define LSM303_ACC_FIFO_CTRL_FTH        0x00 // Set FIFO threshold whenever there is a sample in the FIFO
#define LSM303_ACC_FIFO_CTRL_TR_INT1    0x00 // Trigger INT1
#define LSM303_ACC_FIFO_CTRL_TR_INT2    0x20 // Trigger INT2
#define LSM303_ACC_FIFO_CTRL_MD_BP      0x00 // FIFO mode: Bypass
#define LSM303_ACC_FIFO_CTRL_MD_FIFO    0x40 //            FIFO
#define LSM303_ACC_FIFO_CTRL_MD_STREAM  0x80 //            Stream
#define LSM303_ACC_FIFO_CTRL_MD_STR2FIF 0xC0 //            Stream-to-FIFO

#define LSM303_ACC_FIFO_SRC             0x2F // FIFO Status register
#define LSM303_ACC_FIFO_SRC_EMPTY       0x20 // FIFO empty
#define LSM303_ACC_FIFO_SRC_OVR         0x40 // FIFO full
#define LSM303_ACC_FIFO_SRC_FTH         0x80 // FIFO reached threshold

/* Accelerometer Interrupt Generator registers 1 - 2 -------------------------*/
#define LSM303_ACC_IG1_CFG              0x30
#define LSM303_ACC_IG1_CFG_XLIE         0x01 // Enable interrupt generation on event: X low
#define LSM303_ACC_IG1_CFG_XHIE         0x02 //                                       X high
#define LSM303_ACC_IG1_CFG_YLIE         0x04 //                                       Y low
#define LSM303_ACC_IG1_CFG_YHIE         0x08 //                                       Y high
#define LSM303_ACC_IG1_CFG_ZLIE         0x10 //                                       Z low
#define LSM303_ACC_IG1_CFG_ZHIE         0x20 //                                       Z high
#define LSM303_ACC_IG1_CFG_AOI_OR       0x00 // OR combination of interrupts
#define LSM303_ACC_IG1_CFG_AOI_MOV      0x40 // 6D movement recognition
#define LSM303_ACC_IG1_CFG_AOI_AND      0x80 // AND combination of interrupts
#define LSM303_ACC_IG1_CFG_AOI_POS      0xC0 // 6D position recognition

#define LSM303_ACC_IG1_SRC              0x31 // IG 1: status
#define LSM303_ACC_IG1_SRC_ACTIVE       0x40 // IG 1: active (interrupt generated)

#define LSM303_ACC_IG1_THS              0x32 // IG 1: threshold
#define LSM303_ACC_IG1_DUR              0x33 // IG 1: duration

#define LSM303_ACC_IG2_CFG              0x34
#define LSM303_ACC_IG2_CFG_XLIE         0x01 // Enable interrupt generation on event: X low
#define LSM303_ACC_IG2_CFG_XHIE         0x02 //                                       X high
#define LSM303_ACC_IG2_CFG_YLIE         0x04 //                                       Y low
#define LSM303_ACC_IG2_CFG_YHIE         0x08 //                                       Y high
#define LSM303_ACC_IG2_CFG_ZLIE         0x10 //                                       Z low
#define LSM303_ACC_IG2_CFG_ZHIE         0x20 //                                       Z high
#define LSM303_ACC_IG2_CFG_AOI_OR       0x00 // OR combination of interrupts
#define LSM303_ACC_IG2_CFG_AOI_MOV      0x40 // 6D movement recognition
#define LSM303_ACC_IG2_CFG_AOI_AND      0x80 // AND combination of interrupts
#define LSM303_ACC_IG2_CFG_AOI_POS      0xC0 // 6D position recognition

#define LSM303_ACC_IG2_SRC              0x35 // IG 2: status
#define LSM303_ACC_IG2_SRC_ACTIVE       0x40 // IG 2: active (interrupt generated)

#define LSM303_ACC_IG2_THS              0x36 // IG 2: threshold
#define LSM303_ACC_IG2_DUR              0x37 // IG 2: duration

/* Accelerometer Click registers ---------------------------------------------*/
#define LSM303_ACC_CLCK_CFG             0x38
#define LSM303_ACC_CLCK_CFG_X_SINGLE    0x01 // Click detection: X axis, single
#define LSM303_ACC_CLCK_CFG_X_DOUBLE    0x02 //                  X axis, double
#define LSM303_ACC_CLCK_CFG_Y_SINGLE    0x04 //                  Y axis, single
#define LSM303_ACC_CLCK_CFG_Y_DOUBLE    0x08 //                  Y axis, double
#define LSM303_ACC_CLCK_CFG_Z_SINGLE    0x10 //                  Z axis, single
#define LSM303_ACC_CLCK_CFG_Z_DOUBLE    0x20 //                  Z axis, double

#define LSM303_ACC_CLCK_SRC             0x39
#define LSM303_ACC_CLCK_SRC_X           0x01 // X high event
#define LSM303_ACC_CLCK_SRC_Y           0x02 // Y high event
#define LSM303_ACC_CLCK_SRC_Z           0x04 // Z high event
#define LSM303_ACC_CLCK_SRC_SIGN        0x08 // Detection sign (0: positive, 1: negative)
#define LSM303_ACC_CLCK_SRC_SINGLE_EN   0x10 // Single click enabled
#define LSM303_ACC_CLCK_SRC_DOUBLE_EN   0x20 // Double click enabled
#define LSM303_ACC_CLCK_SRC_ACTIVE      0x40 // Interrupt generated

#define LSM303_ACC_CLCK_THS             0x3A

/* Accelerometer Time registers ----------------------------------------------*/
#define LSM303_ACC_TIME_LIMIT           0x3B
#define LSM303_ACC_TIME_LAT             0x3C
#define LSM303_ACC_TIME_WIND            0x3D

/* Accelerometer Time registers ----------------------------------------------*/
#define LSM303_ACC_ACT_THS              0x3E
#define LSM303_ACC_ACT_DUR              0x3F


/* Magnetometer Offset registers ---------------------------------------------*/
#define LSM303_MAG_OFFSET_X_L       0x45
#define LSM303_MAG_OFFSET_X_H       0x46
#define LSM303_MAG_OFFSET_Y_L       0x47
#define LSM303_MAG_OFFSET_Y_H       0x48
#define LSM303_MAG_OFFSET_Z_L       0x49
#define LSM303_MAG_OFFSET_Z_H       0x4A

/* Magnetometer Who Am I register --------------------------------------------*/
#define LSM303_MAG_WHO_AM_I         0x4F
#define LSM303_MAG_WHO_AM_I_VAL     0x40

/* Magnetometer Control register 1 -------------------------------------------*/
#define LSM303_MAG_CTRL1            0x60
#define LSM303_MAG_CTRL1_MD_CONT    0x00 // Mode select: continuous
#define LSM303_MAG_CTRL1_MD_SINGLE  0x01 //              single
#define LSM303_MAG_CTRL1_MD_IDLE    0x02 //              idle
#define LSM303_MAG_CTRL1_ODR10      0x00 // ODR: 10  Hz
#define LSM303_MAG_CTRL1_ODR20      0x04 //      20  Hz
#define LSM303_MAG_CTRL1_ODR50      0x08 //      50  Hz
#define LSM303_MAG_CTRL1_ODR100     0x0C //      100 Hz
#define LSM303_MAG_CTRL1_LP         0x10 // Low-power mode
#define LSM303_MAG_CTRL1_SOFT_RST   0x20 // Reset config and user registers
#define LSM303_MAG_CTRL1_REBOOT     0x40 // Reboot memory content
#define LSM303_MAG_CTRL1_TEMP_EN    0x80 // Enable temperature compensation

/* Magnetometer Control register 2 -------------------------------------------*/
#define LSM303_MAG_CTRL2            0x61
#define LSM303_MAG_CTRL2_LPF        0x01 // Enable low-pass filter
#define LSM303_MAG_CTRL2_OFF_CANC   0x02 // Enable offset cancellation
#define LSM303_MAG_CTRL2_SET_ODR    0x00 // Set pulse after every 63 ODR
#define LSM303_MAG_CTRL2_SET_PD     0x04 // Set pulse after PD condition
#define LSM303_MAG_CTRL2_OFF_INT    0x08 // Discover interrupt after hard-iron offset correction
#define LSM303_MAG_CTRL2_OFF_CANC_S 0x10 // Enable offset cancellation in single measurement mode

/* Magnetometer Control register 3 -------------------------------------------*/
#define LSM303_MAG_CTRL3            0x62
#define LSM303_MAG_CTRL3_DRDY_EN    0x01 // Enable data-ready output
#define LSM303_MAG_CTRL3_ST_EN      0x02 // Enable self-test
#define LSM303_MAG_CTRL3_END_LITTLE 0x00 // Little endian
#define LSM303_MAG_CTRL3_END_BIG    0x08 // Big endian
#define LSM303_MAG_CTRL3_BDU        0x10 // Block data update
#define LSM303_MAG_CTRL3_I2C_DIS    0x20 // I2C disabled
#define LSM303_MAG_CTRL3_INT_MAG    0x40 // Interrupt signal is driven on pin INT_MAG

/* Magnetometer Interrupt Generator registers --------------------------------*/
#define LSM303_MAG_IG_CFG           0x63 // IG config
#define LSM303_MAG_IG_CFG_EN        0x01 // Interrupt enabled
#define LSM303_MAG_IG_CFG_LATCHED   0x02 // Defines whether interrupt is pulsed (0) or latched (1)
#define LSM303_MAG_IG_CFG_ACTIVE_H  0x04 // Active high (0: low)
#define LSM303_MAG_IG_CFG_ZIEN      0x20 // Enable on axis: Z
#define LSM303_MAG_IG_CFG_YIEN      0x40 //                 Y
#define LSM303_MAG_IG_CFG_XIEN      0x80 //                 X

#define LSM303_MAG_IG_SRC           0x64 // IG status
#define LSM303_MAG_IG_SRC_ACTIVE    0x01 // IG: active (interrupt generated)

#define LSM303_MAG_IG_THS_L         0x65 // IG threshold low
#define LSM303_MAG_IG_THS_H         0x66 //              high

/* Magnetometer Status register ----------------------------------------------*/
#define LSM303_MAG_STATUS           0x67
#define LSM303_MAG_STATUS_ZYXDA     0x08 // Data available for X, Y and Z axis
#define LSM303_MAG_STATUS_ZYXOR     0x80 // Data overrun   for X, Y and Z axis

/* Magnetometer Data registers -----------------------------------------------*/
#define LSM303_MAG_X_L              0x68 // X low:  X[1: 8]
#define LSM303_MAG_X_H              0x69 // X high: X[9:16]
#define LSM303_MAG_Y_L              0x6A // Y low:  Y[1: 8]
#define LSM303_MAG_Y_H              0x6B // Y high: Y[9:16]
#define LSM303_MAG_Z_L              0x6C // Z low:  Z[1: 8]
#define LSM303_MAG_Z_H              0x6D // Z high: Z[9:16]

/* Debug Printing ------------------------------------------------------------*/
#define IMU_PRINT_OFF               (0x00)    // Default: Printing off
#define IMU_PRINT_ACC               (1 << 0)  // Bit 0: Print accelerometer data
#define IMU_PRINT_MAG               (1 << 1)  // Bit 1: Print magnetometer data
#define IMU_PRINT_DEBUG             (1 << 2)  // Bit 2: Print debug messages

/* Enumerations --------------------------------------------------------------*/
typedef enum
{
    IMU_SINGLE_SHOT   = 0, // Single-shot: only record when ACG is triggered
    IMU_TRIGGERED_HP  = 1, // Triggered mode, with high-pass
    IMU_TRIGGERED_REF = 2, // Triggered mode, with reference
    IMU_CONT          = 3  // Stream mode (always-on)
} ImuOpMode_t;

/* Functions -----------------------------------------------------------------*/
void        IMU_init();
void        IMU_configModeTriggeredHP();
void        IMU_configModeTriggeredRef();
void        IMU_configModeStream(uint32_t signalFifoOverrun);

void        IMU_setHighODR();
void        IMU_setLowODR();
void        IMU_triggerEnable();
void        IMU_triggerDisable();
void        IMU_setReferenceSignal();
uint32_t    IMU_dataCalculateSAD(uint8_t numOfData);
void        IMU_calculateAvg(rb_t* rb);
void        IMU_magEnable();
void        IMU_magDisable();
void        IMU_accEnable();
void        IMU_accDisable();

uint8_t     IMU_emptyFifo(bool notifyLogging, rb_t* ringBuffer);
uint8_t     IMU_readFifoLevel();
uint8_t     IMU_readFromFifo(uint8_t count, bool notifyLogging, rb_t* ringBuffer);
uint32_t    IMU_readInterrupts();

void        IMU_printDataPoint(ImuData_t* imu_buf);

#endif /* __LSM303AGR_H_ */
