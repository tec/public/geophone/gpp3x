/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __RTC_H
#define __RTC_H

/* Defines -------------------------------------------------------------------*/
#define RTC_WAKEUP_IT_PRIO              RTOS_ISR_PRIO_WAKEUP_RTC    /* Medium priority */
#define RTC_ALARM_IT_PRIO               RTOS_ISR_PRIO_ALARM_RTC
#define RTC_SETTIME_OVERHEAD_US         300                         /* Estimated overhead for setting the RTC time */
#define RTC_MAX_DRIFT_COMP_PPM          488                         /* Max. possible drift compensation */
#define RTC_DRIFT_COMP_GRANULARITY_PPM  0.9537f
#define RTC_SHIFT_MAX_WAIT_TIME_MS      1000                        /* RTC will freeze for up to 1s until a shift (update) is complete */
#define RTC_TIMESTAMP_2000_01_01        946684800

/* Macros --------------------------------------------------------------------*/
#define RTC_TIMESTAMP()                 RTC_getTimestampUS()

/* RTC Functions -------------------------------------------------------------*/
void                   RTC_init(void);
void                   RTC_reset(void);
void                   RTC_sync(void);
void                   RTC_startWakeUpTimer(uint32_t period_s);
void                   RTC_stopWakeUpTimer(void);
const RTC_DateTypeDef* RTC_getDate(bool update);
const RTC_TimeTypeDef* RTC_getTime(bool update);
void                   RTC_getDateTime(const RTC_DateTypeDef** date, const RTC_TimeTypeDef** time);
uint32_t               RTC_getDay(void);                                                                          /* Get current date day */
const char*            RTC_getFormattedString(void);                                                              /* Get current Date/Time as a formatted string */
const char*            RTC_getFormattedDate(void);                                                                /* Get current Date as a formatted string */
uint32_t               RTC_getEpoch(void);                                                                        /* Get current unix timestamp */
uint64_t               RTC_getTimestampUS(void);                                                                  /* Get the current UNIX timestamp in microseconds */
uint64_t               RTC_getTimestampMS(void);                                                                  /* Get the current UNIX timestamp in milliseconds */
void                   RTC_convertEpochToDatetime(uint32_t epoch, RTC_DateTypeDef* date, RTC_TimeTypeDef* time);  /* Convert unix timestamp to Date/Time */
uint32_t               RTC_convertDatetimeToEpoch(RTC_DateTypeDef date, RTC_TimeTypeDef time);                    /* Convert date/time to unix timestamp */
void                   RTC_convertEpochToFatFSTimestamp(uint32_t epoch, uint32_t* fdatetime);
uint32_t               RTC_convertToDayOfYear(uint16_t year, uint8_t month, uint8_t day);
void                   RTC_adjustTimeZone(int8_t offset);                                                         /* Adjust timezone */
void                   RTC_setFromEpoch(uint32_t epoch);                                                          /* Set RTC date/time from unix timestamp */
void                   RTC_setAlarmFromEpoch(uint32_t epoch);                                                     /* Set RTC alarm from unix timestamp */
uint32_t               RTC_getUptime(void);                                                                       /* System uptime in seconds */
uint64_t               RTC_getUptimeMS(void);                                                                     /* System uptime in milliseconds */
void                   RTC_resetUptime(void);
bool                   RTC_compensateDrift(int32_t offset_ppm);                                                   /* Adjust for clock drift (a positive value will speed up the clock) */

#endif /* __RTC_H */
