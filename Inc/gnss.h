/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __GNSS_H
#define __GNSS_H

/*
 * Driver for the uBlox ZED-F9P-00B and NEO-M8T GNSS receivers
 */

/* Configurations ------------------------------------------------------------*/
#define GNSS_TP_EN                  1           /* Enable TP messages */
#define GNSS_NUM_OF_TP              2           /* Number of TP messages required for a sync */
#define GNSS_NUM_MEAS_MIN           4           /* Min. number of measurements required for a position/time fix */
#define GNSS_TIMEZONE               0           /* Timezone setting (0: UTC) */
#define GNSS_TIMEREF_UTC            0           /* Use UTC as timeref (instead of GPS) */
#define GNSS_GPS_TO_UNIX_TIME_OFS   315964800   /* Offset of GPS time epoch compared to UNIX time */
#define GNSS_PWRON_DELAY_MS         1500        /* Time to wait for the GNSS receiver to start up after a power on */
#define GNSS_VERSION_NUMBER         0x01        /* Version number the GNSS data should have */
#define GNSS_UART_IDLE_TIMEOUT_MS   3000        /* Wait for idle UART timeout, in ms (must be >= 10) */
#define GNSS_UART_HIGH_BAUDRATE     115200      /* Max. supported is 921600 (F9P, but doesn't appear to work reliably) and 460800 (M8), respectively */
#define GNSS_UART_TX_TIMEOUT        200         /* Timeout for UART transmissions, in HAL ticks */
#define GNSS_UART_RX_TIMEOUT        200         /* Timeout for UART reception, in HAL ticks */
#define GNSS_UART_TX_DELAY          10          /* Delay after a UART transmission, in HAL ticks */
#define GNSS_UART_DMARX_TIMEOUT_MS  10          /* UART DMA RX timeout in ms, must be between 1 and 1000 */
#define GNSS_RX_BUFFER_SIZE         1024        /* UART RX DMA buffer size */
#define GNSS_MSG_OUTPUT_HZ          1           /* Output rate on enabled ports */
#define GNSS_MEASUREMENT_PERIOD_MS  1000        /* Measurement period in ms */

#ifdef GNSS_MODEL_M8
  #define GNSS_VERSION_STRING         "NEO-M8"
  #define GNSS_UART_DEFAULT_BAUDRATE  9600      /* Default UART baudrate of the GNSS receiver */
  #define GNSS_MAX_TRACKING_CH        72
#endif
#ifdef GNSS_MODEL_F9P
  #define GNSS_VERSION_STRING         "ZED-F9P"
  #define GNSS_UART_DEFAULT_BAUDRATE  38400     /* Default UART baudrate of the GNSS receiver */
  #define GNSS_MAX_TRACKING_CH        182
#endif


/* Defines -------------------------------------------------------------------*/

/* UBX Message structure:
 *  | SYNC CHARS | MSG CLASS | MSG ID | LENGTH | PAYLOAD | CRC |
 *  |      2     |      1    |    1   |    2   |    X    |  2  | */

/* UBX Protocol Package Structure (Little Endian format):
 * 2 bytes: SYNC CHAR    {0xB5, 0x62}
 * 1 byte:  CLASS        {NAV: 0x01, RXM: 0x02, INF: 0x04, ACK: 0x05, CFG: 0x06}
 * 1 byte:  MSG ID
 * 2 bytes: LENGTH (of payload)
 * X bytes: PAYLOAD
 * 2 bytes: CRC
 */

#define GNSS_MSG_LEN_HDR            6       /* Header length with sync chars */
#define GNSS_MSG_LEN_SYNC           2       /* 2 sync chars in the header */
#define GNSS_MSG_LEN_CRC            2
#define GNSS_MSG_LEN_MIN            (GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC)
#define GNSS_MSG_PAYLOAD_LEN_MAX    512
#define GNSS_MSG_LEN_MAX            (GNSS_MSG_LEN_HDR + GNSS_MSG_PAYLOAD_LEN_MAX + GNSS_MSG_LEN_CRC)   /* Max. message length */
#define GNSS_MSG_LEN(ubx_msg)       ((ubx_msg)->len + GNSS_MSG_LEN_HDR + GNSS_MSG_LEN_CRC)

// Sync
#define GNSS_MSG_SYNC_CHAR_1        0xB5
#define GNSS_MSG_SYNC_CHAR_2        0x62

// Message Class IDs
#define GNSS_MSG_CLASS_NAV          0x01
#define GNSS_MSG_CLASS_RXM          0x02
#define GNSS_MSG_CLASS_INF          0x04
#define GNSS_MSG_CLASS_ACK          0x05
#define GNSS_MSG_CLASS_CFG          0x06
#define GNSS_MSG_CLASS_UPD          0x09
#define GNSS_MSG_CLASS_MON          0x0A
#define GNSS_MSG_CLASS_TIM          0x0D
#define GNSS_MSG_CLASS_MGA          0x13
#define GNSS_MSG_CLASS_LOG          0x21
#define GNSS_MSG_CLASS_SEC          0x27

// Message IDs
#define GNSS_MSG_ID_ACK_NAK         0x00
#define GNSS_MSG_ID_ACK_ACK         0x01
#define GNSS_MSG_ID_CFG_PRT         0x00
#define GNSS_MSG_ID_CFG_MSG         0x01
#define GNSS_MSG_ID_CFG_INF         0x02
#define GNSS_MSG_ID_CFG_RST         0x04
#define GNSS_MSG_ID_CFG_RATE        0x08
#define GNSS_MSG_ID_CFG_CFG         0x09
#define GNSS_MSG_ID_CFG_TP5         0x31
#define GNSS_MSG_ID_CFG_GNSS        0x3E
#define GNSS_MSG_ID_CFG_VALSET      0x8A
#define GNSS_MSG_ID_INF_WARNING     0x01
#define GNSS_MSG_ID_MON_VER         0x04
#define GNSS_MSG_ID_RXM_RAWX        0x15
#define GNSS_MSG_ID_TIM_TP          0x01
#define GNSS_MSG_ID_TIM_TM2         0x03

// Message payload lengths
#define GNSS_MSG_LEN_CFG_PRT        20
#define GNSS_MSG_LEN_CFG_MSG        8
#define GNSS_MSG_LEN_CFG_INF(N)     ((N) * 10)
#define GNSS_MSG_LEN_CFG_RATE       6
#define GNSS_MSG_LEN_CFG_TP5        32
#define GNSS_MSG_LEN_CFG_GNSS(N)    (4 + (N) * 8)
#define GNSS_MSG_LEN_MON_VER_EXT    30
#define GNSS_MSG_LEN_MON_VER_MIN    40
#define GNSS_MSG_LEN_MON_VER(N)     (GNSS_MSG_LEN_MON_VER_MIN + (N) * GNSS_MSG_LEN_MON_VER_EXT)
#define GNSS_MSG_LEN_RXM_RAWX_MEAS  32
#define GNSS_MSG_LEN_RXM_RAWX(N)    (16 + (N) * GNSS_MSG_LEN_RXM_RAWX_MEAS)

// UBX_CFG: Configuration Keys
#define GNSS_UBX_CFG_IDX_LENGTH     4
#define GNSS_UBX_CFG_IDX_KEY        10
#define GNSS_UBX_CFG_IDX_VALUE      14

#define GNSS_UBX_CFG_INFMSG_ERROR   0x01
#define GNSS_UBX_CFG_INFMSG_WARNING 0x02
#define GNSS_UBX_CFG_INFMSG_NOTICE  0x04
#define GNSS_UBX_CFG_INFMSG_TEST    0x08
#define GNSS_UBX_CFG_INFMSG_DEBUG   0x10

#define GNSS_UBX_CFG_INFMSG_UBX_UART1              0x20920002
#define GNSS_UBX_CFG_INFMSG_UBX_UART1_LEN          1
#define GNSS_UBX_CFG_INFMSG_NMEA_UART1             0x20920007
#define GNSS_UBX_CFG_INFMSG_NMEA_UART1_LEN         1

#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_DTM_UART1      0x209100a7
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_DTM_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GBS_UART1      0x209100de
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GBS_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GGA_UART1      0x209100bb
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GGA_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GLL_UART1      0x209100ca
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GLL_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GNS_UART1      0x209100b6
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GNS_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GRS_UART1      0x209100cf
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GRS_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSA_UART1      0x209100c0
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSA_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GST_UART1      0x209100d4
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GST_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSV_UART1      0x209100c5
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_GSV_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_RMC_UART1      0x209100ac
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_RMC_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_VLW_UART1      0x209100e8
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_VLW_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_VTG_UART1      0x209100b1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_VTG_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_ZDA_UART1      0x209100d9
#define GNSS_UBX_CFG_MSGOUT_NMEA_ID_ZDA_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_UBX_LOG_INFO_UART1     0x2091025a
#define GNSS_UBX_CFG_MSGOUT_UBX_LOG_INFO_UART1_LEN 1
#define GNSS_UBX_CFG_MSGOUT_UBX_RXM_RAWX_UART1     0x209102a5
#define GNSS_UBX_CFG_MSGOUT_UBX_RXM_RAWX_UART1_LEN 1
#define GNSS_UBX_CFG_MSGOUT_UBX_TIM_TP_UART1       0x2091017e
#define GNSS_UBX_CFG_MSGOUT_UBX_TIM_TP_UART1_LEN   1
#define GNSS_UBX_CFG_MSGOUT_UBX_TIM_TM2_UART1      0x20910179
#define GNSS_UBX_CFG_MSGOUT_UBX_TIM_TM2_UART1_LEN  1
#define GNSS_UBX_CFG_MSGOUT_UBX_NAV_TIMELS_UART1   0x20910061
#define GNSS_UBX_CFG_MSGOUT_UBX_NAV_TIMELS_UART1_LEN  1

#define GNSS_UBX_CFG_RATE_MEAS                     0x30210001
#define GNSS_UBX_CFG_RATE_MEAS_LEN                 2
#define GNSS_UBX_CFG_RATE_NAV                      0x30210002
#define GNSS_UBX_CFG_RATE_NAV_LEN                  2
#define GNSS_UBX_CFG_RATE_TIMEREF                  0x20210003
#define GNSS_UBX_CFG_RATE_TIMEREF_LEN              1

#define GNSS_UBX_CFG_SIGNAL_GPS_ENA                0x1031001f
#define GNSS_UBX_CFG_SIGNAL_GPS_ENA_LEN            1
#define GNSS_UBX_CFG_SIGNAL_GPS_L1CA_ENA           0x10310001
#define GNSS_UBX_CFG_SIGNAL_GPS_L1CA_ENA_LEN       1
#define GNSS_UBX_CFG_SIGNAL_GPS_L2C_ENA            0x10310003
#define GNSS_UBX_CFG_SIGNAL_GPS_L2C_ENA_LEN        1
#define GNSS_UBX_CFG_SIGNAL_GAL_ENA                0x10310021
#define GNSS_UBX_CFG_SIGNAL_GAL_ENA_LEN            1
#define GNSS_UBX_CFG_SIGNAL_GAL_E1_ENA             0x10310007
#define GNSS_UBX_CFG_SIGNAL_GAL_E1_ENA_LEN         1
#define GNSS_UBX_CFG_SIGNAL_GAL_E5B_ENA            0x1031000a
#define GNSS_UBX_CFG_SIGNAL_GAL_E5B_ENA_LEN        1
#define GNSS_UBX_CFG_SIGNAL_BDS_ENA                0x10310022
#define GNSS_UBX_CFG_SIGNAL_BDS_ENA_LEN            1
#define GNSS_UBX_CFG_SIGNAL_BDS_B1_ENA             0x1031000d
#define GNSS_UBX_CFG_SIGNAL_BDS_B1_ENA_LEN         1
#define GNSS_UBX_CFG_SIGNAL_BDS_B2_ENA             0x1031000e
#define GNSS_UBX_CFG_SIGNAL_BDS_B2_ENA_LEN         1
#define GNSS_UBX_CFG_SIGNAL_QZSS_ENA               0x10310024
#define GNSS_UBX_CFG_SIGNAL_QZSS_ENA_LEN           1
#define GNSS_UBX_CFG_SIGNAL_QZSS_L1CA_ENA          0x10310012
#define GNSS_UBX_CFG_SIGNAL_QZSS_L1CA_ENA_LEN      1
#define GNSS_UBX_CFG_SIGNAL_QZSS_L2C_ENA           0x10310015
#define GNSS_UBX_CFG_SIGNAL_QZSS_L2C_ENA_LEN       1
#define GNSS_UBX_CFG_SIGNAL_GLO_ENA                0x10310025
#define GNSS_UBX_CFG_SIGNAL_GLO_ENA_LEN            1
#define GNSS_UBX_CFG_SIGNAL_GLO_L1_ENA             0x10310018
#define GNSS_UBX_CFG_SIGNAL_GLO_L1_ENA_LEN         1
#define GNSS_UBX_CFG_SIGNAL_GLO_L2_ENA             0x1031001a
#define GNSS_UBX_CFG_SIGNAL_GLO_L2_ENA_LEN         1

#define GNSS_UBX_CFG_TP_PULSE_DEF                  0x20050023
#define GNSS_UBX_CFG_TP_PULSE_DEF_LEN              1
#define GNSS_UBX_CFG_TP_PULSE_LENGTH_DEF           0x20050030
#define GNSS_UBX_CFG_TP_PULSE_LENGTH_DEF_LEN       1
#define GNSS_UBX_CFG_TP_PERIOD_TP1                 0x40050002
#define GNSS_UBX_CFG_TP_PERIOD_TP1_LEN             4
#define GNSS_UBX_CFG_TP_PERIOD_LOCK_TP1            0x40050003
#define GNSS_UBX_CFG_TP_PERIOD_LOCK_TP1_LEN        4
#define GNSS_UBX_CFG_TP_FREQ_TP1                   0x40050024
#define GNSS_UBX_CFG_TP_FREQ_TP1_LEN               4
#define GNSS_UBX_CFG_TP_FREQ_LOCK_TP1              0x40050025
#define GNSS_UBX_CFG_TP_FREQ_LOCK_TP1_LEN          4
#define GNSS_UBX_CFG_TP_LEN_TP1                    0x40050004
#define GNSS_UBX_CFG_TP_LEN_TP1_LEN                4
#define GNSS_UBX_CFG_TP_LEN_LOCK_TP1               0x40050005
#define GNSS_UBX_CFG_TP_LEN_LOCK_TP1_LEN           4
#define GNSS_UBX_CFG_TP_DUTY_TP1                   0x5005002a
#define GNSS_UBX_CFG_TP_DUTY_TP1_LEN               8
#define GNSS_UBX_CFG_TP_DUTY_LOCK_TP1              0x5005002b
#define GNSS_UBX_CFG_TP_DUTY_LOCK_TP1_LEN          8
#define GNSS_UBX_CFG_TP_USER_DELAY_TP1             0x40050006
#define GNSS_UBX_CFG_TP_USER_DELAY_TP1_LEN         4
#define GNSS_UBX_CFG_TP_TP1_ENA                    0x10050007
#define GNSS_UBX_CFG_TP_TP1_ENA_LEN                1
#define GNSS_UBX_CFG_TP_SYNC_GNSS_TP1              0x10050008
#define GNSS_UBX_CFG_TP_SYNC_GNSS_TP1_LEN          1
#define GNSS_UBX_CFG_TP_USE_LOCKED_TP1             0x10050009
#define GNSS_UBX_CFG_TP_USE_LOCKED_TP1_LEN         1
#define GNSS_UBX_CFG_TP_ALIGN_TO_TOW_TP1           0x1005000a
#define GNSS_UBX_CFG_TP_ALIGN_TO_TOW_TP1_LEN       1
#define GNSS_UBX_CFG_TP_POL_TP1                    0x1005000b
#define GNSS_UBX_CFG_TP_POL_TP1_LEN                1
#define GNSS_UBX_CFG_TP_TIMEGRID_TP1               0x2005000c
#define GNSS_UBX_CFG_TP_TIMEGRID_TP1_LEN           1

#define GNSS_UBX_CFG_UART1_BAUDRATE                0x40520001
#define GNSS_UBX_CFG_UART1_BAUDRATE_LEN            4
#define GNSS_UBX_CFG_UART1_STOPBITS                0x20520002
#define GNSS_UBX_CFG_UART1_STOPBITS_LEN            1
#define GNSS_UBX_CFG_UART1_DATABITS                0x20520003
#define GNSS_UBX_CFG_UART1_DATABITS_LEN            1
#define GNSS_UBX_CFG_UART1_PARITY                  0x20520004
#define GNSS_UBX_CFG_UART1_PARITY_LEN              1
#define GNSS_UBX_CFG_UART1_ENABLED                 0x10520005
#define GNSS_UBX_CFG_UART1_ENABLED_LEN             1
#define GNSS_UBX_CFG_UART1INPROT_UBX               0x10730001
#define GNSS_UBX_CFG_UART1INPROT_UBX_LEN           1
#define GNSS_UBX_CFG_UART1INPROT_NMEA              0x10730002
#define GNSS_UBX_CFG_UART1INPROT_NMEA_LEN          1
#define GNSS_UBX_CFG_UART1INPROT_RTCM3X            0x10730004
#define GNSS_UBX_CFG_UART1INPROT_RTCM3X_LEN        1
#define GNSS_UBX_CFG_UART1OUTPROT_UBX              0x10740001
#define GNSS_UBX_CFG_UART1OUTPROT_UBX_LEN          1
#define GNSS_UBX_CFG_UART1OUTPROT_NMEA             0x10740002
#define GNSS_UBX_CFG_UART1OUTPROT_NMEA_LEN         1
#define GNSS_UBX_CFG_UART1OUTPROT_RTCM3X           0x10740004
#define GNSS_UBX_CFG_UART1OUTPROT_RTCM3X_LEN       1

/* Enumerations --------------------------------------------------------------*/
typedef enum                                    /* GNSS states */
{
    GNSS_OFF = 0,                               /* GNSS receiver is off */
    GNSS_ON = 1,                                /* GNSS receiver is on but not yet configured, DMA is disabled */
    GNSS_DISABLED = 2,                          /* GNSS receiver is on, but all UBX messages are disabled */
    GNSS_ENABLED = 3,                           /* GNSS receiver is on and UBX message output is enabled */
    GNSS_NUM_OF_STATES
} GNSS_state_t;

/* Structures ----------------------------------------------------------------*/
typedef struct  __attribute__((__packed__))
{
    uint8_t  header[2];   /* 2 sync chars */
    uint8_t  class;
    uint8_t  id;
    uint16_t len;         /* Payload length, little endian */
    union
    {
        struct
        {
            uint8_t  portId;
            uint8_t  reserved1;
            uint16_t txReady;
            uint32_t mode;
            uint32_t baudRate;
            uint16_t inProtoMask;
            uint16_t outProtoMask;
            uint16_t flags;
            uint16_t reserved2;
        } cfg_prt;

        struct
        {
            uint8_t  protocolId;
            uint8_t  reserved[3];
            uint8_t  infMsgMask[6];
        } cfg_inf[GNSS_MSG_PAYLOAD_LEN_MAX / 10];

        struct
        {
            uint16_t measRate;
            uint16_t navRate;
            uint16_t timeRef;
        } cfg_rate;

        struct
        {
            uint8_t  msgClass;
            uint8_t  msgId;
            uint8_t  rate[6];
        } cfg_msg;

        struct
        {
            uint8_t  tpIdx;
            uint8_t  version;
            uint8_t  reserved[2];
            uint8_t  unused[4];
            uint32_t freqPeriod;
            uint32_t freqPeriodLock;
            uint32_t pulseLenRatio;
            uint32_t pulseLenRatioLock;
            int32_t  userConfigDelay;
            uint32_t flags;
        } cfg_tp5;

        struct
        {
            uint8_t  msgVer;
            uint8_t  numTrkChHw;                  /* Read-only field */
            uint8_t  numTrkChUse;
            uint8_t  numConfigBlocks;
            struct
            {
                uint8_t  gnssId;
                uint8_t  resTrkCh;
                uint8_t  maxTrkCh;
                uint8_t  reserved2;
                uint32_t flags;
            } gnss[(GNSS_MSG_PAYLOAD_LEN_MAX - 4) / 8];
        } cfg_gnss;

        struct
        {
            char swVersion[30];
            char hwVersion[10];
            struct
            {
                char str[30];
            } extInfo[(GNSS_MSG_PAYLOAD_LEN_MAX - GNSS_MSG_LEN_MON_VER_MIN) / GNSS_MSG_LEN_MON_VER_EXT];
        } mon_ver;

        struct
        {
            char str[GNSS_MSG_PAYLOAD_LEN_MAX];
        } inf_warning;

        struct
        {
            uint8_t  rcvTow[8];
            uint16_t week;
            int8_t   leapS;
            uint8_t  numMeas;
            uint8_t  recStat;
            uint8_t  reserved[3];
            struct
            {
                uint8_t  prMes[8];
                uint8_t  cpMes[8];
                uint8_t  doMes[4];
                uint8_t  gnssId;
                uint8_t  svId;
                uint8_t  reserved2;
                uint8_t  freqId;
                uint16_t locktime;
                uint8_t  cno;
                uint8_t  prStDev;
                uint8_t  cpStDev;
                uint8_t  doStDev;
                uint8_t  trkStat;
                uint8_t  reserved3;
            } meas_blk[(GNSS_MSG_PAYLOAD_LEN_MAX - GNSS_MSG_LEN_RXM_RAWX(0)) / GNSS_MSG_LEN_RXM_RAWX_MEAS];
        } rxm_rawx;

        struct
        {
            uint8_t   ch;
            uint8_t   flags;
            uint16_t  count;
            uint16_t  wnR;
            uint16_t  wnF;
            uint32_t  towMsR;
            uint32_t  towSubMsR;
            uint32_t  towMsF;
            uint32_t  towSubMsF;
            uint32_t  accEst;
        } tim_tm2;

        struct
        {
            uint32_t  towMS;
            uint32_t  towSubMS;
            int32_t   qErr;
            uint16_t  week;
            uint8_t   flags;
            uint8_t   refInfo;
        } tim_tp;

        uint8_t payload[GNSS_MSG_PAYLOAD_LEN_MAX + GNSS_MSG_LEN_CRC];   /* Also allocate memory for the CRC */
    };
} gnss_ubx_msg_t;

/* Functions -----------------------------------------------------------------*/
uint32_t    GNSS_timestampFromRAWX(const gnss_ubx_msg_t* ubx_raw_msg);  /* Returns timestamp from raw        message | 0: on error */
uint32_t    GNSS_timestampFromTP(const gnss_ubx_msg_t* ubx_tp_msg);     /* Returns timestamp from time pulse message | 0: on error */
uint32_t    GNSS_timestampFromTM(const gnss_ubx_msg_t* ubx_tm_msg);     /* Returns timestamp from time mark  message | 0: on error */
void        GNSS_timePulseCallback(void);                               /* Called upon rising edge of the PPS pin */
void        GNSS_updateLeapSecs(const gnss_ubx_msg_t* ubx_raw_msg);     /* Extracts the leap seconds from a RAW message */
bool        GNSS_checkCRC(const gnss_ubx_msg_t* msg, uint16_t size);    /* Returns true if the message CRC is valid, false otherwise */

void        GNSS_init(void);                                            /* Initialize GNSS struct */
void        GNSS_enable(void);                                          /* Enable GNSS UART & Rx DMA (can only be used when GNSS was previously disabled) */
void        GNSS_disable(void);                                         /* Disable GNSS UART & Rx DMA */
void        GNSS_powerOn(void);                                         /* Power-On GNSS - Receive DMA can be enabled if needed */
void        GNSS_powerOff(void);                                        /* Power-Off GNSS */
void        GNSS_reinitUART(bool enable_dma);                           /* Deinit and re-initialize the UART interface */

bool        GNSS_requestInfo(gnss_ubx_msg_t* out_info, uint32_t len);   /* Request Receiver/Software version */
const char* GNSS_requestModVer(void);                                   /* Returns the GNSS module version string */
bool        GNSS_requestConfig(uint8_t msg_id, gnss_ubx_msg_t* out_cfg, uint32_t len);

bool        GNSS_sendUBXmsg(gnss_ubx_msg_t* msg, uint16_t payload_len); /* Send UBX message to GNSS module */
void        GNSS_setUBXConfigValue(uint32_t key, uint8_t* value, uint8_t value_length);


#endif /* __GNSS_H */
