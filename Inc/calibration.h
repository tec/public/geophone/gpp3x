/*
 * Copyright (c) 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __CALIBRATION_H
#define __CALIBRATION_H

/* Trigger bias in mV (at 25°C) */
static const int16_t geo_trg_bias[][NUM_GEO_TRG_BIAS] =
{
    /* Columns: Device ID, Stage 1 AmpOut (-1500mV), Stage 2 AmpOut (-1500mV), neg TH bias (at 1300mV), pos TH bias (at 1700mV) */
    { 22011,  5,  33,   2,   0 },
    { 22012,  4,   0,  -3,  -2 },
    { 22013,  2,   0,   2,  -2 },
    { 22014,  2,   0,   1,   7 },
    { 22015,  4,   0,   1,   1 },
    { 22016,  0,   0,   0,   0 },
    { 22017,  0,   0,   0,   0 },
    { 22018,  0,   0,   0,   0 },
    { 22019,  0,   0,   0,   0 },
    { 22020,  0,   0,   0,   0 }
};
static const uint16_t geo_trg_bias_cnt = sizeof(geo_trg_bias) / (sizeof(int16_t) * NUM_GEO_TRG_BIAS);


#endif
