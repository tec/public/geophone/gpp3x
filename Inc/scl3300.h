/*
 * Copyright (c) 2019 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SCL3300_H_
#define __SCL3300_H_

#include <stdint.h>

/* Commands ------------------------------------------------------------------*/

// SPI commands as listed in Sec. 5.1.4; each frame is a 32bit package including CRC

// Bit  [31]  RW - Read = 0, Write = 1
// Bits [30:26]  OP Addr
// Bits [25:24] Return status (RS)
// Bits [23: 8]  Data
// Bits [ 7: 0] CRC

// Read sensor data
#define SCL3300_READ_ACC_X  0x040000F7
#define SCL3300_READ_ACC_Y  0x080000FD
#define SCL3300_READ_ACC_Z  0x0C0000FB
#define SCL3300_READ_ANG_X  0x240000C7
#define SCL3300_READ_ANG_Y  0x280000CD
#define SCL3300_READ_ANG_Z  0x2C0000CB

// Read system information
#define SCL3300_READ_STO  0x100000E9
#define SCL3300_READ_TEMP  0x140000EF
#define SCL3300_READ_STATUS  0x180000E5
#define SCL3300_READ_ERR1  0x1C0000E3
#define SCL3300_READ_ERR2  0x200000C1
#define SCL3300_READ_CMD  0x340000DF
#define SCL3300_READ_WHOAMI  0x40000091
#define SCL3300_READ_SER1  0x640000A7
#define SCL3300_READ_SER2  0x680000AD
#define SCL3300_READ_CURR  0x7C0000B3

// Enable outputs and modes
#define SCL3300_EN_ANGLE  0xB0001F6F
#define SCL3300_EN_MODE1  0xB400001F
#define SCL3300_EN_MODE2  0xB4000102
#define SCL3300_EN_MODE3  0xB4000225
#define SCL3300_EN_MODE4  0xB4000338
#define SCL3300_EN_BANK1  0xFC000073
#define SCL3300_EN_BANK2  0xFC00016E

// Power management
#define SCL3300_WK_UP    0xB400001F
#define SCL3300_PW_DOWN    0xB400046B
#define SCL3300_RESET    0xB4002098


// Command building blocks
#define SCL3300_MASK_RW    0x80000000
#define SCL3300_OFFSET_RW  31
#define SCL3300_MASK_REG  0x7C000000
#define SCL3300_OFFSET_REG  26
#define SCL3300_MASK_RS    0x03000000
#define SCL3300_OFFSET_RS  24
#define SCL3300_MASK_DATA  0x00FFFF00
#define SCL3300_OFFSET_DATA  8
#define SCL3300_MASK_CRC  0x000000FF
#define SCL3300_OFFSET_CRC  0

// Read/Write
#define SCL3300_FRAME_R    0b0
#define SCL3300_FRAME_W    0b1

// Registers
#define SCL3300_REG_ACC_X  0x01
#define SCL3300_REG_ACC_Y  0x02
#define SCL3300_REG_ACC_Z  0x03
#define SCL3300_REG_STO    0x04
#define SCL3300_REG_TEMP  0x05
#define SCL3300_REG_STATUS  0x06
#define SCL3300_REG_ERR1  0x07
#define SCL3300_REG_ERR2  0x08
#define SCL3300_REG_ANG_X  0x09
#define SCL3300_REG_ANG_Y  0x0A
#define SCL3300_REG_ANG_Z  0x0B
#define SCL3300_REG_ANG_CTL  0x0C
#define SCL3300_REG_MODE  0x0D
#define SCL3300_REG_WHOAMI  0x10
#define SCL3300_REG_BANK1  0x19
#define SCL3300_REG_BANK2  0x1A
#define SCL3300_REG_SELBANK  0x1F

// CRC algorithm can be found in the datasheet


/* Return values -------------------------------------------------------------*/

// Notice that the LSC is operated in off-frame protocol mode; i.e. the response to the request is sent with the next request frame

// Return Status (RS): [25:24] of each SPI Frame
#define SCL3300_RS_STARTUP  0b00
#define SCL3300_RS_NORMAL   0b01
#define SCL3300_RS_RESERVED 0b10
#define SCL3300_RS_ERROR    0b11

// STATUS
#define SCL3300_STATUS_DIG1  0x0200
#define SCL3300_STATUS_DIG2  0x0100
#define SCL3300_STATUS_CLK  0x0080
#define SCL3300_STATUS_SAT  0x0040
#define SCL3300_STATUS_TEMP  0x0020
#define SCL3300_STATUS_PWR  0x0010
#define SCL3300_STATUS_MEM  0x0008
#define SCL3300_STATUS_PD  0x0004
#define SCL3300_STATUS_MODE  0x0002
#define SCL3300_STATUS_PIN  0x0001

// ERR_FLAG1
#define SCL3300_ERR1_ADC  0x0800
#define SCL3300_ERR1_AFE  0x07FE
#define SCL3300_ERR1_MEM  0x0001

// ERR_FLAG2
#define SCL3300_ERR2_EXT_D  0x4000
#define SCL3300_ERR2_EXT_A  0x2000
#define SCL3300_ERR2_AGND  0x1000
#define SCL3300_ERR2_VDD  0x0800
#define SCL3300_ERR2_MODE  0x0200
#define SCL3300_ERR2_PD    0x0100
#define SCL3300_ERR2_CRC  0x0080
#define SCL3300_ERR2_APWR_1  0x0020
#define SCL3300_ERR2_DPWR  0x0010
#define SCL3300_ERR2_VREF  0x0008
#define SCL3300_ERR2_APWR_2  0x0004
#define SCL3300_ERR2_TEMP  0x0002
#define SCL3300_ERR2_CLK  0x0001

// CMD
#define SCL3300_CMD_SW_RST  0x0020
#define SCL3300_CMD_PD    0x0004
#define SCL3300_CMD_MODE  0x0003
#define SCL3300_CMD_CHANGE  0x0002
#define SCL3300_CMD_WK_UP  0x0001

// WHOAMI
#define SCL3300_WHOAMI    0x00C1


/* Constants -----------------------------------------------------------------*/

// Operation modes
#define SCL3300_ACC_SENSITIVITY_MODE1   6000
#define SCL3300_ACC_SENSITIVITY_MODE2   3000
#define SCL3300_ACC_SENSITIVITY_MODE3   12000
#define SCL3300_ACC_SENSITIVITY_MODE4   12000
#define SCL3300_ACC_SCALE               1000
#define SCL3300_ANG_OFFSET              90
#define SCL3300_ANG_SENSITIVITY         182
#define SCL3300_ANG_SENSITIVITY_NOM     90
#define SCL3300_ANG_SENSITIVITY_DENOM   (1 << 14)
#define SCL3300_ANG_SCALE               100
#define SCL3300_TEMP_OFFSET             0
#define SCL3300_TEMP_SENSITIVITY        189
#define SCL3300_TEMP_SCALE              10


/* Functions -----------------------------------------------------------------*/

// Start up Sequence for the SCL3300
void SCL_init(void);

// Inclino wake up
void SCL_up(void);

// Inclino go to power down mode
/* Important note: Never put the inclinometer into down mode twice in a row, breaks some functionality */
void SCL_down(void);

// Send a command and populate a given buffer with the response
void SCL_sendCommandToBuf(uint8_t* buffer, uint32_t cmd);

// Send a command and populate an internal buffer with the response
void SCL_sendCommand(uint32_t cmd);

// Assembles the response from a given buffer
uint16_t SCL_getResponseFromBuf(uint8_t* buffer);

// Assembles the response from an internal buffer
uint16_t SCL_getResponse();

// Verifies that the RS is correct in a given buffer
uint32_t SCL_verifyResponseFromBuf(uint8_t* rx_buf);

// Verifies that the RS is correct in an internal buffer
uint32_t SCL_verifyResponse();

// Verifies that the Status is correct
uint32_t SCL_verifyStatus(uint8_t startup_check);

// Calculate CRC for 24 MSB's of the 32 bit dword
uint8_t SCL_calculateCRC(uint32_t Data);

// Conversion functions
uint16_t SCL_convertAcceleration(uint8_t* rx_buf, uint8_t mode);
uint16_t SCL_convertAngle(uint8_t* rx_buf);
uint16_t SCL_convertTemperature(uint8_t* rx_buf);

#endif /* __SCL3300_H_ */
