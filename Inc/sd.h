/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __SD_H
#define __SD_H

/* Defines -------------------------------------------------------------------*/
#define SD_POWER_ON_DELAY_MS    1
#define SD_DIR_MAX_LEN          16                /* Max. length incl. terminating zero character */
#define SD_USE_FILESYSTEM_EXFAT FF_FS_EXFAT       /* Whether to use exFAT file system instead of FAT32 */
#define SD_USE_LONG_FILENAMES   FF_USE_LFN        /* If disabled (0), all filenames must be shorter than or equal to 8 characters (plus up to 3 chars for extension) */
#if SD_USE_LONG_FILENAMES
  #define SD_DIR_FORMAT         "%04u-%02u-%02u"  /* YYYY-MM-DD (length must be < SD_DIR_MAX_LEN) */
#else
  #define SD_DIR_FORMAT         "%04u%02u%02u"    /* YYYYMMDD (length must be shorter or equal to 8 chars) */
#endif
#define SD_MAX_ACCESS_TRIES     5
#define SD_FILL_LEVEL_WARN_TH   99                /* Max. SD fill level (in percent) before a warning (error in selftest) is issued */
#define SD_RETRIES_DELAY_MS     10
#define SD_REINIT_DELAY_MS      250
#define SD_FILE_MAX_PATH_LEN    (SD_DIR_MAX_LEN + FF_MAX_LFN + 1)
#define SD_FILE_MAX_LINE_LEN    1024              /* Max. length of one line in a text file */
/* Note: if SD_USE_LONG_FILENAMES is disabled, all filenames must be shorter or equal to 8 characters (plus up to 3 chars extension) */
#define SD_FILENAME_CONFIG      "CONFIG.TXT"
#define SD_FILENAME_INFO        "INFO.TXT"
#define SD_FILENAME_SCHEDULE    "SCHEDULE.TXT"
#define SD_FILENAME_RESETS      "_RESETS.TXT"
#define SD_FILENAME_ACQID       "_ACQID.TXT"
#define SD_FILENAME_ACQ         "_ACQ.CSV"
#define SD_FILENAME_EVENT       "_EVENTS.CSV"
#define SD_FILENAME_HEALTH      "_HEALTH.CSV"
#define SD_FILENAME_IMUSTAT     "_IMU.CSV"
#define SD_FILENAME_INCLINO     "_INCLINO.CSV"
#if LOG_GNSSDATA_BIN
  #define SD_FILENAME_GNSSDATA  "GNSS.UBX"
#else
  #define SD_FILENAME_GNSSDATA  "GNSS.CSV"
#endif
#define SD_FILENAME_COMDATA     "_COMDATA.CSV"
#define SD_FILENAME_TSYNC       "_TSYNC.CSV"
#define SD_FILENAME_PSD         "_PSD.CSV"
#if SD_USE_LONG_FILENAMES
 #if LOG_ADCDATA_MINISEED
  #define SD_FILENAME_ADCDATA   "%07lu.%s.MSEED"
 #else
  #define SD_FILENAME_ADCDATA   "%07lu.%s.DAT"
 #endif
#else
  #define SD_FILENAME_ADCDATA   "%07lu%s.DAT"
#endif
#define SD_FILENAME_IMUDATA     "%07luC.DAT"
#define SD_DEFAULT_DATE         "2000-01-01"      /* default init date when no time sync is available */
#define SD_ARCHIVE_DIR          "ARCHIVE"         /* archive directory for files in the 'default date' directory, undefine to disable archiving */
#if LOG_ADCDATA_MINISEED
  #define SD_ARCHIVE_FILETYPE   "MSEED"           /* which file types to archive */
#else
  #define SD_ARCHIVE_FILETYPE   "DAT"
#endif

/* SD-usage Functions --------------------------------------------------------*/
void     SD_init(void);                     /* Initialize SD card, load config/parameters and update counters */
bool     SD_mount(void);                    /* Mount SD card (and initialize if needed). Returns true when OK. */
void     SD_unmount(void);                  /* Unmount SD card */
bool     SD_isMounted(void);                /* Returns true if the SD card is mounted */
void     SD_eject(void);                    /* Eject SD card */
bool     SD_format(void);                   /* Format the SD card */
bool     SD_checkCWD(void);                 /* Make sure the working directory exists, update the path if necessary */
const char* SD_getCWD(void);                /* Get the current working directory */
bool     SD_createDirRecursive(const char* root, char* next_dir);
void     SD_updateSize(void);               /* Update free space & total size of SD card */
uint8_t  SD_getFillLevel(void);             /* Percentage of used space */
uint32_t SD_getSizeFree(void);              /* Amount of free disk space, in kB */
uint32_t SD_getSizeTotal(void);             /* Total amount of disk space, in kB */

bool     SD_readConfig(void);
bool     SD_writeConfig(void);
bool     SD_readSchedule(void);
bool     SD_addToSchedule(const ScheduleEntry_t* schedEntry);
bool     SD_clearSchedule(void);
uint32_t SD_readResetCounter(void);
bool     SD_writeResetCounter(uint32_t cnt);
uint32_t SD_readAcqID(void);
bool     SD_writeAcqID(uint32_t id);
void     SD_writeInfo(void);

bool     SD_fileExists(const char* filename);
bool     SD_findFile(const char* filename, char* out_path);
bool     SD_initFile(const char* filename, const char* header);      /* If file does not exist, it will be created and initialized with the given header (string) */
uint32_t SD_readFile(const char* filename, uint32_t offset, uint16_t num_bytes, uint8_t* out_data);
bool     SD_writeFile(const char* filename, const void* data, uint32_t len);
bool     SD_writeAtOffset(const char* filename, int32_t ofs, const uint8_t* data, uint16_t len);
bool     SD_appendPadding(const char* filename, uint8_t val, uint16_t num_bytes);
bool     SD_archiveFiles(const char* dir, const char* file_ext);

void     SD_benchmark(uint32_t num_files, uint32_t chunks_per_file, uint32_t chunk_size, uint32_t delay_ms, uint32_t delay_mod);

#endif /* __SD_H */
