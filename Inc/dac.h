/*
 * Copyright (c) 2020 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __DAC_H_
#define __DAC_H_

/*
 * Driver for the MAX5532/MAX5533 12-bit DAC
 *
 * Name | CONTROL | DATA     |
 * Bit  | C3 - C0 | D11 - D0 |
 */

/* Commands ------------------------------------------------------------------*/
#define DAC_CMD_LOAD_INPUT_A            0x1
#define DAC_CMD_LOAD_INPUT_B            0x2
#define DAC_CMD_LOAD_DAC_A_B            0x8
#define DAC_CMD_LOAD_INPUT_DAC_A_DAC_B  0x9
#define DAC_CMD_LOAD_INPUT_DAC_B_DAC_A  0xA
#define DAC_CMD_ENTER_STANDBY           0xC
#define DAC_CMD_ENTER_NORMAL            0xD
#define DAC_CMD_ENTER_SHUTDOWN          0xE
#define DAC_CMD_LOAD_INPUT_DAC_A_B      0xF

/* Internal reference voltage (MAX5533 only) ---------------------------------*/
#define DAC_VREF_1214_MV                0x0
#define DAC_VREF_1940_MV                0x1
#define DAC_VREF_2425_MV                0x2
#define DAC_VREF_3885_MV                0x3
#define DAC_VREF_STARTUP_DELAY_US       5000

/* Macros --------------------------------------------------------------------*/
#define DAC_CMD_REG(control, data)      ( ((control) << 12) | (data & 0x0fff) )       /* 4-bit command and up to 12 data bits */
#define DAC_CMD_STATE(control, vref)    ( ((control) << 12) | ((vref & 0x3) << 10) )  /* 4-bit command and 2 data bits */
#define DAC_CONV_VREF(vref)             ( ((vref == DAC_VREF_1214_MV) * 1214) + ((vref == DAC_VREF_1940_MV) * 1940) + ((vref == DAC_VREF_2425_MV) * 2425) + ((vref == DAC_VREF_3885_MV) * 3885))

/* General -------------------------------------------------------------------*/
#define DAC_MAX_VALUE                   4095            /* 12-bit DAC */
#define DAC_SPI_TIMEOUT_MS              10              /* timeout for SPI transactions and semaphore acquisition */
#define DAC_SETTLE_TIME_MS              10

/* Error Checks --------------------------------------------------------------*/
#if defined(DAC_INT_VREF) && ((DAC_INT_VREF < DAC_VREF_1214_MV) || (DAC_INT_VREF > DAC_VREF_3885_MV))
#error "Invalid setting for DAC_INT_VREF"
#endif

/* Functions -----------------------------------------------------------------*/
void DAC_init(void);
bool DAC_sendCmd(uint8_t cmd, uint16_t val);

#endif /* __DAC_H_ */
