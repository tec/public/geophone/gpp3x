/*
 * Copyright (c) 2018 - 2023, ETH Zurich, Computer Engineering Group (TEC)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __RTOS_H
#define __RTOS_H

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
#include "queue.h"
#include "semphr.h"

/* Definitions --------------------------------------------------------------*/
#define BTN_PRESSED_MAX_DURATION_S  5                       /* Button press timeout [s] */
#define BTN_USBMOUNT_DURATION_S     1                       /* How long the button needs to be pressed until USB MSC is engaged [s] */
#define IDLE_TIME_BEFORE_SLEEP      2000U                   /* ms */
#define LOG_TASK_BUFFERED_MSEED     1                       /* If set to 1 and miniseed is enabled, the data will be buffered and only written to the file once a full miniseed block is complete (requires more memory, but reduces the number of file accesses) */
#define LOG_TASK_SEM_WAIT_MS        1000                    /* How long the task should wait for the required semaphore */
#define LOG_TASK_ERR_MAX_TRIES      1                       /* How many times the log task will attempt to write the same data to the SD card in case writing fails */
#define LOG_TASK_ERR_RETRY_DELAY_MS 10                      /* Delay between two write attempts */
#define LOG_TASK_RUNTIME_WARN_TH_MS 300                     /* Print a warning and send event to Bolt if the log task runtime exceeds this threshold */
#define LOG_TASK_RUNTIME_ERR_TH_MS  700                     /* Same as LOG_TASK_RUNTIME_WARN_TH_MS, but additionally log the event to the SD card */
#define LOG_TASK_SD_FULL_WARN_TH    90                      /* Percentage at which the SD full event should be triggered */
#define STREAM_TASK_PERIOD_MIN_MS   500                     /* Min. period with which the streaming task will run to check for new data (in backlog mode) */
#define STREAM_TASK_PERIOD_MAX_MS   15000                   /* Max. period with which the streaming task will run to check for new data (in backlog mode) */
#define STREAM_TASK_RUNTIME_TH_MS   300                     /* Print a warning and send event to Bolt if the log task runtime exceeds this threshold */
#define STREAM_TASK_SEM_WAIT_MS     5000                    /* How long the task should wait for the required semaphore */
#define BOLT_TASK_SPI_TIMEOUT_MS    100                     /* Timeout for SPI transactions to Bolt */
#define BOLT_TASK_SEM_WAIT_MS       10                      /* How long the task should wait for the required semaphore */
#define GEO_TASK_SEM_WAIT_MS        10                      /* How long the task should wait for the required semaphore */

#define RTOS_TIMER_PERIOD_MS        200                     /* Must be <= 1000ms */
#define RTOS_TIMER_MAXWAIT_MS       10                      /* Max. wait time in case the timer cannot be notified immediately */
#define RTOS_TASK_RUNTIME_TH_MS     200                     /* Warn if a task runs longer than this */
#define RTOS_MEMUSAGE_WARN_TH       90                      /* If the heap/stack usage is higher than this threshold (in percentage), a warning will be issued */

/* Task IDs */
#define RTOS_TASK_ID_ERROR          0
#define RTOS_TASK_ID_GEO            1
#define RTOS_TASK_ID_BOLT           2
#define RTOS_TASK_ID_HEALTH         3
#define RTOS_TASK_ID_IMU            4
#define RTOS_TASK_ID_INCLINO        5
#define RTOS_TASK_ID_GNSS           6
#define RTOS_TASK_ID_LOG            7
#define RTOS_TASK_ID_TSYNC          8
#define RTOS_TASK_ID_STREAM         9
#define RTOS_TASK_ID_IDLE          10
#define RTOS_TASK_ID_SCHEDULE      11

/* Task priorities
 * Note: These are RTOS priorities, higher value means higher priority!
 *       The idle task has priority 0.
 */
#define RTOS_TASK_PRIO_GEO          6       /* Highest priority task */
#define RTOS_TASK_PRIO_IMU          5
#define RTOS_TASK_PRIO_INCLINO      5
#define RTOS_TASK_PRIO_GNSS         5
#define RTOS_TASK_PRIO_TSYNC        4
#define RTOS_TASK_PRIO_HEALTH       3
#define RTOS_TASK_PRIO_BOLT         3
#define RTOS_TASK_PRIO_LOG          2
#define RTOS_TASK_PRIO_STREAM       1       /* Lowest priority task, should only run if all other tasks finished */
#define RTOS_TASK_PRIO_SCHEDULE     6

/* Task stack size */
#define RTOS_TASK_STACK_GEO         configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_BOLT        configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_HEALTH      configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_IMU         configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_INCLINO     configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_GNSS        configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_TSYNC       configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_LOG         (configMINIMAL_STACK_SIZE + 128)
#define RTOS_TASK_STACK_STREAM      configMINIMAL_STACK_SIZE
#define RTOS_TASK_STACK_SCHEDULE    configMINIMAL_STACK_SIZE

/* Queue size */
#define RTOS_QUEUE_LOG_SIZE         (RB_GEO_MAX_ITEMS + \
                                     RB_EVENT_MAX_ITEMS + \
                                     RB_HEALTH_MAX_ITEMS + \
                                     RB_IMUSTAT_MAX_ITEMS + \
                                     RB_IMUDATA_MAX_ITEMS + \
                                     RB_SCHEDULE_MAX_ITEMS + \
                                     RB_INCLINODATA_MAX_ITEMS + \
                                     RB_GNSSDATA_MAX_ITEMS + \
                                     RB_COMDATA_MAX_ITEMS + \
                                     RB_TSYNC_MAX_ITEMS + \
                                     10)                    /* Margin for other notifications (e.g. prevent/allow change dir, get SD size or write config) */

/* Interrupt priorities
 * Note: These are NVIC priority levels, a lower value means higher priority.
 *       Levels 0 - 15 are available, although configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY+ is required to allow RTOS system calls.
 */
#define RTOS_ISR_PRIO_BOLT_SPI_DMA  (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 3)    /* Bolt read/write (only used if SPI2_USE_DMA is set) */
#define RTOS_ISR_PRIO_BOLT_IMU_EXTI (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 3)    /* Bolt IND/ACK lines and IMU pins */
#define RTOS_ISR_PRIO_BTN_PPS_EXTI  (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 2)    /* User button and GNSS PPS pin */
#define RTOS_ISR_PRIO_ADC_SPI_DMA   (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 1)    /* ADC sample transfer (must have the same priority as RDY_EXTI) */
#define RTOS_ISR_PRIO_ADC_RDY_EXTI  (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 1)    /* ADC ready pin (must have the same priority as SPI_DMA) */
#define RTOS_ISR_PRIO_GNSS_UART_DMA (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 3)    /* GNSS UART RX */
#define RTOS_ISR_PRIO_LOG_SDMMC_DMA (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 3)    /* Data logging to SD card */
#define RTOS_ISR_PRIO_LOG_UART_DMA  (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 3)    /* Data logging (streaming) to LPUART */
#define RTOS_ISR_PRIO_SYSTICK       (configKERNEL_INTERRUPT_PRIORITY)                     /* SysTick for RTOS scheduler (should have the lowest available priority) */
#define RTOS_ISR_PRIO_TSYNC_TIM     (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 2)    /* Tsync timeout timer */
#define RTOS_ISR_PRIO_TRG_EXTI      (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 2)    /* Trigger pin */
#define RTOS_ISR_PRIO_TRG_TIM       (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 2)    /* Debounce timers for trigger */
#define RTOS_ISR_PRIO_WAKEUP_RTC    (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 4)    /* Periodic system wakeup handled by the RTC */
#define RTOS_ISR_PRIO_ALARM_RTC     (configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY + 4)    /* RTC alarm functionality */


/* Macros --------------------------------------------------------------------*/
#define IS_RTOS_STARTED()           (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED)
#define IS_RTOS_RUNNING()           (xTaskGetSchedulerState() == taskSCHEDULER_RUNNING)
#define RTOS_SUSPEND()              vTaskSuspendAll()
#define RTOS_RESUME()               xTaskResumeAll()
#define RTOS_ENTER_CRITICAL()       UBaseType_t uxSavedInterruptStatus = 0; if (IS_ISR()) { uxSavedInterruptStatus = taskENTER_CRITICAL_FROM_ISR(); } else { taskENTER_CRITICAL(); }
#define RTOS_LEAVE_CRITICAL()       if (IS_ISR()) { taskEXIT_CRITICAL_FROM_ISR(uxSavedInterruptStatus); } else { taskEXIT_CRITICAL(); }
#define RTOS_TASK_DELAY(ms)         if (IS_RTOS_RUNNING()) { vTaskDelay(pdMS_TO_TICKS(ms)); } else { HAL_Delay(ms); }
#define IS_ISR()                    ((SCB->ICSR & SCB_ICSR_VECTACTIVE_Msk) != 0)          /* Note: does the same as xPortIsInsideInterrupt() since ICSR[0:8] and IPSR contain the same info */

#if BOLT_ENABLE
#define NOTIFY_BOLT_TASK()            xTaskNotifyGive(xTaskHandle_Bolt)
#define NOTIFY_BOLT_TASK_FROM_ISR(x)  vTaskNotifyGiveFromISR(xTaskHandle_Bolt, x)
#else
#define NOTIFY_BOLT_TASK()
#define NOTIFY_BOLT_TASK_FROM_ISR(x)
#endif

#if !TSYNC_METHOD_NONE || TSYNC_MASTER
#define NOTIFY_TSYNC_TASK()           xTaskNotifyGive(xTaskHandle_TSync)
#define NOTIFY_TSYNC_TASK_FROM_ISR(x) vTaskNotifyGiveFromISR(xTaskHandle_TSync, x)
#else
#define NOTIFY_TSYNC_TASK()
#define NOTIFY_TSYNC_TASK_FROM_ISR(x)
#endif

#if !STREAMING_TARGET_NONE
#define NOTIFY_STREAM_TASK()            xTaskNotifyGive(xTaskHandle_Stream)
#define NOTIFY_STREAM_TASK_FROM_ISR(x)  vTaskNotifyGiveFromISR(xTaskHandle_Stream, x)
#else
#define NOTIFY_STREAM_TASK()
#define NOTIFY_STREAM_TASK_FROM_ISR(x)
#endif

#if USE_GNSS
#define NOTIFY_GNSS_TASK(n)             xTaskNotify(xTaskHandle_GNSS, n, eSetValueWithoutOverwrite)
#define NOTIFY_GNSS_TASK_FROM_ISR(n, x) xTaskNotifyFromISR(xTaskHandle_GNSS, n, eSetValueWithoutOverwrite, x)
#else
#define NOTIFY_GNSS_TASK()
#define NOTIFY_GNSS_TASK_FROM_ISR(x)
#endif

#define RTOS_TIMER_S_TO_TICKS(s)    ((s) * 1000 / RTOS_TIMER_PERIOD_MS)

/* Enumerations --------------------------------------------------------------*/
typedef enum
{
    DC_CNTR_START,
    DC_CNTR_RESUME,
    DC_CNTR_STOP,
    DC_CNTR_PAUSE,
    DC_CNTR_RESET,
    DC_CNTR_GET,
} DCCounterAction_t;

typedef enum
{
    BOLT_MSG_READ = 0,
    BOLT_MSG_WRITE,
    NUM_OF_BOLT_MSG_TYPES
} BoltNotificationType_t;

typedef enum
{
    GNSS_NOTIFY_INV = 0,                      /* Invalid notification */
    GNSS_NOTIFY_SAMPLE = 1,                   /* Collect GNSS samples */
    GNSS_NOTIFY_TSYNC_INIT = 2,               /* Initiate time synchronization */
    GNSS_NOTIFY_RX = 3,                       /* Message received */
    GNSS_NOTIFY_UARTERR = 4,                  /* UART error */
    GNSS_NOTIFY_TSYNC_ABORT = 5,              /* Abort time synchronization */
    GNSS_NOTIFY_STOP = 6,                     /* Stop tsync and raw data collection */
    NUM_OF_GNSS_NOTIFY_TYPES,
} GNSSNotificationType_t;

/* Global variables / handles ------------------------------------------------*/
extern TaskHandle_t       xTaskHandle_Bolt;
extern TaskHandle_t       xTaskHandle_Geo;
extern TaskHandle_t       xTaskHandle_GNSS;
extern TaskHandle_t       xTaskHandle_Health;
extern TaskHandle_t       xTaskHandle_Imu;
extern TaskHandle_t       xTaskHandle_Inclino;
extern TaskHandle_t       xTaskHandle_Schedule;
extern TaskHandle_t       xTaskHandle_Stream;
extern TaskHandle_t       xTaskHandle_TSync;
extern SemaphoreHandle_t  xSemaphore_GeoStop;
extern SemaphoreHandle_t  xSemaphore_ImuStop;
extern SemaphoreHandle_t  xSemaphore_Schedule;
extern SemaphoreHandle_t  xSemaphore_SD;
extern SemaphoreHandle_t  xSemaphore_SPI1;
extern SemaphoreHandle_t  xSemaphore_SPI2;
extern QueueHandle_t      xQueue_Log;
extern QueueHandle_t      xQueue_Stream;
extern TimerHandle_t      xTimer_Button;

/* Functions -----------------------------------------------------------------*/
void     RTOS_init(void);
void     RTOS_buzzer(uint16_t period_ms, uint16_t cnt);
void     RTOS_blink(led_t led, uint16_t period_ms, uint16_t cnt);
void     RTOS_event(dpp_event_type_t type, uint32_t value, uint8_t flag);
void     RTOS_queueSendLog(LogType_t type);
void     RTOS_queueSendLogFromISR(LogType_t type, BaseType_t* higherPriorityTaskWoken);
uint8_t  RTOS_getHighestStackUsage(uint32_t* taskID);        /* Get highest stack usage, in percent */
uint8_t  RTOS_getHeapUsage(void);                            /* Get percentage of RTOS heap  */
uint16_t RTOS_dutyCycle(DCCounterAction_t action);           /* CPU duty cycle counter */
void     RTOS_errorHandlerInIdle(const char* file, int line);
#if USE_STOP2
void     RTOS_resumeFromStopMode(void);
#else
#define  RTOS_resumeFromStopMode()
#endif


#endif /* __RTOS_H */
